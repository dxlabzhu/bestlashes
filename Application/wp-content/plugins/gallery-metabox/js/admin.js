jQuery(function($) {

	function postok_lekerese_tipus_szerint() {
		$('.form_content.alap_beallitasok div.letezo_post_url_kivalasztasa').on( 'change', 'select.select_post_type', function(){
			var $this = $(this);
			
			if ( $this.val() != "" ) {

					jQuery.post(
						ajaxurl,
						{
							post_type : $this.val(),									
							action : 'postok_lekerese_tipus_szerint'
						},
						function(data) {
							if (data != 0) {
								$('div.fancybox-wrap.metabox_ajax_content .right_side .form_content.alap_beallitasok div.letezo_post_url_kivalasztasa select.select_post').html( data );
							}
						}
					);
			} else {
				$('div.fancybox-wrap.metabox_ajax_content .right_side .form_content.alap_beallitasok div.letezo_post_url_kivalasztasa select.select_post').html( '<option value="">-</option>' );
			}
		});
		
		$('div.fancybox-wrap.metabox_ajax_content .right_side .form_content.alap_beallitasok div.letezo_post_url_kivalasztasa').on('click', 'div.hozzaadas_btn', function(){
			var $this = $(this);				
			var post_url = $this.closest('div.letezo_post_url_kivalasztasa').find('select.select_post option:selected').val();
		
			if ( post_url != "" ) {
				var post_url = post_url.replace( window.location.origin, "");				
				$this.closest( 'div.form_content.alap_beallitasok' ).find('input[name="added_gallery_img_url"]').val( post_url );						
			}		
		});
		
	}

	function megjelenitesi_beallitasok() {
		$("#metabox_ajax_content div.form_content.megjelenitesi_beallitasok .beallitas.kep_szeleinek_vagasa input, #metabox_ajax_content div.form_content.megjelenitesi_beallitasok .beallitas.kep_szeleinek_vagasa label").on( 'click', function(){
			var $this = $(this);
			
			if ( $this.closest("tr").hasClass("full_size") ) { 
				$("#metabox_ajax_content div.form_content.megjelenitesi_beallitasok .beallitas.hatterszin").slideDown();
				$("#metabox_ajax_content div.form_content.megjelenitesi_beallitasok .beallitas.vagas_elhelyezese").slideUp();
			} else {
				$("#metabox_ajax_content div.form_content.megjelenitesi_beallitasok .beallitas.hatterszin").slideUp();
				$("#metabox_ajax_content div.form_content.megjelenitesi_beallitasok .beallitas.vagas_elhelyezese").slideDown();
			}		
					
		});
	}

	// kep_szerkesztesi_menu aktiválás kezelés és tartalom váltása (popup)
	function kep_szerkesztesi_menu_kezelese() {
		
		$("div.fancybox-wrap ul#kep_szerkesztesi_menu li").on('click', function(){
			var $this = $(this);
			
			$this.closest("ul").find("li.active").removeClass("active");
			$this.addClass("active");
			
			if ( $this.index() == 0 ) {
				
				$this.closest('div.right_side').find('div.form_content.megjelenitesi_beallitasok').hide();				
				$this.closest('div.right_side').find('div.form_content.alap_beallitasok').fadeIn(200);
				
			} else {
				
				$this.closest('div.right_side').find('div.form_content.alap_beallitasok').hide();
				$this.closest('div.right_side').find('div.form_content.megjelenitesi_beallitasok').fadeIn(200);
				
			}
			
			 
		});
	}


	/* feltöltött képek és dokumentumok számának kiírása a menübe */
	function csatolt_mediafajlok_darabszama() {
		
		if ( $("#galeria_metabox_wrapper div.metabox_content ul.attachments li").length ) {
			var kepek = $("#galeria_metabox_wrapper div.metabox_content ul.attachments li.kep").length;
			var doksik = $("#galeria_metabox_wrapper div.metabox_content ul.attachments li.dokumentum").length;
			
			if ( kepek > 0 ) {
				$("div.menu_ul_wrapper ul li[wrapper-class='galeria_content'] span.quantity").html( kepek ).show();
			}else {
				$("div.menu_ul_wrapper ul li[wrapper-class='galeria_content'] span.quantity").html('').hide();				
			}
			
			if ( doksik > 0 ) {
				$("div.menu_ul_wrapper ul li[wrapper-class='dokumentumok_content'] span.quantity").html( doksik ).show();
			} else {
				$("div.menu_ul_wrapper ul li[wrapper-class='dokumentumok_content'] span.quantity").html('').hide();				
			}
					
		}		
	}
	csatolt_mediafajlok_darabszama();
	

	// aktuális aktív menü bejegyzése
	function aktiv_menu_bejegyzese() {
    	if ( $("#galeria_metabox_wrapper .menu_ul_wrapper ul li.active").length ) {
    		$("#galeria_metabox_wrapper").attr("active-menu", $("#galeria_metabox_wrapper .menu_ul_wrapper ul li.active").attr("wrapper-class") );
    	}
	}

	// értesítés az ajax mentésről
	function mentes_uzenet_megjelenitese() {
		if ( $("#galeria_metabox h3 .metabox_mentes_uzenet").length ) { }
		else {
			csatolt_mediafajlok_darabszama();
			$("#galeria_metabox_wrapper .metabox_mentes_uzenet").clone().insertAfter('#galeria_metabox h3 span').hide().fadeIn().delay( 3000 ).fadeOut(function(){
				$(this).remove();
			});
		}
	}


	// Azért kell törölni a meta inputokat, mert ezek ajaxal frissűlnek és wp frissítés gombja az "előző állapotot" hozza vissza
	function metabox_inputok_torlese() {

		mentes_uzenet_megjelenitese();

		$("#postcustom table input[value='galeria_azonositok']").closest("tr").html("");

    	$("#postcustom table input").each(function(){
    		var $this = $(this);
    		var metabox_key = $this.attr("value");

    		if ( metabox_key.indexOf( "attachment-" ) >= 0 ) { $this.closest("tr").html(""); }
    	});

		$("#postcustom table input[value='galeria_megj_sorrend']").closest("tr").html("");
	}

    function uj_sorrend_mentes() {
 		var $this = $("#galeria_metabox_wrapper .metabox_content ul li");
 		var result = "";
		var post_id = $this.closest("ul").attr("post-id");

	 		// új sorrend összeállítása
	    	$("#galeria_metabox_wrapper .metabox_content ul li").each(function() {
	    		var attachment_id = $(this).attr("attachment-id");
	    		if ( attachment_id > 0 ) { result += "'"+ attachment_id +"',"; }	    		
	    	});

		    	// sorrend mentése
		    	if ( result.length ) {

					jQuery.post(
						ajaxurl,
						{
							post_id : post_id,
							uj_sorrend : result,													
							action : 'uj_sorrend_mentese'
						},
						function(data) {
							if (data != 0) {
								metabox_inputok_torlese();
								//console.log( "Új sorrend mentve." );
							}
						}
					);
		    	}
    }

    $(document).ready(function(){

    	// Fancybox
    	if ( $("#galeria_metabox_wrapper .fancybox.fullscreen_btn").length ) {
    		$("#galeria_metabox_wrapper .fancybox.fullscreen_btn").fancybox();
    	}
    

    	// menü nyitás-összecsukás
    	if ( $("#galeria_metabox_wrapper .menu_content").length ) {

    		$("#galeria_metabox_wrapper .menu_content .menu_btn").click(function() {
    			var $this = $(this);
    			var menu_content = $this.closest(".menu_content");

    			$(window).resize();

    			// menu content
    			if ( menu_content.hasClass("menu_open") ) { 

    				// move
    				menu_content.find(".menu_ul_wrapper").animate({right: '-500px'}, 150, function(){

	    				// show or hide
	    				menu_content.removeClass("menu_open").addClass("menu_close");    					
    				});

    			} else if ( menu_content.hasClass("menu_close") ) { 
    				// show or hide
    				menu_content.removeClass("menu_close").addClass("menu_open"); 

    				// move
    				menu_content.find(".menu_ul_wrapper").animate({right: '0'}, 250);			
    			}

    			// menu btn hide
    			$this.fadeOut();

    			// another menu btn show
    			if ( $this.hasClass("close_menu_btn") ) { menu_content.find(".show_menu_btn").fadeIn(); }
	   			else if ( $this.hasClass("show_menu_btn") ) { menu_content.find(".close_menu_btn").fadeIn(); }
    		});
    	}


    	// menü méretének beállítása: ugyanakkora legyen mint a wrapper
		$(window).bind("load resize", function() {

			if ( $("#galeria_metabox").length ) {
				var galeria_metabox_Height = $("#galeria_metabox").height() - 35;
				//console.log( galeria_metabox_Height );
				$("#galeria_metabox #galeria_metabox_wrapper .menu_content").css("height", galeria_metabox_Height);
			}

		});

		// menü autómatikus bezárása (ha nem bele kattintunk)
		$(document).click(function(event) {
			if ( $("#galeria_metabox_wrapper .menu_content.menu_open").length ) {

			    if (!$(event.target).closest("#galeria_metabox_wrapper .menu_content.menu_open").length) {
			        $("#galeria_metabox_wrapper .menu_content .close_menu_btn").click();
			    }
			}
		});

		// menüelem kiválasztására megjelenik a megfelelő tartalom
		if ( $("#galeria_metabox #galeria_metabox_wrapper .menu_content").length ) {
			aktiv_menu_bejegyzese();

			$("#galeria_metabox_wrapper .menu_content.menu_open li").click(function(){
				$this = $(this);
				var megnyitni = $this.attr("tartalom");

				if ( $this.hasClass("active") ) {  }
				else {

					// aktiv menü megjelölés
					$("#galeria_metabox_wrapper .menu_content.menu_open li").removeClass("active");
					$this.addClass("active");

					$("#galeria_metabox").css("overflow","hidden");

					// ami eddig megjelenítve volt azt elrejteni
					$("#galeria_metabox_wrapper .metabox_content.show").animate({top: '10000'}, 500, function(){
						$("#galeria_metabox_wrapper .metabox_content.show").removeClass("show").hide();
					});

					// új tartalom megjelenítése
					$("#galeria_metabox_wrapper .metabox_content."+megnyitni).css("top", -10000+"px" ).show().animate({top: '0'}, 500, function(){
						$(this).addClass("show");

						$(window).resize();
						$("#galeria_metabox").css("overflow","visible");
					});

	    			aktiv_menu_bejegyzese();
				}
			});
		}


		// több kép ID beillesztése a médiatárból
		if ( $("#galeria_metabox_wrapper .menu_content .mediatar_btn.kepek").length ) {
			$('#galeria_metabox_wrapper .menu_content .mediatar_btn.kepek').click(function() {
			    if (this.window === undefined) {
			        this.window = wp.media({
			                title: 'Insert a media',
			                library: {type: 'image'},
				            multiple: true,
				            button: {text:'Insert'}
				        });

				        var self = this; //needed to retrieve the function below
				        this.window.on('select',function(){
				            var files = self.window.state().get('selection').toArray();
				            var values, attachment_url, attachment_id;
				            
				            var file_azonositok = "[";
				            for (var i = 0; i < files.length; i++) {
				                var file = files[i].toJSON();
				                
			                    var attachment_url = file.url;
			                    var attachment_id = file.id;        
			                
				                if (i!=0) { var sep = ","; } else { var sep = ""; }
								if (attachment_id != "") { file_azonositok += sep+"'"+attachment_id+"'"; }
				            };
				            file_azonositok += ']';
				            
				            // kiválasztott attachment azonosítók mentése

				            var post_id = $("#galeria_metabox_wrapper .galeria_content ul").attr("post-id") // oldal / CPT / bejegyzés id
							jQuery.post(
								ajaxurl,
								{
									post_id : post_id,
									file_azonositok : file_azonositok,
									action : 'galeria_metabox_content'
								},
								function(data) {
									$("#galeria_metabox_wrapper .galeria_content").fadeOut('fast', function(){
										$(this).html(data).fadeIn('fast');
										metabox_inputok_torlese();
										$(window).resize();
									});
								}
							);
				        });
			    }

			    this.window.open();
			});
		}

		if ( $("#galeria_metabox_wrapper .menu_content .mediatar_btn.dokumentumok").length ) {
			$('#galeria_metabox_wrapper .menu_content .mediatar_btn.dokumentumok').click(function() {
			    if (this.window === undefined) {
			        this.window = wp.media({
			                title: 'Insert a media',
			                library: {type: 'application'},
				            multiple: true,
				            button: {text:'Insert'}
				        });

				        var self = this; //needed to retrieve the function below
				        this.window.on('select',function(){
				            var files = self.window.state().get('selection').toArray();
				            var values, attachment_url, attachment_id;
				            
				            var file_azonositok = "[";
				            for (var i = 0; i < files.length; i++) {
				                var file = files[i].toJSON();
				                
			                    var attachment_url = file.url;
			                    var attachment_id = file.id;        
			                
				                if (i!=0) { var sep = ","; } else { var sep = ""; }
								if (attachment_id != "") { file_azonositok += sep+"'"+attachment_id+"'"; }
				            };
				            file_azonositok += ']';
				            
				            // kiválasztott attachment azonosítók mentése

				            var post_id = $("#galeria_metabox_wrapper .galeria_content ul").attr("post-id") // oldal / CPT / bejegyzés id
							jQuery.post(
								ajaxurl,
								{
									post_id : post_id,
									file_azonositok : file_azonositok,
									action : 'galeria_metabox_content'
								},
								function(data) {
									$("#galeria_metabox_wrapper .galeria_content").fadeOut('fast', function(){
										$(this).html(data).fadeIn('fast');
										metabox_inputok_torlese();
										$(window).resize();
									});
								}
							);
				        });
			    }

			    this.window.open();
			});
		}		

		// kiválasztott elem törlése
		if ( $("#galeria_metabox_wrapper .metabox_content ul li .del_btn").length ) {
			$("#galeria_metabox_wrapper .metabox_content ul li .del_btn").click(function(){
				var $this = $(this);
				var attachment_id = $this.closest("li").attr("attachment-id");
	            var post_id = $("#galeria_metabox_wrapper .galeria_content ul").attr("post-id") // oldal / CPT / bejegyzés id

				if ( attachment_id > 0 ) {

					jQuery.post(
						ajaxurl,
						{
							post_id : post_id,
							torlendo_id : attachment_id,
							action : 'galeria_azonosito_torles'
						},
						function(data) {
							if ( data == 1 ) {
								$("#galeria_metabox_wrapper .galeria_content ul li[attachment-id="+attachment_id+"]").fadeOut('slow', function(){
									$(this).remove();
									metabox_inputok_torlese();
									$(window).resize();
								})
							}
						}
					);
				}
			});
		}


		// szerkesztő gombra
		if ( $("#galeria_metabox_wrapper .metabox_content ul li.kep a.fancybox.edit_btn").length ) {

			// Fancybox létrehozás
			$("#galeria_metabox_wrapper .metabox_content ul li.kep a.fancybox.edit_btn").fancybox({			
																			maxWidth	: 800,
																			maxHeight	: 600,
																			fitToView	: false,
																			width		: '90%',
																			height		: '90%',
																			autoSize	: false,
																			closeClick	: false,
																			wrapCSS		: 'metabox_ajax_content',																							
																			
																			// előző vagy következő kép adatainak betöltése
																			afterLoad: function(current, previous) {
																				var prev_or_next = "";
																				var current_attachment_id = $( current.href ).find( 'div.update_btn.button' ).attr( 'attachment-id' );
																				var current_post_id = $( current.href ).find( 'div.update_btn.button' ).attr( 'post-id' );
																				var $attachments_list = $('div#galeria_metabox div.metabox_content.galeria_content ul.attachments');
																				var attachments = [];
																			
																			    if (previous) {
																			    							
																					$attachments_list.find('li.kep').each(function(){
																						var $this = $(this);
																						
																						if ( $this.attr('attachment-id') != "" ) {
																							attachments.push( $this.attr('attachment-id') );					
																						}
																					});			
																					var new_attachments_id = attachments[current.index];
																					
																					// előzmény törlése
																			        $( current.href ).html( '<div class="gallery_metabox_spinner"><i class="fa fa-cog fa-spin"></i></div>' );
																			        
																					// "Új" adatok lekérése
																					jQuery.post(
																						ajaxurl,
																						{
																							post_id : current_post_id,
																							attachment_id : new_attachments_id,
																							action : 'fajl_adatainak_szerkesztese'
																						},
																						function(data) {
																							if ( data != 0 ) {
																						        $( current.href ).html( data );
																								
																								kep_szerkesztesi_menu_kezelese();
																								megjelenitesi_beallitasok();
																								postok_lekerese_tipus_szerint();
																							}
																						}
																					);																			        
																			        
																			    }
																			},
																		});

			// Kép adatainak betöltése ajaxal
			$("#galeria_metabox_wrapper .metabox_content ul li.kep a.fancybox.edit_btn").click(function(){
				var $this = $(this);
				var metabox_ajax_content = $("#galeria_metabox_wrapper #metabox_ajax_content");

				var attachment_id = $this.closest("li").attr("attachment-id");
	            var post_id = $("#galeria_metabox_wrapper .galeria_content ul").attr("post-id") // oldal / CPT / bejegyzés id

				// előzmény törlése
				metabox_ajax_content.html("");

				// "Új" adatok lekérése
				jQuery.post(
					ajaxurl,
					{
						post_id : post_id,
						attachment_id : attachment_id,
						action : 'fajl_adatainak_szerkesztese'
					},
					function(data) {
						if ( data != 0 ) {
							metabox_ajax_content.html( data );
							
							kep_szerkesztesi_menu_kezelese();
							megjelenitesi_beallitasok();
							postok_lekerese_tipus_szerint();
						}
					}
				);

			});

		}
		
		
		// Megjelenítési beállítások előnézete (betölti a vágott képet az aktuális beállítások alapján)
		$('body').on('click', '#metabox_ajax_content div.form_content.megjelenitesi_beallitasok div.elonezet_btn', function() {
			var $this = $(this);
			var post_id = $this.attr('post-id');
			var $megjelenitesi_beallitasok = $this.closest( '#metabox_ajax_content' ).find( '.megjelenitesi_beallitasok' );
			var attachment_id = $megjelenitesi_beallitasok.find(".update_btn").attr("attachment-id");

			var kep_szeleinek_vagasa = $megjelenitesi_beallitasok.find( ".kep_szeleinek_vagasa input[type='radio']:checked" ).val();			
			var vagas_elhelyezese = $megjelenitesi_beallitasok.find( ".vagas_elhelyezese .kep_vagasi_pont input[type='radio']:checked" ).val();
			var hatterszin = $megjelenitesi_beallitasok.find( "div.beallitas.hatterszin input[type='radio']:checked" ).val();
			var egyedi_hatterszin = $megjelenitesi_beallitasok.find( "div.beallitas.hatterszin input[name='custom_bg_color']" ).val();

			jQuery.post(
				ajaxurl,
				{
					attachment_id : attachment_id,
					post_id : post_id,
					
					kep_szeleinek_vagasa : kep_szeleinek_vagasa,
					vagas_elhelyezese : vagas_elhelyezese,
					hatterszin : hatterszin,
					egyedi_hatterszin : egyedi_hatterszin,
							
					action : 'kep_elonezet_elkeszitese'
				},
				function(data) {
					if (data != 0) {
						$("#metabox_ajax_content .left_side img").fadeOut(200, function(){
							$(this).remove();
							$( data ).insertBefore( 'div#metabox_ajax_content div.left_side div.bottom_shadow' ).hide().fadeIn(200);
						});
					}
				}
			);
		});
				
		
		// szerkesztő ablak mentés gombjára kattintva (alap adatok + megjelenítési beállítások mentése)
		$('body').on('click', 'div.fancybox-wrap.metabox_ajax_content div.form_content .update_btn', function() {
			var $this = $(this);
			var fancybox_wrap = $this.closest("div.fancybox-wrap.metabox_ajax_content");

			var post_id = $this.attr("post-id");
			var attachment_id = $this.attr("attachment-id");

			// alap adatok
			var basic_media_title = fancybox_wrap.find("input[name=basic_media_title]").val();
			var basic_media_desc = fancybox_wrap.find("input[name=basic_media_desc]").val();					
			var added_gallery_img_title = fancybox_wrap.find("input[name=added_gallery_img_title]").val();
			var added_gallery_img_desc = fancybox_wrap.find("textarea[name=added_gallery_img_desc]").val();
			var added_gallery_img_url = fancybox_wrap.find("input[name=added_gallery_img_url]").val();

			// megjelenítési beállítások
			var $megjelenitesi_beallitasok = $this.closest( '#metabox_ajax_content' ).find( '.megjelenitesi_beallitasok' );
			var kep_szeleinek_vagasa = $megjelenitesi_beallitasok.find( ".kep_szeleinek_vagasa input[type='radio']:checked" ).val();			
			var vagas_elhelyezese = $megjelenitesi_beallitasok.find( ".vagas_elhelyezese .kep_vagasi_pont input[type='radio']:checked" ).val();
			var hatterszin = $megjelenitesi_beallitasok.find( "div.beallitas.hatterszin input[type='radio']:checked" ).val();
			var egyedi_hatterszin = $megjelenitesi_beallitasok.find( "div.beallitas.hatterszin input[name='custom_bg_color']" ).val();
		
				
			// ajax töltés jelzése
			fancybox_wrap.find(".form_content .spinner").css('visibility', 'visible');
			
			jQuery.post(
				ajaxurl,
				{
					post_id : post_id,
					attachment_id : attachment_id,
					
					basic_media_title : basic_media_title,
					basic_media_desc : basic_media_desc,
					added_gallery_img_title : added_gallery_img_title,
					added_gallery_img_desc : added_gallery_img_desc,
					added_gallery_img_url : added_gallery_img_url,
					
					kep_szeleinek_vagasa : kep_szeleinek_vagasa,
					vagas_elhelyezese : vagas_elhelyezese,
					hatterszin : hatterszin,
					egyedi_hatterszin : egyedi_hatterszin,
							
					action : 'fajl_adatainak_mentese'
				},
				function(data) {
					if (data != 0) {
						fancybox_wrap.find(".form_content .spinner").css('visibility', 'hidden');
						metabox_inputok_torlese();
						$("div.fancybox-wrap.metabox_ajax_content .right_side .metabox_mentes_uzenet").hide().removeClass("hidden").fadeIn().delay( 3000 ).fadeOut(function(){
																																						$(this).addClass("hidden");
																																					});
					}
				}
			);
		});

		if ( $("#galeria_metabox_wrapper .metabox_content ul").length ) {
		    $('#galeria_metabox_wrapper .metabox_content ul').sortable({ 
		    																items: "li",
		    																placeholder: "sortable-placeholder",
		    																revert: 200,
		    																stop: function( event, ui ) {

		    																	uj_sorrend_mentes();
		    																}
		    															});
	   	}



			// editor megjelenítése vagy elrejtése (beállítástól függően) - kapcsolóra kattintva			
			$("#show_cpt_editor").click(function(){
				var $this = $(this);
				if($this.attr('checked')) { $(".wp-editor-expand").fadeIn();
				} else { $(".wp-editor-expand").fadeOut(); }

				$(window).resize();
			});	
			//------


		/*********** Beállítások **********/

		// galéria megjelenítési sorrend beállítása
		if ( $("input#gallery_metabox_order").length ) {
			$("input#gallery_metabox_order").change(function(){
				var input_status = "";
	            var post_id = $("#galeria_metabox_wrapper .galeria_content ul").attr("post-id") // oldal / CPT / bejegyzés id

				if ( $(this).is(':checked') ) {	input_status = "random"; } 
				else { input_status = "sorrend"; }

				if ( input_status != "" ) {

					jQuery.post(
						ajaxurl,
						{
							input_status : input_status,
							post_id : post_id,													
							action : 'galeria_megj_sorrend'
						},
						function(data) {
							if (data != 0) {
								metabox_inputok_torlese();
							}
						}
					);
				}

			});
		}

		// Editor megjelenítése vagy elrejtése
		if ( $("input#show_cpt_editor").length ) {
			$("input#show_cpt_editor").change(function(){
				var input_status = "";
	            var post_id = $("#galeria_metabox_wrapper .galeria_content ul").attr("post-id") // oldal / CPT / bejegyzés id

				if ( $(this).is(':checked') ) {	input_status = "show"; } 
				else { input_status = "hide"; }

				if ( input_status != "" ) {

					jQuery.post(
						ajaxurl,
						{
							input_status : input_status,
							post_id : post_id,													
							action : 'editor_megjelenitese'
						},
						function(data) {
							if (data != 0) {
								metabox_inputok_torlese();
							}
						}
					);
				}

			});
		}

    });
});