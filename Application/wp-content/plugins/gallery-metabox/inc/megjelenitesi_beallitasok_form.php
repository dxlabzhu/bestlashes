<?php


	$metadata_array = get_gallery_plugin_img_metadata( $post_id, $attachment_id );


	$timthumb_beallitasok_metabox = $metadata_array['megjelenitesi_beallitasok'];





		$selected_timthumb_crop_aligment = $timthumb_beallitasok_metabox['vagas_elhelyezese'];


		if ( empty($selected_timthumb_crop_aligment)) { $selected_timthumb_crop_aligment = "c"; }





		$cropping = $timthumb_beallitasok_metabox['kep_szeleinek_vagasa'];


		if ( empty($cropping)) { $cropping = "cropped"; }





		$bg_color = $timthumb_beallitasok_metabox['hatterszin'];


		if ( empty($bg_color)) { $bg_color = "transparent"; }





		$custom_bg_color = $timthumb_beallitasok_metabox['egyedi_hatterszin'];





	if ( empty($selected_timthumb_crop_aligment) ) { $selected_timthumb_crop_aligment = "c"; }


?>





<div id="timthumb_beallitasok_metabox_content">


	<h5><?php _e("A Timthumb-al megjelenített képek beállításainak szerkesztése.", "galeria_metabox"); ?></h5>


	


	<form action="" method="post">


			


		<div class="beallitas kep_szeleinek_vagasa">


			<table>


				<tr class="cropped">


					<td><input type="radio" value="cropped" name="cropping_<?php echo $uniqid1 = uniqid(); ?>" id="vagas_<?php echo $uniqid = uniqid(); ?>" <?php checked( $cropping, 'cropped' ); ?> /></td>


					<th><label for="vagas_<?php echo $uniqid; ?>"><?php _e("Kép széleinek vágása", "galeria_metabox"); ?></label></th>


				</tr>


				<tr class="full_size">


					<td><input type="radio" value="full_size" name="cropping_<?php echo $uniqid1; ?>" id="vagas_<?php echo $uniqid = uniqid(); ?>" <?php checked( $cropping, 'full_size' ); ?> /></td>


					<th><label for="vagas_<?php echo $uniqid; ?>"><?php _e("Teljes kép megjelenítése", "galeria_metabox"); ?></label></th>


				</tr>


			</table>


		</div>	





		<div class="beallitas vagas_elhelyezese <?php if ( $cropping == "full_size" ) { echo "hidden"; } ?> ">


			<div class="left_side">


				<?php _e("Látható rész megjelölése:", "galeria_metabox"); ?>


			</div>


			<div class="right_side">


				<div class="kep_vagasi_pont">





					<input type="radio" value="tl" name="vpont" <?php checked( $selected_timthumb_crop_aligment, 'tl' ); ?> /> <!-- top-left -->


					<input type="radio" value="t"  name="vpont"  <?php checked( $selected_timthumb_crop_aligment, 't' ); ?> /> <!-- top -->


					<input type="radio" value="tr" name="vpont" <?php checked( $selected_timthumb_crop_aligment, 'tr' ); ?> /> <!-- top-right -->





					<input type="radio" value="l" name="vpont" <?php checked( $selected_timthumb_crop_aligment, 'l' ); ?> /> <!-- left -->


					<input type="radio" value="c" name="vpont" <?php checked( $selected_timthumb_crop_aligment, 'c' ); ?> /> <!-- center -->


					<input type="radio" value="r" name="vpont" <?php checked( $selected_timthumb_crop_aligment, 'r' ); ?> /> <!-- right -->





					<input type="radio" value="bl" name="vpont" <?php checked( $selected_timthumb_crop_aligment, 'bl' ); ?> /> <!-- bottom-left -->


					<input type="radio" value="b"  name="vpont" <?php checked( $selected_timthumb_crop_aligment, 'b' ); ?> /> <!-- bottom -->


					<input type="radio" value="br" name="vpont" <?php checked( $selected_timthumb_crop_aligment, 'br' ); ?> /> <!-- bottom-right -->





				</div>


			</div>


		</div>





		<div class="beallitas hatterszin <?php if ( $cropping == "cropped" ) { echo "hidden"; } ?> ">


			<h4><?php _e("Alapértelmezett háttérszín", "galeria_metabox"); ?></h4>


			<table>


				<tr>


					<td><input type="radio" name="kep_hatter" value="transparent" id="bg_color_<?php echo $uniqid = uniqid(); ?>" <?php checked( $bg_color, 'transparent' ); ?> /></td>


					<th><label for="bg_color_<?php echo $uniqid; ?>"><?php _e("Átlátszó <span>(csak PNG kép esetén)</span>", "galeria_metabox"); ?></label></th>


				</tr>


				<tr>


					<td><input type="radio" name="kep_hatter" value="white" id="bg_color_<?php echo $uniqid = uniqid(); ?>" <?php checked( $bg_color, 'white' ); ?> /></td>


					<th><label for="bg_color_<?php echo $uniqid; ?>"><?php _e("Fehér", "galeria_metabox"); ?></label></th>


				</tr>


				<tr>


					<td><input type="radio" name="kep_hatter" value="black"  id="bg_color_<?php echo $uniqid = uniqid(); ?>" <?php checked( $bg_color, 'black' ); ?> /></td>


					<th><label for="bg_color_<?php echo $uniqid; ?>"><?php _e("Fekete", "galeria_metabox"); ?></label></th>


				</tr>


				<tr>


					<td><input type="radio" name="kep_hatter" value="custom_color" id="bg_color_<?php echo $uniqid = uniqid(); ?>" <?php checked( $bg_color, 'custom_color' ); ?> /></td>


					<th>


						<label for="bg_color_<?php echo $uniqid; ?>"><?php _e("Egyéb:", "galeria_metabox"); ?></label>


						<input type="text" placeholder="#9cc303" name="custom_bg_color" value="<?php echo $custom_bg_color; ?>" />


					</th>


				</tr>										


			</table>


		</div>





		<hr />


		<div class="beallitas elonezet_keres">


			<div class="elonezet_btn button button-large" post-id="<?php echo $post_id; ?>" ><i class="fa fa-eye"></i><?php _e("Előnézet", "galeria_metabox") ?></div>


			<div class="leiras"><?php _e("Az előnézeti kép oldal aránya eltérhet a weboldalon megjelenített kép arányától.", "galeria_metabox") ?></div>


		</div>


		<hr />


		


		<div class="clear"></div>


	</form>


</div>