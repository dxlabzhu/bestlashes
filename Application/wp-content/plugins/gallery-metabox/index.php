<?php
/**
 * Plugin Name: Galéria metabox
 * Plugin URI: http://auretto.works
 * Description: Post típusokhoz rendelhető galéria funkció, amiben külön tárolható kép és dokumentum.
 * Version: 2.14
 * Author: Auretto Works
 * Author URI: http://auretto.works
 */

 
/*
 * 2.14
 *  - A képek adatainak szerkesztésénél át lehet váltani a következő vagy előző kép adataira
 * */
 

/*
 * 2.13 (CSAK ABBAN AZ ESETBEN LEHET FRISSÍTENI ERRE A VERZIÓRA HA AZ OLDALON NINCS HASZNÁLATBAN A [gallery_metabox] SHORTKÓD! )
 *  - Minden képhez be lehet állítani, hogy melyik része látszódjon mindenképpen.
 *  - Kép megjelenítési beállításnál előnézet funkció (nem szükséges menteni előtte)
 *  - megjelenítési beállítások használata [gallery_metabox] shortkódnál ( csak egyedi méret megadása esetén! pl: size="400x300"  )
 *  - CSS javítások
 *  - [gallery_metabox] shortkódnál style="list" megadási lehetőség
 *  - Létező oldal vagy post URL kiválasztása és hozzáadása az URL mezőbe.
 * */

 
/*
 * 2.8
 * 	- 1 képhez tartozó metaadatokat lekérdező függvény előállítása - get_gallery_plugin_img_metadata()
 * 	- képeknél a 2. leírás textarea lett
 *	- képekhez tetszőleges URL tárolás
 * 	- doksi szerkesztési nézete új oldalon nyíljon meg
 * 	- számláló jelzi a menüben, hogy hány db kép vagy doksi van feltöltve
 * 	- [gallery] shortkód átnevezése [gallery_metabox]-ra
 */ 
 

/*
*	2.4
*	 - képek/doksik megjelenítése shortkódal
*	 - képek és/vagy doksik lekérése függvény létrehozása
* 	 - külön beállítás oldal + post típusok megadása
*	 - kép adatszerkesztői oldalon timthumb képvágás
*	 - fájlok random megjelenítése a felületen (ha engedélyezett)
*/


/**********************/
/*   GLOBAL VALUES    */
/**********************/

$enable_post_types = get_option( 'geleria_metabox_plugin_enable_post_types' );
global $enable_post_types;

$gallery_plugin_timthumb_url = plugin_dir_url( __FILE__ ) . 'lib/timthumb/timthumb.php?src=';
global $gallery_plugin_timthumb_url;

/**/  
 

/********************************/
/*         CSS & JQUERY         */
/********************************/

add_action('admin_enqueue_scripts', 'galeria_metabox_enqueue_scripts_admin');
function galeria_metabox_enqueue_scripts_admin () {
	/* CSS */
	wp_enqueue_style('gallery_metabox_fancybox_css', plugin_dir_url( __FILE__ ).'lib/fancybox/source/jquery.fancybox.css');
	wp_enqueue_style('gallery_metabox_jquery_smoothness_css', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
	wp_enqueue_style('gallery_metabox_font_awesome_css', plugin_dir_url( __FILE__ ).'lib/font_awesome/css/font-awesome.min.css');
	wp_enqueue_style('galeria_metabox_admin_css', plugin_dir_url( __FILE__ ).'css/admin.css');
	
	/* JS */
	wp_enqueue_script( 'gallery_metabox_fancybox_js', plugin_dir_url( __FILE__ ) . 'lib/fancybox/source/jquery.fancybox.js' );
	wp_enqueue_script( 'gallery_metabox_jquery_smoothness_js', '//code.jquery.com/ui/1.11.4/jquery-ui.js' );
	wp_enqueue_script( 'galeria_metabox_admin_js', plugin_dir_url( __FILE__ ) . 'js/admin.js' );
}

/**/


/*************************/
/*    OTHER FUNCTIONS    */
/*************************/

// előnézeti kép vágásának összerakása
function get_timthumb_settings_for_gallery_img_preview($vagasi_adatok) {
	$result = "";

	if ( !empty($vagasi_adatok) ) {
		$vagas_elhelyezese = $vagasi_adatok['vagas_elhelyezese'];
		$kep_szeleinek_vagasa = $vagasi_adatok['kep_szeleinek_vagasa'];
		$hatterszin = $vagasi_adatok['hatterszin'];
		$egyedi_hatterszin = 	$vagasi_adatok['egyedi_hatterszin'];

			$selected_timthumb_crop_aligment = $vagas_elhelyezese;
				if ( !empty($selected_timthumb_crop_aligment)) { $result .= "&a=" . $selected_timthumb_crop_aligment; }
	
			$cropping = $kep_szeleinek_vagasa;
				if ( $cropping == "cropped" ) { $result .= ""; } 
				else if ( $cropping == "full_size" ) { $result .= "&zc=2"; }

			$bg_color = $hatterszin;
			$custom_bg_color = $egyedi_hatterszin;
				if ( $bg_color == "transparent" ) { $result .= "&ct=1"; }
				else if ( $bg_color == "white" ) { $result .= "&ct=0&cc=ffffff"; }
				else if ( $bg_color == "black" ) { $result .= "&ct=0&cc=000000"; }
				else if ( ($bg_color == "custom_color") && !empty($custom_bg_color) ) {
					 $result .= "&ct=0&cc=". substr($custom_bg_color, 1); // remove -> # from: #646464
				}
	
			return $result;
	}
	return "";
}

// Ezt a függvényt kell a kép méretének megadása után beilleszteni
function get_timthumb_settings_for_gallery_img($post_id, $img_id) {
	if ( empty($post_id) || empty($img_id) ) { return ""; }
	$result = "";

	$metadata = get_post_meta( $post_id, 'attachment-'.$img_id, true );
	if ( !empty($metadata) ) {		
		$metadata = unserialize($metadata);
		$vagas_elhelyezese = $metadata['megjelenitesi_beallitasok']['vagas_elhelyezese'];
		$kep_szeleinek_vagasa = $metadata['megjelenitesi_beallitasok']['kep_szeleinek_vagasa'];
		$hatterszin = $metadata['megjelenitesi_beallitasok']['hatterszin'];
		$egyedi_hatterszin = 	$metadata['megjelenitesi_beallitasok']['egyedi_hatterszin'];

			$selected_timthumb_crop_aligment = $vagas_elhelyezese;
				if ( !empty($selected_timthumb_crop_aligment)) { $result .= "&a=" . $selected_timthumb_crop_aligment; }
	
			$cropping = $kep_szeleinek_vagasa;
				if ( $cropping == "cropped" ) { $result .= ""; } 
				else if ( $cropping == "full_size" ) { $result .= "&zc=2"; }
	
			$bg_color = $hatterszin;
			$custom_bg_color = $egyedi_hatterszin;
				if ( $bg_color == "transparent" ) { $result .= "&ct=1"; }
				else if ( $bg_color == "white" ) { $result .= "&ct=0&cc=ffffff"; }
				else if ( $bg_color == "black" ) { $result .= "&ct=0&cc=000000"; }
				else if ( ($bg_color == "custom_color") && !empty($custom_bg_color) ) {
					 $result .= "&ct=0&cc=". substr($custom_bg_color, 1); // remove -> # from: #646464
				}
	
			return $result;	
	}
	return "";
}


// lekéri egy képnek az összes metaadatát és visszaadja egy tömbként
function get_gallery_plugin_img_metadata( $post_id, $img_id ) {
	if ( empty($post_id) || empty($img_id) ) { return ""; }

	$metadata = get_post_meta( $post_id, 'attachment-'.$img_id, true );
	if ( !empty($metadata) ) {		
		return unserialize($metadata);
		// return --> array(3) {  ["title"]=> string(7) "fhgdhdh" ["desc"]=> string(56) "fddsf <b>dhg</b> fdgfdsgfs"  ["url"]=>  string(14) "hhgfdhjfdhjggf" }
	}
	return "";
}


// képek, doksik vagy mindkettő azonosítóinak lekérése egy tömbbe POST ID alapján
function get_gallery_plugin_post_attachments( $post_id, $type="img" ) {
	if ( empty($post_id) ) { return ""; }
	$post_id = (int) $post_id;

	global $post;
	$result_array = array();

	$csatolt_fajlazonositok = json_decode( get_post_meta( $post_id, "galeria_azonositok", true ) );

	// összekeverés (ha beállított)
	if ( get_post_meta( $post_id, "galeria_megj_sorrend", true ) == "random" ) { shuffle($csatolt_fajlazonositok); }
	$csatolt_fajlazonositok = array_values(array_filter(array_unique($csatolt_fajlazonositok)));


	if ( count($csatolt_fajlazonositok) > 0 ) {
		foreach ($csatolt_fajlazonositok as $key => $media_id) {

			if ( $type == "doc" ) {
				if ( !wp_attachment_is_image($media_id) ) { 

					$result_array[] = $media_id;
				}
			} else if ( $type == "img" ) {
				if ( wp_attachment_is_image($media_id) ) { 
				
					$result_array[] = $media_id;
				}
			} else {
				// doc & img
				$result_array[] = (int) $media_id;
			}
		}
	}

	if ( count($result_array) > 0 ) { return $result_array; }
	return "";
}

if (!function_exists('string_convertTo_int')) {
	function string_convertTo_int($string) {
		if (!empty($string)) {
			preg_match_all('!\d+!', $string, $matches);
			return (int) join($matches[0]);
		}
	}
}

function galeria_metabox_beallitas_content() {
	global $post;
	$html="";

	$galeria_megj_sorrend = get_post_meta( $post->ID, "galeria_megj_sorrend", true );
	$editor_megjelenitese = get_post_meta( $post->ID, "editor_megjelenitese", true );

	// wp editor elrejtés
	if ( $editor_megjelenitese == "hide" ) {
		?><style type="text/css"> .wp-editor-expand { display: none; } </style><?php
	}

	$html .= '<div id="galeria_megjelenitesi_mod">
				   <div class="left_side">
				      <span class="title">'. __( "Galéria megjelenítési sorrend", "galeria_metabox" ) .'</span>
				      <div class="desc">
				         <p>'. __( "A főoldalon való megjelenítési sorrend beállítása. <br>Minden galériára külön értendő!", "galeria_metabox" ) .'</p>
				      </div>
				   </div>
				   <div class="right_side">
				      <div class="onoffswitch">
				         <input type="checkbox" class="onoffswitch-checkbox" id="gallery_metabox_order" '.checked( $galeria_megj_sorrend, "random", false ).' />
				         <label class="onoffswitch-label" for="gallery_metabox_order">
					         <span class="onoffswitch-inner">
						         <span class="onoffswitch-active"><span class="onoffswitch-switch">'. __( "Random", "galeria_metabox" ) .'</span></span>
						         <span class="onoffswitch-inactive"><span class="onoffswitch-switch">'. __( "Sorrend", "galeria_metabox" ) .'</span></span>
					         </span>
				         </label>
				      </div>
				   </div>
				   <div class="clear"></div>
			</div>
			<div class="separator_line"></div>

			<div class="clear"></div>
			<div id="editor_megjelenitese" class="settings_metabox_item">
					<div class="left_side">
						<span class="title">'. __( "Editor megjelenítése:", "galeria_metabox" ) .'</span>
					</div>
					<div class="right_side">
						<div class="onoffswitch">
						    <input type="checkbox" class="onoffswitch-checkbox" id="show_cpt_editor" '.checked( $editor_megjelenitese, "show", false ).' />
						    <label class="onoffswitch-label" for="show_cpt_editor">
						        <span class="onoffswitch-inner">
						            <span class="onoffswitch-active"><span class="onoffswitch-switch">'. __( "Mutat", "galeria_metabox" ) .'</span></span>
						            <span class="onoffswitch-inactive"><span class="onoffswitch-switch">'. __( "Elrejt", "galeria_metabox" ) .'</span></span>
						        </span>
						    </label>
						</div>
					</div>
			  </div>
			
			<div class="clear"></div>
			<div class="gallery_shortcode settings_metabox_item">
					<div class="left_side">
						<span class="title">Shortcode:</span>

						<div class="shortcode_size_list">
							<div>size="full"</div>
							<div>size="large"</div>
							<div>size="medium"</div>
							<div>size="thumbnail"</div>
							<div>size="400x300"</div>
						</div>

						<div class="shortcode_type_list">
							<div>type="img"</div>
							<div>type="doc"</div>
						</div>

						<div class="shortcode_style_list">
							<div>style="list"</div>
						</div>
						
					</div>
					<div class="right_side">
						<input type="text" value="[gallery_metabox id=&#34;'.$post->ID.'&#34; size=&#34;large&#34; type=&#34;img&#34; ]" disabled />
					</div>
			  </div>';
	return $html;
}

// feleslegessé vált kép meta adatainak törlése (ha szükséges!)
function attachment_meta_torles($post_id, $galeria_kepek) {
	if ( !empty($galeria_kepek) && !empty($post_id) ) {

		$meta_values = get_post_meta( $post_id );
		$hozzaadott_meta_adatok = array();

		foreach ($meta_values as $key => $value) {
			$meta_key = (string) $key;
			$pos = strrpos($meta_key, "attachment-");
			if ($pos === 0) {
				$hozzaadott_meta_adatok[] = (int) substr($meta_key, 11); 
			}
		}

		// fölösleges meta értékek törlése
		foreach ($hozzaadott_meta_adatok as $key => $value) {
			if ( !in_array($value, $galeria_kepek) ) {
				delete_post_meta($post_id, "attachment-".$value);
			}
		}
	}
}

/**/


/**************************/
/*        SIDEBARS	  */
/**************************/


/**/


/**********************/
/*    SHORTCODE       */
/**********************/


add_shortcode('gallery_metabox', 'gallery_metabox_sc_function');
function gallery_metabox_sc_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'id' => "",
		'size' => "",
		'type' => "",
		'style' => "",
		'open_new_tab' => "",
	), $atts));

	if ( empty($id) ) { return ""; }
	if ( empty($type) ) { $type = "img"; }

	if ( strtolower($open_new_tab) == 'true' ) { $open_new_tab = '_blank'; }
	else { $open_new_tab = ""; }

	global $post, $gallery_plugin_timthumb_url;
	$img_sizes_array = array( 'full', 'large', 'medium', 'thumbnail' );
	$result_array = array();

	$galeria_kepek = json_decode( get_post_meta( $id, "galeria_azonositok", true ) );

	// összekeverés (ha beállított)
	if ( get_post_meta( $id, "galeria_megj_sorrend", true ) == "random" ) { shuffle($galeria_kepek); }

	foreach ($galeria_kepek as $key => $media_id) {
		$media_url = "";

		// doksik összeállítása
		if ( $type == "doc" ) {
			if ( !wp_attachment_is_image($media_id) ) {

				$file_url = wp_get_attachment_url($media_id);
				$result_array[] = '<a target="'.$open_new_tab.'" href="'.$file_url.'" data-attachment-id="'.$media_id.'" data-gallery-id="'.$id.'" >'.basename($file_url).'</a>';
			}
		} 

		// képek összeállítása
		else if ( $type == "img" ) {

			if ( wp_attachment_is_image($media_id) ) {
				
				$img_size = "large"; // <-- default

				// img size
				if ( !empty($size) ) {
					// alap wp képméret egyike
					if ( in_array($size, $img_sizes_array) ) { $img_size = $size; }

					// egyedi képméret
					if ( strpos( $size, "x" ) > 0 ) {
						$width = (int) substr( $size,0, strpos( $size, "x" ) );
						$height = (int) substr( $size, strpos( $size, "x" )+1 );

						$array = wp_get_attachment_image_src( $media_id, "large" ); $media_url = $array[0];
						$eredeti_media_url = $media_url;
						$media_url = $gallery_plugin_timthumb_url . $media_url . "&w=".$width."&h=".$height . get_timthumb_settings_for_gallery_img( $id, $media_id );
					}
				}

				if ( empty($media_url) ) {
					$array = wp_get_attachment_image_src( $media_id, $img_size ); $media_url = $array[0];
				}

				if ( !empty($media_url) ) {
					
					$result_array[] = '<a href="'.$eredeti_media_url.'" target="'.$open_new_tab.'" data-attachment-id="'.$media_id.'" data-gallery-id="'.$id.'" ><img src="'.$media_url.'" /></a>';
				}
				
			}
		}
	} // /foreach
		
	// kapott adatok összeállítása listába vagy lista nélkül egymás után
	
	if ( empty($result_array) ) { return "";
	} else if ( !empty($result_array) && empty($style) ) {

		return implode('', $result_array);
	} else if ( !empty($result_array) && ($style == "list") ) {

		$result = '<ul class="'.$type.'-list">';
			foreach ( $result_array as $key => $attachment_value ) {									
				$result .= '<li>'.$attachment_value.'</li>';
			}
	return 
		$result .= '</ul>';
	}
}


/**/


/************************/
/*       METABOX        */
/************************/

add_action( 'add_meta_boxes', 'so_custom_meta_box' );
function so_custom_meta_box($post) {
	global $enable_post_types;

	foreach ($enable_post_types as $post_type) {
		if (post_type_exists($post_type)) {
			add_meta_box('galeria_metabox', 'Képek és dokumentumok csatolása', 'galeria_metabox_function', $post_type, 'normal', 'default');
		}
	}
}


function galeria_metabox_function() {
	
	?>
		<div id='galeria_metabox_wrapper'>

			<div id="metabox_ajax_content">
				<!-- Ide töltődik be minden szerkesztésre megnyitott kép adatai -->
			</div>

			<!--  metabox_content -->
			<div class="metabox_content galeria_content show">
				<?php echo galeria_metabox_content( "kepek" ); ?>
			</div>

			<div class="metabox_content beallitas_content">
				<?php echo galeria_metabox_beallitas_content(); ?>
			</div>
			<!--  /metabox_content -->

			<!--  menu_content -->
			<div class='menu_content menu_open'>
				<i class="fa fa-times menu_btn close_menu_btn"></i>
				<i class="fa fa-bars menu_btn show_menu_btn"></i>

				<div class="menu_ul_wrapper">
					<ul>
						<li wrapper-class="galeria_content" class="active" tartalom="galeria_content"><?php _e('Galéria', 'galeria_metabox'); ?> <span class="quantity">455</span></li>
						<li wrapper-class="dokumentumok_content" tartalom="galeria_content"><?php _e('Dokumentumok', 'galeria_metabox'); ?> <span class="quantity">459</span></li>
						<li wrapper-class="beallitas_content" tartalom="beallitas_content"><?php _e('Beállítások', 'galeria_metabox'); ?></li>	
					</ul>

					<div class="mediatar_btn kepek"></div>
					<div class="mediatar_btn dokumentumok"></div>
				</div>
			</div>
			<!--  /menu_content -->

			<div class="clear"></div>
			<div class="metabox_mentes_uzenet"><?php _e("Változások frissítve.", "galeria_metabox"); ?></div>
		</div>
	<?php
}


/**/


/*************************************/
/*    SAVE POST / METABOX / etc...   */
/*************************************/

add_action( 'init', 'gallery_metabox_plugin_save_post' );
function gallery_metabox_plugin_save_post() {


	// galéria beállítás oldalon elmentett adatok
	if ( isset($_POST['geleria_metabox_plugin_enable_post_types']) ) {

		// használní kívánt post típusok mentése
		update_option( 'geleria_metabox_plugin_enable_post_types', $_POST['selected_post_types'] );


	}

}

/**/


/*******************/
/*      CPT        */
/*******************/

	
	
/**/


/*******************************/
/*          ADD ACTION         */
/*******************************/

add_action('admin_menu', 'galeria_metabox_plugin_beallitas_oldal');
function galeria_metabox_plugin_beallitas_oldal() {
	add_submenu_page( 'tools.php', 'Galéria beállítás', 'Galéria beállítás', 'manage_options', 'galeria_beallitas', 'galeria_metabox_plugin_beallitas_oldal_function' );
}

function galeria_metabox_plugin_beallitas_oldal_function() {
	
	$post_types = get_post_types( '', 'names' ); 
	asort($post_types);

	$geleria_metabox_plugin_enable_post_types = get_option( 'geleria_metabox_plugin_enable_post_types' );

	?>
	<div class="wrap"><div id="icon-tools" class="icon32"></div>
		<h2><?php _e('Galéria metabox beállításai', 'galeria_metabox'); ?></h2>

		<form action="" method="post" class="post_types">
			<h3><?php _e('Használni kívánt post típusok', 'galeria_metabox'); ?></h3>
			<table>
				<?php 
				foreach ($post_types as $key => $post_type) {
					$checked = "";
					if ( in_array($post_type, $geleria_metabox_plugin_enable_post_types) ) { $checked = "checked"; }

					?>
					<tr>
						<td><input type='checkbox' value='<?php echo $post_type; ?>' name='selected_post_types[]' id='post_type_<?php echo $post_type; ?>'  <?php echo $checked; ?> /></td>
						<th><label for='post_type_<?php echo $post_type; ?>' ><?php echo $post_type; ?></label></th>
					</tr>		
					<?php
				}
				?>
			</table>
			<input type="submit" class="button button-primary button-large" value="<?php _e('Mentés', 'galeria_metabox'); ?>" name="geleria_metabox_plugin_enable_post_types" />
		</form>


	</div>
	<?php

}

/**/


/*******************************/
/*          DO ACTION          */
/*******************************/


/**/


/*******************************/
/*           FILTER            */
/*******************************/


/**/


/*******************************/
/*          AJAX 	     	   */
/*******************************/


// Galléria metabox content előállítása
add_action( 'wp_ajax_galeria_metabox_content', 'galeria_metabox_content' );
add_action( 'wp_ajax_nopriv_galeria_metabox_content', 'galeria_metabox_content' );
function galeria_metabox_content( $kepek_vagy_dokument ) {
	global $post;

	// új kép(ek) azonosítóinak mentése
	if (isset($_POST['file_azonositok']) && !empty($_POST['file_azonositok']) && !empty($_POST['post_id'])) {
		$post_id = (int) $_POST['post_id']; $post->ID = $post_id;
		$uj_azonositok_id = array();

		$eddig_beillesztett_azonositok = json_decode( get_post_meta( $post_id, 'galeria_azonositok', true) );

		$file_azonositok = explode("'", $_POST['file_azonositok']);

		foreach ($file_azonositok as $key => $file_azonosito) {
			$file_azonosito = string_convertTo_int( $file_azonosito );

			if (!empty($file_azonosito) && $file_azonosito > 0) {

				if (!in_array($file_azonosito, $eddig_beillesztett_azonositok)) {
					$uj_azonositok_id[] = $file_azonosito;
				}
			}
		}

		if (!empty($uj_azonositok_id)) {

			if (!empty($eddig_beillesztett_azonositok)) {
				$galeria_azonositok_frissites = array_merge($eddig_beillesztett_azonositok, $uj_azonositok_id);

				$galeria_azonositok_frissites = array_unique($galeria_azonositok_frissites);
				$galeria_azonositok_frissites = array_filter($galeria_azonositok_frissites);
				$galeria_azonositok_frissites = array_values($galeria_azonositok_frissites);
			} else {
				$galeria_azonositok_frissites = $uj_azonositok_id;
			}

			update_post_meta( $post_id, 'galeria_azonositok', json_encode( $galeria_azonositok_frissites ) );
		}
	}
	//----
	$html="";

	$galeria_kepek_adatai = array();

	$galeria_kepek = json_decode( get_post_meta( $post->ID, "galeria_azonositok", true ) );

	// feleslegessé vált kép meta adatainak törlése (ha szükséges!)
	attachment_meta_torles( $post->ID, $galeria_kepek );

	foreach ($galeria_kepek as $key => $galeria_kep_id) {
		if (!empty($galeria_kep_id)) {

			$galeria_kep_thumbnail_url = wp_get_attachment_image_src( $galeria_kep_id, "thumbnail" ); $galeria_kep_thumbnail_url = $galeria_kep_thumbnail_url[0];
			$galeria_kepek_adatai[] = array( 'id' => $galeria_kep_id, 'thumbnail' => $galeria_kep_thumbnail_url, 'attachment_url' => wp_get_attachment_url( $galeria_kep_id ) );
		}
	}

	foreach ($galeria_kepek_adatai as $key => $galeria_kep_adatai) {

		//----------------- Kép összeállítása ------------------

		if ( wp_attachment_is_image( $galeria_kep_adatai['id'] ) ) {

			$html .= '<li class="kep" attachment-id="'.$galeria_kep_adatai['id'].'">
						<img src="'.$galeria_kep_adatai['thumbnail'].'" alt="" title="" />

							<div class="onhover">
								<div class="del_btn dashicons-before dashicons-trash"></div>

								<a href="#metabox_ajax_content" class="fancybox edit_btn" rel="gallery_img">
									<div class="dashicons-before dashicons-edit"></div>
								</a>
								
								<a class="fancybox fullscreen_btn" rel="gallery1" href="'.$galeria_kep_adatai['attachment_url'].'">
									<div class="dashicons-before dashicons-visibility"></div>											
								</a>
							</div>
					  </li>';
		} else {

		//----------------- Dokumentum összeállítása ------------------

			$html .= '<li class="dokumentum" attachment-id="'.$galeria_kep_adatai['id'].'">
						<div class="filename">'.basename($galeria_kep_adatai['attachment_url']).'</div>
						<div class="onhover">
							<div class="del_btn dashicons-before dashicons-trash"></div>

							<a target="_blank" href="'. get_admin_url().'post.php?post='.$galeria_kep_adatai['id'].'&action=edit' .'" class="fancybox edit_btn">
								<div class="dashicons-before dashicons-edit"></div>
							</a>
							
							<a class="fullscreen_btn" target="_blank" href="'.$galeria_kep_adatai['attachment_url'].'">
								<div class="dashicons-before dashicons-visibility"></div>											
							</a>
						</div>
					  </li>';
		}
	}

	$html = "<ul post-id='".$post->ID."' class='attachments'>". $html ."</ul>";

	// ajax kérés esetén "echo" !!!
	if (isset($_POST['file_azonositok']) && !empty($_POST['file_azonositok']) && !empty($_POST['post_id'])) { 
		echo $html; exit;
	} else {

		return $html;
	}
}


// kiválasztott galéria azonosítő törlése
add_action( 'wp_ajax_galeria_azonosito_torles', 'galeria_azonosito_torles' );
add_action( 'wp_ajax_nopriv_galeria_azonosito_torles', 'galeria_azonosito_torles' );
function galeria_azonosito_torles() {
	global $post;

	if (isset($_POST['torlendo_id']) && !empty($_POST['torlendo_id']) && !empty($_POST['post_id'])) {	
		$eddig_beillesztett_azonositok_temp = array();

		$post_id = (int) $_POST['post_id']; $post->ID = $post_id;
		$torlendo_id = (int) $_POST['torlendo_id'];

		// lekérés
		$eddig_beillesztett_azonositok = json_decode( get_post_meta( $post_id, 'galeria_azonositok', true) );

		// átnézés + törlés
		foreach ($eddig_beillesztett_azonositok as $key => $eddig_beillesztett_azonosito) {
			if ( $torlendo_id != $eddig_beillesztett_azonosito) { $eddig_beillesztett_azonositok_temp[] = $eddig_beillesztett_azonosito; }
		}

		// update
		update_post_meta( $post_id, 'galeria_azonositok', json_encode( $eddig_beillesztett_azonositok_temp ) );

		echo 1; exit;
	}
}

// kiválasztott "kép" adatainak lekérése szerkesztésre
add_action( 'wp_ajax_fajl_adatainak_szerkesztese', 'fajl_adatainak_szerkesztese' );
add_action( 'wp_ajax_nopriv_fajl_adatainak_szerkesztese', 'fajl_adatainak_szerkesztese' );
function fajl_adatainak_szerkesztese() {
	global $post, $gallery_plugin_timthumb_url;

	if (isset($_POST['attachment_id']) && !empty($_POST['attachment_id']) && !empty($_POST['post_id'])) {

		$post_id = (int) $_POST['post_id']; $post->ID = $post_id;
		$attachment_id = (int) $_POST['attachment_id'];

			$attachment = get_post( $attachment_id );
			$medium_img_url = wp_get_attachment_image_src( $attachment_id, "medium" ); $medium_img_url = $medium_img_url[0];

			$attachment_data_array = array(
										'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
										'description' => $attachment->post_content,
										'href' => get_permalink( $attachment->ID ),
										'title' => $attachment->post_title,
										'post_author' => $attachment->post_author,
									);

			$author_id = (int) $attachment_data_array['post_author'];
			$author_data = get_user_by("id", $author_id);
			$author_display_name = $author_data->display_name;


			$attachment_data_array2 = wp_get_attachment_metadata( $attachment_id );

			$hozzaadott_adatok = get_post_meta( $post_id, "attachment-".$attachment_id, true );
			if ( !empty($hozzaadott_adatok) ) { $hozzaadott_adatok = unserialize($hozzaadott_adatok); }

	} else { return ""; }

	?>
		<div class="left_side">
			<img src="<?php echo $gallery_plugin_timthumb_url . $medium_img_url . "&w=250&h=250&zc=2&ct=0"; ?>" alt="" title="" />
			<div class="bottom_shadow"></div>

			<div class="attachment_info_content">
				<p><?php _e("Feltöltés dátuma", "galeria_metabox"); echo " <b>" . get_the_date("Y.m.d H:i", $attachment_id) . "</b>"; ?></p>
				<p><?php _e("Fájlformátum:", "galeria_metabox"); echo " <b>".$attachment_data_array2['sizes']['thumbnail']['mime-type']. "</b>"; ?></p>
				<p><?php _e("Feltöltő:", "galeria_metabox"); echo " <b><a target='_blank' href='".get_edit_user_link( $author_id )."'>".$author_display_name. "</a></b>"; ?></p>
				<a target="_blank" href="<?php echo get_admin_url().'post.php?post='.$attachment_id.'&action=edit'; ?>"><?php _e("Tovább a médiatárba", "galeria_metabox"); ?></a>
			</div>

		</div>
		<div class="right_side">
			<ul id="kep_szerkesztesi_menu">
				<li class="active"><?php _e("Adatok szerkesztése", "galeria_metabox") ?></li>
				<li><?php _e("Megjelenítési beállítások", "galeria_metabox") ?></li>
			</ul>
			
			<div class="form_content alap_beallitasok">
				<p><?php _e("Médiatár adatok hozzáadása/szerkesztése", "galeria_metabox") ?><span><?php _e("A kép adata mindenhol megfog változni!", "galeria_metabox") ?></span></p>
				<label>Cím:</label><input type="text" value="<?php echo $attachment_data_array['title']; ?>" name="basic_media_title">
				<label>Leírás:</label><input type="text" value="<?php echo $attachment_data_array['description']; ?>" name="basic_media_desc">
				
				<p><?php _e("Galérián belüli adatok hozzáadása/szerkesztése <span>A kép adata csak a galérián belül változik, ha máshol is használva van ez a kép akkor ott más néven lesz.</span>", "galeria_metabox"); ?></p>
				<label><?php _e("Cím:", "galeria_metabox") ?></label><input type="text" value="<?php echo $hozzaadott_adatok['title']; ?>"  name="added_gallery_img_title">
				<label><?php _e("Leírás:", "galeria_metabox") ?></label><textarea name="added_gallery_img_desc"><?php echo $hozzaadott_adatok['desc']; ?></textarea>
				<label><?php _e("URL:", "galeria_metabox") ?></label><input type="text" value="<?php echo $hozzaadott_adatok['url']; ?>" name="added_gallery_img_url">
				
				<div class="letezo_post_url_kivalasztasa">
					<label><?php _e("Létező oldal vagy post URL kiválasztása:", "galeria_metabox") ?></label>
					<?php
					$post_types_array_temp = array();
					$post_types_array = get_post_types();
					$post_types_exceptions_array = array( 'attachment', 'wpcf7_contact_form', 'nav_menu_item' );
					foreach ($post_types_array as $key => $value) {
						if ( !in_array($value, $post_types_exceptions_array) ) {
							$post_types_array_temp[] = $value;
						}
					}
					$post_types_array = $post_types_array_temp;			
					usort($post_types_array);	
					if ( !empty($post_types_array) ) {
						?><select class="select_post_type">
							<option value=""> - </option><?php
						foreach ($post_types_array as $key => $post_type) {
							echo '<option value="'.$post_type.'">'.$post_type.'</option>';
						}
						?>
						</select>
						
						<select class="select_post">
							<option value=""> - </option>					
						</select>
						<div class="hozzaadas_btn"><i class="fa fa-plus-circle"></i></div>
						<?php
					}
					?>
				</div>
				
				<div class="clear"></div>
				<div post-id="<?php echo $post_id; ?>" attachment-id="<?php echo $attachment_id; ?>" class="update_btn button button-primary button-large"><?php _e("Mentés", "galeria_metabox") ?></div>
				<span class="spinner"></span>
				<div class="metabox_mentes_uzenet hidden"><?php _e("Változások frissítve.", "galeria_metabox"); ?></div>
			</div>
	
			<div class="form_content megjelenitesi_beallitasok">
				<?php include('inc/megjelenitesi_beallitasok_form.php'); ?>

				<div style="display: block; width: 100%; margin-top: 15px;"></div>
				<div post-id="<?php echo $post_id; ?>" attachment-id="<?php echo $attachment_id; ?>" class="update_btn button button-primary button-large"><?php _e("Mentés", "galeria_metabox") ?></div>
				<span class="spinner"></span>
				<div class="metabox_mentes_uzenet hidden"><?php _e("Változások frissítve.", "galeria_metabox"); ?></div>
			</div>

		</div>
		<div class="bottom_side">
			<div class="dashicons-before dashicons-no exit_btn"></div>
		</div>
	<?php
	exit;
}

// kép adatainak mentése a szerkesztő frissítés gombra kattintva
add_action( 'wp_ajax_fajl_adatainak_mentese', 'fajl_adatainak_mentese' );
add_action( 'wp_ajax_nopriv_save_img_info_from_editor', 'fajl_adatainak_mentese' );
function fajl_adatainak_mentese() {
	global $post;

	if (isset($_POST['attachment_id']) && !empty($_POST['attachment_id']) && !empty($_POST['post_id'])) {

		$post_id = (int) $_POST['post_id'];
		$attachment_id = (int) $_POST['attachment_id'];
		
		// alap adatok
		$basic_media_title = $_POST['basic_media_title'];
		$basic_media_desc = $_POST['basic_media_desc'];
		$added_gallery_img_title = $_POST['added_gallery_img_title'];
		$added_gallery_img_desc = $_POST['added_gallery_img_desc'];
		$added_gallery_img_url = $_POST['added_gallery_img_url'];

		// megjelenítési beállítások
		$kep_szeleinek_vagasa = $_POST['kep_szeleinek_vagasa'];
		$vagas_elhelyezese = $_POST['vagas_elhelyezese'];
		$hatterszin = $_POST['hatterszin'];
		$egyedi_hatterszin = $_POST['egyedi_hatterszin'];

		$megjelenitesi_beallitasok = array(
			"vagas_elhelyezese" => $vagas_elhelyezese,
			"kep_szeleinek_vagasa" => $kep_szeleinek_vagasa,
			"hatterszin" => $hatterszin,
			"egyedi_hatterszin" => $egyedi_hatterszin
		);


		// kép adatainak frissítése a médiatárban
		$new_attachment_data = array(
	      'ID'           => $attachment_id,
	      'post_title' => $basic_media_title,
	      'post_content' => $basic_media_desc
	  	);
		wp_update_post( $new_attachment_data );

		// kép adatainak frissítése a hozzáadott post meta elemben
		update_post_meta( $post_id, "attachment-".$attachment_id, serialize(array("title" => $added_gallery_img_title, "desc" => $added_gallery_img_desc, "url" => $added_gallery_img_url, "megjelenitesi_beallitasok" => $megjelenitesi_beallitasok)) );

		echo 1;
		exit;
	}
}


// módosított megjelenési sorrend mentése
add_action( 'wp_ajax_uj_sorrend_mentese', 'uj_sorrend_mentese' );
add_action( 'wp_ajax_nopriv_uj_sorrend_mentese', 'uj_sorrend_mentese' );
function uj_sorrend_mentese() {
	global $post;

	if ( isset($_POST['uj_sorrend']) && !empty($_POST['uj_sorrend']) && !empty($_POST['post_id']) ) {
		$post_id = (int) $_POST['post_id'];
		$uj_sorrend = explode("'", $_POST['uj_sorrend']);

		$uj_sorrend_temp = array();
		foreach ($uj_sorrend as $key => $value) {
			$value = (int) $value;
			if ($value > 0) {
				$uj_sorrend_temp[] = (int) $value;
			}
		}

		$uj_sorrend = json_encode( $uj_sorrend_temp );
		update_post_meta( $post_id, "galeria_azonositok", $uj_sorrend);

		echo $uj_sorrend; exit;
		echo 1; exit;
	}
}


// galéria megjelenítési sorrend mentése
add_action( 'wp_ajax_galeria_megj_sorrend', 'galeria_megj_sorrend' );
add_action( 'wp_ajax_nopriv_galeria_megj_sorrend', 'galeria_megj_sorrend' );
function galeria_megj_sorrend() {
	global $post;
	if ( isset($_POST['input_status']) && !empty($_POST['post_id']) && ($_POST['input_status'] == "sorrend" || $_POST['input_status'] == "random") ) {
		$post_id = (int) $_POST['post_id'];

		update_post_meta( $post_id, "galeria_megj_sorrend", $_POST['input_status'] );
		echo 1; exit;
	}
}


// Editor megjelenítése vagy elrejtése
add_action( 'wp_ajax_editor_megjelenitese', 'editor_megjelenitese' );
add_action( 'wp_ajax_nopriv_editor_megjelenitese', 'editor_megjelenitese' );
function editor_megjelenitese() {
	global $post;
	if ( isset($_POST['input_status']) && !empty($_POST['post_id']) && ($_POST['input_status'] == "show" || $_POST['input_status'] == "hide") ) {
		$post_id = (int) $_POST['post_id'];

		update_post_meta( $post_id, "editor_megjelenitese", $_POST['input_status'] );
		echo 1; exit;
	}
}


// Kép előnézetének elkészítése a kapott értékek alapján
add_action( 'wp_ajax_kep_elonezet_elkeszitese', 'kep_elonezet_elkeszitese' );
add_action( 'wp_ajax_nopriv_kep_elonezet_elkeszitese', 'kep_elonezet_elkeszitese' );
function kep_elonezet_elkeszitese() {
	global $post, $gallery_plugin_timthumb_url;
	
	$img_id = $_POST['attachment_id'];
	$post_id = $_POST['post_id'];
	$kep_szeleinek_vagasa = $_POST['kep_szeleinek_vagasa'];
	$vagas_elhelyezese = $_POST['vagas_elhelyezese'];
	$hatterszin = $_POST['hatterszin'];
	$egyedi_hatterszin = $_POST['egyedi_hatterszin'];

	$vagasi_adatok = array(
		'kep_szeleinek_vagasa' => $kep_szeleinek_vagasa,
		'vagas_elhelyezese' => $vagas_elhelyezese,
		'hatterszin' => $hatterszin,
		'egyedi_hatterszin' => $egyedi_hatterszin,
	);

	$array = wp_get_attachment_image_src( $img_id, 'full' );
	$img_url = $array[0];

	if ( !empty($img_url) ) {
		echo ' <img src="' . $gallery_plugin_timthumb_url . $img_url.'&w=250&h=250'. get_timthumb_settings_for_gallery_img_preview($vagasi_adatok) . '" />';
	}
	echo 1; exit;
}


add_action( 'wp_ajax_postok_lekerese_tipus_szerint', 'postok_lekerese_tipus_szerint' );
add_action( 'wp_ajax_nopriv_postok_lekerese_tipus_szerint', 'postok_lekerese_tipus_szerint' );
function postok_lekerese_tipus_szerint() {
	global $post;
	if ( empty($_POST['post_type']) ) { echo '0'; exit; }

	$posts_array = array();
	$posts = new WP_Query(array( 'post_type' => $_POST['post_type'], 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' ));
	while ($posts -> have_posts()) {
		$posts->next_post();
		echo '<option value="'.get_the_permalink( $posts->post->ID ).'">'.get_the_title( $posts->post->ID ).'</option>';
	}
	exit;
}

/**/