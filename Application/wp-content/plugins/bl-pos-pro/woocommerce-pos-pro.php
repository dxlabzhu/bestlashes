<?php

/**
 * Plugin Name: Best Lashes - WooCommerce POS Pro
 * Version:     0.4.19
 * Text Domain: woocommerce-pos-pro
 * Domain Path: /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Define plugin constants.
 */
define( 'WC_POS_PRO_VERSION', '0.4.19' );
define( 'WC_POS_PRO_PLUGIN_FILE', plugin_basename( __FILE__ ) ); // 'woocommerce-pos-pro/woocommerce-pos-pro.php'
define( 'WC_POS_PRO_PLUGIN_NAME', 'woocommerce-pos-pro' );
define( 'WC_POS_PRO_PLUGIN_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'WC_POS_PRO_PLUGIN_URL', trailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );

/**
 * The code that runs during plugin activation.
 */
require_once WC_POS_PRO_PLUGIN_PATH . 'includes/class-wc-pos-pro-activator.php';
new WC_POS_Pro_Activator( plugin_basename( __FILE__ ) );

/**
 * The code that runs during plugin deactivation.
 */
require_once WC_POS_PRO_PLUGIN_PATH . 'includes/class-wc-pos-pro-deactivator.php';
new WC_POS_Pro_Deactivator( plugin_basename( __FILE__ ) );