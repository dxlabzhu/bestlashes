<?php

/************************************/
/*     IMPORT PREV PLUGIN DATAS     */
/************************************/

class AurettoEmailImportPrevPluginFunctions {

	public function __construct($aee) {
		$this->aee = $aee;
	}

	// Get The Prev Plugin Templates Data --> Return: Array || FALSE
	function get_the_prev_plugin_templates_data() {
		$email_templates_array = get_option('email_templates');
		$templates_and_datas_array = array();

		if ( isset($email_templates_array) && !empty($email_templates_array) ) {
			$email_templates_array = unserialize($email_templates_array);

			if ( isset($email_templates_array[0]['email_template_name']) ) {
				foreach ($email_templates_array as $key => $datas) {
					$templates_and_datas_array[$datas['email_template_id']] = $datas;
				}
			}
		}

		if ( !empty($templates_and_datas_array) ) {
			foreach ($templates_and_datas_array as $template_id => $template_datas) {
				$email_template_array = unserialize( get_option('email_template_' . $template_id) );

				if ( isset($email_template_array) && !empty($email_template_array) ) {
					$templates_and_datas_array[$template_id]['subject'] = htmlspecialchars(stripslashes( $email_template_array["subject"] ));
					$templates_and_datas_array[$template_id]['sender'] = htmlspecialchars(stripslashes( $email_template_array["sender"] ));
					$templates_and_datas_array[$template_id]['sender_name'] = htmlspecialchars(stripslashes( $email_template_array["sender_name"] ));
					$templates_and_datas_array[$template_id]['msg'] = htmlspecialchars(stripslashes( $email_template_array["msg"] ));
					$templates_and_datas_array[$template_id]['attachments'] = $email_template_array["attachments"];

					if ( !empty($templates_and_datas_array[$template_id]['attachments']) ) {
						$templates_and_datas_array[$template_id]['attachments'] = json_decode($templates_and_datas_array[$template_id]['attachments'], true);

						foreach ($templates_and_datas_array[$template_id]['attachments'] as $key => $media_id) {
							if ( (intval($media_id) > 0) && (get_post_type($media_id) == 'attachment') ) {
								$templates_and_datas_array[$template_id]['attachments'][$key] = intval($media_id);
							}
							else { unset($templates_and_datas_array[$template_id]['attachments'][$key]); }
						}
					}
				}
			}
		}

		if ( is_array($templates_and_datas_array) && !empty($templates_and_datas_array) ) { return $templates_and_datas_array; }
		else { return FALSE; }
	}


	// Check the previous plugin is active --> Return: TRUE || FALSE
	function check_prev_plugin_is_active() {
		$result = FALSE;
		$active_plugins = get_option('active_plugins');

		foreach ($active_plugins as $key => $val) {
			if ( $val == 'email_sablon_szerkeszto/index.php' ) { $result = TRUE; break; }
		}

		return $result;
	}

}
