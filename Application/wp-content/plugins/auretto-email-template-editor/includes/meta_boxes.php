<?php

/**********************/
/*     META BOXES     */
/**********************/

class AurettoEmailMetaBoxes {

	public function __construct($aee) {
		$this->aee = $aee;
	}

	function addAurettoEmailEditorSettingsMetabox($post) {
		add_meta_box(
			Auretto_Email_Editor::MAIN_METABOX_NAME,
				'Settings',
				array($this, 'aurettoEmailEditorSettingsMetaboxFunction'),
				Auretto_Email_Editor::MAIN_CPT_NAME,
				'normal',
				'high'
		);
	}

	function aurettoEmailEditorSettingsMetaboxFunction($post) {

		$all_meta = get_post_meta($post->ID);
		$global_settings_array = get_option('aee_email_template_settings');
		$cc_emails_array = $this->aee->functions->get_cc_emails_by_post_id(intval($post->ID));
		$template_datas = $this->aee->functions->get_email_template(intval($post->ID));

		$is_woocommerce_active_class = "";
		if ( $this->aee->functions->is_woocommerce_active() ) {
			$is_woocommerce_active_class = 'is_woocommerce_active';
		}

		$global_settings_email_template_url = "";
		if ( isset($global_settings_array['email_template_url']) && !empty($global_settings_array['email_template_url']) ) {
			$global_settings_email_template_url = $global_settings_array['email_template_url'];
		}

		$media_files = "";
		$file_sizes = "";
		if ( isset($all_meta['aee_attachments'][0]) ) {
			$attachments = unserialize($all_meta['aee_attachments'][0]);

			if ( !empty($attachments) ) {
				$file_sizes = array();
				foreach ($attachments as $key => $attachment_id) {
					$attachment = get_attached_file( $attachment_id );
					$file_sizes []= filesize($attachment);

					if ( $attachment_id > 0 ) {
						$media_files .= '<li>
							<a href="'. get_edit_post_link($attachment_id) .'" target="_blank">'. basename($attachment) .'</a>
							<div class="remove_btn dashicons dashicons-trash"></div>
							<input type="hidden" value="'. $attachment_id .'" name="aee_attachments[]" />
						</li>';
					}
				}
			}
		}


		// calc filesize
		if ( !empty($file_sizes) ) {
			$file_sizes = array_sum($file_sizes);
			if ( $file_sizes > 0 ) {
				$file_sizes = ($file_sizes / 1024) / 1024;
				$file_sizes = round($file_sizes,2);
				$file_sizes = 'All file sizes: <b>'. $file_sizes .'Mb</b>';
			}
		}

		// Email format
		$email_format = $all_meta['aee_format'][0];

		if( empty( $email_format ) ){
			$email_format = $template_datas['email_format'];
		}


		echo '<div class="hidden">'.
			'<input type="text" name="subject" value="'. $all_meta['aee_subject'][0] .'" placeholder="'. __('Subject', Auretto_Email_Editor::TEXTDOMAIN) .'" />
			<input type="hidden" name="post_title_placeholder" value="'. __('Template Name', Auretto_Email_Editor::TEXTDOMAIN) .'" />'.
		'</div>';

		if ( intval($_GET['post']) > 0 ) {
			echo '<input type="hidden" name="aee_actual_post_id" value="'. intval($_GET['post']) .'" />';
		}


		?>
		<div class="menu-section">
			<ul class="menu">
				<!--<li data-menu-item="send_email">
					<i class="material-icons">mail_outline</i>
					<span><?php _e('Send a test email', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
				</li>-->
				<li data-menu-item="files_attachments" class="active">
					<i class="material-icons">attach_file</i>
					<span><?php _e('Attachments', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
				</li>
				<li data-menu-item="sender_settings">
					<i class="material-icons">settings</i>
					<span><?php _e('Sender settings', Auretto_Email_Editor::TEXTDOMAIN); ?></li></span>
				<li data-menu-item="template_settings">
					<i class="material-icons">description</i>
					<span><?php _e('Template', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
				</li>
				<li data-menu-item="sent_emails_log">
					<i class="material-icons">history</i>
					<span><?php _e('Sent E-mails Log', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
				</li>
			</ul>
		</div>
		<div class="content-wrapper <?php echo $is_woocommerce_active_class; ?> ">

			<!--<div class="content-section send_email">
				<h4><?php _e('Send a test E-mail', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>

				<?php require_once $this->aee->getPluginDirPath() .'/includes/email_editor_settings_metabox_parts/send_email.php'; ?>
			</div>-->

			<div class="content-section files_attachments active">
				<h4><?php _e('Files / Attachments', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>

				<?php require_once $this->aee->getPluginDirPath() .'/includes/email_editor_settings_metabox_parts/files_attachments.php'; ?>
			</div>

			<div class="content-section sender_settings">
				<h4><?php _e('Sender Setting', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>

				<?php require_once $this->aee->getPluginDirPath() .'/includes/email_editor_settings_metabox_parts/sender_settings.php'; ?>
			</div>

			<div class="content-section template_settings">
				<h4><?php _e('Email Format', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>
				<div class="radio-form">
					<input type="radio" value="plain" name="aee_format" id="aee_format_plain" <?php checked( $email_format, 'plain' ); ?> />
					<label for="aee_format_plain"><?php _e('Plain text', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
				</div>
				<div class="radio-form">
					<input type="radio" value="html" name="aee_format" id="aee_format_html" <?php checked( $email_format, 'html' ); ?> />
					<label for="aee_format_html"><?php _e('HTML template', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
				</div>
				<div class="radio-form">
					<input type="radio" value="wc" name="aee_format" id="aee_format_wc" <?php checked( $email_format, 'wc' ); ?> />
					<label for="aee_format_wc"><?php _e('WooCommerce template', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
				</div>
				<div class="clearfix"></div>
				<hr>

				<h4><?php _e('Template', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>
				<label for="email_template_url"><?php _e('Template URL:', Auretto_Email_Editor::TEXTDOMAIN); ?></label>
				<input type="text" value="<?php echo $all_meta['aee_email_template_url'][0]; ?>" name="email_template_url" id="email_template_url" placeholder="<?php echo $global_settings_email_template_url; ?>" />
				<div class="clearfix"></div>
			</div>

			<div class="content-section sent_emails_log">
				<h4><?php _e('Sent E-mails Log', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>

				<?php require_once $this->aee->getPluginDirPath() .'/includes/email_editor_settings_metabox_parts/sent_emails_log.php'; ?>
			</div>

			<?php wp_nonce_field( Auretto_Email_Editor::MAIN_METABOX_NAME .'-action-'. intval($post->ID), Auretto_Email_Editor::MAIN_METABOX_NAME .'-field-'. intval($post->ID)); ?>
		</div>
		<?php
	}

}
