<?php

/***************************/
/*     BASIC FUNCTIONS     */
/***************************/

class AurettoEmailFunctions {

	public function __construct($aee) {
		$this->aee = $aee;
	}


	// WooCommerce Email Template
	// https://hotexamples.com/examples/-/WC_Email/style_inline/php-wc_email-style_inline-method-examples.html
	function wc_email_template($subject = "", $message = "") {
		$mailer = WC()->mailer();
		$email = new WC_Email();
		return apply_filters('woocommerce_mail_content', $email->style_inline($mailer->wrap_message($subject, $message)));
	}


	// Check: WooCommerce plugin in active
	function is_woocommerce_active() {
		return (bool) is_plugin_active('woocommerce/woocommerce.php');
	}


	// Get Attachment ID By URL
	function get_attachment_id_by_url($media_file_url = "") {
		if ( empty($media_file_url) ) { return FALSE; }
		global $wpdb;

		$query = "SELECT ID FROM {$wpdb->posts} WHERE guid = '{$media_file_url}' ";
		$id = $wpdb->get_var($query);
		if ( intval($id) > 0 ) { return intval($id); }

		return FALSE;
	}


	// Check: SMTP is completely filled
	function smtp_is_filled() {
		$template_settings = get_option('aee_email_template_settings');
		$smtp_array = array(	'smtp_host' => "",
													'smtp_port' => "",
													'smtp_user' => "",
													'smtp_pass' => "",
													'smtp_secure' => "",
													'smtp_from' => "",
													'smtp_name' => "",
												);

		foreach ($smtp_array as $key => $value) {
			if ( isset($template_settings[$key]) && !empty($template_settings[$key]) ) {  }
			else { return FALSE; }
		}

		return TRUE;
	}


	// Check: SMTP is enabled
	function smtp_is_enabled() {
		$template_settings = get_option('aee_email_template_settings');

		if ( isset($template_settings['smtp_enabled']) && (intval($template_settings['smtp_enabled']) == 1) ) { return TRUE; }
		return FALSE;
	}


	// Get Main CC Emails from option --> Return: Array || FALSE
	function get_main_cc_emails() {
		$cc_emails_array = get_option('aee_email_template_settings');

		if ( isset($cc_emails_array['cc_emails']) ) {
			if ( is_array($cc_emails_array['cc_emails']) && !empty($cc_emails_array['cc_emails']) ) {
				return $cc_emails_array['cc_emails'];
			}
		}
		return FALSE;
	}


	// Get CC Emails from Main Settings --> Return: Array || FALSE
	function get_cc_emails_from_main_settings() {
		$cc_emails_array = get_option('aee_email_template_settings');

		if ( isset($cc_emails_array['cc_emails']) ) {
			if ( is_array($cc_emails_array['cc_emails']) && !empty($cc_emails_array['cc_emails']) ) {
				return $cc_emails_array['cc_emails'];
			}
		}
		return FALSE;
	}


	// Get CC Emails from meta --> Return: Array || FALSE
	function get_cc_emails_by_post_id($post_id = "") {
		if ( $post_id > 0 ) {  }
		else { return FALSE; }

		$cc_emails_array = get_post_meta($post_id, 'aee_cc_emails', TRUE);

		if ( is_array($cc_emails_array) && !empty($cc_emails_array) ) {
			return $cc_emails_array;
		}
		return FALSE;
	}


	// Configuration for SMTP
	function smtp_mailer_config(PHPMailer $mailer) {
		$global_settings_array = get_option('aee_email_template_settings');
		$datas = array( 'smtp_host', 'smtp_port', 'smtp_user', 'smtp_pass', 'smtp_secure', 'smtp_from', 'smtp_name' );

		if ( $this->aee->functions->smtp_is_enabled() === TRUE ) {
			if ( $this->aee->functions->smtp_is_filled() === TRUE ) {

				$mailer->isSMTP();
				$mailer->Host = $global_settings_array['smtp_host']; // your SMTP server
				$mailer->SMTPAuth = true; // Force it to use Username and Password to authenticate
				$mailer->Port = $global_settings_array['smtp_port'];
				$mailer->Username = $global_settings_array['smtp_user'];
				$mailer->Password = $global_settings_array['smtp_pass'];
				$mailer->SMTPSecure = $global_settings_array['smtp_secure']; // Choose SSL or TLS, if necessary for your server
				$mailer->From = $global_settings_array['smtp_from']; // you@yourdomail.com
				$mailer->FromName = $global_settings_array['smtp_name']; // Your Name
				$mailer->SMTPDebug = 0; // write 0 if you don't want to see client/server communication in page
				$mailer->CharSet  = "utf-8";

			} else {
				die; /* <-- Before send the email */
			}
		}
	}


	function short_text($text = "", $limit = "") {
		if (empty($limit)) { $chars_limit = 50; }
		else { $chars_limit = $limit; }

		$chars_text = strlen($text);
		$text = $text." ";
		$text = substr($text,0,$chars_limit);
		$text = substr($text,0,strrpos($text,' '));

		if ($chars_text > $chars_limit) {
			$text = $text ."...";
		}
		return $text;
	}


	function get_template_content($post_id = "") {
		if ( intval($post_id) > 0 ) {  }
		else { return ""; }

		$file_path = $this->aee->functions->get_email_template($post_id, 'email_template_url');
		$raw_file_content = file_get_contents($file_path);

		if ( !empty($raw_file_content) && $raw_file_content !== FALSE ) { return $raw_file_content; }
		return "";
	}


	function replace_words($raw_content = "", $replaceable_words_array = "") {
		if ( !empty($raw_content) && !is_string($raw_content) ) { return ""; }
		if ( isset($replaceable_words_array['replaceable_content_words']) && is_array($replaceable_words_array['replaceable_content_words']) ) {  }
		else { return $raw_content; }

		return str_replace(array_keys($replaceable_words_array['replaceable_content_words']), $replaceable_words_array['replaceable_content_words'], $raw_content);
	}


	function get_template_file_path_by_url($original_template_url = "") {
		if ( empty($original_template_url) ) { return ""; }

		$template_dir = explode('/', $this->aee->getTemplateDirPath());
		$template_dir = array_values(array_filter(array_unique($template_dir)));
		$template_dir = array_slice($template_dir, -1);
		$template_dir = implode('/', $template_dir) .'/';

		$template_file = substr($original_template_url, strpos($original_template_url, $template_dir) + strlen($template_dir) );

		if ( file_exists($this->aee->getTemplateDirPath(). $template_file) ) {
			return $this->aee->getTemplateDirPath(). $template_file;
		}
		return "";
	}


	function email_template_url_validation($original_template_url = "") {
		if ( empty($original_template_url) ) { return ""; }

		$template_dir = explode('/', $this->aee->getTemplateDirPath());
		$template_dir = array_values(array_filter(array_unique($template_dir)));
		$template_dir = array_slice($template_dir, -1);
		$template_dir = implode('/', $template_dir) .'/';

		$template_file = substr($original_template_url, strpos($original_template_url, $template_dir) + strlen($template_dir) );

		if ( file_exists($this->aee->getTemplateDirPath(). $template_file) ) {
			return TRUE;
		}
		return FALSE;
	}


	function get_default_sender_name() {
		$admin_email = get_option("admin_email");
		$admin_name = "";

		if ( !empty($admin_email) ) {
			$admin_data = get_user_by( 'email', $admin_email );

			if ( !empty($admin_data->first_name) && !empty($admin_data->last_name) ) {
				$admin_name = $admin_data->first_name .' '. $admin_data->last_name;

			} else if ( !empty($admin_data->display_name) ) {
				$admin_name = $admin_data->display_name;
			}
		}
		return $admin_name;
	}


	function get_default_sender_email() {
		return get_option("admin_email");
	}


	function get_the_content_by_id($post_id = "", $format = false) {
		$page_data = get_page($post_id);
		if ( $page_data ) {
			if ( $format === TRUE ) {
				return apply_filters('the_content',$page_data->post_content);
			} else {
				return $page_data->post_content;
			}
		}
		return false;
	}


	// Get post ID by post slug
	function get_post_id_by_slug($page_slug = "", $output = OBJECT) {
		global $wpdb;
		$post_id = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $page_slug, Auretto_Email_Editor::MAIN_CPT_NAME ) );
		return intval($post_id);
	}


	// Returns the post slug by ID
	function get_email_template_name_by_id($post_id = ""){
		if (empty($post_id)) { return ""; }
		return get_post_field( 'post_name', $post_id );
	}


	// Get email template datas by slug or ID
	function get_email_template($post_slug_or_id = "", $response = "all") {
		if (empty($post_slug_or_id)) { return ""; }
		$result = "";

		if ( is_string($post_slug_or_id) ) 				{ $post_id = $this->aee->functions->get_post_id_by_slug($post_slug_or_id); }
		else if ( is_numeric($post_slug_or_id) ) 	{ $post_id = (int) $post_slug_or_id; }

		if ( $post_id > 0 ) {
			$global_settings_array = get_option('aee_email_template_settings');
			$all_meta = get_post_meta( $post_id );


			// Pre filter
			if ( isset($all_meta['aee_attachments'][0]) && !empty($all_meta['aee_attachments'][0]) ) {
				$all_meta['aee_attachments'][0] = unserialize($all_meta['aee_attachments'][0]);
			} else { $all_meta['aee_attachments'][0] = ""; }

			if ( !isset($global_settings_array['sender_name']) || empty($global_settings_array['sender_name']) ) {
				$global_settings_array['sender_name'] = $this->aee->functions->get_default_sender_name();
			}
			if ( !isset($global_settings_array['sender_email']) || empty($global_settings_array['sender_email']) ) {
				$global_settings_array['sender_email'] = $this->aee->functions->get_default_sender_email();
			}


			/* Default template settings */

			$email_template_url = $global_settings_array['email_template_url'];
			$send_mode = $global_settings_array['email_format'];

			$smtp_host = $global_settings_array['smtp_host'];
			$smtp_port = $global_settings_array['smtp_port'];
			$smtp_user = $global_settings_array['smtp_user'];
			$smtp_pass = $global_settings_array['smtp_pass'];
			$smtp_secure = $global_settings_array['smtp_secure'];
			$smtp_from = $global_settings_array['smtp_from'];
			$smtp_name = $global_settings_array['smtp_name'];

			$sender_name = $global_settings_array['sender_name'];
			$sender_email = $global_settings_array['sender_email'];

			/* --- */


			/* Post settings (overwrites the original values) */

			if ( isset($all_meta['aee_email_template_url'][0]) && !empty($all_meta['aee_email_template_url'][0]) ) {
				$email_template_url = $all_meta['aee_email_template_url'][0];
			}
			if ( isset($all_meta['aee_sender_name'][0]) && !empty($all_meta['aee_sender_name'][0]) ) {
				$sender_name = $all_meta['aee_sender_name'][0];
			}
			if ( isset($all_meta['aee_sender_email'][0]) && !empty($all_meta['aee_sender_email'][0]) ) {
				$sender_email = $all_meta['aee_sender_email'][0];
			}

			/* Template */
			$email_format = '';

			if( !empty( $all_meta['aee_format'] ) ){
				$email_format = $all_meta['aee_format'][0];
			} else if( !empty( $global_settings_array['email_format'] ) ) {
				$email_format = $global_settings_array['email_format'];
			} else {
				$email_format = 'plain';
			}

			if( $email_format == 'wc' && ( $this->aee->functions->is_woocommerce_active() !== TRUE ) ){
				$email_format = 'html';
			}

			$subject = $all_meta['aee_subject'][0];
			$msg = $this->aee->functions->get_the_content_by_id($post_id, TRUE);

			$attachments = $all_meta['aee_attachments'][0];
			if ( !empty($attachments) ) {
				$attachments_temp = array();
				foreach ($attachments as $key => $media_id) {
					$attachments_temp []= array(	'media_id' => $media_id,
																				'url' => wp_get_attachment_url($media_id),
																				'path' => get_attached_file($media_id),
																			);
				}
				$attachments = serialize($attachments_temp);
			}

			/* --- */

			$result = array(
				"subject" 			=> $subject,
				"msg" 					=> $msg,
				"send_mode" 		=> $send_mode,
				"attachments" 	=> $attachments,

				"email_template_url" 	=> $this->aee->functions->get_template_file_path_by_url($email_template_url),
				"email_format"			=> $email_format,

				"smtp_enabled" 	=> $this->aee->functions->smtp_is_enabled(),
				"smtp_host" 		=> $smtp_host,
				"smtp_port" 		=> $smtp_port,
				"smtp_user" 		=> $smtp_user,
				"smtp_pass" 		=> $smtp_pass,
				"smtp_secure" 	=> $smtp_secure,
				"smtp_from" 		=> $smtp_from,
				"smtp_name" 		=> $smtp_name,

				"sender_email"	=> $sender_email,
				"sender_name" 	=> $sender_name,
			);


			// Merge & Add CC input fields
			$cc_emails_array_temp = array();
			$cc_emails_option_array = $this->aee->functions->get_main_cc_emails();
			$cc_emails_meta_array = $this->aee->functions->get_cc_emails_by_post_id($post_id);

			if ( $cc_emails_option_array !== FALSE ) {
				foreach ($cc_emails_option_array as $email => $name) { $cc_emails_array_temp[$email] = $name; }
				$result['main_cc_emails'] = $cc_emails_option_array;
			}
			if ( $cc_emails_meta_array !== FALSE ) {
				foreach ($cc_emails_meta_array as $email => $name) { $cc_emails_array_temp[$email] = $name; }
				$result['postmeta_cc_emails'] = $cc_emails_meta_array;
			}

			// Merged CC Emails & Names
			if ( !empty($cc_emails_array_temp) ) {
				$result['cc_emails'] = $cc_emails_array_temp;
			}
			// ---


			if ( !empty($response) && ($response != 'all') && isset($result[$response]) ) { return $result[$response]; }
			else if ( !empty($response) && ($response != 'all') && !isset($result[$response]) ) { return ""; }
		}
		return $result;
	}

}
