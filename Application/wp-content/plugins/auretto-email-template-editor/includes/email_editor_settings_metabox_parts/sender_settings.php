<input type="text" value="<?php echo $all_meta['aee_sender_name'][0]; ?>" name="sender_name" placeholder="<?php _e('Sender Name', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
<input type="email" value="<?php echo $all_meta['aee_sender_email'][0]; ?>" name="sender_email" placeholder="<?php _e('Sender Email', Auretto_Email_Editor::TEXTDOMAIN); ?>" />

<div class="cc-input-fields">
	<h4><?php _e('Cc Emails', Auretto_Email_Editor::TEXTDOMAIN); ?></h4>

	<?php
	if ( $cc_emails_array !== FALSE ) {
		foreach ($cc_emails_array as $cc_email => $cc_name) {
			?>
			<div class="cc-input-field">
				<input type="text" value="<?php echo $cc_name; ?>" name="cc_names[]" placeholder="<?php _e('Name for cc', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
				<input type="email" value="<?php echo $cc_email; ?>" name="cc_emails[]" placeholder="<?php _e('Email for cc', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
				<div class="remove_btn dashicons dashicons-trash"></div>
			</div>
			<?php
		}
	} else {
		?>
		<div class="cc-input-field">
			<input type="text" value="" name="cc_names[]" placeholder="<?php _e('Name for cc', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
			<input type="email" value="" name="cc_emails[]" placeholder="<?php _e('Email for cc', Auretto_Email_Editor::TEXTDOMAIN); ?>" />
			<div class="remove_btn dashicons dashicons-trash"></div>
		</div>
		<?php
	}
	?>

	<button class="cc-add-more-btn button button-primary button-large">
		<div class="dashicons dashicons-plus"></div>
		<span><?php _e('Add new input fields', Auretto_Email_Editor::TEXTDOMAIN); ?></span>
	</button>

</div>
