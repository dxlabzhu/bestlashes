<?php

/* Registration verify success */
function bl_open_user_popup_success_verify_hash(){
	ob_start(); ?>

	<div class="popup-wrapper verify-hash success-verification">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-registration-verify-success-title' ); ?></h3>
	    		<p><?php echo bl_get_option_lang( 'bl-popup-registration-verify-success-content' ); ?></p>

				<div class="buttons">
					<a class="rounded-button w-button login-btn" href="<?php echo get_permalink( BL_PAGE_LOGIN ); ?>">
						<?php _e( 'Login', 'bl' ); ?>
						<span class="chart-button-icon">→</span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" charset="utf-8">
		jQuery(window).load(function() {
			showPopup("verify-hash.success-verification");
		});
	</script>

	<?php echo ob_get_clean();
}


/* Registration verify fail */
function bl_open_user_popup_failed_verify_hash(){
	ob_start(); ?>

	<div class="popup-wrapper verify-hash failed-verification">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-registration-verify-fail-title' ); ?></h3>
        		<p><?php echo bl_get_option_lang( 'bl-popup-registration-verify-fail-content' ); ?></p>
			</div>
		</div>
	</div>

	<script type="text/javascript" charset="utf-8">
		jQuery(window).load(function() {
			showPopup("verify-hash.failed-verification");
		});
	</script>

	<?php echo ob_get_clean();
}


/* Forgotten password verify fail */
function bl_open_user_popup_failed_verify_password_change_hash(){
	ob_start(); ?>

	<div class="popup-wrapper verify-hash failed-password-change-verification">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-forgotten-password-password-change-fail-title' ); ?></h3>
        		<p><?php echo bl_get_option_lang( 'bl-popup-forgotten-password-password-change-fail-content' ); ?></p>
			</div>
		</div>
	</div>

	<script type="text/javascript" charset="utf-8">
		jQuery(window).load(function() {
			showPopup("verify-hash.failed-password-change-verification");
		});
	</script>

	<?php echo ob_get_clean();
}


/* Newsletter subscription verify success */
function bl_open_newsletter_subscription_popup_success_verify_hash(){
	ob_start(); ?>

	<div class="popup-wrapper verify-hash newsletter-success-verification">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-newsletter-subscription-verify-success-title' ); ?></h3>
	    		<p><?php echo bl_get_option_lang( 'bl-popup-newsletter-subscription-verify-success-content' ); ?></p>
			</div>
		</div>
	</div>

	<script type="text/javascript" charset="utf-8">
		jQuery(window).load(function() {
			showPopup("verify-hash.newsletter-success-verification");
		});
	</script>

	<?php echo ob_get_clean();
}


/* Newsletter subscription verify fail */
function bl_open_newsletter_subscription_popup_failed_verify_hash(){
	ob_start(); ?>

	<div class="popup-wrapper verify-hash failed-newsletter-verification">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-newsletter-subscription-verify-fail-title' ); ?></h3>
        		<p><?php echo bl_get_option_lang( 'bl-popup-newsletter-subscription-verify-fail-content' ); ?></p>
			</div>
		</div>
	</div>

	<script type="text/javascript" charset="utf-8">
		jQuery(window).load(function() {
			showPopup("verify-hash.failed-newsletter-verification");
		});
	</script>

	<?php echo ob_get_clean();
}


/* Newsletter unsubscription verify success */
function bl_open_newsletter_unsubscription_popup_success_verify_hash(){
	ob_start(); ?>

	<div class="popup-wrapper verify-hash newsletter-success-verification">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-newsletter-unsubscription-verify-success-title' ); ?></h3>
	    		<p><?php echo bl_get_option_lang( 'bl-popup-newsletter-unsubscription-verify-success-content' ); ?></p>
			</div>
		</div>
	</div>

	<script type="text/javascript" charset="utf-8">
		jQuery(window).load(function() {
			showPopup("verify-hash.newsletter-success-verification");
		});
	</script>

	<?php echo ob_get_clean();
}


/* Newsletter unsubscription verify fail */
function bl_open_newsletter_unsubscription_popup_failed_verify_hash(){
	ob_start(); ?>

	<div class="popup-wrapper verify-hash failed-newsletter-verification">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-newsletter-unsubscription-verify-fail-title' ); ?></h3>
        		<p><?php echo bl_get_option_lang( 'bl-popup-newsletter-unsubscription-verify-fail-content' ); ?></p>
			</div>
		</div>
	</div>

	<script type="text/javascript" charset="utf-8">
		jQuery(window).load(function() {
			showPopup("verify-hash.failed-newsletter-verification");
		});
	</script>

	<?php echo ob_get_clean();
}


/* Delete user - delete confirmation */
function bl_open_user_popup_delete_confirmation(){
	ob_start(); ?>

	<div class="popup-wrapper delete-account delete-user-delete-confirmation">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-delete-user-confirmation-title' ); ?></h3>
        		<p><?php echo bl_get_option_lang( 'bl-popup-delete-user-confirmation-content' ); ?></p>
        		<div class="buttons two-buttons">
        			<a class="rounded-button w-button delete-user-confirm-no" href="#">
        				<span class="chart-button-icon">←</span>
						<?php _e( 'No', 'bl' ); ?>
					</a>
					<a class="rounded-button w-button delete-user-confirm-yes" href="#">
						<?php _e( 'Yes', 'bl' ); ?>
						<span class="chart-button-icon">→</span>
					</a>
        		</div>
			</div>
		</div>
	</div>

	<?php echo ob_get_clean();
}


/* Delete user - delete success */
function bl_open_user_popup_user_deleted(){
	ob_start(); ?>

	<div class="popup-wrapper delete-account delete-user-account-success">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-delete-user-success-title' ); ?></h3>
        		<p><?php echo bl_get_option_lang( 'bl-popup-delete-user-success-content' ); ?></p>
			</div>
		</div>
	</div>

	<script type="text/javascript" charset="utf-8">
		jQuery(window).load(function() {
			showPopup("delete-account.delete-user-account-success");
		});
	</script>

	<?php echo ob_get_clean();
}


/* Add to cart video popup */
function bl_open_add_to_cart_video_popup(){
	ob_start(); ?>

	<div class="popup-wrapper add-to-cart-video-popup">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
        		<div class="popup-video-container"></div>
			</div>
		</div>
	</div>

	<?php echo ob_get_clean();
}


function bl_search_popup(){
	ob_start(); ?>

	<div class="popup-wrapper search-popup">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				<div class="bl-logo"></div>
				<h3><?php echo bl_get_option_lang( 'bl-popup-search-title' ); ?></h3>
				<p><?php echo bl_get_option_lang( 'bl-popup-search-content' ); ?></p>
				<form action="<?php echo get_permalink( BL_PAGE_WEBSHOP ); ?>" method="get" class="search-form" accept-charset="utf-8">
					<div class="search-container">
						<input type="text" class="footer-newsletter-field w-input search-input" name="<?php _e('search', 'bl') ?>" placeholder="<?php _e('Search product', 'bl'); ?>" value="<?php echo ( !empty( $_GET[ __('search', 'bl') ] ) ? $_GET[ __('search', 'bl') ] : '' ); ?>">
						<a href="#" class="round-button w-button search-submit">→</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<?php echo ob_get_clean();
}