<div class="inside-box">
	<div class="content-wrapper">
		<div class="title-wrapper">
			<h3><?php _e('Invoice preview', 'bl'); ?></h3>
		</div>
		<div class="container">
			<div class="row">

				<?php if( !empty( $invoice_data['elado'] ) ){ ?>

					<div class="col flex-3">
						<img src="http://bestlashes.test/wp-content/uploads/2018/11/BestLashes-logo.png" alt="">
					</div>
					<div class="col flex-3"></div>
					<div class="col flex-6">
						<h4><?php _e('Bank information', 'bl'); ?></h4>
						
						<div class="label">
							<strong><?php _e('Bank name', 'bl'); ?>:</strong> <?php echo $invoice_data['elado']['bank']; ?>
						</div>
						<div class="label">
							<strong><?php _e('Bank account number', 'bl'); ?>:</strong> <?php echo $invoice_data['elado']['bankszamlaszam']; ?>
						</div>
					</div>

				<?php } ?>

			</div>
			<div class="row">

				<?php if( !empty( $invoice_data['vevo'] ) ){ ?>

					<div class="col flex-6">
						<h4><?php _e('Billing information', 'bl'); ?></h4>

						<div class="label">
							<strong><?php echo $invoice_data['vevo']['nev']; ?></strong>
						</div>
						<div class="label">
							<strong><?php echo $invoice_data['vevo']['irsz']; ?> <?php echo $invoice_data['vevo']['telepules']; ?></strong>
						</div>
						<div class="label">
							<strong><?php echo $invoice_data['vevo']['cim']; ?></strong>
						</div>
						<div class="label">
							<strong><?php echo $invoice_data['vevo']['email']; ?></strong>
						</div>
						<div class="label">
							<strong><?php echo $invoice_data['vevo']['adoszam']; ?></strong>
						</div>
					</div>
					
				<?php } ?>

				<?php if( !empty( $invoice_data['fejlec'] ) ){ ?>

					<div class="col flex-6">
						<h4><?php _e('Payment information', 'bl'); ?></h4>

						<div class="label">
							<strong><?php _e('Payment method', 'bl'); ?>:</strong> <?php echo $invoice_data['fejlec']['fizmod']; ?>
						</div>
						<div class="label">
							<strong><?php _e('Fulfilment date', 'bl'); ?>:</strong> <?php echo $invoice_data['fejlec']['teljesitesDatum']; ?>
						</div>
						<div class="label">
							<strong><?php _e('Date of issue', 'bl'); ?>:</strong> <?php echo $invoice_data['fejlec']['keltDatum']; ?>
						</div>
						<div class="label">
							<strong><?php _e('Payment deadline', 'bl'); ?>:</strong> <?php echo $invoice_data['fejlec']['fizetesiHataridoDatum']; ?>
						</div>
					</div>
					
				<?php } ?>

			</div>
			<div class="row">
				<div class="col flex-1">

					<?php if( !empty( $invoice_data['tetelek'] ) ){
						$total_net_price = 0;
						$total_vat_price = 0;
						$total_gross_price = 0; ?>

						<table>
							<thead>
								<tr>
									<th><?php _e( 'Title', 'bl' ); ?></th>
									<th><?php _e( 'Qua', 'bl' ); ?></th>
									<th><?php _e( 'Unit price', 'bl' ); ?></th>
									<th><?php _e( 'Net price', 'bl' ); ?></th>
									<th><?php _e( 'VAT', 'bl' ); ?></th>
									<th><?php _e( 'Price of VAT', 'bl' ); ?></th>
									<th><?php _e( 'Gross price', 'bl' ); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ( $invoice_data['tetelek']['tetel'] as $tetel ) {
									$total_net_price += $tetel['nettoErtek'];
									$total_vat_price += $tetel['afaErtek'];
									$total_gross_price += $tetel['bruttoErtek']; ?>

									<tr>
										<td><?php echo $tetel['megnevezes']; ?></td>
										<td><?php echo $tetel['mennyiseg']; ?> <?php echo $tetel['mennyisegiEgyseg'] ?></td>
										<td><?php echo $tetel['nettoEgysegar']; ?></td>
										<td><?php echo $tetel['nettoErtek']; ?></td>
										<td><?php echo $tetel['afakulcs']; ?>%</td>
										<td><?php echo $tetel['afaErtek']; ?></td>
										<td><?php echo $tetel['bruttoErtek']; ?></td>
									</tr>

								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th><?php _e('Total', 'bl'); ?></th>
									<th colspan="3"><?php echo $total_net_price; ?></th>
									<th colspan="2"><?php echo $total_vat_price; ?></th>
									<th><?php echo $total_gross_price; ?></th>
								</tr>
							</tfoot>
						</table>

					<?php } ?>

				</div>
			</div>
			<div class="row">
				<div class="col flex-1 button-container">
					<button class="submit-invoice"><?php _e( 'Generate invoice', 'bl' ); ?></button>
				</div>
			</div>
		</div>
	</div>
</div>