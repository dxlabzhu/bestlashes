<div class="my-account-delete-account-content">
	<form action="" method="post" class="delete-account" enctype="multipart/form-data">
		<p><?php _e('After you delete your account, you can not log in whit this email address.', 'bl'); ?></p>
		<div class="pull-right">
			<button type="submit" class="woocommerce-Button button" name="delete-account-submit" value="<?php _e('Delete account', 'bl'); ?>">
				<?php _e('Delete account', 'bl'); ?><span class="chart-button-icon">→</span>
			</button>
			<input type="hidden" name="bl_delete_account" value="1">
			<?php wp_nonce_field('bl_delete_account_nonce', 'user_delete_account_from_frontend'); ?>
		</div>
	</form>
</div>