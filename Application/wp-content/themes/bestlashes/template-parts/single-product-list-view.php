<div class="<?php echo $container_classes; ?>">
	<a href="<?php echo get_permalink( $prod_id ); ?>" class="productlist-item w-inline-block">
		<?php if( !empty( $label_text ) ){ ?>
			<div class="productlist-stamp"><?php echo $label_text; ?></div>
		<?php } ?>
		<div class="productlist-image-container">
			<?php echo $image; ?>
		</div>
		<div class="productlist-detail-block">
			<div class="productlist-description-block">
				<div class="productlist-text"><?php echo get_the_title( $prod_id ); ?></div>
			</div>
			<div class="productlist-price-block">
				<div class="product-price">
					<?php if( $_product->is_on_sale() ) { ?>
						<span class="new-price"><?php echo $prices['gross_price_formatted']; ?><br></span>
						<span class="old-price"><?php echo $prices['gross_regular_price_formatted']; ?></span>
					<?php } else {
						echo $prices['gross_price_formatted'];
					} ?>
				</div>
			</div>
		</div>
	</a>
</div>