<?php
$users_training_courses = array();
$training_courses_ids = array();
$training_courses = array();

// Get users orders
$args['posts_per_page'] = -1;
$args['post_type'] = wc_get_order_types();
$args['post_status'] = array_keys( wc_get_order_statuses() );
$args['meta_query'] = array();
$args['meta_query'][] = array(
	'key' => '_customer_user',
	'value' => get_current_user_id()
);
$args['fields'] = 'ids';

$user_orders_loop = new WP_Query( $args );

if( $user_orders_loop->have_posts() ){
	while ( $user_orders_loop->have_posts() ) {
		$order_id = $user_orders_loop->next_post();
		$order = wc_get_order( $order_id );

		foreach ($order->get_items() as $item_id => $item_data) {
			$product_id = $item_data['product_id'];
			$is_training_course = get_post_meta( $product_id, 'training_course', true );

			if( $is_training_course == '1' ){
				$users_training_courses[] = array(
					'course_id' => $product_id,
					'order_id' => $order_id
				);

				$training_courses_ids[] = $product_id;
			}
		}
	}
}


if( !empty( $training_courses_ids ) ){
	foreach ( $training_courses_ids as $post_id ) {
		$training_course_timestamp = get_post_meta( $post_id, 'training_course_date_time', true );

		if( !empty( $training_course_timestamp ) ){
			$training_course_formatted_date_time = date_i18n( 'Y.m.d. H:i', $training_course_timestamp );
			$training_course_key = date_i18n( 'Y-m', $training_course_timestamp );
			$city = get_post_meta( $post_id, 'training_course_city', true );
			$_product = wc_get_product( $post_id );

			$training_courses[ $training_course_key ][] = array(
				'ID' => $post_id,
				'title' => get_the_title( $post_id ),
				'date-day' => date_i18n( 'd', $training_course_timestamp ),
				'date-weekday' => date_i18n( 'l', $training_course_timestamp ),
				'date-time' => date_i18n( 'H:i', $training_course_timestamp ),
				'city' => $city,
				'address' => get_post_meta( $post_id, 'training_course_address', true ),
				'prices' => bl_get_product_prices( $post_id ),
				'is_on_sale' => $_product->is_on_sale(),
				'stock_quantity' => $_product->get_stock_quantity(),
				'content' => get_the_content_by_id( $post_id ),
				'timestamp' => $training_course_timestamp
			);
		}
	}
} 


// Order courses
if( !empty( $training_courses ) ){
	ksort( $training_courses );

	foreach ( $training_courses as $key => $courses ) {
		$ordered_courses = $courses;
		usort( $ordered_courses, 'bl_order_training_courses' );
		$training_courses[ $key ] = $ordered_courses;
	}
} ?>

<div id="courses" class="my-training-courses">
	<div class="table-content">
		<?php if( !empty( $training_courses ) ){
			foreach ( $training_courses as $date => $courses ) {
				$month_start = $date . '-01 00:00';
				$month_start_date = DateTime::createFromFormat( 'Y-m-d H:i', $month_start );
				$month_start_timestamp = $month_start_date->format('U');
				$month_name = date_i18n( 'F', $month_start_timestamp );
				$year = date_i18n( 'Y', $month_start_timestamp ); ?>

				<div class="table-header big">
					<div class="table-column _100percent">
						<div class="heading-03 nopadding">
							<?php echo $month_name; ?> 
							<span class="heading-span">| <?php echo $year; ?></span>
						</div>
					</div>
				</div>

				<?php if( !empty( $courses ) ){
					foreach ( $courses as $course ) { ?>
						
						<div class="table-row big">
							<div class="table-column _50percent">
								<div class="table-column _40percent training">
									<div class="circle-counter">
										<?php echo $course['date-day']; ?><br>
										<span class="circle-counter-span"><?php echo $course['date-weekday']; ?></span>
									</div>
								</div>
								<div class="table-column _60percent">
									<div>
										<?php echo $course['city'] . ' / ' . $course['date-time']; ?><br>
										<span class="table-small-text"><?php echo $course['address']; ?></span>
									</div>
								</div>
							</div>
							<div class="table-column _50percent">
								<div class="table-link"><?php echo $course['title']; ?></div>
							</div>
						</div>

					<?php }
				} ?>

			<?php }
		} else { ?>

		<?php } ?>
		
	</div>
</div>