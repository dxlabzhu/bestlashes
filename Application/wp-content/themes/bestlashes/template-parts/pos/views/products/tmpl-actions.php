<div class="input-group">
    <div class="table-column _10percent center">
        <span class="input-group-addon">
            <i class="icon-search"></i>
        </span>
    </div>
    <div class="table-column _70percent">
        <input class="form-control" tabindex="1" type="search" placeholder="<?php /* translators: woocommerce */ _e( 'Search products', 'woocommerce' ); ?>">
        <span class="input-group-addon clear">
            <i data-action="clear" class="icon-times-circle icon-lg"></i>
        </span>
        <span class="input-group-btn">
            <a class="btn btn-secondary" href="#" data-action="sync"><i class="icon-refresh"></i></a>
        </span>
    </div>
    <div class="table-column _20percent right">

    </div>
</div>