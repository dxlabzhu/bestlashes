<div class="table-wrapper invoice products bl-wrapper">
	<div class="table-content">
		<div class="table-row search">
			<div class="panel-header list-actions products-actions table-column _100percent"></div>
		</div>
	</div>
	<div class="table-content">
		<div class="list-header products-header table-header">
			<div class="table-column _10percent">
				<div class="img"></div>
			</div>
			<div class="table-column _30percent">
				<div class="title"><?php /* translators: woocommerce */ _e('Product', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _20percent">
				<div><?php /* translators: woocommerce */ _e('In stock', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _20percent">
				<div><?php /* translators: woocommerce */ _e('Regular price', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _20percent">
				<div><?php /* translators: woocommerce */ _e('Sale price', 'woocommerce'); ?></div>
			</div>
		</div>
	</div>
	<div class="table-content">
		<div class="panel-body list products-list list-striped"></div>
		<div class="panel-footer products-footer"></div>
	</div>
</div>