{{#is type 'variation'}}
    <div class="table-row-small-tansparent product bl-row">
        <div class="table-column _10percent">
            <div class="img"><img src="{{featured_src}}" title="#{{id}}"></div>
        </div>
        <div class="table-column _30percent">
            <div class="title">
                {{#each attributes}}
                    {{name}}: {{#if option}}{{option}}{{else}}{{#list options ', '}}{{this}}{{/list}}{{/if}} <span class="separation">|</span>
                {{/each}}
            </div>
        </div>
        <div class="table-column _20percent">
            <div>
                <input type="text" name="stock_quantity" data-label="<?php /* translators: woocommerce */ _e( 'Quantity', 'woocommerce' ); ?>" data-placement="bottom" data-numpad="quantity" class="form-control autogrow price-input w-input">
                <label class="small c-input c-checkbox" for="managing_stock[{{id}}]">
                    <input type="checkbox" name="managing_stock" id="managing_stock[{{id}}]">
                    <span class="c-indicator"></span>
                    <?php /* translators: woocommerce */ _e( 'Manage stock?', 'woocommerce' ); ?>
                </label>
            </div>
        </div>
        <div class="table-column _20percent">
            <div>
                {{#is type 'variable'}}
                    <span data-name="regular_price"></span>
                    <label class="small c-input c-checkbox" for="taxable[{{id}}]">
                        <input type="checkbox" name="taxable" id="taxable[{{id}}]">
                        <span class="c-indicator"></span>
                        <?php /* translators: woocommerce */ _e( 'Taxable', 'woocommerce' ); ?>
                    </label>
                {{else}}
                    <input type="text" name="regular_price" data-label="<?php /* translators: woocommerce */ _e('Regular price', 'woocommerce'); ?>" data-placement="bottom" data-numpad="amount" class="form-control autogrow price-input w-input">
                {{/is}}
            </div>
        </div>
        <div class="table-column _10percent">
            <div>
                {{#is type 'variable'}}
                    <span data-name="sale_price"></span>
                {{else}}
                    <input type="text" name="sale_price" data-label="<?php /* translators: woocommerce */ _e('Sale price', 'woocommerce'); ?>" data-placement="bottom" data-numpad="amount" class="form-control autogrow price-input w-input">
                    <label class="small c-input c-checkbox" for="on_sale[{{id}}]">
                        <input type="checkbox" name="on_sale" id="on_sale[{{id}}]">
                        <span class="c-indicator"></span>
                        <?php _e( 'On Sale?', 'woocommerce-pos-pro' ); ?>
                    </label>
                {{/is}}
            </div>
        </div>
        <div class="table-column _10percent"></div>
    </div>
{{else}}
    <div class="table-row product bl-row">
        <div class="table-column _10percent">
            <div class="img"><img src="{{featured_src}}" title="#{{id}}"></div>
        </div>
        <div class="table-column _30percent">
            <div class="title">
                <strong contenteditable="true">{{name}}</strong>
            </div>
        </div>
        <div class="table-column _20percent">
            <div>
                <input type="text" name="stock_quantity" data-label="<?php /* translators: woocommerce */ _e( 'Quantity', 'woocommerce' ); ?>" data-placement="bottom" data-numpad="quantity" class="form-control autogrow price-input w-input">
                <label class="small c-input c-checkbox" for="managing_stock[{{id}}]">
                    <input type="checkbox" name="managing_stock" id="managing_stock[{{id}}]">
                    <span class="c-indicator"></span>
                    <?php /* translators: woocommerce */ _e( 'Manage stock?', 'woocommerce' ); ?>
                </label>
            </div>
        </div>
        <div class="table-column _20percent">
            <div>
                {{#is type 'variable'}}
                    <span data-name="regular_price"></span>
                    <label class="small c-input c-checkbox" for="taxable[{{id}}]">
                        <input type="checkbox" name="taxable" id="taxable[{{id}}]">
                        <span class="c-indicator"></span>
                        <?php /* translators: woocommerce */ _e( 'Taxable', 'woocommerce' ); ?>
                    </label>
                {{else}}
                    <input type="text" name="regular_price" data-label="<?php /* translators: woocommerce */ _e('Regular price', 'woocommerce'); ?>" data-placement="bottom" data-numpad="amount" class="form-control autogrow price-input w-input">
                    <label class="small c-input c-checkbox" for="taxable[{{id}}]">
                        <input type="checkbox" name="taxable" id="taxable[{{id}}]">
                        <span class="c-indicator"></span>
                        <?php /* translators: woocommerce */ _e( 'Taxable', 'woocommerce' ); ?>
                    </label>
                {{/is}}
            </div>
        </div>
        <div class="table-column _10percent">
            <div>
                {{#is type 'variable'}}
                    <span data-name="sale_price"></span>
                {{else}}
                    <input type="text" name="sale_price" data-label="<?php /* translators: woocommerce */ _e('Sale price', 'woocommerce'); ?>" data-placement="bottom" data-numpad="amount" class="form-control autogrow price-input w-input">
                    <label class="small c-input c-checkbox" for="on_sale[{{id}}]">
                        <input type="checkbox" name="on_sale" id="on_sale[{{id}}]">
                        <span class="c-indicator"></span>
                        <?php _e( 'On Sale?', 'woocommerce-pos-pro' ); ?>
                    </label>
                {{/is}}   
            </div>
        </div>
        <div class="table-column _10percent">
            {{#with product_variations}}
                <small>
                    <a href="#" data-action="expand" class="expand-all round-button w-button">↓</a>
                    <a href="#" data-action="close" class="close-all round-button w-button">↑</a>
                </small>
            {{/with}}
        </div>
    </div>
{{/is}}