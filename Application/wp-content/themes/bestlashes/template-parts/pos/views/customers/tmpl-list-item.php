<div class="table-row customer bl-row">
    <div class="table-column _10percent">
        <div class="img">{{#if avatar_url}}<img src="{{avatar_url}}" title="#{{id}}">{{/if}}</div>
    </div>
    <div class="table-column _10percent">
        <div class="username">{{#if username}}<a data-action="edit" href="#">{{username}}</a>{{/if}}</div>
    </div>
    <div class="table-column _10percent">
        <div class="first-name">{{first_name}}</div>
    </div>
    <div class="table-column _10percent">
        <div class="last-name">{{last_name}}</div>
    </div>
    <div class="table-column _20percent">
        <div class="email">{{email}}</div>
    </div>
    <div class="table-column _10percent">
        <div class="spent">{{{money total_spent}}}</div>
    </div>
    <div class="table-column _10percent">
        <div class="orders">
            {{#compare orders_count '!==' 0}}
                <a data-action="showOrders" href="#">{{orders_count}}</a>
            {{else}}
                {{orders_count}}
            {{/compare}}
        </div>
    </div>
    <div class="table-column _10percent">
        <div class="last-order">
            {{#if last_order_id}}
                <a data-action="showOrder" href="#" title="{{formatDate last_order_date format='MMMM Do YYYY, h:mm:ss a'}}" data-toggle="tooltip">
            {{last_order_id}}
                </a>
            {{/if}}
        </div>
    </div>
    <div class="table-column _10percent right">
        <div class="actions">
            <a class="btn btn-success" data-action="edit" href="#"><?php /* translators: wordpress */ _e( 'Edit' ); ?></a>
        </div>
    </div>
</div>