<div class="table-wrapper invoice customers bl-wrapper">
	<div class="table-content">
		<div class="table-row search">
			<div class="list-actions"></div>
		</div>
	</div>
	<div class="table-content">
		<div class="list-header  orders-customers table-header">
			<div class="table-column _10percent">
				<div class="img"></div>
			</div>
			<div class="table-column _10percent">
				<div class="username"><?php /* translators: wordpress */ _e('Username'); ?></div>
			</div>
			<div class="table-column _10percent">
				<div class="first-name"><?php /* translators: woocommerce */ _e('First Name', 'bl' ); ?></div>
			</div>
			<div class="table-column _10percent">
				<div class="last-name"><?php /* translators: woocommerce */ _e('Last Name', 'bl' ); ?></div>
			</div>
			<div class="table-column _20percent">
				<div class="email"><?php /* translators: wordpress */ _e('Email'); ?></div>
			</div>
			<div class="table-column _10percent">
				<div class="spent"><?php /* translators: woocommerce */ _e('Money Spent', 'bl' ); ?></div>
			</div>
			<div class="table-column _10percent">
				<div class="orders"><?php /* translators: woocommerce */ _e('Orders', 'woocommerce' ); ?></div>
			</div>
			<div class="table-column _10percent">
				<div class="last-order"><?php /* translators: woocommerce */ _e('Last order', 'woocommerce' ); ?></div>
			</div>
			<div class="table-column _10percent right">
				<div class="actions"><?php /* translators: woocommerce */ _e('Actions', 'woocommerce'); ?></div>
			</div>
		</div>
	</div>
	<div class="table-content">
		<div class="panel-body list list-striped"></div>
		<div class="panel-footer list-footer"></div>
	</div>
</div>