<?php
  // using global user info
  global $current_user;
?>

<div class="header invoice">
    <div class="navigation invoice">
        <div class="logo-hamburger-container">
            <a href="#" class="logo-container w-inline-block">
                <div class="logo-block">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo-b.svg" width="60" height="60" class="logo-b">
                    <div class="logo-background"></div>
                </div>
                <div class="logo-text-container">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo-bestlashes.svg" width="185" class="logo-best-lashes">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo-professional.svg" width="141" class="logo-professional">
                </div>
            </a>
            <a href="#" class="hamburger-container w-inline-block" data-ix="hamburger-animation">
                <div class="hamburger-slices">
                    <div class="hamburger-slice"></div>
                    <div class="hamburger-slice"></div>
                    <div class="hamburger-slice"></div>
                </div>
            </a>
        </div>
        <div class="nav-container invoice">
            <a href="#" class="nav-item w-inline-block">
                <div class="nav-text"><?php _e('Sale', 'bl'); ?></div>
                <div class="link-underline"></div>
            </a>
            <a href="#products" class="nav-item w-inline-block">
                <div class="nav-text"><?php _e('Products', 'bl'); ?></div>
                <div class="link-underline"></div>
            </a>
            <a href="#orders" class="nav-item w-inline-block">
                <div class="nav-text"><?php _e('Orders', 'bl'); ?></div>
                <div class="link-underline"></div>
            </a>
            <a href="#customers" class="nav-item w-inline-block">
                <div class="nav-text"><?php _e('Customers', 'bl'); ?></div>
                <div class="link-underline"></div>
            </a>
            <a href="#coupons" class="nav-item w-inline-block">
                <div class="nav-text"><?php _e('Coupons', 'bl'); ?></div>
                <div class="link-underline"></div>
            </a>
            <a href="<?php echo get_admin_url(); ?>" class="nav-item w-inline-block">
                <div class="nav-text"><?php _e('WP Dashboard', 'bl'); ?></div>
                <div class="link-underline"></div>
            </a>
            <div class="menu-space"></div>
            <a href="#" class="nav-item-user w-inline-block" style="background-image: url(<?php echo get_avatar_url( $current_user->ID ); ?>);"></a>
            <div class="nav-username"><?php echo $current_user->display_name; ?></div>
            <div class="menu-space"></div>
            <a href="<?php echo wp_logout_url( home_url() ); ?>" class="nav-item w-inline-block">
                <div class="nav-text">kilépés</div>
                <div class="link-underline"></div>
            </a>
        </div>
    </div>
</div>

<!--<div>
  <a href="#" id="menu-btn"><i class="icon-bars"></i> <span><?php /* translators: wordpress */ _e( 'Menu' ); ?></span></a>
</div>
<h1 class="center-block">{{name}}</h1>
<div>
  <a href="#" data-toggle="dropdown">
    <?php echo get_avatar( $current_user->ID, 20 ); ?>
    <span><?php echo $current_user->display_name ?></span>
  </a>
  <div class="dropdown-list">
    <ul>
      <li><a href="<?php echo wp_logout_url( home_url() ); ?>"><?php /* translators: wordpress */ _e( 'Log Out' ); ?></a></li>
    </ul>
  </div>
</div>-->