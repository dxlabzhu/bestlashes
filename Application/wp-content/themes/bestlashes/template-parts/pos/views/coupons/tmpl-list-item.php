<div class="table-row coupon bl-row">
	<div class="table-column _20percent">
		<div>{{code}}</div>
	</div>
	<div class="table-column _20percent">
		<div>{{type}}</div>
	</div>
	<div class="table-column _10percent">
		<div>{{{money amount}}}</div>
	</div>
	<div class="table-column _20percent">
		<div>{{description}}</div>
	</div>
	<div class="table-column _10percent">
		<div>
			{{#each product_ids}}
				<a href="#products/{{this}}">{{this}}</a>
			{{/each}}
		</div>
	</div>
	<div class="table-column _10percent">
		<div>{{usage_count}} / {{usage_limit}}</div>
	</div>
	<div class="table-column _10percent">
		<div>{{formatDate expiry_date format='MMMM Do YYYY'}}</div>
	</div>
</div>