<div class="table-wrapper invoice coupons bl-wrapper">
	<div class="table-content">
		<div class="table-row search">
			<div class="list-actions"></div>
		</div>
	</div>
	<div class="table-content">
		<div class="list-header coupons-header table-header">
			<div class="table-column _20percent">
				<div><?php /* translators: woocommerce */ _e('Code', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _20percent">
				<div><?php /* translators: woocommerce */ _e('Coupon type', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _10percent">
				<div><?php /* translators: woocommerce */ _e('Coupon amount', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _20percent">
				<div><?php /* translators: woocommerce */ _e('Description', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _10percent">
				<div><?php /* translators: woocommerce */ _e('Products IDs', 'bl'); ?></div>
			</div>
			<div class="table-column _10percent">
				<div><?php /* translators: woocommerce */ _e('Usage Limit', 'bl'); ?></div>
			</div>
			<div class="table-column _10percent">
				<div><?php /* translators: woocommerce */ _e('Expiry Date', 'bl'); ?></div>
			</div>
		</div>
	</div>
	<div class="table-content">
		<div class="panel-body list list-striped"></div>
		<div class="panel-footer list-footer"></div>
	</div>
</div>