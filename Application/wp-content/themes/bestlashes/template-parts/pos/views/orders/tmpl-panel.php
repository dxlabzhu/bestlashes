<div class="table-wrapper invoice orders bl-wrapper">
	<div class="table-content">
		<div class="table-row search">
			<div class="panel-header list-actions"></div>
		</div>
	</div>
	<div class="table-content">
		<div class="list-header orders-header table-header">
			<div class="table-column _10percent">
				<div class="status"><?php /* translators: woocommerce */ _e('Status', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _10percent">
				<div class="order"><?php /* translators: woocommerce */ _e('Order', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _20percent">
				<div class="customer"><?php /* translators: woocommerce */ _e('Customer', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _20percent">
				<div class="note"><?php /* translators: woocommerce */ _e('Note', 'woocommerce'); ?></div>
			</div>
			<div class="table-column _20percent">
				<div class="date"><?php /* translators: woocommerce */ _e('Date', 'woocommerce' ); ?></div>
			</div>
			<div class="table-column _10percent">
				<div class="total"><?php /* translators: woocommerce */ _e('Total', 'woocommerce' ); ?></div>
			</div>
			<div class="table-column _10percent right">
				<div class="actions"><?php /* translators: woocommerce */ _e('Actions', 'woocommerce'); ?></div>
			</div>
		</div>
	</div>
	<div class="table-content">
		<div class="panel-body list list-striped"></div>
		<div class="panel-footer list-footer"></div>
	</div>
</div>