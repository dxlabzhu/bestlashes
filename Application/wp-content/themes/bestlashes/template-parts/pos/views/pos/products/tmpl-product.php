{{#is type 'variation'}}
    <div class="table-row-small-tansparent pos-product-row pos-product-variation">
        <div class="table-column _10percent"></div>
        <div class="table-column _60percent">
            <div class="table-links">
                <a class="table-link">
                    {{#each attributes}}
                        {{name}}: {{#if option}}{{option}}{{else}}{{#list options ', '}}{{this}}{{/list}}{{/if}}<span class="separation">|</span>
                    {{/each}}
                </a>
                {{#if managing_stock}}
                    <div class="table-stock"><?php /* translators: woocommerce */ printf( __( '%s in stock', 'woocommerce' ), '{{number stock_quantity precision="auto"}}' ); ?></div>
                {{/if}}
            </div>
        </div>
        <div class="table-column _20percent right">
            <div class="table-price">
                {{#if on_sale}}
                    <div class="old-price">{{#list regular_price ' - '}}{{{money this}}}{{/list}}</div>
                {{/if}}
                <div class="price">{{#list price ' - '}}{{{money this}}}{{/list}}
                </div>
            </div>
        </div>
        <div class="table-column _10percent">
            <div class="action">
                <a data-action="add" href="#" class="round-button w-button">+</a>
            </div>
        </div>
    </div>
{{else}}
    <div class="table-row pos-product-row">
        <div class="table-column _10percent">
            <img src="{{featured_src}}" width="60" title="#{{id}}" class="cart-product-image">
        </div>
        <div class="table-column _60percent">
            <div class="table-links">
                <a class="table-link">
                    {{name}}<br>
                </a>
                {{#if managing_stock}}
                    <div class="table-stock"><?php /* translators: woocommerce */ printf( __( '%s in stock', 'woocommerce' ), '{{number stock_quantity precision="auto"}}' ); ?></div>
                {{/if}}
            </div>
        </div>
        <div class="table-column _20percent right">
            <div class="table-price">
                {{#if on_sale}}
                    <div class="old-price">{{#list regular_price ' - '}}{{{money this}}}{{/list}}</div>
                {{/if}}
                <div class="price">{{#list price ' - '}}{{{money this}}}{{/list}}
                </div>
            </div>
        </div>
        {{#is type 'variable'}}
            <div class="table-column _10percent">
                <div class="action">
                    <a href="#" data-action="expand" class="expand-all round-button w-button">↓</a>
                    <a href="#" data-action="close" class="close-all round-button w-button">↑</a>
                </div>
            </div>
        {{else}}
            <div class="table-column _10percent">
                <div class="action">
                    <a data-action="add" href="#" class="round-button w-button">+</a>
                </div>
            </div>
        {{/is}}
    </div>
{{/is}}