{{#each line_items}}
    <div class="table-row-small-tansparent list-row bl-row">
        <div class="table-column _10percent">
            <div class="qty">{{number quantity precision="auto"}}</div>
        </div>
        <div class="table-column _40percent">
            <div class="title">
                {{name}}
                {{#with meta}}
                    <dl class="meta">
                        {{#each []}}
                            <dt>{{#if label}}{{label}}{{else}}{{key}}{{/if}}:</dt>
                            <dd>{{value}}</dd>
                        {{/each}}
                    </dl>
                {{/with}}
            </div>
        </div>
        <div class="table-column _20percent right">
            <div class="price">
                {{#if on_sale}}
                    <div class="old-price">{{{money regular_price}}}</div>
                    <div class="price">{{{money price}}}</div>
                {{else}}
                    <div class="price">{{{money price}}}</div>
                {{/if}}
            </div>
        </div>
        <div class="table-column _30percent right">
            <div class="total">
                {{#if on_sale}}
                    <del>{{{money subtotal}}}</del> {{{money total}}}
                {{else}}
                    {{{money total}}}
                {{/if}}
            </div>
        </div>
    </div>
{{/each}}