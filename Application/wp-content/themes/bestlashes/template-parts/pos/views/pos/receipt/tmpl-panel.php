<div class="table-wrapper invoice product-recieipt bl-wrapper">
	<div class="table-content search">
		<div class="table-row search">
			<div class="panel-header receipt-status products-actions table-column _100percent"></div>
		</div>
	</div>
	<div class="table-content">
		<div class="list-header receipt-header table-header">
			<div class="table-column _10percent">
				<div class="qty"><?php _ex( 'Qty', 'Abbreviation of Quantity', 'woocommerce-pos' ); ?></div>
			</div>
			<div class="table-column _40percent">
				<div class="title"><?php /* translators: woocommerce */ _e( 'Product', 'woocommerce' ); ?></div>
			</div>
			<div class="table-column _20percent right">
				<div class="price"><?php /* translators: woocommerce */ _e( 'Price', 'woocommerce' ); ?></div>
			</div>
			<div class="table-column _30percent right">
				<div class="total"><?php /* translators: woocommerce */ _e( 'Total', 'woocommerce' ); ?></div>
			</div>
		</div>
	</div>
	<div class="table-content">
		<div class="panel-body list receipt-list" data-order-id="{{order_number}}" data-payment-deadline="<?php echo date( 'Y-m-d' ); ?>" data-vat-type="27" data-paid="true" data-user-id="{{customer_id}}"></div>
		<div class="list list-totals receipt-totals"></div>
		<div class="list-actions receipt-actions"></div>
	</div>
</div>