<li class="list-row table-row-small-tansparent subtotal">
    <div class="table-column _70percent right"><?php /* translators: woocommerce */ _e( 'Subtotal', 'woocommerce' ); ?>:</div>
    <div class="total table-column _30percent right">{{{money subtotal}}}</div>
</li>
{{#if has_discount}}
    <li class="list-row table-row-small-tansparent cart-discount">
        <div class="table-column _70percent right"><?php _e( 'Discount', 'bl' ); ?>:</div>
        <div class="total table-column _30percent right">{{{money cart_discount negative=true}}}</div>
    </li>
{{/if}}
{{#each shipping_lines}}
    <li class="list-row table-row-small-tansparent">
        <div class="table-column _70percent right">{{method_title}}:</div>
        <div class="total table-column _30percent right">{{{money total}}}</div>
    </li>
{{/each}}
{{#each fee_lines}}
    <li class="list-row table-row-small-tansparent">
        <div class="table-column _70percent right">{{name}}:</div>
        <div class="total table-column _30percent right">{{{money total}}}</div>
    </li>
{{/each}}
{{#if has_tax}}
    {{#if itemized}}
        {{#each tax_lines}}
            {{#if has_tax}}
                <li class="list-row table-row-small-tansparent tax">
                    <div class="table-column _70percent right">
                        {{#if ../../incl_tax}}
                            <small>(<?php _ex( 'incl.', 'abbreviation for includes (tax)', 'bl' ); ?>)</small>
                        {{/if}}
                        {{title}}:
                    </div>
                    <div class="total table-column _30percent right">{{{money total}}}</div>
                </li>
            {{/if}}
        {{/each}}
    {{else}}
        <li class="list-row table-row-small-tansparent tax">
            <div class="table-column _70percent right">
                {{#if incl_tax}}
                    <small>(<?php _ex( 'incl.', 'abbreviation for includes (tax)', 'bl' ); ?>)</small>
                {{/if}}
                <?php echo esc_html( WC()->countries->tax_or_vat() ); ?>:
            </div>
            <div class="total table-column _30percent right">{{{money total_tax}}}</div>
        </li>
    {{/if}}
{{/if}}

<!-- order_discount removed in WC 2.3, included for backwards compatibility -->
{{#if has_order_discount}}
    <li class="list-row table-row-small-tansparent order-discount">
        <div class="table-column _70percent right"><?php _e( 'Discount', 'bl' ); ?>:</div>
        <div class="total table-column _30percent right total">{{{money order_discount negative=true}}}</div>
    </li>
{{/if}}

<!-- end order_discount -->
<li class="list-row table-row-small-tansparent order-total">
    <div class="table-column _70percent right"><?php /* translators: woocommerce */ _e( 'Order total', 'bl' ); ?>:</div>
    <div class="total table-column _30percent right">{{{money total}}}</div>
</li>
{{#if note}}
    <li class="list-row table-row-small-tansparent note">
        <div class="action table-column _100percent">
            <div>{{note}}</div>
        </div>
    </li>
{{/if}}