<div class="w-row">
    <div class="table-wrapper invoice">
        <div class="table-content">
            <div class="table-row search" style="height: 90px;">
                <div class="table-column _10percent">
                    <div class="input-group-btn">
                        <a class="btn btn-secondary" href="#" data-toggle="dropdown">
                            <div class="fa-solid"></div>
                        </a>
                        <div class="dropdown-list">
                            <ul>
                                <li data-action="search"><i class="icon-search"></i> <?php /* translators: woocommerce */ _e( 'Search products', 'woocommerce' ); ?></li>
                                <li data-action="barcode"><i class="icon-barcode"></i> <?php _e( 'Scan Barcode', 'woocommerce-pos' ); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="table-column _90percent invoice">
                    <div class="table-form _100percent w-form input-group">
                        <input type="search" tabindex="1" class="table-form-input w-input form-control" placeholder="Search product<?php _e('Search product', 'bl'); ?>">
                        <span class="input-group-addon clear">
                            <i data-action="clear" class="icon-times-circle icon-lg"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>