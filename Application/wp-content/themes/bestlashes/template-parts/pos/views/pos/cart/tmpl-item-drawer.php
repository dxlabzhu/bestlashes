<div class="invoice-toggle">
    {{#if product_id}}
        <div class="table-row-small-tansparent">
            <div class="table-column _40percent right">
                <div><?php /* translators: woocommerce */ _e( 'Regular price', 'woocommerce' ); ?>:</div>
            </div>
            <div class="table-column _60percent">
                <div class="table-form _100percent w-form">
                    <input name="regular_price" id="regular_price" class="form-control autogrow" type="text" data-numpad="amount" data-label="<?php /* translators: woocommerce */ _e( 'Regular price', 'woocommerce' ); ?>" />
                </div>
            </div>
        </div>
    {{/if}}

    {{#is type 'shipping'}}
        <div class="table-row-small-tansparent">
            <div class="table-column _40percent right">
                <div><?php /* translators: woocommerce */ _e( 'Shipping Method', 'woocommerce' ); ?>:</div>
            </div>
            <div class="table-column _60percent">
                <div class="table-form _100percent w-form">
                    <select class="c-select form-control" name="method_id" id="method_id"></select>
                </div>
            </div>
        </div>
    {{/is}}

    <div class="table-row-small-tansparent">
        <div class="table-column _40percent right">
            <div><?php /* translators: woocommerce */ _e( 'Taxable', 'woocommerce' ); ?>:</div>
        </div>
        <div class="table-column _60percent">
            <div class="table-form _100percent w-form">
                <label class="c-input c-checkbox">
                    <input type="checkbox" name="taxable" id="taxable" class="form-control">
                    <span class="c-indicator"></span>
                </label>
                <select class="c-select" name="tax_class" id="tax_class" {{#unless taxable}}disabled{{/unless}}></select>
            </div>
        </div>
    </div>

    {{#if product_id}}
        <div class="table-row-small-tansparent meta">
            <div class="table-column _40percent right">
                <div><?php /* translators: woocommerce */ _e( 'Add&nbsp;meta', 'woocommerce' ); ?>:</div>
            </div>
            <div class="table-column _60percent meta-datas">
                <div>
                    {{#each meta}}
                        <span data-key="{{key}}">
                            <input name="meta.label" value="{{label}}" type="text" class="form-control" placeholder="<?php _e('Meta label', 'bl'); ?>">
                            <textarea name="meta.value" class="form-control" placeholder="<?php _e('Meta value', 'bl'); ?>">{{value}}</textarea>
                            <a href="#" data-action="remove-meta" class="round-button w-button">
                                <i class="icon-remove icon-lg"></i>
                            </a>
                        </span>
                    {{/each}}
                    <a href="#" data-action="add-meta" class="round-button w-button">+</a>
                </div>
            </div>
        </div>
    {{/if}}
</div>