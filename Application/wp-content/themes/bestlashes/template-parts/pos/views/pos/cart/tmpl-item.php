<div class="table-row">
    {{#if product}}
        <div class="table-column _20percent left">
            <div class="qty">
                <a href="#" class="input-minus w-button">-</a>
                <div class="form-block-2 w-form">
                    <input type="text" name="quantity" data-label="<?php /* translators: woocommerce */ _e( 'Quantity', 'woocommerce' ); ?>" data-numpad="quantity" class="form-control autogrow quantity-input w-input">
                </div>
                <a href="#" class="input-plus w-button">+</a>
            </div>
        </div>
        <div class="table-column _30percent">
            <div class="title">
                <span data-name="title" contenteditable="true">{{name}}</span>
                <dl class="meta"></dl>
                <a data-action="more" href="#" class="btn btn-default btn-circle-sm round-button small w-button">↓</a>
            </div>
        </div>
        <div class="table-column _20percent center">
            <div class="price form-block w-form">
                <input type="text" name="item_price" data-label="<?php /* translators: woocommerce */ _e( 'Price', 'woocommerce' ); ?>" data-numpad="discount" data-original="regular_price" data-percentage="off" class="form-control autogrow price-input w-input"> 
            </div>
            <div>
                Ft
            </div>
        </div>
    {{else}}
        <div class="table-column _20percent left">
            <div class="qty"></div>
        </div>
        <div class="table-column _30percent">
            <div class="title">
                {{#if method_title}}
                    <span data-name="method_title" contenteditable="true">{{method_title}}</span>
                {{else}}
                    <span data-name="title" contenteditable="true">{{title}}</span>
                {{/if}}
                <a data-action="more" href="#" class="btn btn-default btn-circle-sm round-button small w-button">↓</a>
            </div>
        </div>
        <div class="table-column _20percent center">
            <div class="price">
                <input type="text" name="item_price" data-label="<?php /* translators: woocommerce */ _e( 'Price', 'woocommerce' ); ?>" data-numpad="amount" class="form-control autogrow">
            </div>
        </div>
    {{/if}}
    <div class="table-column _20percent right">
        <div class="total"></div>
    </div>
    <div class="table-column _10percent right">
        <div class="action">
            <a data-action="remove" href="#" class="round-button w-button">
                -
            </a>
        </div>
    </div>
</div>