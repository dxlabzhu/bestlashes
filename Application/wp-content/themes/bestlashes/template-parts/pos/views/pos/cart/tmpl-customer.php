<div class="table-column _10percent">
	<img src="<?php echo get_template_directory_uri(); ?>/images/icon-login.svg" class="icon-login">
</div>
<div class="table-column _60percent invoice">
	<div class="table-form _100percent w-form">
		<input data-toggle="dropdown" type="text" class="form-control" placeholder="<?php /* translators: woocommerce */ _e( 'Search customers', 'woocommerce' ); ?>">
	</div>
</div>

<div class="table-column _30percent">
	{{#if customer_id}}
		<a href="#" data-action="remove" class="rounded-button small w-button" title="<?php /* translators: wordpress */ _e( 'Remove' ); ?>"><i class="icon-remove"></i></a>
	{{/if}}
	<a href="#" class="rounded-button small w-button" data-action="edit" title="<?php /* translators: wordpress */ _e( 'Edit' ); ?>" data-user-id="{{customer_id}}">
		{{formatCustomerName customer}}
	</a>
</div>