<div class="table-wrapper invoice">
	<div class="table-content">
		<div class="table-row search table-row-container">
			<div class="cart-customer"></div>
		</div>
	</div>
	<div class="table-content">
		<div class="table-header panel-header list-header cart-header">
			<div class="qty table-column _20percent">
				<div><?php _ex( 'Qty', 'Abbreviation of Quantity', 'woocommerce-pos' ); ?></div>
			</div>
			<div class="title table-column _30percent">
				<div><?php /* translators: woocommerce */ _e( 'Product', 'woocommerce' ); ?></div>
			</div>
			<div class="price table-column _20percent center">
				<div><?php /* translators: woocommerce */ _e( 'Price', 'woocommerce' ); ?></div>
			</div>
			<div class="total table-column _20percent right">
				<div><?php /* translators: woocommerce */ _e( 'Total', 'woocommerce' ); ?></div>
			</div>
			<div class="action table-column _10percent">&nbsp;</div>
		</div>
	 	<div class="panel-body list cart-list"></div>
		<div class="table-row table-row-container">
			<div class="list list-totals cart-totals"></div>
		</div>
		<div class="table-footer table-row-container">
			<div class="list-actions cart-actions"></div>
		</div>
		<div class="cart-notes"></div>
		<div class="table-tab-container">
			<div class="panel-footer cart-footer"></div>
		</div>
	</div>
</div>