<li class="list-row subtotal">
    <div class="table-column _70percent right"><?php /* translators: woocommerce */ _e( 'Subtotal', 'woocommerce' ); ?>:</div>
    <div class="total table-column _20percent right">{{{money subtotal}}}</div>
    <div class="action table-column _10percent"></div>
</li>
{{#if has_discount}}
    <li class="list-row cart-discount">
        <div class="table-column _70percent right"><?php _e( 'Discount', 'bl' ); ?>:</div>
        <div class="total table-column _20percent right">{{{money cart_discount negative=true}}}</div>
        <div class="action table-column _10percent"></div>
    </li>
{{/if}}
{{#compare total_tax '!==' 0}}
    {{#if itemized}}
        {{#each tax_lines}}
            {{#compare total '!==' 0}}
                <li class="list-row tax">
                    <div class="table-column _70percent right">
                        {{#if ../../incl_tax}}
                            <small>(<?php _ex( 'incl.', 'abbreviation for includes (tax)', 'bl' ); ?>)</small>
                        {{/if}}
                        {{label}}:
                    </div>
                    <div class="total table-column _20percent right">{{{money total}}}</div>
                    <div class="action table-column _10percent"></div>
                </li>
            {{/compare}}
        {{/each}}
    {{else}}
        <li class="list-row tax">
            <div class="table-column _70percent right">
                {{#if incl_tax}}<small>(<?php _ex( 'incl.', 'abbreviation for includes (tax)', 'bl' ); ?>)</small>{{/if}}
                <?php echo esc_html( WC()->countries->tax_or_vat() ); ?>:
            </div>
            <div class="total table-column _20percent right">{{{money total_tax}}}</div>
            <div class="action table-column _10percent"></div>
        </li>
    {{/if}}
{{/compare}}
<li class="list-row order-total">
    <div class="table-column _70percent right"><?php /* translators: woocommerce */ _e( 'Order total', 'bl' ); ?>:</div>
    <div class="total table-column _20percent right">{{{money total}}}</div>
    <div class="action table-column _10percent"></div>
</li>