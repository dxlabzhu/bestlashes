<?php
$users_videos = array();
$video_ids = array();
$lang = ICL_LANGUAGE_CODE;

// Get users orders
$args['posts_per_page'] = -1;
$args['post_type'] = wc_get_order_types();
$args['post_status'] = array_keys( wc_get_order_statuses() );
$args['meta_query'] = array();
$args['meta_query'][] = array(
	'key' => '_customer_user',
	'value' => get_current_user_id()
);
$args['fields'] = 'ids';

$user_orders_loop = new WP_Query( $args );

if( $user_orders_loop->have_posts() ){
	while ( $user_orders_loop->have_posts() ) {
		$order_id = $user_orders_loop->next_post();
		$order = wc_get_order( $order_id );

		if( 'completed' === $order->get_status() ){
			foreach ($order->get_items() as $item_id => $item_data) {
				$product_id = $item_data['product_id'];
				$product = wc_get_product( $product_id );

				if( $product->is_virtual() ){
					$embed_video = get_post_meta( $product_id, 'video-embed-code', true );

					if( !empty( $embed_video ) ){
						$users_videos[] = array(
							'product_id' => $product_id,
							'title' => get_the_title( $product_id ),
							'order_id' => $order_id,
							'embed_video' => $embed_video
						);
					}
				}
			}
		}
	}
} ?>

<div id="videos" class="my-videos">
	<?php if( !empty( $users_videos ) ){
		foreach ( $users_videos as $video ) { ?>

			<?php if( !empty( $video ) ){ ?>
				<div class="my-video">
					<h1 class="heading-01"><?php echo $video['title']; ?></h1>
					<div class="separator-line"></div>
					<div class="video-wrapper">
						<?php echo $video['embed_video']; ?>
					</div>
				</div>
			<?php } ?>

		<?php }
	} else { ?>

	<?php } ?>
</div>