<?php

$prod_top_categories = get_terms( 
    array( 
        'taxonomy' => 'product_cat', 
        'hide_empty' => false,
        'parent' => 0,
        'exclude' => apply_filters( 'bl_shop_sidebar_category_list_exlude', array( BL_PRODUCT_CAT_ID_OTHERS, BL_PRODUCT_CAT_ID_TRAINING_COURSES, BL_PRODUCT_CAT_ID_DORCAY, BL_PRODUCT_CAT_ID_LASH_INC ) )
    )
); ?>


<div class="col w-col w-col-3 w-col-stack">
    <div class="sidebar-menu-container">
        <a href="#" class="sidebar-button w-inline-block">
            <div class="link-text"><?php _e( 'Choose on category', 'bl' ); ?></div>
            <div class="link-icon-right">⇵</div>
        </a>
    </div>
    <div class="sidebar-wrapper<?php if( $bl_open_sidebar_wrapper ) echo ' open'; ?>">
        <?php 
        $sale_products = bl_get_fixed_lang_sale_products();
        
        if( !empty( $sale_products ) ){ ?>
            <a href="<?php echo get_permalink( BL_PAGE_WEBSHOP ); ?>?<?php _e('filter', 'bl'); ?>=<?php echo sanitize_title( __( 'Sale products', 'bl' ) ); ?>" class="sidebar-link-block w-inline-block <?php echo ( ( $_GET[ __('filter', 'bl') ] == sanitize_title( __( 'New products', 'bl' ) ) ) ? 'active' : '' ); ?>">
                <div class="link-text"><?php _e( 'Sale products', 'bl' ); ?></div>
                <div class="link-icon-right small">%</div>
            </a>
        <?php } ?>

        <a href="<?php echo get_permalink( BL_PAGE_WEBSHOP ); ?>?<?php _e('filter', 'bl'); ?>=<?php echo sanitize_title( __( 'New products', 'bl' ) ); ?>" class="sidebar-link-block w-inline-block <?php echo ( ( $_GET[ __('filter', 'bl') ] == sanitize_title( __( 'Sale products', 'bl' ) ) ) ? 'active' : '' ); ?>">
            <div class="link-text"><?php _e( 'New products', 'bl' ); ?></div>
            <div class="link-icon-right small">★</div>
        </a>
        <div class="sidebar-separator"></div>

        <!-- Categories -->
        <?php if( !empty( $prod_top_categories ) ){
            foreach ( $prod_top_categories as $prod_top_category ) {
                // Check children
                $prod_sub_categories = get_terms( 
                    array( 
                        'taxonomy' => 'product_cat',
                        'hide_empty' => false,
                        'parent' => $prod_top_category->term_id,
                    )
                );

                if( !empty( $prod_sub_categories ) ){ 
                    
                    // Parent active class
                    $parent_active_class = '';
                    $dropdown_active_class = '';
                    foreach ( $prod_sub_categories as $prod_sub_category ) {
                        if( !empty( $active_category ) && $active_category->term_id == $prod_sub_category->term_id ){
                            $parent_active_class = 'active';
                            $dropdown_active_class = 'open';
                        }
                    } ?>

                    <div class="sidebar-dropdown">
                        <div class="sidebar-dropdown-trigger <?php echo $parent_active_class; ?>">
                            <div class="link-text"><?php echo $prod_top_category->name; ?></div>
                            <div class="link-icon-right">+</div>
                        </div>
                        <div class="dropdown-toggle noshaddow <?php echo $dropdown_active_class; ?>">
                            <?php foreach ( $prod_sub_categories as $prod_sub_category ) {
                                $is_active_class = ( ( !empty( $active_category ) && $active_category->term_id == $prod_sub_category->term_id ) ? 'active' : '' ); ?>
                                
                                <a href="<?php echo get_term_link( $prod_sub_category->term_id, 'product_cat' ); ?>" class="sidebar-link-block w-inline-block <?php echo $is_active_class; ?>">
                                    <div class="link-icon-left">•</div>
                                    <div class="sidebar-dropdown-text"><?php echo $prod_sub_category->name; ?></div>
                                </a>

                            <?php }

                            if( $prod_top_category->term_id == BL_PRODUCT_CAT_ID_TOOLS ){ ?>
                                <a href="<?php echo get_term_link( $prod_top_category->term_id, 'product_cat' ); ?>" class="sidebar-link-block w-inline-block <?php echo $is_active_class; ?>">
                                    <div class="link-icon-left">•</div>
                                    <div class="sidebar-dropdown-text"><?php _e('All Tools', 'bl'); ?></div>
                                </a>
                            <?php } ?>
                        </div>
                    </div>

                <?php } else {
                    $is_active_class = ( ( !empty( $active_category ) && $active_category->term_id == $prod_top_category->term_id ) ? 'active' : '' ); ?>

                    <a href="<?php echo get_term_link( $prod_top_category->term_id, 'product_cat' ); ?>" class="sidebar-link-block w-inline-block <?php echo $is_active_class; ?>">
                        <div class="link-text"><?php echo $prod_top_category->name; ?></div>
                        <div class="link-icon-right">⚬</div>
                    </a>

                <?php }
            }
        } ?>

        <!--<a href="<?php echo get_permalink( BL_PAGE_WEBSHOP ); ?>" class="sidebar-link-block w-inline-block">
            <div class="link-text"><?php _e( 'All products', 'bl' ); ?></div>
            <div class="link-icon-right">●</div>
        </a>-->        

        <a href="<?php echo get_permalink( BL_PAGE_TRAINING_COURSES ); ?>" class="sidebar-link-block w-inline-block">
            <div class="link-text"><?php _e('Training courses', 'bl'); ?></div>
            <div class="link-icon-right">⚬</div>
        </a>

        <div class="sidebar-separator"></div>
        <!--<a href="#" class="sidebar-link-block w-inline-block">
            <img src="<?php echo get_template_directory_uri() ?>/images/logo-dorcay.svg" class="sidebar-icon-logo">
        </a>-->
        <?php /*if( ICL_LANGUAGE_CODE == 'hu' && BL_LANG == 'HU' ){ ?>
            <a href="<?php echo get_permalink( BL_PAGE_LASH_INC_MAGAZINE ); ?>" class="sidebar-link-block w-inline-block">
                <img src="<?php echo get_template_directory_uri() ?>/images/logo-lashinc.svg" class="sidebar-icon-logo">
            </a>
        <?php }*/ ?>
    </div>
</div>