<?php

// Basic query
$args = array();
$args['post_type'] = array( 'product' );
$args['posts_per_page'] = '-1';
$args['post_status'] = 'publish';
$args['fields'] = 'ids';


// Taxonomy
if( !empty( $active_category ) ){
	$args['tax_query'][] = array(
		'taxonomy' => 'product_cat',
		'field' => 'term_id',
		'terms' => array( $active_category->term_id )
	);
}

// New products
if( isset( $_GET[ __('filter', 'bl') ] ) && $_GET[ __('filter', 'bl') ] == sanitize_title( __( 'New products', 'bl' ) ) ){
	$args['meta_query'][] = array(
		'key' => 'product_new_product',
		'value' => '1',
	);
}

// Training courses excluded
$args['post__not_in'] = bl_get_training_courses_ids();

// Orderby
if( !empty( $_COOKIE['product-list']['order'] ) ){
	// Popularity
	if( $_COOKIE['product-list']['order'] == 'popularity' ){
		$args['orderby'] = 'meta_value_num';
		$args['meta_key'] = 'total_sales';
		$args['order'] = 'asc';
	}

	// Price - ASC
	if( $_COOKIE['product-list']['order'] == 'price' ){
		$args['orderby'] = 'meta_value_num';
		$args['meta_key'] = '_price';
		$args['order'] = 'asc';
	}

	// Price - DESC
	if( $_COOKIE['product-list']['order'] == 'price-desc' ){
		$args['orderby'] = 'meta_value_num';
		$args['meta_key'] = '_price';
		$args['order'] = 'desc';
	}

	// Default
	if( $_COOKIE['product-list']['order'] == 'menu_order' ){
		$args['orderby'] = 'menu_order';
		$args['order'] = 'asc';
	}
} else {
	$args['orderby'] = 'menu_order';
	$args['order'] = 'asc';
}

// Search
if( !empty( $_GET[ __('search', 'bl') ] ) ){
	$args['s'] = $_GET[ __('search', 'bl') ];
}

$products_loop = new WP_Query( $args );
$i = 0;

if( empty( $_COOKIE['product-list']['view'] ) ){
	if( $products_loop->found_posts <= BL_WEBSHOP_PRODUCTS_VIEW_LIMIT ){
		$product_view = 'row';
	} else {
		$product_view = 'boxed';
	}
}

// TEMP: fix order of the posts
if( isset( $_GET[ __('filter', 'bl') ] ) && $_GET[ __('filter', 'bl') ] == sanitize_title( __( 'New products', 'bl' ) ) && BL_LANG == 'HU' ){
	$post_ids = $products_loop->posts;

	if( !empty( $post_ids ) ){
		$new_post_ids = array();
		$first_post_ids = array();

		// Fruity Power Ananász illatú szempillaragasztó 2g
		if( in_array( apply_filters( 'wpml_object_id', 22370, 'product', TRUE ), $post_ids ) ){
			$first_post_ids[] = apply_filters( 'wpml_object_id', 22370, 'product', TRUE );
		}

		// Fruity Power Cseresznye illatú szempillaragasztó 2g
		if( in_array( apply_filters( 'wpml_object_id', 22364, 'product', TRUE ), $post_ids ) ){
			$first_post_ids[] = apply_filters( 'wpml_object_id', 22364, 'product', TRUE );
		}

		// Fruity Power Ananász illatú szempillaragasztó 2g
		if( in_array( apply_filters( 'wpml_object_id', 22359, 'product', TRUE ), $post_ids ) ){
			$first_post_ids[] = apply_filters( 'wpml_object_id', 22359, 'product', TRUE );
		}

		// Prémium Platinum Szempillaragasztó 2g
		if( in_array( apply_filters( 'wpml_object_id', 21703, 'product', TRUE ), $post_ids ) ){
			$first_post_ids[] = apply_filters( 'wpml_object_id', 21703, 'product', TRUE );
		}

		// Prémium Pure Power Szempillaragasztó 2g
		if( in_array( apply_filters( 'wpml_object_id', 21707, 'product', TRUE ), $post_ids ) ){
			$first_post_ids[] = apply_filters( 'wpml_object_id', 21707, 'product', TRUE );
		}

		// Prémium My Icon Szempillaragasztó 2g
		if( in_array( apply_filters( 'wpml_object_id', 21714, 'product', TRUE ), $post_ids ) ){
			$first_post_ids[] = apply_filters( 'wpml_object_id', 21714, 'product', TRUE );
		}

		// Attrap
		if( in_array( apply_filters( 'wpml_object_id', 21718, 'product', TRUE ), $post_ids ) ){
			$first_post_ids[] = apply_filters( 'wpml_object_id', 21718, 'product', TRUE );
		}

		// Extra szempillafésű
		if( in_array( apply_filters( 'wpml_object_id', 5309, 'product', TRUE ), $post_ids ) ){
			$first_post_ids[] = apply_filters( 'wpml_object_id', 5309, 'product', TRUE );
		}

		// Vitamin Shot hidratáló – és ápoló folyadék liftinghez
		if( in_array( apply_filters( 'wpml_object_id', 20254, 'product', TRUE ), $post_ids ) ){
			$first_post_ids[] = apply_filters( 'wpml_object_id', 20254, 'product', TRUE );
		}

		$new_post_ids = $first_post_ids;

		foreach ( $post_ids as $post_id) {
			if( !in_array( $post_id, $first_post_ids ) ){
				$new_post_ids[] = $post_id;
			}
		}

		if( !empty( $new_post_ids ) ){
			$products_loop->posts = $new_post_ids;
		}
	}
}

if( $products_loop->have_posts() ){
	$products_found = $products_loop->found_posts;

	while ( $products_loop->have_posts() ) {
		$prod_id = $products_loop->next_post();
		$_product = wc_get_product( $prod_id );

		if( $product_view == 'boxed' ){
			// Image
			$image = $_product->get_image( 'product-list-boxed', array( 'class' => 'product-image' ) );

			// Price
			$prices = bl_get_product_prices( $_product );

			// Label
			$label_text = get_post_meta( $prod_id, 'product-label-text', true );
			
			if( empty( $label_text ) && $_product->is_on_sale() ){
				if( !empty( $prices['gross_regular_price'] ) ){
					$price_percentage = round( ( 1 - ( $prices['gross_price'] / $prices['gross_regular_price'] ) ) * 100 );

					if( $price_percentage != 0 ){
						$label_text = -1 * abs( $price_percentage ) . '%';
					}
				}
			}

			$link = get_permalink( $prod_id );

			include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-boxed.php', false, false ) );

			$i++;

		} else {
			// Image
			$image = $_product->get_image( 'product-list-row', array( 'class' => 'product-image' ) );

			// Price
			$prices = bl_get_product_prices( $_product );

			// Label
			$label_text = get_post_meta( $prod_id, 'product-label-text', true );

			if( empty( $label_text ) && $_product->is_on_sale() ){
				if( !empty( $prices['gross_regular_price'] ) ){
					$price_percentage = round( ( 1 - ( $prices['gross_price'] / $prices['gross_regular_price'] ) ) * 100 );

					if( $price_percentage != 0 ){
						$label_text = -1 * abs( $price_percentage ) . '%';
					}
				}
			}

			if( $_product->is_type( 'variable' ) ){

				// Variable cache datas
				$product_variation_available_variations_data_cache = wp_cache_get( 'bl_product_variation_available_variations_data_cache_' . $prod_id . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );
				$product_variation_available_attributes_data_cache = wp_cache_get( 'bl_product_variation_available_attributes_data_cache_' . $prod_id . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );
				$product_variation_attributes_data_cache = wp_cache_get( 'bl_product_variation_attributes_data_cache_' . $prod_id . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );
				$product_variation_variable_datas_data_cache = wp_cache_get( 'bl_product_variation_variable_datas_data_cache_' . $prod_id . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );

				// VIP Role check
				if( is_vip_user() ){
					$role_slug = bl_get_user_role_slug();

					if( !empty( $role_slug ) ){
						$product_variation_variable_datas_data_cache = wp_cache_get( 'bl_product_variation_variable_datas_data_cache_' . $prod_id . '_' . $role_slug . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );
					}
				}

				// Cache check
				if( empty( $product_variation_available_variations_data_cache ) || empty( $product_variation_available_attributes_data_cache ) || empty( $product_variation_attributes_data_cache ) || empty( $product_variation_variable_datas_data_cache ) ){
					
					// Variation data
					$variation_data = bl_generate_variations_data( $_product );
					
					$available_variations = $variation_data['available_variations'];
					$available_attributes = $variation_data['available_attributes'];
					$attributes = $variation_data['attributes'];
					$variable_datas = $variation_data['variable_datas'];
				} else {
					$available_variations = $product_variation_available_variations_data_cache;
					$available_attributes = $product_variation_available_attributes_data_cache;
					$attributes = $product_variation_attributes_data_cache;
					$variable_datas = $product_variation_variable_datas_data_cache;
				}
			} else {
				$available_variations = array();
				$attributes = array();
				$variable_datas = array();
			}

			$link = get_permalink( $prod_id );
			
			include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-row.php', false, false ) );
		}

	}
} else {
	include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-no-products-found.php', false, false ) );
}