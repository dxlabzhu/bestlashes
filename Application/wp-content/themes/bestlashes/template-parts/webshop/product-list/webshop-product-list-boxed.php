<?php if( $i == 0 ){ ?>
	<div class="w-row">
<?php }
if( $i % 4 == 0 && $i != 0 ){ ?>
	</div><div class="w-row">
<?php } ?>

<div class="col w-col w-col-3 w-col-medium-3 w-col-small-6 w-col-tiny-6">
	<a href="<?php echo $link; ?>" class="productlist-item w-inline-block">
		<div class="productlist-image-container">
			<?php echo $image; ?>
		</div>

		<?php if( !empty( $label_text ) ){ ?>
			<div class="productlist-stamp">
				<strong class="bold-text"><?php echo $label_text; ?></strong>
			</div>
		<?php } ?>
		
		<div class="productlist-detail-block shoplist">
			<div class="productlist-description-block shoplist<?php if( $_product->is_type( 'variation' ) ) echo ' product-variation'; ?>">
				<div class="productlist-text">
					<?php echo apply_filters( 'bl_product_variation_name', $_product, get_the_title( $prod_id ) ); ?>
				</div>
				<?php if( $_product->is_type('variation') ){
					$variation_attributes = $_product->get_variation_attributes(); ?>

					<span class="variation-list"><?php echo wc_get_formatted_variation( $variation_attributes, true ); ?></span>

				<?php } ?>
			</div>
			<div class="productlist-price-block shoplist">
				<?php if( $_product->is_in_stock() ){ ?>
					<div class="product-price">
						<?php if( $_product->is_on_sale( 'edit' ) ) { ?>
							<span class="new-price"><?php echo $prices['gross_price_formatted']; ?><br></span>
							<span class="old-price"><?php echo $prices['gross_regular_price_formatted']; ?></span>
						<?php } else {
							echo $prices['gross_price_formatted'];
						} ?>
					</div>
				<?php } else { ?>
					<div class="not-in-stock"><?php _e( 'Not in stock', 'bl' ); ?></div>
				<?php } ?>
			</div>
		</div>
	</a>
</div>

<?php if( $i == $products_found ){ ?>
	</div>
<?php }