<div class="w-row product-row">
    <div class="col w-col w-col-5">
        <div class="productlist-item">
            <?php if( $_product->is_type( 'variable' ) ){ ?>
                <div class="productlist-option-container">
                    <a href="#" class="productlist-option-button w-inline-block">
                        <div><?php _e( 'More options', 'bl' ); ?></div>
                    </a>
                    <div class="productlist-options">
                        <div class="variation-options">
                            <?php if( !empty( $available_attributes ) ){
                                foreach ( $available_attributes as $attribute_slug => $attribute ) {
                                    if( !empty( $attribute['attribute'] ) ){
                                        if ( $attribute['attribute']->is_taxonomy() ) { ?>

                                            <div class="option-container" data-attribute-slug="<?php echo $attribute_slug; ?>">
                                                <div class="option-trigger small">
                                                    <div class="option-name">
                                                        <?php $attribute_name = bl_get_option_lang('bl-product-variation-label-'. $attribute['attribute']->get_id() );
                                                        if( empty( $attribute_name ) ){
                                                            $attribute_name = wc_attribute_label( $attribute['attribute']->get_name(), $_product );
                                                        } ?>
                                                        <?php echo $attribute_name; ?>:
                                                        <strong></strong>
                                                    </div>
                                                    <div class="link-icon-right">+</div>
                                                </div>
                                                <div class="dropdown-toggle">
                                                    <div class="option-button-conainer small">
                                                        <?php if( !empty( $attribute['variations'] ) ){
                                                            foreach ( $attribute['variations'] as $term ) { ?>
                                                                <a href="#" class="option-button small w-inline-block" data-variation-slug="<?php echo $term->slug; ?>">
                                                                    <div><?php echo $term->name; ?></div>
                                                                </a>
                                                            <?php }
                                                        } ?>
                                                        <a href="#" class="option-button small w-inline-block delete-selection"">
                                                            <div>&#x2715;</div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php }
                                    }
                                }
                            } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="productlist-image-container">
                <a href="<?php echo get_permalink( $prod_id ); ?>">
                    <?php echo $image; ?>
                </a>
            </div>

            <?php if( !empty( $label_text ) ){ ?>
                <div class="productlist-stamp">
                    <strong class="bold-text"><?php echo $label_text; ?></strong>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="col w-col w-col-7">
        <div class="productlist-detail-block-big w-clearfix">
            <a href="<?php echo get_permalink( $prod_id ); ?>">
                <h3 class="heading-03"><?php echo apply_filters( 'bl_product_variation_name', $_product, get_the_title( $prod_id ) ); ?></h3>
            </a>
            <div class="separator-line"></div>
            <div class="productlist-text-big">
                <?php if( $_product->is_type('variation') ){
                    $variation_attributes = $_product->get_variation_attributes(); ?>

                    <span class="variation-list"><?php echo wc_get_formatted_variation( $variation_attributes, true ); ?></span>

                <?php } ?>

                <?php echo get_the_excerpt_by_id( $prod_id ); ?>
            </div>
            <a href="<?php echo get_permalink( $prod_id ); ?>" class="underline-button w-inline-block product-more-info-button">
                <div class="underline-button-text"><?php _e( 'More information', 'bl' ); ?> → </div>
                <div class="link-underline"></div>
            </a> 
            <div class="add-to-cart-container" data-variation-data='<?php if( $_product->is_type('variable') ){ echo wp_json_encode( $variable_datas ); } ?>'>
                <?php if( $_product->is_in_stock() ){ ?>
                    <form action="" method="post" accept-charset="utf-8" class="add-to-cart">
                        <div class="price-block" data-default-price="<?php echo $prices['gross_price_formatted']; ?>" data-default-net-price="<?php echo $prices['net_price_formatted'] ?>">
                            <?php if( $_product->is_on_sale('edit') ) { ?>
                                <div class="new-price"><?php echo $prices['gross_price_formatted']; ?><br></div>
                                <div class="old-price"><?php echo $prices['gross_regular_price_formatted']; ?></div>
                                <div class="net-price"><?php echo $prices['net_price_formatted'] ?></div>
                            <?php } else { ?>
                                <div class="price"><?php echo $prices['gross_price_formatted']; ?></div>
                                <div class="net-price"><?php echo $prices['net_price_formatted'] ?></div>
                            <?php } ?>
                        </div>
                        <div class="quantity-container"><a href="#" class="input-minus w-button">-</a>
                            <div class="w-form">
                                <input type="number" class="quantity-input qty w-input" maxlength="256" name="quantity" placeholder="1" step="1" min="1" pattern="[0-9]*" inputmode="numeric" value="1">
                            </div>
                            <a href="#" class="input-plus w-button">+</a>
                        </div>

                        <?php if( $_product->is_type('variable') ){
                            if( !empty( $attributes ) ){
                                foreach ( $attributes as $attribute_name => $attribute ) { ?>
                                    <input type="hidden" name="attribute_<?php echo $attribute_name; ?>" class="variation-slugs" value="">
                                <?php }
                            }
                        } else if( $_product->is_type('variation') ){
                            $variation_attributes = $_product->get_variation_attributes();

                            if( !empty( $variation_attributes ) ){
                                foreach ( $variation_attributes as $variation_attribute => $variation_attribute_value ) { ?>
                                    <input type="hidden" name="<?php echo $variation_attribute; ?>" class="variation-slugs" value="<?php echo $variation_attribute_value; ?>">
                                <?php }
                            }
                        } ?>

                        <input type="hidden" name="add-to-cart" value="<?php echo $prod_id; ?>">
                        
                        <?php if( bl_is_user_eligible_to_buy_product( $_product->get_id() ) ){ ?>
                            <a href="#" class="add-to-cart-button w-inline-block">
                                <div class="add-to-text"><?php _e('Add to cart', 'bl'); ?></div>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-bag.svg" class="icon-basket" alt="<?php _e('Add to cart', 'bl') ?>">
                                <div class="loading-button"></div>
                            </a>
                        <?php } ?>
                    </form>
                <?php } else { ?>
                    <div class="price-block">
                        <div class="not-in-stock"><?php _e( 'Not in stock', 'bl' ); ?></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>