<?php

$sale_products_list = false;

// Sale products
if( isset( $_GET[ __('filter', 'bl') ] ) && $_GET[ __('filter', 'bl') ] == sanitize_title( __( 'Sale products', 'bl' ) ) ){
	$sale_products_list = true;

	$product_variations_on_sale = bl_get_fixed_lang_sale_products();
}

// Sale products
if( !empty( $product_variations_on_sale ) ){

	$products_found = count( $product_variations_on_sale );
	
	$sale_products = array();
	$product_sale_page_ids = array();

	if( isset( $_GET[ __( 'group', 'bl' ) ] ) && !empty( $_GET[ __( 'group', 'bl' ) ] ) ){
		// Grouped products

		foreach ( $product_variations_on_sale as $key => $prod_id ) {
			$_product = wc_get_product( $prod_id );

			if( $_product ){
				if( $_product->get_stock_quantity() > 0 && $_product->get_stock_status() != 'outofstock' && $_product->is_on_sale( 'edit' ) ){
					if( $_product->is_type('variation') ){

						if( $_product->get_parent_id() == $_GET[ __( 'group', 'bl' ) ] ){
							$sale_products[] = $prod_id;
						}
					}
				}
			}
		}
	} else {
		$grouped_sale_products = array();
		
		// Group variations
		foreach ( $product_variations_on_sale as $key => $prod_id ) {
			$_product = wc_get_product( $prod_id );

			if( $_product ){
				if( $_product->get_stock_quantity() > 0 && $_product->get_stock_status() != 'outofstock' && $_product->is_on_sale( 'edit' ) ){
					if( $_product->is_type('variation') ){
						$grouped_sale_products[ $_product->get_parent_id() ][] = $prod_id;
					} else {
						$grouped_sale_products[ $prod_id ][] = $prod_id;
					}
				}
			}
		}

		if( !empty( $grouped_sale_products ) ){
			foreach ( $grouped_sale_products as $prod_id => $variations ) {

				if( sizeof( $variations ) > BL_SALE_VARIATIONS_LIMIT ){
					// Product with sale variations link
					$sale_products[] = $prod_id;
					$product_sale_page_ids[] = $prod_id;

				} else {
					// Product or variation
					foreach ( $variations as $variation_id ) {
						$sale_products[] = $variation_id;
					}
				}
			}
		}
	}

	if( !empty( $sale_products ) ){
		$i = 0;

		foreach ( $sale_products as $prod_id ) {
			$_product = wc_get_product( $prod_id );

			if( $_product ){
				// Image
				$image = $_product->get_image( 'product-list-boxed', array( 'class' => 'product-image' ) );

				// Price
				$prices = bl_get_product_prices( $_product );

				// Label
				$label_text = get_post_meta( $prod_id, 'product-label-text', true );

				if( in_array( $prod_id, $product_sale_page_ids ) ){
					$link = add_query_arg( array(
					    __('filter', 'bl') => sanitize_title( __( 'Sale products', 'bl' ) ),
					    __('group', 'bl') => $prod_id,
					), get_permalink( BL_PAGE_WEBSHOP ) );
				} else {
					$link = get_permalink( $prod_id );
				}
				
				if( empty( $label_text ) && $_product->is_on_sale() ){
					if( isset( $prices['gross_regular_price'] ) && !empty( $prices['gross_regular_price'] ) ){
						$price_percentage = round( ( 1 - ( $prices['gross_price'] / $prices['gross_regular_price'] ) ) * 100 );

						if( $price_percentage != 0 ){
							$label_text = -1 * abs( $price_percentage ) . '%';
						}
					}
				}

				include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-boxed.php', false, false ) );

				$i++;
			}
		}
	}

	/*foreach ( $product_variations_on_sale as $key => $prod_id ) {
		$_product = wc_get_product( $prod_id );

		if( $_product ){
			if( $_product->get_stock_quantity() > 0 && $_product->get_stock_status() != 'outofstock' && $_product->is_on_sale( 'edit' ) ){
				
				if( $product_view == 'boxed' ){
					// Image
					$image = $_product->get_image( 'product-list-boxed', array( 'class' => 'product-image' ) );

					// Price
					$prices = bl_get_product_prices( $_product );

					// Label
					$label_text = get_post_meta( $prod_id, 'product-label-text', true );
					
					if( empty( $label_text ) && $_product->is_on_sale() ){
						if( !empty( $prices['gross_regular_price'] ) ){
							$price_percentage = round( ( 1 - ( $prices['gross_price'] / $prices['gross_regular_price'] ) ) * 100 );

							if( $price_percentage != 0 ){
								$label_text = -1 * abs( $price_percentage ) . '%';
							}
						}
					}

					include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-boxed.php', false, false ) );

					$i++;

				} else {
					// Image
					$image = $_product->get_image( 'product-list-row', array( 'class' => 'product-image' ) );

					// Price
					$prices = bl_get_product_prices( $_product );

					// Label
					$label_text = get_post_meta( $prod_id, 'product-label-text', true );

					if( empty( $label_text ) && $_product->is_on_sale() ){
						if( !empty( $prices['gross_regular_price'] ) ){
							$price_percentage = round( ( 1 - ( $prices['gross_price'] / $prices['gross_regular_price'] ) ) * 100 );

							if( $price_percentage != 0 ){
								$label_text = -1 * abs( $price_percentage ) . '%';
							}
						}
					}

					$available_variations = array();
					$attributes = array();
					$variable_datas = array();
					
					include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-row.php', false, false ) );
				}
			}
		}
	}*/

} else {
	include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-no-products-found.php', false, false ) );
}

?>