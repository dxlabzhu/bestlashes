<?php

// If sale products, show only variations
if( isset( $_GET[ __('filter', 'bl') ] ) && $_GET[ __('filter', 'bl') ] == sanitize_title( __( 'Sale products', 'bl' ) ) ){
	include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-loop-sale-products.php', false, false ) );
} else {
	include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-loop-normal-products.php', false, false ) );
}