<?php

$catalog_orderby_options = apply_filters( 'woocommerce_catalog_orderby', 
    array( 
        'menu_order' => __( 'Default sorting', 'bl' ),
        'popularity' => __( 'Sort by popularity', 'bl' ),
        'price' => __( 'Sort by price: low to high', 'bl' ),
        'price-desc' => __( 'Sort by price: high to low', 'bl' ),
    )
); ?>


<div class="col w-col w-col-9 w-col-stack">
    <div class="productlist-wrapper">
        <div class="shop-head">
            <div class="shop-head-left-col">
                <div class="shop-header">
                    <?php if ( !empty( $active_category ) ){
                        echo $active_category->name;
                    } else if( !empty( $_GET[ __('filter', 'bl') ] ) ){
                        if( $_GET[ __('filter', 'bl') ] == sanitize_title( __( 'Sale products', 'bl' ) ) ){
                            _e( 'Sale products', 'bl' );
                        }
                        if( $_GET[ __('filter', 'bl') ] == sanitize_title( __( 'New products', 'bl' ) ) ){
                            _e( 'New products', 'bl' );
                        }
                    } else if( !empty( $_GET[ __('search', 'bl') ] ) ){
                        echo sprintf( __( 'Searched for: %1$s', 'bl' ), $_GET[ __('search', 'bl') ] );
                    } ?>
                </div>
            </div>
            <div class="shop-head-right-col">
                <form action="" method="post" class="product-filter">
                    <?php if( !isset( $_GET[ __('filter', 'bl') ] ) ){ ?>
                        <div class="shop-layout-container">
                            <a href="#" class="shop-layout <?php if( $product_view == 'boxed' ){ echo 'active'; } ?>" data-view="<?php echo 'boxed'; ?>">☷</a>
                            <a href="#" class="shop-layout <?php if( $product_view == 'row' ){ echo 'active'; } ?>" data-view="<?php echo 'row'; ?>">☰</a>
                        </div>
                    <?php } ?>
                    <div class="dropdown table-filter-dropdown">
                        <select name="order" class="orderby select-2 table-header-select">
                            <?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
                                <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $_COOKIE['product-list']['order'], $id ); ?>><?php echo esc_html( $name ); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <input type="hidden" name="view" value="<?php echo $product_view; ?>" class="product-view">
                    <input type="hidden" name="set-product-list-filter" value="1">
                </form>
            </div>
        </div>
        <div class="shop-filter-container"></div>