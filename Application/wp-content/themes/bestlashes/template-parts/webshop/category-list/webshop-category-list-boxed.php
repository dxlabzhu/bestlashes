<?php  if( $i == 0 ){ ?>
	<div class="w-row">
<?php }
if( $i % 6 == 0 && $i != 0 ){ ?>
	</div><div class="w-row">
<?php } ?>

<div class="col w-col w-col-4 <?php echo implode( ' ', $eclass ); ?>">
	<div class="margin">
		<a href="<?php echo $link; ?>" class="categories-link-block w-inline-block">
			<?php echo $image; ?>
			<h3 class="categories-heading"><?php echo $name; ?></h3>
		</a>
	</div>
</div>

<?php if( $i == ( $count_prod_categories - 1 ) ){ ?>
	</div>
<?php }