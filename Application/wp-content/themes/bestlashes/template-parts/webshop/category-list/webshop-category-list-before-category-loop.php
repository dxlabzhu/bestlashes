<div class="col w-col w-col-9 w-col-stack">
    <div class="productlist-wrapper">
        <div class="shop-head mobilehide">
            <div class="shop-head-left-col">
                <div class="shop-header">← <?php _e('Browse our products by category', 'bl'); ?></div>
            </div>
        </div>
        <div class="shop-filter-container"></div>
        <?php if( '' != bl_get_option_lang( 'bl-before-category-list-banner-content' ) ){ ?>
            <div class="margin">
                <div class="shop-categories-eyelashes" style="background-image: url(<?php echo bl_get_option_lang('bl-before-category-list-banner-background'); ?>)">
                    <div class="w-row">
                        <div class="col w-col w-col-6"></div>
                        <div class="col w-col w-col-6">
                            <div class="margin-shop">
                                <?php echo apply_filters( 'the_content', bl_get_option_lang( 'bl-before-category-list-banner-content' ) ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>