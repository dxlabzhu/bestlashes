		<?php if( '' != bl_get_option_lang( 'bl-after-category-list-banner-content' ) ){ ?>
			<div class="margin">
				<div class="shop-categories-training" style="background-image: url(<?php echo bl_get_option_lang('bl-after-category-list-banner-background'); ?>)">
					<div class="w-row">
						<div class="col w-col w-col-6"></div>
						<div class="col w-col w-col-6">
							<div class="margin-shop w-clearfix">
								<?php echo apply_filters( 'the_content', bl_get_option_lang( 'bl-after-category-list-banner-content' ) ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<<?php } ?>
	</div>
</div>