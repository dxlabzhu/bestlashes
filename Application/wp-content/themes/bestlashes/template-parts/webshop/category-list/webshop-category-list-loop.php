<?php

$prod_top_categories = get_terms( 
    array( 
        'taxonomy' => 'product_cat', 
        'hide_empty' => false,
        'parent' => 0,
        'exclude' => apply_filters( 'bl_shop_category_list_exlude', array( BL_PRODUCT_CAT_ID_OTHERS, BL_PRODUCT_CAT_ID_DORCAY, BL_PRODUCT_CAT_ID_LASH_INC, BL_PRODUCT_CAT_ID_REFLECTOCIL ) )
    )
);

$i = 0;
$count_prod_categories = count( $prod_top_categories );
$count_prod_categories++; // New products

$sale_products = bl_get_fixed_lang_sale_products();

if( !empty( $sale_products ) ){
    $count_prod_categories++;
}

/*if( BL_LANG == 'HU' ){
    $count_prod_categories += 1; // +1 for Lash magazin
}*/

if( !empty( $prod_top_categories ) ){

    // Sales products
    if( !empty( $sale_products ) ){
        $name = __( 'Sale products', 'bl' );
        $link = get_permalink( BL_PAGE_WEBSHOP ) .'?'. __('filter', 'bl') .'='. sanitize_title( __( 'Sale products', 'bl' ) );
        $image_id = BL_IMAGE_SALE_PRODUCT_HU;
        $eclass = array();

        if( BL_LANG == 'SK' ){
            $image_id = BL_IMAGE_SALE_PRODUCT_SK;
        } else if( BL_LANG == 'AT' ){
            $image_id = BL_IMAGE_SALE_PRODUCT_AT;
        }

        $image = wp_get_attachment_image( $image_id, 'product-category-boxed', false, array( 'class' => 'shop-categories-image' ) );

        include( locate_template( 'template-parts/webshop/category-list/webshop-category-list-boxed.php', false, false ) );

        $i++;
    }

    // New products
    $name = __( 'New products', 'bl' );
    $link = get_permalink( BL_PAGE_WEBSHOP ) .'?'. __('filter', 'bl') .'='. sanitize_title( __( 'New products', 'bl' ) );
    $image_id = BL_IMAGE_NEW_PRODUCT_HU;
    $eclass = array();

    if( BL_LANG == 'SK' ){
        $image_id = BL_IMAGE_NEW_PRODUCT_SK;
    } else if( BL_LANG == 'AT' ){
        $image_id = BL_IMAGE_NEW_PRODUCT_AT;
    }

    $image = wp_get_attachment_image( $image_id, 'product-category-boxed', false, array( 'class' => 'shop-categories-image' ) );

    include( locate_template( 'template-parts/webshop/category-list/webshop-category-list-boxed.php', false, false ) );

    $i++;

    // Other categories
    foreach ( $prod_top_categories as $prod_top_category ) { 
    	$thumbnail_id = get_woocommerce_term_meta( $prod_top_category->term_id, 'thumbnail_id', true );
    	$image = wp_get_attachment_image( $thumbnail_id, 'product-category-boxed', false, array( 'class' => 'shop-categories-image' ) );
        $link = get_term_link( $prod_top_category->term_id, 'product_cat' );
        $name = $prod_top_category->name;
        $eclass = array();

        if( $prod_top_category->term_id == BL_PRODUCT_CAT_ID_TRAINING_COURSES ){
            $link = get_permalink( BL_PAGE_TRAINING_COURSES );
        }

    	include( locate_template( 'template-parts/webshop/category-list/webshop-category-list-boxed.php', false, false ) );

    	$i++;
	}

    /*if( BL_LANG == 'HU' ){
        // Magazin
        $name = get_the_title( BL_PRODUCT_ID_MAGAZIN );
        $link = get_permalink( BL_PRODUCT_ID_MAGAZIN );
        $_product = wc_get_product( BL_PRODUCT_ID_MAGAZIN );
        $image = get_the_post_thumbnail( BL_PRODUCT_ID_MAGAZIN, 'product-category-boxed', array( 'class' => 'shop-categories-image' ) );
        $eclass = array();

        include( locate_template( 'template-parts/webshop/category-list/webshop-category-list-boxed.php', false, false ) );
    }*/
}


