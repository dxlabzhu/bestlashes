    <?php
    // Mobile social icons
    $social_facebook_link = get_option('bl-right-side-menu-facebook-link');
    $social_instagram_link = get_option('bl-right-side-menu-instagram-link');

    if( !empty( $social_facebook_link ) || !empty( $social_instagram_link ) ){ ?>
        <div class="footer-social">
            <div class="footer-social-container">
                <h4 class="heading-04 footer-social-heading"><?php _e( 'Follow us:', 'bl' ); ?></h4>

                <?php if( !empty( $social_facebook_link ) ){ ?>
                    <a href="<?php echo $social_facebook_link; ?>" target="_blank" class="nav-item-circle w-inline-block">
                        <div class="fa-brands"></div>
                    </a>
                <?php } ?>

                <?php if( !empty( $social_instagram_link ) ){ ?>
                    <a href="<?php echo $social_instagram_link; ?>" target="_blank" class="nav-item-circle w-inline-block">
                        <div class="fa-brands"></div>
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    
    <footer class="footer">
        <div class="footer-wrapper">
            <div class="container">
                <div class="w-row">
                    <?php dynamic_sidebar( 'footer-sidebar-left' ); ?>
                    <?php dynamic_sidebar( 'footer-sidebar-right' ); ?>
                </div>
            </div>
        </div>
        <div class="subfooter">
            <div class="container">
                <div class="subfooter-container">
                    <div class="copyright-container">
                        <div class="small-capital-text">
                            <?php echo wpautop( bl_get_option_lang('bl-copyright-text') ); ?>
                            <?php $lang = ICL_LANGUAGE_CODE; ?>
                            <?php if ($lang == 'hu') : ?>
                                <?php echo sprintf( __( '%1$sPrivacy policy%2$s', 'bl' ), '<a href="'. get_permalink( get_option( 'wp_page_for_privacy_policy' ) ) .'">', '</a>'  ); ?>
                            <?php elseif ($lang == 'en') : ?>
                                <?php echo sprintf( __( '%1$sPrivacy policy%2$s', 'bl' ), '<a href="'. get_permalink( 5384 ) .'">', '</a>'  ); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="overlay-wrapper"></div>
    <?php wp_footer(); ?>
</body>
</html>