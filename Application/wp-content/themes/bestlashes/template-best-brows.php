<?php 

/*

	Template Name: Best Brows

*/

get_header(); ?>

<div class="subpage-wrapper">
	<section class="subpage-hero bestbrows">
		<div class="container">
			<div class="floating-boxes-content bestbrows">
				<h1 class="heading-01 bestbrows">A szemöldökfestés itt kezdődik</h1>
			</div>
		</div>
		<div class="bestbrows-hero-packshot"><a href="#first" class="bestbrows-hero-readmore">Ismerd meg!</a><img src="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-500.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-800.png 800w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-1080.png 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-1600.png 1600w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-2000.png 2000w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products.png 2200w" sizes="100vw" alt="" class="bestbrows-hero-packages"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-500.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-800.png 800w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-1080.png 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-1600.png 1600w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-2000.png 2000w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff.png 2200w" sizes="100vw" alt="" class="bestbrows-hero-stuff"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-500.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-800.png 800w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-1080.png 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-1600.png 1600w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-2000.png 2000w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background.png 2200w" sizes="100vw" alt="" class="bestbrows-hero-background"></div>
	</section>
	<section id="first" class="section bestbrows-sec">
		<div class="container">
			<div class="columns">
				<div class="w-row">
					<div class="col w-col w-col-6">
						<div class="margin-big">
							<div class="paragraph-big bestbrows">A <span class="text-logo">Brow Henna</span>TINT festékek több mint 70%-ban természetes hatóanyagokat tartalmaznak, valamint akár 6 hétig tartó intenzív színeket biztosítanak. </div>
						</div>
					</div>
					<div class="col w-col w-col-6">
						<div class="margin w-clearfix">
							<div class="paragraph">Használatával pigmentáltabb és ragyogóbb árnyalatokat kapunk, különböző arányú hígítása pedig számos tónust eredményez, így egyénre szabható. Innovatív formulájú szemöldökfesték, mely stílusteremtő a szemöldökfestés és formázás világában. </div>
							<a href="#" class="underline-button bestbrows w-inline-block">
								<div class="underline-button-text">Beszerzem →</div>
								<div class="link-underline bestbrows"></div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bestbrow-big-image-mobile"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/bb-big-01-mobile.jpg" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bb-big-01-mobile-p-500.jpeg 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-big-01-mobile-p-1080.jpeg 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-big-01-mobile.jpg 1200w" sizes="100vw" alt="" class="image"></section>
	<section class="fullwidth-image-block bestbrows-big-01">
		<div class="container">
			<div class="columns">
				<div class="w-row">
					<div class="col w-col w-col-6 w-col-stack"></div>
					<div class="col w-col w-col-6 w-col-stack">
						<div class="margin w-clearfix">
							<h3 class="heading-03">Professzionális, lenyűgöző</h3>
							<div class="paragraph">A <span class="text-logo">Best BrowS PRO</span>fejlesztői csapata külföldi szakemberekkel közreműködve alkotta meg a Brow Henna Tint szemöldökfestékeket, melyek alkalmazásához nincs szükség hozzáadott hidrogén-peroxidra és nem tartalmaz ammóniát, ezáltal a hagyományos szemöldökfestés remek természetesebb alternatíváját kínálja. Ajánlható a szemöldök színének felélénkítésére, hiányos szemöldök korrigálására és a kikopott szemöldöktetoválás frissítésére is. Használatával a mai, divatos púderes és az ombre hatású szemöldökök könnyedén kivitelezhetők.<br></div>
							<a href="#" class="underline-button bestbrows w-inline-block">
								<div class="underline-button-text">Irány a Webshop →</div>
								<div class="link-underline bestbrows"></div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section bestbrows-slider-container">
		<div class="container">
			<div class="columns">
				<div class="margin">
					<div data-delay="4000" data-animation="cross" data-autoplay="1" data-nav-spacing="10" data-duration="1000" data-infinite="1" class="bestbrows-slider w-slider">
						<div class="w-slider-mask">
							<div class="bestbrows-slide-01 w-slide"></div>
							<div class="bestbrows-slide-02 w-slide"></div>
							<div class="bestbrows-slide-03 w-slide"></div>
							<div class="bestbrows-slide-04 w-slide"></div>
						</div>
						<div class="bestbrows-slider-left-arrow w-slider-arrow-left">
							<div class="w-icon-slider-left"></div>
						</div>
						<div class="bestbrows-slider-right-arrow w-slider-arrow-right">
							<div class="w-icon-slider-right"></div>
						</div>
						<div class="bestbrows-slider-nav w-slider-nav w-round"></div>
					</div>
					<style type="text/css" media="screen">
						.bestbrows-slide-01 {
						    background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/bb-slide-01-p-1600.jpeg);
						}
						@media (-webkit-min-device-pixel-ratio: 1.25), (min-resolution: 120dpi) {
							.bestbrows-slide-01 {
							    background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/bb-slide-01.jpg);
							}
						}
						.bestbrows-slide-02 {
						    background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/bb-slide-02-p-1600.jpeg);
						}
						@media (-webkit-min-device-pixel-ratio: 1.25), (min-resolution: 120dpi) {
							.bestbrows-slide-02 {
							    background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/bb-slide-02.jpg);
							}
						}
						.bestbrows-slide-03 {
						    background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/bb-slide-03-p-1600.jpeg);
						}
						@media (-webkit-min-device-pixel-ratio: 1.25), (min-resolution: 120dpi) {
							.bestbrows-slide-03 {
							    background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/bb-slide-03.jpg);
							}
						}
						.bestbrows-slide-04 {
						    background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/bb-slide-04-p-1600.jpeg);
						}
						@media (-webkit-min-device-pixel-ratio: 1.25), (min-resolution: 120dpi) {
							.bestbrows-slide-04 {
							    background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/bb-slide-04.jpg);
							}
						}
					</style>
				</div>
			</div>
		</div>
	</section>
	<section class="section">
		<div class="container">
			<div class="columns">
				<div class="w-row">
					<div class="col w-col w-col-8">
						<div class="margin w-clearfix">
							<h3 class="heading-03"><span class="text-logo-h3">BEST BROWS PRO</span> Oktatási rendszer<br></h3>
							<div class="paragraph">Magyarországon egyedi képzési tematikával oktatjuk a Henna Brow Tint termékek használatát, melyek között mindenki megtalálhatja a számára legmegfelelőbbet.<br></div>
							<div class="paragraph">Online képzés otthonodban a szemöldökfestésről és formázásról ajándék tréningfüzettel vagy választható egy és két napos gyakorlatorientált kiscsoportos tanfolyam cérnás-szemöldökszedési technikával.<br></div>
							<div class="paragraph"><strong>Te melyiket választod?</strong><br></div>
							<a href="#" class="underline-button bestbrows w-inline-block">
								<div class="underline-button-text">Bővebb információ, jelentkezés →</div>
								<div class="link-underline bestbrows"></div>
							</a>
						</div>
					</div>
					<div class="col w-col w-col-4">
						<div class="margin"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/bb-education.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bb-education.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-education.png 800w" sizes="(max-width: 479px) 92vw, (max-width: 767px) 95vw, (max-width: 991px) 29vw, 26vw" alt="" class="image"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth-image-block bestbrows-big-02">
		<div class="container">
			<div class="columns">
				<div class="w-row">
					<div class="col w-col w-col-6 w-col-stack"></div>
					<div class="col w-col w-col-6 w-col-stack">
						<div class="margin w-clearfix">
							<h3 class="heading-03">Shop</h3>
							<div class="paragraph"><span class="text-logo">Brow henna</span> termékcsaládunkat és hozzá minden kiegészítőt, mely a szemöldökfestés és formázás művészetéhez szükséges megvásárolhatod egy helyen. Nálunk!<br></div>
							<a href="#" class="underline-button bestbrows w-inline-block">
								<div class="underline-button-text">Irány a Webshop →</div>
								<div class="link-underline bestbrows"></div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bestbrow-big-image-mobile"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/bb-big-02-mobile.jpg" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bb-big-02-mobile-p-1080.jpeg 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-big-02-mobile.jpg 1200w" sizes="100vw" alt="" class="image"></section>
	<div style="opacity:1;display:block" class="bestbrows-overlay"></div>
</div>

<?php get_footer(); ?>