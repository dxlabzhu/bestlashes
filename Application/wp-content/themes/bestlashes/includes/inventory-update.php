<?php

// Get orders items for inventory after 'DATE'
//add_action( 'init', 'bl_get_order_items_for_inventory' );
function bl_get_order_items_for_inventory(){
	if( isset( $_GET['bl-inv'] ) && $_GET['bl-inv'] == 'EAg4SBkDNpbTDOOf4WAc' ){
		// ezutani orderek kellenek : 2019.01.18. 14:03:02 // direkt 2 mp-vel hamarabbra raktam, hogy ez is beleszámítson (időzónák ??)
		$last_order_timestamp = "2019-01-18 14:03:00";
		$processed_orders = array();

		$args = array(
		    'date_after' => $last_order_timestamp,
		    'posts_per_page' => -1
		);

		$orders = wc_get_orders( $args );

		if( !empty( $orders ) ){
			foreach( $orders as $order ) {
				if( !in_array( $order->get_status(), array( 'failed', 'cancelled' ) ) ){
					$items = $order->get_items();

					foreach( $items as $item ) {
						$product_id = $item->get_product_id();
						$variation_id = $item->get_variation_id();
						$qua = $item->get_quantity();

						if( !empty( $variation_id ) && $variation_id != 0 ){
							$product_id = $variation_id;
						}

						if( empty( $processed_orders[ $product_id ] ) ){
							$processed_orders[ $product_id ] = $qua;
						} else {
							$processed_orders[ $product_id ] += $qua;
						}
					}
				}
			}
		}

		if( !empty( $processed_orders ) ){
			echo '<table>';
			foreach ( $processed_orders as $prod_id => $qua ) {
				echo '<tr>
					<td>'. $prod_id .'</td>
					<td>'. $qua .'</td>
				</tr>';
			}
			echo '</table>';
		}

		die();
	}
}


// Update inventory from bl_inventroy.csv
//add_action( 'init', 'bl_update_inventory' );
function bl_update_inventory(){
	if( isset( $_GET['bl-inventory-update'] ) && $_GET['bl-inventory-update'] == 'vxPK3Pgwokh0qwRapTEA' ){
		$log = array();

		if (($handle = fopen("bl_inventory.csv", "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				$num = count( $data );

				$post_id = (int)$data[0];
				$qua = (int)$data[1];
				
				if( !empty( $data[1] ) ){
					$update_return = update_post_meta( $post_id, '_stock', $qua );
				} else {
					$log[] = 'No quantity: ' . $post_id;
				}
			}
		}

		echo '<pre>';
		var_dump( $log );
		echo '</pre>';

		die();
	}
}