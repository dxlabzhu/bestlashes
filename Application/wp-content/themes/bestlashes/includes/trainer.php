<?php


add_action( 'init', 'bl_register_trainer_cpt' );
function bl_register_trainer_cpt() {
	$labels = array(
		'name' 					=> __( 'Trainers', 'bl' ),
		'singular_name' 		=> __( 'Trainer', 'bl' ),
		'add_new' 				=> __( 'Add New', 'trainer', 'bl' ),
		'add_new_item' 			=> __( 'Add New Trainer', 'bl' ),
		'edit_item' 			=> __( 'Edit Trainer', 'bl' ),
		'new_item' 				=> __( 'New Trainer', 'bl' ),
		'view_item' 			=> __( 'View Trainer', 'bl' ),
		'search_items' 			=> __( 'Search Trainers', 'bl' ),
		'not_found' 			=> __( 'No Trainers found.', 'bl' ),
		'not_found_in_trash' 	=> __( 'No Trainers found in Trash.', 'bl' ),
		'parent_item_colon' 	=> ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'exclude_from_search' => true,
		'rewrite' => array( 'slug' => 'trainer' ),
		'has_archive' => false,
		'menu_position' => 30,
		'menu_icon' => 'dashicons-groups',
		'supports' => array('title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes')
	);
	register_post_type('trainer', $args);
}

	/**************************/
	/*   Taxonomy for CPTs	*/
	/**************************/

add_action( 'init', 'bl_custom_taxonomies_for_trainer', 0 );
function bl_custom_taxonomies_for_trainer() {
	 

///////////////////////////// Trainer categories

	$labels = array(
		'name'							=>  __( 'Categories', 'bl' ),
		'singular_name'					=>  __( 'Category', 'bl' ),
		'search_items'					=>  __( 'Search Categories', 'bl' ),
		'all_items'						=>  __( 'All Categories', 'bl' ),
		'parent_item'					=> null,
		'parent_item_colon'				=> null,
		'edit_item'						=>  __( 'Edit Category', 'bl' ),
		'update_item'					=>  __( 'Update Category', 'bl' ),
		'add_new_item'					=>  __( 'Add New Category', 'bl' ),
		'new_item_name'					=>  __( 'New Category Name', 'bl' ),
		'separate_items_with_commas' 	=>  __( 'Separate categories with commas', 'bl' ),
		'add_or_remove_items'			=>  __( 'Add or remove categories', 'bl' ),
		'choose_from_most_used'			=>  __( 'Choose from the most used categories', 'bl' ),
		'not_found'						=>  __( 'No categories found.', 'bl' ),
		'menu_name'						=>  __( 'Category', 'bl' ),
	);

	$args = array(
		'hierarchical'					=> true,
		'labels'						=> $labels,
		'show_ui'						=> true,
		'show_tagcloud'					=> false,
		'show_admin_column'				=> true,
		'query_var'						=> true,
		'show_in_nav_menus'				=> true,
		'rewrite'						=> array( 'slug' => 'trainer-category' )
	);

	register_taxonomy('trainer_category', 'trainer', $args);

}


	/**************************/
	/*	  Custom Fields	 */
	/**************************/

add_action('add_meta_boxes', 'bl_add_custom_meta_boxes_to_trainer');
function bl_add_custom_meta_boxes_to_trainer() {
	
	// Beállítások
	add_meta_box("trainer_settings", __('Trainer settings', 'bl'), "trainer_settings_meta", "trainer", "normal", "default" );

}


function trainer_settings_meta() {
	global $post;
	$custom = get_post_custom($post -> ID); ?>
	
	<div class="aw-settings-section">
		<div class="menu-wrapper">
			<ul class="menu">
				<li data-menu-item="main-settings" class="active">
					<span><?php _e( 'Main settings', 'bl' ); ?></span>
				</li>
			</ul>
		</div>

		<div class="content-wrapper">
			<div class="content-box main-settings active">
				<h4><?php _e('Short description', 'bl'); ?></h4>
				<div class="text-box short">
					<textarea name="short-description"><?php echo ( isset( $custom['short-description'][0] ) ? $custom['short-description'][0] : '' ); ?></textarea>
				</div>
				<hr>
				<h4><?php _e('City', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="city" value="<?php echo ( isset( $custom['city'][0] ) ? $custom['city'][0] : '' ); ?>" />
				</div>
			</div>
			
		</div>
	</div>

<?php }

///////////////////////////// Save CFs

add_action('save_post', 'bl_trainer_save_custom_postdata');
function bl_trainer_save_custom_postdata($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	global $post;

	if( isset( $_POST['post_type'] ) ){
		if ($_POST['post_type'] == "trainer") {
			
			// Main settings
			update_post_meta( $post->ID, 'short-description', $_POST['short-description'] );
			update_post_meta( $post->ID, 'city', $_POST['city'] );
		}
	}
}