<?php

function bl_get_training_courses_ids( $filter = array() ){
	$training_course_ids = array();
	$args = array();
	$args['post_type'] = array( 'product' );
	$args['posts_per_page'] = '-1';
	$args['post_status'] = 'publish';
	$args['fields'] = 'ids';
	$args['meta_query'][] = array(
		'key' => 'training_course',
		'value' => '1',
		'compare' => '=='
	);

	if( !empty( $filter ) ){
		$args = array_merge( $args, $filter );
	}

	$training_courses_loop = new WP_Query( $args );

	if( $training_courses_loop->have_posts() ){
		while ( $training_courses_loop->have_posts() ) {
			$training_course_ids[] = $training_courses_loop->next_post();
		}
	}
	
	return $training_course_ids;
}


function bl_order_training_courses( $a, $b ){
	if ( $a['timestamp'] == $b['timestamp'] ) {
        return 0;
    }
    return ( $a['timestamp'] < $b['timestamp'] ) ? -1 : 1;
}




/* Admin - add info to training courses name */
add_filter( 'manage_posts_custom_column', 'bl_add_info_to_training_courses_name' );
function bl_add_info_to_training_courses_name( $column_name = '' ){
	
	if( $column_name == 'name' ){
		$training_course = get_post_meta( get_the_ID(), 'training_course', true );

		if( $training_course == '1' ){
			$lang = ICL_LANGUAGE_CODE;
			$training_course_timestamp = get_post_meta( get_the_ID(), 'training_course_date_time', true );
			$training_course_formatted_date_time = date_i18n( 'Y.m.d. H:i', $training_course_timestamp );
			$city = get_post_meta( get_the_ID(), 'training_course_city', true );
			if( !empty( $lang ) ){
				$full_price = get_post_meta( get_the_ID(), 'training_course_full_price_' . $lang, true );
				$full_price_formatted = strip_tags( wc_price( $full_price ) );
			}

			echo '<div class="training-courses-info">' . $training_course_formatted_date_time . ' - ' . $city . ' - ' . $full_price_formatted . '</div>';
		}
	}
}


add_action( 'bl_woocommerce_email_order_item_meta_data', 'bl_woocommerce_email_training_courses_meta_data_to_admin', 10, 5 );
function bl_woocommerce_email_training_courses_meta_data_to_admin( $item_id = 0, $item = array(), $order = array(), $plain_text = false, $sent_to_admin = true ){
	if( !empty( $item ) && $sent_to_admin ){
		$meta_string = '';
		$training_course = get_post_meta( $item->get_product_id(), 'training_course', true );

		if( $training_course == '1' ){
			$training_course_timestamp = get_post_meta( $item->get_product_id(), 'training_course_date_time', true );
			$training_course_formatted_date_time = date_i18n( 'Y.m.d. H:i', $training_course_timestamp );
			$city = get_post_meta( $item->get_product_id(), 'training_course_city', true );

			$meta_string = '<ul class="wc-item-meta">';

			$meta_string .= '<li><strong class="wc-item-meta-label" style="float: left; margin-right: .25em; clear: both">'. __('Date/Time', 'bl') .':</strong> '. $training_course_formatted_date_time .'</li>';
			$meta_string .= '<li><strong class="wc-item-meta-label" style="float: left; margin-right: .25em; clear: both">'. __('City', 'bl') .':</strong> '. $city .'</li>';

			$meta_string .= '</ul>';
		}

		echo $meta_string;
	}
}