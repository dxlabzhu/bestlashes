<?php

// Header scripts
add_action('wp_head', 'bl_header_scripts');
function bl_header_scripts(){ ?>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">
        ! function(o, c) {
            var n = c.documentElement,
                t = " w-mod-";
            n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
        }(window, document);
    </script>
<?php }



add_filter( 'wp_nav_menu_items', 'bl_extra_menu_items', 10, 2 );
function bl_extra_menu_items( $items, $args ) {
    global $woocommerce;
    
    if( $args->theme_location == 'main_menu' ){
        
        // Menu space
        $items .= '<div class="menu-space"></div>';

        // Logos
        /*$items .= '<a href="#" class="nav-item brands w-inline-block">
                    <img src="'. get_stylesheet_directory_uri() .'/images/logo-dorcay_1.svg" class="logo-dorcay" alt="">
                    <div class="link-underline"></div>
                </a>';*/

        if( ICL_LANGUAGE_CODE == 'hu' && BL_LANG == 'HU' ){
//            $items .= '<a href="'. get_permalink( BL_PAGE_LASH_INC_MAGAZINE ) .'" class="nav-item brands w-inline-block">
//                        <img src="'. get_stylesheet_directory_uri() .'/images/logo-lashinc.svg" class="logo-lash-inc" alt="">
//                        <div class="link-underline"></div>
//                    </a>';
        }

        // Menu space
        $items .= '<div class="menu-space"></div>';

        // Search
        $items .= '<a href="#" class="nav-item-search w-inline-block open-seach-popup only-thin-mobile">
                    <img src="'. get_stylesheet_directory_uri() .'/images/icon-search.svg" alt="" class="icon-login" alt="' . __('Search', 'bl') . '">
                </a>';

        // Login
        if( is_user_logged_in() ){
            $items .= '<a href="'. get_permalink( wc_get_page_id( 'myaccount' ) ) .'" class="nav-item-login w-inline-block only-thin-mobile">
                    <img src="'. get_stylesheet_directory_uri() .'/images/icon-login.svg" class="icon-login" alt="' . __('Profile', 'bl') . '">
                    '. __( 'Profile', 'bl' ) .'
                </a>';
        } else {
            $items .= '<a href="'. get_permalink( BL_PAGE_LOGIN ) .'" class="nav-item-login w-inline-block only-thin-mobile">
                    <img src="'. get_stylesheet_directory_uri() .'/images/icon-login.svg" class="icon-login" alt="' . __('Log in', 'bl') . '">
                    '. __( 'Log in', 'bl' ) .'
                </a>';
        }

        // Currency switcher
        ob_start();
        dynamic_sidebar('header-currency-switcher');
        $items .= ob_get_clean();

        // Basket
        ob_start();
        $eclass_for_minicart = array( 'only-thin-mobile' );
        wc_get_template( 'minicart.php', array( 'eclass_for_minicart' => $eclass_for_minicart ) );
        $items .= ob_get_clean();

        // Language
        if( BL_LANG == 'HU' ){
            if( function_exists( 'icl_get_languages' ) ){
                $langs = icl_get_languages('skip_missing=0');

                if( !empty( $langs ) ){
                    $items .= '<div class="nav-item-lang only-thin-mobile">';

                    // Current lang
                    foreach ( $langs as $lang_key => $lang ) {
                        if( $lang_key == ICL_LANGUAGE_CODE ){
                            $items .= '<a href="'. $lang['url'] .'" class="nav-item-lang-item w-inline-block">
                                        <div class="menu-item-small-text">'. $lang['code'] .'</div>
                                    </a>';
                        }
                    }

                    // Other lang
                    foreach ( $langs as $lang_key => $lang ) {
                        if( $lang_key != ICL_LANGUAGE_CODE ){
                            $items .= '<a href="'. $lang['url'] .'" class="nav-item-lang-item w-inline-block">
                                        <div class="menu-item-small-text">'. $lang['code'] .'</div>
                                    </a>';
                        }
                    }

                    $items .= '</div>';
                }
            }
        }
    }

    return $items;
}

add_action( 'bl_after_header_menu', 'bl_add_extra_buttons_after_header_menu' );
function bl_add_extra_buttons_after_header_menu(){
    $second_menu_buttons = '';

    $second_menu_buttons = '<div class="menu-container second-menu">';

    // Search
    $second_menu_buttons .= '<a href="#" class="nav-item-search w-inline-block open-seach-popup">
                <img src="'. get_stylesheet_directory_uri() .'/images/icon-search.svg" alt="" class="icon-login" alt="' . __('Search', 'bl') . '">
            </a>';

    // Login
    if( is_user_logged_in() ){
        $second_menu_buttons .= '<a href="'. get_permalink( wc_get_page_id( 'myaccount' ) ) .'" class="nav-item-login w-inline-block">
                <img src="'. get_stylesheet_directory_uri() .'/images/icon-login.svg" class="icon-login" alt="' . __('Profile', 'bl') . '">
                '. __( 'Profile', 'bl' ) .'
            </a>';
    } else {
        $second_menu_buttons .= '<a href="'. get_permalink( BL_PAGE_LOGIN ) .'" class="nav-item-login w-inline-block">
                <img src="'. get_stylesheet_directory_uri() .'/images/icon-login.svg" class="icon-login" alt="' . __('Log in', 'bl') . '">
                '. __( 'Log in', 'bl' ) .'
            </a>';
    }

    // Currency switcher
    ob_start();
    dynamic_sidebar('header-currency-switcher');
    $second_menu_buttons .= ob_get_clean();

    // Basket
    ob_start();
    wc_get_template( 'minicart.php' );
    $second_menu_buttons .= ob_get_clean();

    // Language
    if( BL_LANG == 'HU' ){
        if( function_exists( 'icl_get_languages' ) ){
            $langs = icl_get_languages('skip_missing=0');

            if( !empty( $langs ) ){
                $second_menu_buttons .= '<div class="nav-item-lang">';

                // Current lang
                foreach ( $langs as $lang_key => $lang ) {
                    if( $lang_key == ICL_LANGUAGE_CODE ){
                        $second_menu_buttons .= '<a href="'. $lang['url'] .'" class="nav-item-lang-item w-inline-block">
                                    <div class="menu-item-small-text">'. $lang['code'] .'</div>
                                </a>';
                    }
                }

                // Other lang
                foreach ( $langs as $lang_key => $lang ) {
                    if( $lang_key != ICL_LANGUAGE_CODE ){
                        $second_menu_buttons .= '<a href="'. $lang['url'] .'" class="nav-item-lang-item w-inline-block">
                                    <div class="menu-item-small-text">'. $lang['code'] .'</div>
                                </a>';
                    }
                }

                $second_menu_buttons .= '</div>';
            }
        }
    }

    $second_menu_buttons .= '</div>';

    echo $second_menu_buttons;
}

add_action( 'bl_after_header_close', 'bl_right_side_menu' );
function bl_right_side_menu(){ ?>
    <div class="right-side-menu">
        <?php
        $social_facebook_link = get_option('bl-right-side-menu-facebook-link');
        if( !empty( $social_facebook_link ) ){ ?>
            <a href="<?php echo $social_facebook_link; ?>" target="_blank" class="nav-item-circle w-inline-block">
                <div class="fa-brands"></div>
            </a>
        <?php } ?>

        <?php
        $social_instagram_link = get_option('bl-right-side-menu-instagram-link');
        if( !empty( $social_instagram_link ) ){ ?>
            <a href="<?php echo $social_instagram_link; ?>" target="_blank" class="nav-item-circle w-inline-block">
                <div class="fa-brands"></div>
            </a>
        <?php } ?>

        <?php
        $social_email = get_option('bl-right-side-menu-email');
        if( !empty( $social_email ) ){ ?>
            <a href="mailto:<?php echo $social_email ?>" class="nav-item-circle w-inline-block">
                <div class="fa-solid"></div>
            </a>
        <?php } ?>
    </div>
<?php }


add_action('init', 'bl_add_search_popup_to_footer');
function bl_add_search_popup_to_footer(){
    add_action( 'wp_footer', 'bl_search_popup', 1000 );
}