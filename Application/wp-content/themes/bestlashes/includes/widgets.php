<?php

add_action('widgets_init', 'bl_widgets_init');

function bl_widgets_init() {
	
	// Sidebar
	register_widget('BestLashesRelatedPostAndProducts');

	// Footer - right
	register_widget('BestLashesNewsletterSubscription');
}



	/************************************************/
	/*		Related posts and products				*/
	/************************************************/
	
class BestLashesRelatedPostAndProducts extends WP_Widget
{
	function __construct(){
		$widget_ops = array( 'description' => __('Bastleshes related posts and products', 'bl') );
		$control_ops = array( 'width' => 300, 'height' => 100 );
		parent::__construct( false, $name = __('Bastleshes related posts and products', 'bl'), $widget_ops, $control_ops );
	}

	/* Displays the Widget in the front-end */
	function widget( $args, $instance ){
		extract($args);
		global $post;

		if( $post->post_type == 'post' ){
			$related_posts = maybe_unserialize( get_post_meta( $post->ID, 'related_posts', true ) );
			$related_products = maybe_unserialize( get_post_meta( $post->ID, 'related_products', true ) ); 

			if( empty( $related_posts ) ){
				$related_posts = array();

				$args = array();
				$args['post_type'] = 'post';
				$args['posts_per_page'] = BL_SINGLE_POST_RELATED_POST_LIMIT;
				$args['post_status'] = 'publish';
				$args['post__not_in'] = array( $post->ID );
				$args['fields'] = 'ids';

				$posts_loop = new WP_Query( $args );

				if( $posts_loop->have_posts() ){
					while ( $posts_loop->have_posts() ) {
						$post_id = $posts_loop->next_post();

						$related_posts[] = $post_id;
					}
				}
			} 

			if( empty( $related_products ) ){
				$related_products = array();
				
				$args = array();
				$args['post_type'] = 'product';
				$args['posts_per_page'] = BL_SINGLE_POST_RELATED_PRODUCT_LIMIT;
				$args['post_status'] = 'publish';
				$args['orderby'] = 'rand';
				$args['fields'] = 'ids';
				$args['post__not_in'] = bl_get_training_courses_ids();

				$products_loop = new WP_Query( $args );

				if( $products_loop->have_posts() ){
					while ( $products_loop->have_posts() ) {
						$post_id = $products_loop->next_post();

						$related_products[] = $post_id;
					}
				}
			} ?>
		
			<div class="related">
				<?php if( !empty( $related_posts ) ){ ?>

					<div class="related-margin">
						<div class="related-blogpost-heading">
							<div><?php _e( 'Related posts', 'bl' ); ?></div>
						</div>
						<?php foreach ( $related_posts as $post_id ) {
							if( !empty( $post_id ) ){ ?>
								<a href="<?php echo get_permalink( $post_id ); ?>" class="blog-related w-inline-block">
									<div class="related-blogpost-title"><?php echo get_the_title( $post_id ); ?></div>
									<h4 class="related-blogpost-date"><?php echo get_the_time( 'Y | F | d', $post_id ); ?></h4>
								</a>
							<?php }
						} ?>
					</div>

				<?php } ?>

				<?php if( !empty( $related_products ) ){ ?>
					
					<div class="related-products">
						<div class="related-margin">
							<div class="related-blogpost-heading">
								<div><?php _e( 'Related products', 'bl' ); ?></div>
							</div>
						</div>

						<?php foreach ( $related_products as $prod_id ) {
							if( !empty( $prod_id ) ){
								$_product = wc_get_product( $prod_id );

								// Image
								$image = $_product->get_image( 'product-featured', array( 'class' => 'product-image' ) );
								
								// Price
								$prices = bl_get_product_prices( $_product );

								// Label
								$label_text = get_post_meta( $prod_id, 'product-label-text', true );
								
								if( empty( $label_text ) && $_product->is_on_sale() ){
									if( !empty( $prices['gross_regular_price'] ) ){
										$price_percentage = round( ( 1 - ( $prices['gross_price'] / $prices['gross_regular_price'] ) ) * 100 );

										$label_text = -1 * abs( $price_percentage ) . '%';
									}
								}

								$container_classes = ''; ?>
								
								<a href="#" class="productlist-item w-inline-block">
									<div class="productlist-image-container">
										<?php echo $image; ?>
									</div>
									<?php if( !empty( $label_text ) ){ ?>
										<div class="productlist-stamp">
											<strong class="bold-text"><?php echo $label_text; ?></strong>
										</div>
									<?php } ?>
									<div class="productlist-detail-block shoplist">
										<div class="productlist-description-block shoplist">
											<div class="productlist-text"><?php echo get_the_title( $prod_id ); ?></div>
										</div>
										<div class="productlist-price-block shoplist">
											<div class="product-price">
												<?php if( $_product->is_on_sale() ) { ?>
													<span class="new-price"><?php echo $prices['gross_price_formatted']; ?><br></span>
													<span class="old-price"><?php echo $prices['gross_regular_price_formatted']; ?></span>
												<?php } else {
													echo $prices['gross_price_formatted'];
												} ?>
											</div>
										</div>
									</div>
								</a>
								
							<?php }
						} ?>
						
					</div>

				<?php } ?>
				
			</div>

		<?php } ?>

		<?php $GLOBALS['widget_number']++;
	}

	/*Saves the settings. */
	function update( $new_instance, $old_instance ){
		
		$instance = $old_instance;	
		return $instance;
	}
	
	 
	/*Creates the form for the widget in the back-end. */
	function form( $instance ){

	}
}



	/************************************************/
	/*		Newsletter subscription 				*/
	/************************************************/
	
class BestLashesNewsletterSubscription extends WP_Widget
{
	function __construct(){
		$widget_ops = array( 'description' => __('Bastleshes newsletter subscription', 'bl') );
		$control_ops = array( 'width' => 300, 'height' => 100 );
		parent::__construct( false, $name = __('Bastleshes newsletter subscription', 'bl'), $widget_ops, $control_ops );
	}

	/* Displays the Widget in the front-end */
	function widget( $args, $instance ){
		extract($args);
		$title = apply_filters( 'widget_title', bl_get_option_lang( 'bl-newsletter-subscription-title' ) );
		$before_content = do_shortcode( nl2br( bl_get_option_lang( 'bl-before-newsletter-subscription-content' ) ) );

		echo $args['before_widget'];

		if ( ! empty( $title ) ){
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>

		<p class="footer-paragraph"><?php echo $before_content; ?></p>
        <div class="form-block w-form">
            <form action="" method="post" accept-charset="utf-8" class="form newsletter-form <?php echo ( BL_LANG == 'HU' ? 'with-topic' : '' ); ?>" data-error-text-all-field-required="<?php _e( 'All fields are required', 'bl' ); ?>">
            	<div class="error-text" style="display: none;">
					<div class="table-column _100percent">
						<div class="text"></div>
					</div>
				</div>
            	<div class="terms-and-condition">
            		<label>
            			<input type="checkbox" name="terms-and-condition" value="1" class="checkbox w-checkbox-input terms-and-condition req" data-error-text-terms-and-condition="<?php _e('Please accept the Terms and Conditions', 'bl'); ?>">
                        <?php $lang = ICL_LANGUAGE_CODE; ?>
                        <?php if ($lang == 'hu') : ?>
                            <?php echo sprintf( __( 'I have read and agree to the %1$sPrivacy policy%2$s', 'bl' ), '<a href="'. get_permalink( get_option( 'wp_page_for_privacy_policy' ) ) .'">', '</a>'  ); ?>
                        <?php elseif ($lang == 'en') : ?>
                            <?php echo sprintf( __( 'I have read and agree to the %1$sPrivacy policy%2$s', 'bl' ), '<a href="'. get_permalink( 5384 ) .'">', '</a>'  ); ?>
                        <?php endif; ?></label>
            	</div>
                <input type="text" class="footer-newsletter-field w-input newsletter-name req" maxlength="256" name="newsletter-name" placeholder="<?php _e( 'Name', 'bl' ); ?>" id="name">
                <input type="text" class="footer-newsletter-field w-input newsletter-email req" maxlength="256" name="newsletter-email" placeholder="<?php _e( 'E-mail address', 'bl' ); ?>" id="email" data-error-text-not-valid="<?php _e( 'Not valid email address', 'bl' ); ?>">
                <?php if( BL_LANG == 'HU' ){ ?>
	                <select name="newsletter-topic" data-placeholder="<?php _e( 'Which topic are you interested in?', 'bl' ); ?>" class="footer-newsletter-field newsletter-topic w-input req">
	                	<option disabled selected><?php _e( 'Which topic are you interested in?', 'bl' ); ?></option>
	                	<option value="<?php echo MARKETER_LIST_BESTLASHES_ID; ?>"><?php _e( 'Eyelash Extension', 'bl' ); ?></option>
	                	<option value="<?php echo MARKETER_LIST_BESTBROWS_ID; ?>"><?php _e( 'Brow Henna', 'bl' ); ?></option>
	                	<option value="<?php echo MARKETER_LIST_BESTLASHES_ID . ',' . MARKETER_LIST_BESTBROWS_ID; ?>"><?php _e( "Eyelash Extension & Brow Henna", 'bl' ); ?></option>
	                </select>
	            <?php } else { ?>
					<input type="hidden" name="newsletter-topic" value="1">
	            <?php } ?>
                <a href="#" class="round-button w-button submit-newsletter-subscription">→</a>
            </form>
        </div>

		<?php echo $args['after_widget'];


		$GLOBALS['widget_number']++;
	}

	/* Saves the settings. */
	function update( $new_instance, $old_instance ){
		return $instance;
	}
	
	 
	/* Creates the form for the widget in the back-end. */
	function form( $instance ){
	
	}
}