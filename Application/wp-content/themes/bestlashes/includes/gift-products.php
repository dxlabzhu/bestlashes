<?php

// Add gift product to cart on visit depending on cart total value
add_action( 'wp', 'bl_add_or_remove_gift_products_to_cart' );
if( !function_exists('bl_add_or_remove_gift_products_to_cart') ){
    function bl_add_or_remove_gift_products_to_cart() {
        global $woocommerce;
        
        if( !is_admin() ){
	        $lang = ICL_LANGUAGE_CODE;
	        $current_time = current_time( 'timestamp' );
	        $gift_product_enabled = bl_get_option_lang('bl-enable-gift-products', $lang );
	        $gift_product_from = bl_get_option_lang('bl-gift-product-from', $lang );
    		$gift_product_to = bl_get_option_lang('bl-gift-product-to', $lang );
	        $cart_total_for_check = $woocommerce->cart->get_cart_contents_total() + $woocommerce->cart->get_cart_contents_tax();
	        $expired_from = false;
	        $expired_to = false;
	        $expired = false;
	        $notice_discounts = array();

	        // Expired
	        if( !empty( $gift_product_from ) ){
	        	if( $gift_product_from > $current_time ){
	        		$expired_from = true;
	        	}
	        }
	        if( !empty( $gift_product_to ) ){
	        	if( $gift_product_to < $current_time ){
	        		$expired_to = true;
	        	}
	        }
	        if( $expired_to || $expired_from ) {
	        	$expired = true;
	        }

	        if( $gift_product_enabled == '1' ){
	        	if( !$expired ){
		        	$gift_products_discount = maybe_unserialize( bl_get_option_lang('bl-gift-products-discount', $lang ) );

		        	if( !empty( $gift_products_discount ) ){
		        		foreach ( $gift_products_discount as $discount ) {
		        			if( $cart_total_for_check >= $discount['limit'] ){
		        				$gift_product_already_in_cart = false;
		        				
		        				if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
		        					foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {

		        						// If gift product
		        						if( isset( $values['gift-product'] ) ){
			        						if( $values['gift-product'] == true ){
			                                    if ( $values['product_id'] == $discount['product'] ){
													if( !empty( $discount['variation'] ) ){
			                                    		if( $values['variation_id'] == $discount['variation'] ){
			                                    			$gift_product_already_in_cart = true;
			                                    		}
			                                    	} else {
		                                        		$gift_product_already_in_cart = true;
			                                        }
			                                    }
			                                }
			                            }
	                                }

	                                // if product not found, add it
	                                if ( ! $gift_product_already_in_cart ){
	                                	$cart_item_data = array(
	                                		'gift-product' => true
	                                	);

	                                    if( empty( $discount['variation'] ) ){
		                                    $woocommerce->cart->add_to_cart( $discount['product'], 1, '', '', $cart_item_data );
		                                } else {
		                                	$variation = wc_get_product( $discount['variation'] );
		                                	$variation_attributes = $variation->get_variation_attributes();
		                                	
		                                	$woocommerce->cart->add_to_cart( $discount['product'], 1, $discount['variation'], $variation_attributes, $cart_item_data );
		                                }
	                                }
	                            }
		        			} else {
		        				// Remove the gift products
		        				if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
		        					$found_cart_item_key = '';

		        					foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $value ) {

		        						// If gift product
		        						if( $value['gift-product'] == true ){
		                                    if ( $value['product_id'] == $discount['product'] ){
		                                    	if( !empty( $discount['variation'] ) ){
		                                    		if( $value['variation_id'] == $discount['variation'] ){
		                                    			$gift_product_already_in_cart = true;
		                                    			$found_cart_item_key = $cart_item_key;
		                                    		}
		                                    	} else {
		                                        	$gift_product_already_in_cart = true;
	                                    			$found_cart_item_key = $cart_item_key;
		                                        }
		                                    }
		                                }
	                                }

	                                if( $gift_product_already_in_cart && !empty( $found_cart_item_key ) ){
	                                	$woocommerce->cart->set_quantity( $found_cart_item_key, 0 );
	                                }
		        				}

		        				// Add to notice discounts
		        				$notice_discounts[] = $discount;
		        			}
		        		}
		        	}
		        }
	        }

	        // Remove expired gift products
	        if( $expired || $gift_product_enabled != '1' ){
				foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $value ) {
		        	if( isset( $value['gift-product'] ) ){
		        		if( $value['gift-product'] == true ){
		        			$woocommerce->cart->set_quantity( $cart_item_key, 0 );
		        		}
		        	}
	            }
	        }

	        // Notice
	        if( $gift_product_enabled == '1' ){
		        if( !$expired && $gift_product_enabled == '1' ){
			        if( is_cart() || is_checkout() && !empty( $notice_discounts ) ){
			        	$notice_discount = array();
			        	$lowest_discount_price = 0;

			        	// Find the closest discount
			        	foreach ( $notice_discounts as $discount ) {
			        		if( empty( $notice_discount ) ){
			        			$notice_discount = $discount;
			        		} else {
			        			if( $discount['limit'] < $notice_discount['limit'] ){
			        				$notice_discount = $discount;
			        			}
			        		}
			        	}

			        	if( !empty( $notice_discount ) ){
			        		$difference = $notice_discount['limit'] - $cart_total_for_check;
			        		$difference_text = strip_tags( wc_price( $difference ) );
			        		
			        		wc_add_notice( sprintf( __('Purchase products for %1$s more and get %2$s for free!', 'bl'), $difference_text, get_the_title( $notice_discount['product'] ) ), 'notice' );
			        	}
			        }
			    }
			}
	    }
    }
}

// Edit gift product price
add_action( 'woocommerce_before_calculate_totals', 'bl_add_custom_price_to_gift_products' );
if( !function_exists('bl_add_custom_price_to_gift_products') ){
    function bl_add_custom_price_to_gift_products( $cart_object ) {
    	$custom_price = 0;

    	if( !is_admin() ){
	        $lang = ICL_LANGUAGE_CODE;

	        foreach ( $cart_object->cart_contents as $key => $value ) {
	        	if( isset( $value['gift-product'] ) ){
	        		if( $value['gift-product'] == true ){
	        			$value['data']->set_price( $custom_price );
	        			$value['data']->set_sale_price( $custom_price );
	        			$value['data']->set_regular_price( $custom_price );
	        		}
	        	}
            }
	    }
    }
}