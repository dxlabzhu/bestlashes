<?php

// Footer scirpts
add_action('wp_footer', 'bl_footer_scripts');
function bl_footer_scripts(){ ?>
	<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
<?php }


// Cookie warning
add_action( 'wp_footer', 'bl_add_cookie_warning_to_footer', 1000 );
function bl_add_cookie_warning_to_footer(){ ?>
	<div class="cookies">
        <div class="small-capital-text">
            <?php _e( 'Our site uses cookies.', 'bl' ); ?>
            <?php $lang = ICL_LANGUAGE_CODE; ?>
            <?php if ($lang == 'hu') : ?>
                <a href="<?php echo get_permalink( get_option( 'wp_page_for_privacy_policy' ) ); ?>" class="cookes-link privacy-policy"><?php _e( 'Why?', 'bl' ); ?></a>
            <?php elseif ($lang == 'en') : ?>
                <a href="<?php echo get_permalink( 5384 ); ?>" class="cookes-link privacy-policy"><?php _e( 'Why?', 'bl' ); ?></a>
            <?php endif; ?>
        </div>
        <a href="#" class="fa-solid cokkies"></a>
        <div class="small-capital-text">
            <a href="#" class="cookes-link"><?php _e( 'accept', 'bl' ); ?></a>
        </div>
    </div>
<?php }