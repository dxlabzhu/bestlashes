<?php

add_action( 'woocommerce_pos_head', 'bl_pos_head', 100 );
function bl_pos_head(){
	if( is_pos( 'template' ) ){
		// CSS
		echo '<link rel="stylesheet" href="'. get_template_directory_uri() .'/css/normalize.css" type="text/css" media="all">';
		echo '<link rel="stylesheet" href="'. get_template_directory_uri() .'/css/components.css" type="text/css" media="all">';
		echo '<link rel="stylesheet" href="'. get_template_directory_uri() .'/css/best-lashes.css" type="text/css" media="all">';
		echo '<link rel="stylesheet" href="'. get_template_directory_uri() .'/css/pos.css" type="text/css" media="all">';
	}
}

add_action( 'woocommerce_pos_footer', 'bl_pos_footer', 100 );
function bl_pos_footer(){
	if( is_pos( 'template' ) ){
		// JS
		//wp_enqueue_script( 'pos-js', get_stylesheet_directory_uri() . '/js/pos.js', array( 'jquery' ), '1.0', true );
		echo '<script src="'. get_template_directory_uri() .'/js/pos.js"></script>';
	}
}

// Only for dev state - regenerate scss
add_action('init', 'bl_pos_regenerate_scss_for_dev_server');
function bl_pos_regenerate_scss_for_dev_server(){
	if( is_dev_server() ){
		if( strpos( $_SERVER['REQUEST_URI'], 'pos') !== false ){
			add_action( 'wp_loaded', 'wp_scss_needs_compiling' );
		}
	}
}


// New order - stock log
add_action('woocommerce_new_order', 'bl_pos_new_order_save_stock_log');
function bl_pos_new_order_save_stock_log( $order_id ){
	/*$order = wc_get_order( $order_id );
	var_dump( $order->get_items() );*/
	/*$orders_products = $wpdb->get_row(
		$wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_order_items WHERE order_item_type = %s AND order_id = %d",
			'line_item',
			absint( $order_id )
		)
	);*/

	/*foreach ( $order->get_items() as $items_key => $item_product ) { 
		$product = $item_product->get_product();
		$order_quantity = $item_product->get_quantity();
		$old_stock = $product->get_stock_quantity();
		$new_stock = $old_stock - $order_quantity;
	}



	bl_product_stock_change( $product, (int)$old_stock, (int)$new_stock, 'pos-new-order' );*/
}

// Stock log
add_action( 'woocommerce_order_status_completed', 'bl_pos_set_order_stock', 10, 1 );
function bl_pos_set_order_stock( $order_id ){
	if ( ! $order_id )
		return;

	$order = wc_get_order( $order_id );
	$payment_gateway = wc_get_payment_gateway_by_order( $order );
	$is_pos_order = get_post_meta( $order_id, '_pos', true );
	$stock_logged = get_post_meta( $order_id, '_stock_logged', true );

	if( $is_pos_order == '1' && $stock_logged != '1' ){

		if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
			return;
		}

		foreach ( $order->get_items() as $item ) {

			if ( $item['product_id'] > 0 ) {
				$_product = $order->get_product_from_item( $item );

				if ( $_product && $_product->exists() && $_product->managing_stock() ) {

					$new_quantity = $_product->stock;

					$qty = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $order, $item );

					$old_stock = $new_quantity + $qty;

					// Log stock
					bl_product_stock_change( $_product, (int)$old_stock, (int)$new_quantity, 'pos-new-order - ' . $order_id );

					// Set status
					update_post_meta( $order_id, '_stock_logged', '1' );
				}
			}
		}
	}
}

// Disable Training courses for pos
//add_action( 'pre_get_posts', 'bl_pos_remove_training_courses_from_product_list' );
function bl_pos_remove_training_courses_from_product_list( $query ){
	
	if ( ( defined('DOING_AJAX') && DOING_AJAX && $_SERVER['HTTP_REFERER'] == home_url( 'pos/' ) ) || $_SERVER['REQUEST_URI'] == '/pos/' ){
		if ( $query->get('post_type') == array( 'product' ) || $query->get('post_type') == array( 'bb_device' ) || $query->get('post_type') == 'product' ) {
	        $meta_query = array(
	        	'relation' => 'OR',
				array(
					'key' => 'training_course',
					'value' => '1',
					'compare' => '!=',
				),
				array(
					'key' => 'training_course',
					'compare' => 'NOT EXISTS',
				),
			);
			$query->set('meta_query',$meta_query);
	    }
	}
}


// Set Default Customer for order
add_action( 'woocommerce_order_status_completed', 'bl_pos_set_default_user_for_order', 99, 1 );
function bl_pos_set_default_user_for_order( $order_id ){
	if ( ! $order_id )
		return;

	$order = wc_get_order( $order_id );
	$is_pos_order = get_post_meta( $order_id, '_pos', true );
	
	if( $is_pos_order == '1' && ( $order->get_payment_method() == 'pos_cash' || $order->get_payment_method() == 'pos_card' ) ){
		if( $order->get_customer_id() == 0 ){
			// User id
			$order->set_customer_id( BL_POS_DEFAULT_USER_ID );

			update_post_meta( $order->get_id(), '_customer_user', BL_POS_DEFAULT_USER_ID );

			// Billing info
			update_post_meta( $order->get_id(), '_billing_first_name', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_first_name', true ) );
			update_post_meta( $order->get_id(), '_billing_last_name', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_last_name', true ) );
			update_post_meta( $order->get_id(), '_billing_company', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_company', true ) );
			update_post_meta( $order->get_id(), '_billing_address_1', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_address_1', true ) );
			update_post_meta( $order->get_id(), '_billing_address_2', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_address_2', true ) );
			update_post_meta( $order->get_id(), '_billing_city', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_city', true ) );
			update_post_meta( $order->get_id(), '_billing_state', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_state', true ) );
			update_post_meta( $order->get_id(), '_billing_postcode', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_postcode', true ) );
			update_post_meta( $order->get_id(), '_billing_country', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_country', true ) );
			update_post_meta( $order->get_id(), '_billing_email', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_email', true ) );
			update_post_meta( $order->get_id(), '_billing_phone', get_user_meta( BL_POS_DEFAULT_USER_ID, 'billing_phone', true ) );

			// Shipping info
			update_post_meta( $order->get_id(), '_shipping_first_name', get_user_meta( BL_POS_DEFAULT_USER_ID, 'shipping_first_name', true ) );
			update_post_meta( $order->get_id(), '_shipping_last_name', get_user_meta( BL_POS_DEFAULT_USER_ID, 'shipping_last_name', true ) );
			update_post_meta( $order->get_id(), '_shipping_company', get_user_meta( BL_POS_DEFAULT_USER_ID, 'shipping_company', true ) );
			update_post_meta( $order->get_id(), '_shipping_address_1', get_user_meta( BL_POS_DEFAULT_USER_ID, 'shipping_address_1', true ) );
			update_post_meta( $order->get_id(), '_shipping_address_2', get_user_meta( BL_POS_DEFAULT_USER_ID, 'shipping_address_2', true ) );
			update_post_meta( $order->get_id(), '_shipping_city', get_user_meta( BL_POS_DEFAULT_USER_ID, 'shipping_city', true ) );
			update_post_meta( $order->get_id(), '_shipping_state', get_user_meta( BL_POS_DEFAULT_USER_ID, 'shipping_state', true ) );
			update_post_meta( $order->get_id(), '_shipping_postcode', get_user_meta( BL_POS_DEFAULT_USER_ID, 'shipping_postcode', true ) );
			update_post_meta( $order->get_id(), '_shipping_country', get_user_meta( BL_POS_DEFAULT_USER_ID, 'shipping_country', true ) );
		} else {
			$tax_number = get_user_meta( $order->get_customer_id(), 'tax-number', true );

			if( !empty( $tax_number ) ){
				update_post_meta( $order->get_id(), '_billing_tax_number', $tax_number );
			}
		}
	}
}


// Add tax number for user
add_filter( 'bl_pos_customer_billing_fields', 'bl_pos_add_tax_number_to_billing_infos' );
function bl_pos_add_tax_number_to_billing_infos( $billing_fields = array() ){

	$billing_fields['tax_number'] = array('label' => __( 'Tax number', 'bl' ));

	return $billing_fields;
}

// Save user tax number for meta field
add_action( 'bl_pos_save_user_meta_fields', 'bl_pos_save_user_tax_number', 10, 2 );
function bl_pos_save_user_tax_number( $user_data, $request ){
	if( !empty( $request['billing_address']['tax_number'] ) && !empty( $user_data ) ){
		update_user_meta( $user_data->ID, 'tax-number', $request['billing_address']['tax_number'] );
	}
}

// Save user tax number after ajax created the user
add_action( 'wp_ajax_bl_pos_save_tax_number_to_user', 'bl_pos_save_tax_number_to_user' );
function bl_pos_save_tax_number_to_user(){
	if ( !empty( $_POST['user_id'] ) && !empty( $_POST['tax_number'] ) ) {
		update_user_meta( $_POST['user_id'], 'tax-number', $_POST['tax_number'] );
	}

	return wp_send_json( array( 'success' => true ) );
}

// Fix user for new order
add_action( 'wp_ajax_bl_pos_fix_user_on_new_order', 'bl_pos_fix_user_on_new_order' );
function bl_pos_fix_user_on_new_order(){
	if ( !empty( $_POST['user_id'] ) && !empty( $_POST['order_id'] ) ) {
		$order_id = $_POST['order_id'];
		$new_user_id = $_POST['user_id'];

		$order = wc_get_order( $order_id );
		$is_pos_order = get_post_meta( $order_id, '_pos', true );
		
		if( $is_pos_order == '1' && ( $order->get_payment_method() == 'pos_cash' || $order->get_payment_method() == 'pos_card' ) ){
			if( $order->get_customer_id() == 0 || $order->get_customer_id() == BL_POS_DEFAULT_USER_ID ){ // No user or default pos user
				// User id
				$order->set_customer_id( $new_user_id );

				update_post_meta( $order->get_id(), '_customer_user', $new_user_id );

				// Billing info
				update_post_meta( $order->get_id(), '_billing_first_name', get_user_meta( $new_user_id, 'billing_first_name', true ) );
				update_post_meta( $order->get_id(), '_billing_last_name', get_user_meta( $new_user_id, 'billing_last_name', true ) );
				update_post_meta( $order->get_id(), '_billing_company', get_user_meta( $new_user_id, 'billing_company', true ) );
				update_post_meta( $order->get_id(), '_billing_address_1', get_user_meta( $new_user_id, 'billing_address_1', true ) );
				update_post_meta( $order->get_id(), '_billing_address_2', get_user_meta( $new_user_id, 'billing_address_2', true ) );
				update_post_meta( $order->get_id(), '_billing_city', get_user_meta( $new_user_id, 'billing_city', true ) );
				update_post_meta( $order->get_id(), '_billing_state', get_user_meta( $new_user_id, 'billing_state', true ) );
				update_post_meta( $order->get_id(), '_billing_postcode', get_user_meta( $new_user_id, 'billing_postcode', true ) );
				update_post_meta( $order->get_id(), '_billing_country', get_user_meta( $new_user_id, 'billing_country', true ) );
				update_post_meta( $order->get_id(), '_billing_email', get_user_meta( $new_user_id, 'billing_email', true ) );
				update_post_meta( $order->get_id(), '_billing_phone', get_user_meta( $new_user_id, 'billing_phone', true ) );
				update_post_meta( $order->get_id(), '_billing_tax_number', get_user_meta( $new_user_id, 'tax-number', true ) );

				// Shipping info
				update_post_meta( $order->get_id(), '_shipping_first_name', get_user_meta( $new_user_id, 'shipping_first_name', true ) );
				update_post_meta( $order->get_id(), '_shipping_last_name', get_user_meta( $new_user_id, 'shipping_last_name', true ) );
				update_post_meta( $order->get_id(), '_shipping_company', get_user_meta( $new_user_id, 'shipping_company', true ) );
				update_post_meta( $order->get_id(), '_shipping_address_1', get_user_meta( $new_user_id, 'shipping_address_1', true ) );
				update_post_meta( $order->get_id(), '_shipping_address_2', get_user_meta( $new_user_id, 'shipping_address_2', true ) );
				update_post_meta( $order->get_id(), '_shipping_city', get_user_meta( $new_user_id, 'shipping_city', true ) );
				update_post_meta( $order->get_id(), '_shipping_state', get_user_meta( $new_user_id, 'shipping_state', true ) );
				update_post_meta( $order->get_id(), '_shipping_postcode', get_user_meta( $new_user_id, 'shipping_postcode', true ) );
				update_post_meta( $order->get_id(), '_shipping_country', get_user_meta( $new_user_id, 'shipping_country', true ) );
			}
		}
	}
}

add_action( 'wp_ajax_bl_pos_get_user_tax_number', 'bl_pos_get_user_tax_number' );
function bl_pos_get_user_tax_number(){
	$tax_number = '';

	if ( !empty( $_POST['id'] ) ) {
		$tax_number = get_user_meta( $_POST['id'], 'tax-number', true );
	}

	return wp_send_json( array( 'tax_number' => $tax_number ) );
}

add_filter( 'woocommerce_pos_i18n', 'bl_pos_modify_pos_translations' );
function bl_pos_modify_pos_translations( $i18n = array() ){

	// Buttons
	if( !empty( $i18n['buttons']['close-all'] ) ){
		$i18n['buttons']['close-all'] = __( 'Close all', 'bl' );
	}

	if( !empty( $i18n['buttons']['discount'] ) ){
		$i18n['buttons']['discount'] = __( 'Discount', 'bl' );
	}

	if( !empty( $i18n['buttons']['expand-all'] ) ){
		$i18n['buttons']['expand-all'] = __( 'Expand all', 'bl' );
	}

	if( !empty( $i18n['buttons']['new-order'] ) ){
		$i18n['buttons']['new-order'] = __( 'New order', 'bl' );
	}

	if( !empty( $i18n['buttons']['refresh'] ) ){
		$i18n['buttons']['refresh'] = __( 'Refresh', 'bl' );
	}

	if( !empty( $i18n['buttons']['restore'] ) ){
		$i18n['buttons']['restore'] = __( 'Restore defaults', 'bl' );
	}
	
	if( !empty( $i18n['buttons']['save'] ) ){
		$i18n['buttons']['save'] = __( 'Save Changes', 'bl' );
	}

	// Messages
	if( !empty( $i18n['messages']['loading'] ) ){
		$i18n['messages']['loading'] = __( 'Loading...', 'bl' );
	}

	if( !empty( $i18n['messages']['no-customer'] ) ){
		$i18n['messages']['no-customer'] = __( 'Customer not found', 'bl' );
	}

	return $i18n;
}