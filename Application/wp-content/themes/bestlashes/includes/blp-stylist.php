<?php

add_action( 'wpcf7_mail_sent', 'bl_blc_stylist_form_submission' ); 
function bl_blc_stylist_form_submission( $contact_form ) {
    $title = $contact_form->title;
    $submission = WPCF7_Submission::get_instance();

    if ( $submission ) {
        $posted_data = $submission->get_posted_data();
		
		if( $posted_data['_wpcf7'] == BL_CF7_ID_BLP_HU || $posted_data['_wpcf7'] == BL_CF7_ID_BLP_EN ){
			$name = $posted_data['bap-name'];
			$email = $posted_data['bap-email'];

			// Email to user
			if( class_exists( 'Auretto_Email_Editor' ) ){
				$AWEmail = new Auretto_Email_Editor();
				$extra_datas = array(
					'receiver_email' => $email,
					'replaceable_content_words' => array(
						'%name%' => $name,
					)
				);
				$AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_BLP_STYLIST_VERIFY_TO_USER, $extra_datas );
			}
		}
    }
}