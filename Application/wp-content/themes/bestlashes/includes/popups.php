<?php

add_action( 'init', 'bl_register_popup_cpt' );
function bl_register_popup_cpt() {
	$labels = array(
		'name' 					=> __( 'Popups', 'bl' ),
		'singular_name' 		=> __( 'Popup', 'bl' ),
		'add_new' 				=> __( 'Add New', 'popup', 'bl' ),
		'add_new_item' 			=> __( 'Add New Popup', 'bl' ),
		'edit_item' 			=> __( 'Edit Popup', 'bl' ),
		'new_item' 				=> __( 'New Popup', 'bl' ),
		'view_item' 			=> __( 'View Popup', 'bl' ),
		'search_items' 			=> __( 'Search Popups', 'bl' ),
		'not_found' 			=> __( 'No Popups found.', 'bl' ),
		'not_found_in_trash' 	=> __( 'No Popups found in Trash.', 'bl' ),
		'parent_item_colon' 	=> ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'exclude_from_search' => true,
		'rewrite' => array( 'slug' => 'popup' ),
		'has_archive' => false,
		'menu_position' => 30,
		'menu_icon' => 'dashicons-share-alt2',
		'supports' => array('title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes')
	);
	register_post_type('popup', $args);
}


	/**************************/
	/*	  Custom Fields	 */
	/**************************/

add_action('add_meta_boxes', 'bl_add_custom_meta_boxes_to_popup');
function bl_add_custom_meta_boxes_to_popup() {
	
	// Beállítások
	add_meta_box("popup_settings", __('Popup settings', 'bl'), "popup_settings_meta", "popup", "normal", "default" );

}


function popup_settings_meta() {
	global $post;
	$custom = get_post_custom( $post->ID );

	$from_raw = $custom['from'][0];
    $to_raw = $custom['to'][0];
    
    $from = '';
    if( isset( $from_raw ) && $from_raw != '' ){
        $timestamp = DateTime::createFromFormat('U', $from_raw);
        $from = $timestamp->format('Y-m-d H:i');
    }
    
    $to = '';
    if( isset( $to_raw ) && $to_raw != '' ){
        $timestamp = DateTime::createFromFormat('U', $to_raw);
        $to = $timestamp->format('Y-m-d H:i');
    } ?>
	
	<div class="aw-settings-section">
		<div class="menu-wrapper">
			<ul class="menu">
				<li data-menu-item="main-settings" class="active">
					<span><?php _e( 'Main settings', 'bl' ); ?></span>
				</li>
			</ul>
		</div>

		<div class="content-wrapper">
			<div class="content-box main-settings active">
				<h3><?php _e('Main settings', 'bl'); ?></h3>
				<h4><?php _e('Delay', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="popup_delay" value="<?php echo ( isset( $custom['popup_delay'][0] ) ? $custom['popup_delay'][0] : '' ); ?>" /> <em><?php _e('secundum', 'bl') ?></em>
				</div>
				<hr>
				<h4><?php _e('From', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="from" class="datetimepicker" value="<?php echo ( isset( $from ) ? $from : '' ); ?>" />
				</div>
				<h4><?php _e('To', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="to" class="datetimepicker" value="<?php echo ( isset( $to ) ? $to : '' ); ?>" />
				</div>
				<hr>
				<h4><?php _e('Extra class', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="popup_eclass" value="<?php echo ( isset( $custom['popup_eclass'][0] ) ? $custom['popup_eclass'][0] : '' ); ?>" />
				</div>
				<h4><?php _e('Link', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="popup_link" value="<?php echo ( isset( $custom['popup_link'][0] ) ? $custom['popup_link'][0] : '' ); ?>" />
				</div>
			</div>
			
		</div>
	</div>

<?php }

///////////////////////////// Save CFs

add_action('save_post', 'bl_popup_save_custom_postdata');
function bl_popup_save_custom_postdata($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	global $post;

	if( isset( $_POST['post_type'] ) ){
		if ($_POST['post_type'] == "popup") {

			// From-to
            if( $_POST['from'] != ''){
                $date_raw = DateTime::createFromFormat('Y-m-d H:i', $_POST['from']);
                $timestamp = $date_raw->format('U');
                update_post_meta( $post_id, 'from', $timestamp );
            }
            
            if( $_POST['to'] != ''){
                $date_raw = DateTime::createFromFormat('Y-m-d H:i', $_POST['to']);
                $timestamp = $date_raw->format('U');
                update_post_meta( $post_id, 'to', $timestamp );
            }
			
			// Main settings
			update_post_meta( $post_id, 'popup_delay', $_POST['popup_delay'] );
			update_post_meta( $post_id, 'popup_eclass', $_POST['popup_eclass'] );
			update_post_meta( $post_id, 'popup_link', $_POST['popup_link'] );
		}
	}
}


// Popup check
add_action( 'wp', 'bl_popup_check' );
function bl_popup_check(){
    global $system_popup;
    if( !isset( $_SESSION['popup_exclude_ids'] ) ){
        $_SESSION['popup_exclude_ids'] = array();
    }
    if( !isset( $_SESSION['popup_toshow'] ) ){
        $_SESSION['popup_toshow'] = 0;
    }

    $exclude_popup_ids = $_SESSION['popup_exclude_ids'];
    $popup_ids = array();
    
    $args = array();
    $args['post_type'] = 'popup';
    $args['posts_per_page'] = -1;
    $args['post_status'] = 'publish';
    $args['meta_query'] = array();
    $args['meta_query']['relation'] = 'AND';
    $args['meta_query'][] = array('key' => 'from', 'value' => current_time( 'timestamp' ), 'compare' => '<=', 'type' => 'NUMERIC');
    $args['meta_query'][] = array('key' => 'to', 'value' => current_time( 'timestamp' ), 'compare' => '>=', 'type' => 'NUMERIC');
    $args['fields'] = 'ids';
    $args['post__not_in'] = $exclude_popup_ids;
    
    $popup_loop = new WP_Query( $args );

    if( $popup_loop->have_posts() ){
        while( $popup_loop->have_posts() ){
            $popup_loop->next_post();

            $popup_ids[] = $popup_loop->post;
        }
    }

    if( !empty( $popup_ids ) ){
        $popup_id = $popup_ids[ array_rand( $popup_ids ) ];
        $_SESSION['popup_toshow'] = $popup_id;

        if( !empty( $exclude_popup_ids ) ){
        	if( !in_array( $popup_id, $exclude_popup_ids ) ){
        		$exclude_popup_ids[] = $popup_id;
        	}
        } else {
        	$exclude_popup_ids[] = $popup_id;
        }
        
        $_SESSION['popup_exclude_ids'] = $exclude_popup_ids;
    }
}


// Popup in footer
add_action( 'wp_footer', 'bl_popup_footer_function' );
function bl_popup_footer_function (){
	global $system_popup;
    if( $_SESSION['popup_toshow'] != 0 ){
        
        
    	$args = array();
    	$args['post_type'] = 'popup';
    	$args['posts_per_page'] = 1;
    	$args['post_status'] = 'publish';
    	$args['p'] = $_SESSION['popup_toshow'];
    	$args['fields'] = 'ids';
    	
    	$popup_loop = new WP_Query( $args );
    	if($popup_loop -> have_posts()){
    		while($popup_loop->have_posts()){
    		    $popup_loop->next_post();
    		    $popup_id = $popup_loop->post;

    			$delay_mp = get_post_meta( $popup_id, 'popup_delay', true );
    			$eclass = get_post_meta( $popup_id, 'popup_eclass', true );
    			$link = get_post_meta( $popup_id, 'popup_link', true ); ?>

            	<div class="marketing-popup-overlay delayed" data-delay-mp="<?php echo $delay_mp; ?>"></div>
            	<div class="marketing-popup-wrapper delayed<?php echo ' '. $eclass; ?>" data-delay-mp="<?php echo $delay_mp; ?>">
					<div class="close_btn">&#x2715;</div>
					<div class="container">
						<div class="inside-box">
							<div class="title-wrapper">
								<h3><?php echo get_the_title( $popup_id ); ?></h3>
							</div>
							<div class="content-wrapper">
								<?php echo wpautop( get_the_content_by_id( $popup_id ) ); ?>
							</div>

							<?php if( isset( $link ) && $link != '' ) { ?>
		            			<a href="<?php echo $link; ?>" class="full-width"></a>
		            		<?php } ?>
						</div>
					</div>
				</div>

    		<?php }
    	}
    	
    	$_SESSION['popup_toshow'] = 0;
    }
}