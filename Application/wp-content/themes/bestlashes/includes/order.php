<?php


	/**************************/
	/*      Custom Fields     */
	/**************************/

add_action('add_meta_boxes', 'bl_add_custom_meta_boxes_to_order');
function bl_add_custom_meta_boxes_to_order() {
	
	// Beállítások
	add_meta_box("order_settings", __('Order settings', 'bl'), "ordert_settings_meta", "shop_order", "normal", "default" );

	// Beállítások
	add_meta_box("order_pagination_settings", __('Prev/Next order', 'bl'), "order_pagination_settings_meta", "shop_order", "side", "high" );
}


function ordert_settings_meta() {
	global $post, $woocommerce;

	$custom = get_post_meta( $post->ID );
	$order_has_training_course = false;
	$order = new WC_Order( $post->ID );
	$current_time = current_time( 'timestamp' );
	$created_via = $custom['_created_via'][0];
	$order_status = $order->get_status();
	$payment_methods = $woocommerce->payment_gateways->get_available_payment_gateways();

	foreach ( $order->get_items() as $items_key => $item_product ) { 
		$training_course = get_post_meta( $item_product->get_product_id(), 'training_course', true );

		if( $training_course == '1' ){
			$order_has_training_course = true;
		}
	}

	// Invoice deadline
	$payment_deadline = $custom['payment_deadline'][0];

	if( empty( $payment_deadline ) ){
		$payment_deadline = '';

		if( $created_via == 'rest-api' ){
			$payment_deadline = date( 'Y-m-d', $current_time );
		} else {
			$payment_deadline_timestamp = strtotime( '+8 days', $current_time );
			$payment_deadline = date( 'Y-m-d', $payment_deadline_timestamp );
		}
	}

	// Invoice Date of completion
	$date_of_completion = $custom['date_of_completion'][0];

	if( empty( $date_of_completion ) ){
		$date_of_completion = '';

		$date_of_completion = date( 'Y-m-d', $current_time );
	}

	// Invoide payment method
	$invoice_payment_method = $order->get_payment_method();

	if( !empty( $custom['invoice_payment_method'][0] ) ){
		$invoice_payment_method = $custom['invoice_payment_method'][0];
	}

	// Invoice deadline - FINAL
	$final_payment_deadline = '';
	
	if( $created_via == 'rest-api' ){
		$final_payment_deadline = date( 'Y-m-d', $current_time );
	} else {
		$final_payment_deadline_timestamp = strtotime( '+8 days', $current_time );
		$final_payment_deadline = date( 'Y-m-d', $final_payment_deadline_timestamp );
	}

	// Invoice Date of completion - FINAL
	$final_date_of_completion = $custom['final_date_of_completion'][0];

	if( empty( $final_date_of_completion ) ){
		$final_date_of_completion = '';

		$final_date_of_completion = date( 'Y-m-d', $current_time );
	}

	// Invoide payment method - FINAL
	$final_invoice_payment_method = $order->get_payment_method();

	if( !empty( $custom['final_invoice_payment_method'][0] ) ){
		$final_invoice_payment_method = $custom['final_invoice_payment_method'][0];
	}

	// Invoice VAT
	if( '1' === $order->get_meta('_is_vip_order') ){
		$vat_type = 0;
	} else {
		$vat_type = BL_HUN_VAT;
	}

	// Invoice VAT - final
	$final_vat_type = BL_HUN_VAT;

	// Invoice payment
	// Invoice payment - final
	if( $order_status == 'completed' ){
		$paid = 'true';
		$final_paid = 'true';
	} else if( $order_status == 'on-hold' || $order_status == 'processing' || $order_status == 'pending' ) {
		$paid = 'false';
		$final_paid = 'false';
	}

	?>
	
	<div class="aw-settings-section">
		<div class="menu-wrapper">
			<ul class="menu">
				<?php if( BL_LANG == 'HU' ){ ?>
					<li data-menu-item="invoice" <?php if( BL_LANG == 'HU' ) echo 'class="active"'; ?>>
						<span><?php _e( 'Invoice', 'bl' ); ?></span>
					</li>
				<?php } ?>
				<?php if( $order_has_training_course ){ ?>
					<?php if( BL_LANG == 'HU' ){ ?>
						<li data-menu-item="final-invoice" class="">
							<span><?php _e( 'Final invoice', 'bl' ); ?></span>
						</li>
					<?php } ?>
					<li data-menu-item="training-course" <?php if( BL_LANG != 'HU' ) echo 'class="active"'; ?>>
						<span><?php _e( 'Training course', 'bl' ); ?></span>
					</li>
				<?php } ?>
			</ul>
		</div>

		<div class="content-wrapper">
			<div class="content-box invoice <?php if( BL_LANG == 'HU' ) echo 'active'; ?>">
				<h3><?php _e( 'Invoice', 'bl' ); ?></h3>
				<h4><?php _e('Current invoice', 'bl'); ?></h4>
				<div class="text-box invice-upload" data-upload-text="<?php _e( 'You can upload a new invoice for this order ', 'bl' ); ?>" data-confirm-regenerate-invoice="<?php _e('Do you really want to regenerate the invoice?', 'bl'); ?>">
					
					<?php if( empty( $custom['_invoice-id'][0] ) ){ ?>

						<a href="<?php echo wp_get_attachment_url( $custom['_invoice-id'][0] ); ?>" class="invoice-link hidden" target="_blank">
							<strong><?php _e('Open invoice: ', 'bl'); ?></strong>
							(<span class="invoice-title"></span>)
						</a>
						<p>
							<span class="text"><?php _e('No invoice for this order. Please generate a new invoice below, or ', 'bl' ); ?></span>
							<a href="#" class="button button-primary upload-invoice-for-order"><?php _e('Upload invoice', 'bl'); ?></a>
						</p>

					<?php } else { ?>

						<a href="<?php echo wp_get_attachment_url( $custom['_invoice-id'][0] ); ?>" class="invoice-link" target="_blank">
							<strong><?php _e('Open invoice', 'bl'); ?></strong>
							(<span class="invoice-title"><?php echo get_the_title( $custom['_invoice-id'][0] ); ?></span>)	
						</a>
						<p>
							<span class="text"><?php _e( 'You can upload a new invoice for this order ', 'bl' ); ?></span>
							<a href="#" class="button button-primary upload-invoice-for-order"><?php _e('Upload invoice', 'bl'); ?></a>
						</p>

					<?php } ?>

				</div>
				<hr>
				<h3><?php _e('Generate new invoice', 'bl'); ?></h3>
				<h4><?php _e( 'Payment deadline', 'bl' ); ?></h4>
				<div class="text-box short">
					<input type="text" name="payment_deadline" value="<?php echo $payment_deadline; ?>" class="date-picker" autocomplete="off" />
				</div>
				<h4><?php _e( 'Date of completion', 'bl' ); ?></h4>
				<div class="text-box short">
					<input type="text" name="date_of_completion" value="<?php echo $date_of_completion; ?>" class="date-picker" autocomplete="off" />
				</div>
				<h4><?php _e( 'Payment method', 'bl' ); ?></h4>
					<div class="text-box short">
						<select name="payment_method">
							<?php if( !empty( $payment_methods ) ){
								foreach( $payment_methods as $key => $payment_method ) {
									if( $payment_method->enabled == "yes" ){ ?>
										<option value="<?php echo $key; ?>" <?php selected( $invoice_payment_method, $key ); ?>><?php echo $payment_method->title; ?></option>
									<?php }
								}
							} ?>
						</select>
					</div>
				<h4><?php _e( 'VAT', 'bl' ); ?></h4>
				<div class="chckbox-box">
					<label><input type="radio" name="vat_type" value="<?php echo BL_HUN_VAT; ?>" autocomplete="off" <?php checked( $vat_type, BL_HUN_VAT ); ?> /><?php echo sprintf( __( '%d VAT', 'bl' ), BL_HUN_VAT ); ?></label>
					<br>
					<label><input type="radio" name="vat_type" value="0" autocomplete="off" <?php checked( $vat_type, '0' ); ?> /><?php _e( '0% VAT', 'bl' ); ?></label>
				</div>
				<h4><?php _e( 'Payment', 'bl' ); ?></h4>
				<div class="chckbox-box">
					<label><input type="radio" name="paid" value="true" autocomplete="off" <?php checked( $paid, 'true' ); ?> /><?php _e( 'Paid', 'bl' ); ?></label>
					<br>
					<label><input type="radio" name="paid" value="false" autocomplete="off" <?php checked( $paid, 'false' ); ?> /><?php _e( 'Payable', 'bl' ); ?></label>
				</div>
				<h4><?php _e('Generate new invoice', 'bl'); ?></h4>
				<div class="text-box">
					<a href="#" class="button button-primary generate-new-invoice-for-order need-preview" data-order-id="<?php echo $post->ID; ?>" data-error-text-payment-deadline="<?php _e('Please fill the Payment deadline field', 'bl'); ?>" data-error-text-vat-type="<?php _e('Please select a VAT type', 'bl'); ?>" data-error-text-paid="<?php _e('Please select a Payment type', 'bl'); ?>"><?php _e('Generate new invoice', 'bl'); ?></a>
					<div class="spinner"></div>
					<div class="error-container">
						<div class="error-text"></div>
					</div>
				</div>
				
				<input type="hidden" name="_invoice-id" value="<?php echo ( !empty( $custom['_invoice-id'][0] ) ? $custom['_invoice-id'][0] : '' ); ?>">
			</div>
			<?php if( $order_has_training_course ){ ?>
				<div class="content-box final-invoice">
					<h3><?php _e( 'Final invoice', 'bl' ); ?></h3>
					<h4><?php _e('Current final invoice', 'bl'); ?></h4>
					<div class="text-box invice-upload" data-upload-text="<?php _e( 'You can upload a new final invoice for this order ', 'bl' ); ?>" data-confirm-regenerate-invoice="<?php _e('Do you really want to regenerate the invoice?', 'bl'); ?>">

						<?php if( empty( $custom['_final-invoice-id'][0] ) ){ ?>

							<a href="<?php echo wp_get_attachment_url( $custom['_final-invoice-id'][0] ); ?>" class="invoice-link hidden" target="_blank">
								<strong><?php _e('Open final invoice: ', 'bl'); ?></strong>
								(<span class="invoice-title"></span>)
							</a>
							<p>
								<span class="text"><?php _e('No final invoice for this order. Please generate a new final invoice below, or ', 'bl' ); ?></span>
								<a href="#" class="button button-primary upload-invoice-for-order"><?php _e('Upload final invoice', 'bl'); ?></a>
							</p>

						<?php } else { ?>

							<a href="<?php echo wp_get_attachment_url( $custom['_final-invoice-id'][0] ); ?>" class="invoice-link" target="_blank">
								<strong><?php _e('Open final invoice', 'bl'); ?></strong>
								(<span class="invoice-title"><?php echo get_the_title( $custom['_final-invoice-id'][0] ); ?></span>)	
							</a>
							<p>
								<span class="text"><?php _e( 'You can upload a new final invoice for this order ', 'bl' ); ?></span>
								<a href="#" class="button button-primary upload-invoice-for-order"><?php _e('Upload final invoice', 'bl'); ?></a>
							</p>

						<?php } ?>

					</div>
					<hr>
					<h3><?php _e('Generate new final invoice', 'bl'); ?></h3>
					<h4><?php _e( 'Payment deadline', 'bl' ); ?></h4>
					<div class="text-box short">
						<input type="text" name="final_payment_deadline" value="<?php echo $final_payment_deadline; ?>" class="date-picker" autocomplete="off" />
					</div>
					<h4><?php _e( 'Date of completion', 'bl' ); ?></h4>
					<div class="text-box short">
						<input type="text" name="final_date_of_completion" value="<?php echo $final_date_of_completion; ?>" class="date-picker" autocomplete="off" />
					</div>
					<h4><?php _e( 'Payment method', 'bl' ); ?></h4>
					<div class="text-box short">
						<select name="final_payment_method">
							<?php if( !empty( $payment_methods ) ){
								foreach( $payment_methods as $key => $payment_method ) {
									if( $payment_method->enabled == "yes" ){ ?>
										<option value="<?php echo $key; ?>" <?php selected( $final_invoice_payment_method, $key ); ?>><?php echo $payment_method->title; ?></option>
									<?php }
								}
							} ?>
						</select>
					</div>
					<h4><?php _e( 'VAT', 'bl' ); ?></h4>
					<div class="chckbox-box">
						<label><input type="radio" name="final_vat_type" value="<?php echo BL_HUN_VAT; ?>" autocomplete="off" <?php checked( $final_vat_type, BL_HUN_VAT ); ?> /><?php echo sprintf( __( '%d VAT', 'bl' ), BL_HUN_VAT ); ?></label>
						<br>
						<label><input type="radio" name="final_vat_type" value="0" autocomplete="off" <?php checked( $final_vat_type, '0' ); ?> /><?php _e( '0% VAT', 'bl' ); ?></label>
					</div>
					<h4><?php _e( 'Payment', 'bl' ); ?></h4>
					<div class="chckbox-box">
						<label><input type="radio" name="final_paid" value="true" autocomplete="off" <?php checked( $final_paid, 'true' ); ?> /><?php _e( 'Paid', 'bl' ); ?></label>
						<br>
						<label><input type="radio" name="final_paid" value="false" autocomplete="off" <?php checked( $final_paid, 'false' ); ?> /><?php _e( 'Payable', 'bl' ); ?></label>
					</div>
					<h4><?php _e('Generate new final invoice', 'bl'); ?></h4>
					<div class="text-box">
						<a href="#" class="button button-primary generate-new-invoice-for-order need-preview" data-order-id="<?php echo $post->ID; ?>" data-error-text-payment-deadline="<?php _e('Please fill the Payment deadline field', 'bl'); ?>" data-error-text-vat-type="<?php _e('Please select a VAT type', 'bl'); ?>" data-error-text-paid="<?php _e('Please select a Payment type', 'bl'); ?>"><?php _e('Generate new final invoice', 'bl'); ?></a>
						<div class="spinner"></div>
						<div class="error-container">
							<div class="error-text"></div>
						</div>
					</div>
					
					<input type="hidden" name="_final-invoice-id" value="<?php echo ( !empty( $custom['_final-invoice-id'][0] ) ? $custom['_final-invoice-id'][0] : '' ); ?>">

				</div>
				<div class="content-box training-course <?php if( BL_LANG != 'HU' ) echo 'active'; ?>">
					<h3><?php _e( 'Training course', 'bl' ); ?></h3>
					<?php if( !empty( $custom['_certificate-of-qualification-document-id'][0] ) ){ ?>
						<h4><?php _e('Certificate of qualification', 'bl'); ?></h4>
						<div class="text-box short">
							<a href="<?php echo wp_get_attachment_url( $custom['_certificate-of-qualification-document-id'][0] ); ?>" target="_blank"><?php _e('Certificate', 'bl'); ?></a>
						</div>
					<?php } else { ?>
						<h4><?php _e('Certificate of qualification', 'bl'); ?></h4>
						<div class="text-box short">
							<p><?php _e( 'No certification required for these training courses', 'bl' ); ?></p>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>
	
	<div class="overlay-wrapper"></div>
	<div class="popup-wrapper invoice-preview">
		<div class="close_btn">&#x2715;</div>
		<div class="container">
			<div class="inside-box">
				
			</div>
		</div>
	</div>

<?php }

function order_pagination_settings_meta(){
	global $post, $wpdb;

	$next_query = "SELECT p.ID FROM $wpdb->posts AS p WHERE p.ID > $post->ID AND p.post_type = 'shop_order' ORDER BY p.post_date ASC LIMIT 1";
	$prev_query = "SELECT p.ID FROM $wpdb->posts AS p WHERE p.ID < $post->ID AND p.post_type = 'shop_order' ORDER BY p.post_date DESC LIMIT 1";

	$next_result = $wpdb->get_var( $next_query );
	$prev_result = $wpdb->get_var( $prev_query );

	$buttons .= '<div class="shop-order-next-prev-buttons-container">';
	
	if( !empty( $prev_result ) ){
		$buttons .= sprintf(
			'<a href="%1$s" id="admin-post-nav-prev" title="%2$s" class="admin-post-nav-prev button">%3$s</a>',
			get_edit_post_link( $prev_result ),
			esc_attr( sprintf( __( 'Previous Order: %1$s', 'bl' ), get_the_title( $prev_result ) ) ),
			__('Previous Order', 'bl')
		);
	}
	if( !empty( $next_result ) ){
		$buttons .= sprintf(
			'<a href="%1$s" id="admin-post-nav-next" title="%2$s" class="admin-post-nav-next button">%3$s</a>',
			get_edit_post_link( $next_result ),
			esc_attr( sprintf( __( 'Next Order: %1$s', 'bl' ), get_the_title( $next_result ) ) ),
			__( 'Next Order', 'bl' )
		);
	}

	if( empty( $prev_result ) && empty( $next_result ) ){
		$buttons .= '<h2>'. __('No Previous/Next order found', 'bl') .'</h2>';
	}

	$buttons .= '</div>';

	echo $buttons;
}



///////////////////////////// Save CFs

add_action('save_post', 'bl_order_save_custom_postdata');
function bl_order_save_custom_postdata( $post_id ) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

	if( isset( $_POST['post_type'] ) ){
		if ($_POST['post_type'] == "shop_order") {
			// Invoices
			if( !empty( $_POST['_invoice-id'] ) ){
				update_post_meta( $post_id, '_invoice-id', $_POST['_invoice-id'] );

				// Set is_invoice to pdf
				update_post_meta( $_POST['_invoice-id'], '_is_invoice', '1' );
			}


			// Final invoice
			if( !empty( $_POST['_invoice-id'] ) ){
				update_post_meta( $post_id, '_final-invoice-id', $_POST['_final-invoice-id'] );

				// Set is_invoice to pdf
				update_post_meta( $_POST['_final-invoice-id'], '_is_invoice', '1' );
			}

			// Tax number
			update_post_meta( $post_id, '_billing_tax_number', $_POST['_billing_tax_number'] );
		}
	}
}

/* Modify display date on order list */
add_action( 'manage_posts_custom_column', 'bl_order_list_modify_display_date' );
function bl_order_list_modify_display_date( $column_name = '' ) {
    global $post;

    if( $column_name  == 'order_date' ) {
        echo '<span class="bl-date">'. date_i18n( 'Y.m.d. H:i:s', strtotime( $post->post_date ) ) . '</span>';
    }
}


/* Add Export GLS list bulk action to shop order list */
add_filter( 'bulk_actions-edit-shop_order', 'bl_export_gls_list_bulk_action', 20, 1 );
function bl_export_gls_list_bulk_action( $actions ) {
    $actions['export_gls_list'] = __( 'Export GLS list', 'bl' );

    return $actions;
}

/* Bulk action - Export GLS list */
add_filter( 'handle_bulk_actions-edit-shop_order', 'bl_handle_export_gls_list_bulk_action', 10, 3 );
function bl_handle_export_gls_list_bulk_action( $redirect_to, $action, $post_ids ) {
    if ( $action !== 'export_gls_list' )
        return $redirect_to; // Exit

    $date_time = current_time( 'Y.m.d. H.i' );
    $filename = 'Best Lashes GLS export - ' . $date_time;
	$rows = array();

	if( !empty( $post_ids ) ){
		header('Pragma: public');
		header('Expires: 0');
		header("Cache-Control: cache, must-revalidate" ) ;
		header('Content-Description: File Transfer');
		header('Content-Type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachement; filename="'. $filename .'.csv"');
		header('Content-Transfer-Encoding: binary');

		$rows[0][0] = __('Total', 'bl');
		$rows[0][1] = __('Name', 'bl');
		$rows[0][2] = __('Postcode', 'bl');
		$rows[0][3] = __('Country', 'bl');
		$rows[0][4] = __('City', 'bl');
		$rows[0][5] = __('Address', 'bl');
		$rows[0][6] = __('Contact name', 'bl');
		$rows[0][7] = __('Contact phone', 'bl');
		$rows[0][8] = __('Contact email', 'bl');
		$rows[0][9] = __('Services', 'bl');
		
		$i = 1;

		foreach ( $post_ids as $post_id ) {
	        $order = wc_get_order( $post_id );

	        if( !empty( $order ) ){
	        	$total = $order->get_total();
	        	$shipping_name = '';
	        	$shipping_address = '';
	        	$services = array( 'FDS()' );

	        	// Total
	        	if ( $order->get_payment_method() != 'cod' ) {
	        		$total = 0;
				}

	        	// Shipping name
	        	if( !empty( $order->get_shipping_company() ) ){
	        		$shipping_name = $order->get_shipping_company() . ', ';
	        	}
	        	$shipping_name .= $order->get_shipping_last_name() . ' ' . $order->get_shipping_first_name();

	        	// Shipping address
	        	$shipping_address = $order->get_shipping_address_1();
	        	if( !empty( $order->get_shipping_address_2() ) ){
	        		$shipping_address .= ' ' . $order->get_shipping_address_2();
	        	}

	        	// Contact name
	        	$contact = $order->get_shipping_last_name() . ' ' . $order->get_shipping_first_name();
	        	if( !empty( $order->get_customer_note() ) ){
	        		$contact .= ', ' . $order->get_customer_note();
	        	}

	        	// Services
	        	if( $order->get_shipping_country() == 'HU' ){
	        		$services[] = '24H()';
	        		$clean_phone_number = str_replace( array( '/', '-', '+' ), '', $order->get_billing_phone() );
	        		$phone_number_first_two_characters = substr( $clean_phone_number, 0, 2 );
	        		$phone_number_first_four_characters = substr( $clean_phone_number, 0, 4 );

	        		// If is HU phone number
	        		if( in_array( $phone_number_first_two_characters, array( '20', '30', '31', '70', '36', '06' ) ) || in_array( $phone_number_first_four_characters, array( '0036' ) ) ){
	        			$services[] = 'SM2()';
	        		}
	        	}

	        	$rows[$i][0] = $total;
	        	$rows[$i][1] = $shipping_name;
	        	$rows[$i][2] = $order->get_shipping_postcode();
	        	$rows[$i][3] = $order->get_shipping_country();
	        	$rows[$i][4] = $order->get_shipping_city();
	        	$rows[$i][5] = $shipping_address;
	        	$rows[$i][6] = $contact;
	        	$rows[$i][7] = $order->get_billing_phone();
	        	$rows[$i][8] = $order->get_billing_email();
	        	$rows[$i][9] = implode( '', $services );
	        }

	        $i++;
	    }

	    $output = fopen("php://output", "w");

		if( $output ) {
			fprintf( $output, chr(0xEF).chr(0xBB).chr(0xBF) );
			
			foreach( $rows as $line ) {
				fputcsv( $output, $line, ';' );
			}
		}

		fclose( $output );
		exit;
	}
}


// Paid order status
add_action( 'init', 'bl_register_awaiting_shipment_order_status' );
function bl_register_awaiting_shipment_order_status() {
    register_post_status( 'wc-paid', 
    	array(
	        'label'                     => __( 'Paid', 'bl' ),
	        'public'                    => true,
	        'exclude_from_search'       => false,
	        'show_in_admin_all_list'    => true,
	        'show_in_admin_status_list' => true,
	        'label_count'               => _n_noop( 'Paid <span class="count">(%s)</span>', 'Paid <span class="count">(%s)</span>' )
	    )
    );
}

// Add Paid to list of WC Order statuses
add_filter( 'wc_order_statuses', 'bl_add_paid_to_order_statuses' );
function bl_add_paid_to_order_statuses( $order_statuses ) {
    $new_order_statuses = array();

    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
        $new_order_statuses[ $key ] = $status;
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-paid'] = __('Paid', 'bl');
        }
    }

    return $new_order_statuses;
}

/* Modify order status after PayPal IPN recived */
add_filter( 'woocommerce_payment_complete_order_status', 'bl_modify_order_status_after_paypal_inp_recived', 10, 2 );
function bl_modify_order_status_after_paypal_inp_recived( $order_status, $order_id ) {
	$order = new WC_Order( $order_id );

	if( $order->get_payment_method() == 'paypal' && $order->has_status( array( 'on-hold', 'pending', 'processing' ) ) && ( $order_status == 'completed' || $order_status == 'processing' ) ){
		return 'paid';
	}

	return $order_status;
}


/* Untrashed order, decrease stock */
add_action( 'untrashed_post', 'bl_untrash_order_decrease_stock' );
function bl_untrash_order_decrease_stock( $post_id = 0 ){
	if( !empty( $post_id ) ){
		if( get_post_type( $post_id ) == 'shop_order' ){
			$order = wc_get_order( $post_id );

			// Set product stock
			if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
				return;
			}

			foreach ( $order->get_items() as $item ) {

				if ( $item['product_id'] > 0 ) {
					$_product = $order->get_product_from_item( $item );

					if ( $_product && $_product->exists() && $_product->managing_stock() ) {

						$old_stock = $_product->stock;

						$qty = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $order, $item );

						$new_quantity = wc_update_product_stock( $_product, $qty, 'decrease');

						do_action( 'woocommerce_auto_stock_restored', $_product, $item );

						$order->add_order_note( sprintf( __( 'Item #%s stock decreased from %s to %s.', 'bl' ), $item['product_id'], $old_stock, $new_quantity ) );

						$order->send_stock_notifications( $_product, $new_quantity, $item['qty'] );

						// Log stock
						bl_product_stock_change( $_product, (int)$old_stock, (int)$new_quantity, 'order-untrashed' );
					}
				}
			}

			// If marked for delete, add _training_course_cron_checked meta and modify _training_course_cron_delete meta for not tagging again by cron
			if( get_post_meta( $post_id, '_training_course_cron_delete', true ) == '1' ){
				update_post_meta( $post_id, '_training_course_cron_checked', '1' );
				update_post_meta( $post_id, '_training_course_cron_delete', 'untrashed' );
			}
		}
	}
}


/* Add gross item total on order details page */
// Order table - header
add_action('woocommerce_admin_order_item_headers', 'bl_admin_order_item_headers' );
function bl_admin_order_item_headers() { ?>

	<th class="item-gross-total line_cost" width="10%"><?php _e('Gross total', 'bl'); ?></th>
	
<?php }

// Order table - value
add_action('woocommerce_admin_order_item_values', 'bl_admin_order_item_values', 10, 3);
function bl_admin_order_item_values( $product, $item, $item_id ) { ?>

	<td class="item-gross-total line_cost" width="10%">
		<?php if (isset( $product ) ) {
			$item_total = $item->get_total() + $item->get_total_tax(); ?>

			<div class="view">
				<strong class="total"><?php echo wc_price( $item_total ); ?></strong>
			</div>
			<div class="edit" style="display: none;">
				<div class="split-input">
					<div class="input">
						<label><?php _e('Gross total', 'bl'); ?></label>
						<input type="text" placeholder="0" value="<?php echo $item_total ?>" class="line_total wc_input_price" disabled="disabled">
					</div>
				</div>
			</div>

		<?php } ?>
	</td>

<?php }

/* Reorder order details tabel */
add_action('woocommerce_admin_order_totals_after_refunded', 'bl_admin_order_item_columns_reorder');
function bl_admin_order_item_columns_reorder( $order_id ){ ?>
	
	<script type="text/javascript" charset="utf-8">
		jQuery('.woocommerce_order_items_wrapper .woocommerce_order_items .item-gross-total').each( function(e){
			jQuery(this).insertBefore( jQuery(this).parent().find('.wc-order-edit-line-item') );
		});
	</script>

<?php }


/* Edit admin order details billing fields */
add_filter( 'woocommerce_admin_billing_fields', 'bl_edit_admin_billing_fields' );
function bl_edit_admin_billing_fields( $billing_fields = array() ){
	global $theorder;

	if ( ! is_object( $theorder ) ) {
		$theorder = wc_get_order( $post->ID );
	}

	$order = $theorder;

	if( !empty( $order ) ){
		$user_tax_number = get_post_meta( $order->get_id(), '_billing_tax_number', true );

		if( empty( $user_tax_number ) ){
			$user_id = get_post_meta( $order->get_id(), '_customer_user', true );

			if( !empty( $user_id ) ){
				$user_tax_number = get_user_meta( $user_id, 'tax-number', true );
			}
		}

		$billing_fields['tax_number'] = array(
			'label' => __('Tax number', 'bl' ),
			'value' => $user_tax_number,
			'show'  => true,
		);
	}

	return $billing_fields;
}