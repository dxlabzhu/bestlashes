<?php

add_action('pre_get_posts', 'bl_remove_invoices_from_media_library');
function bl_remove_invoices_from_media_library( $query ) {
    global $pagenow;

    if ( isset( $_POST['action'] ) && $_POST['action'] === 'query-attachments') {
        if ( current_user_can('manage_options') ) {

            $meta_query = $query->get('meta_query');

            if (count($meta_query) != 0) {
                $meta_query = array();
                $meta_query['relation'] = 'OR';

                array_push($meta_query, array(
                    'key' => '_is_invoice',
                    'value' => '1',
                    'compare' => '!=',
                ));
                array_push($meta_query, array(
                    'key' => '_is_invoice',
                    'compare' => 'NOT EXISTS',
                ));

                $query->set( 'meta_query', $meta_query );
            }

        }
    }

    return $query;
}