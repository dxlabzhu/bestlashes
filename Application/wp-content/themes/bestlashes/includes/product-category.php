<?php

// Add meta field to Product categories
add_action( 'product_cat_edit_form_fields', 'bl_add_product_cat_taxonomy_meta_fields', 10, 2 );
function bl_add_product_cat_taxonomy_meta_fields( $term, $taxonomy ){
    $featured_products = maybe_unserialize( get_term_meta( $term->term_id, 'featured-products', true ) ); 

    $args = array();
	$args['post_type'] = array( 'product' );
	$args['posts_per_page'] = '-1';
	$args['post_status'] = 'publish';
	$args['fields'] = 'ids';

	// Training courses excluded
	$args['post__not_in'] = bl_get_training_courses_ids();

	$products_loop = new WP_Query( $args );?>

    <tr class="form-field term-group-wrap">
        <th scope="row">
        	<label for="featured-products"><?php _e( 'Feature products', 'bl' ); ?></label>
        </th>
        <td>
        	<select class="postform select-2-featured-products" id="featured-products" name="featured-products[]" multiple="multiple">
	            <?php if( $products_loop->have_posts() ){
					while ( $products_loop->have_posts() ) {
						$prod_id = $products_loop->next_post();
						$selected = false;

						if( !empty( $featured_products ) ){
							if( in_array( $prod_id, $featured_products ) ){
								$selected = true;
							}
						} ?>

						<option value="<?php echo $prod_id ?>" <?php selected( $selected, true ); ?>><?php echo get_the_title( $prod_id ); ?></option>
					<?php }
				} ?>
        	</select>
        </td>
    </tr>

    <?php
}

// Save meta field to Product categories
add_action( 'edited_product_cat', 'bl_save_product_cat_taxonomy_meta_fields', 10, 2 );
function bl_save_product_cat_taxonomy_meta_fields( $term_id, $tt_id ){

    if( isset( $_POST['featured-products'] ) && '' !== $_POST['featured-products'] ){
        update_term_meta( $term_id, 'featured-products', serialize( $_POST['featured-products'] ) );
    }
}