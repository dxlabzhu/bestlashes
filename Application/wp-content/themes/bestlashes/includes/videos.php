<?php

add_action( 'init', 'bl_register_video_cpt' );
function bl_register_video_cpt() {
	$labels = array(
		'name' 					=> __( 'Videos', 'bl' ),
		'singular_name' 		=> __( 'Video', 'bl' ),
		'add_new' 				=> __( 'Add New', 'video', 'bl' ),
		'add_new_item' 			=> __( 'Add New Video', 'bl' ),
		'edit_item' 			=> __( 'Edit Video', 'bl' ),
		'new_item' 				=> __( 'New Video', 'bl' ),
		'view_item' 			=> __( 'View Video', 'bl' ),
		'search_items' 			=> __( 'Search Videos', 'bl' ),
		'not_found' 			=> __( 'No videos found.', 'bl' ),
		'not_found_in_trash' 	=> __( 'No videos found in Trash.', 'bl' ),
		'parent_item_colon' 	=> ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'exclude_from_search' => true,
		'rewrite' => array( 'slug' => 'video' ),
		'has_archive' => false,
		'menu_position' => 30,
		'menu_icon' => 'dashicons-video-alt',
		'supports' => array('title', 'thumbnail', 'custom-fields', 'page-attributes')
	);
	register_post_type('video', $args);
}


	/**************************/
	/*   Taxonomy for CPTs	*/
	/**************************/

add_action( 'init', 'bl_custom_taxonomies_for_video', 0 );
function bl_custom_taxonomies_for_video() {
	 

///////////////////////////// Video categories

	$labels = array(
		'name'							=>  __( 'Categories', 'bl' ),
		'singular_name'					=>  __( 'Category', 'bl' ),
		'search_items'					=>  __( 'Search Categories', 'bl' ),
		'all_items'						=>  __( 'All Categories', 'bl' ),
		'parent_item'					=> null,
		'parent_item_colon'				=> null,
		'edit_item'						=>  __( 'Edit Category', 'bl' ),
		'update_item'					=>  __( 'Update Category', 'bl' ),
		'add_new_item'					=>  __( 'Add New Category', 'bl' ),
		'new_item_name'					=>  __( 'New Category Name', 'bl' ),
		'separate_items_with_commas' 	=>  __( 'Separate categories with commas', 'bl' ),
		'add_or_remove_items'			=>  __( 'Add or remove categories', 'bl' ),
		'choose_from_most_used'			=>  __( 'Choose from the most used categories', 'bl' ),
		'not_found'						=>  __( 'No categories found.', 'bl' ),
		'menu_name'						=>  __( 'Category', 'bl' ),
	);

	$args = array(
		'hierarchical'					=> true,
		'labels'						=> $labels,
		'show_ui'						=> true,
		'show_tagcloud'					=> false,
		'show_admin_column'				=> true,
		'query_var'						=> true,
		'show_in_nav_menus'				=> true,
		'rewrite'						=> array( 'slug' => 'video-category' )
	);

	register_taxonomy('video_category', 'video', $args);

}


	/**************************/
	/*	  Custom Fields	 */
	/**************************/

add_action('add_meta_boxes', 'bl_add_custom_meta_boxes_to_video');
function bl_add_custom_meta_boxes_to_video() {
	
	// Beállítások
	add_meta_box("video_settings", __('Video settings', 'bl'), "video_settings_meta", "video", "normal", "default" );

}


function video_settings_meta() {
	global $post;
	$custom = get_post_custom($post -> ID); ?>
	
	<div class="aw-settings-section">
		<div class="menu-wrapper">
			<ul class="menu">
				<li data-menu-item="main-settings" class="active">
					<span><?php _e( 'Main settings', 'bl' ); ?></span>
				</li>
			</ul>
		</div>

		<div class="content-wrapper">
			<div class="content-box main-settings active">
				<h3><?php _e( 'Main settings', 'bl' ); ?></h3>
				<h4><?php _e('Video ID', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="video_id" value="<?php echo ( isset( $custom['video_id'][0] ) ? $custom['video_id'][0] : '' ); ?>" />
				</div>
			</div>
			
		</div>
	</div>

<?php }

///////////////////////////// Save CFs

add_action('save_post', 'bl_video_save_custom_postdata');
function bl_video_save_custom_postdata($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	global $post;

	if( isset( $_POST['post_type'] ) ){
		if ($_POST['post_type'] == "video") {
			
			// Main settings
			update_post_meta($post->ID, 'video_id', $_POST['video_id']);
		}
	}
}




/* Taxonomy meta field */
add_action( 'video_category_edit_form_fields', 'bl_add_featured_image_to_video_category', 10, 2 );
function bl_add_featured_image_to_video_category( $term, $taxonomy ){
	$term_image = get_term_meta( $term->term_id, 'featured_image', true );  ?>

	<tr class="form-field term-group-wrap">
		<th scope="row">
			<label for="feature-group"><?php _e( 'Featured Image', 'bl' ); ?></label>
		</th>
		<td>
			<input type="text" name="featured_image" value="<?php echo $term_image; ?>" placeholder="">
		</td>
	</tr>

<?php }


add_action( 'edited_video_category', 'bl_save_featured_image_to_video_category' );
function bl_save_featured_image_to_video_category( $term_id ){
	if ( isset( $_POST['featured_image'] ) ) {
		$term_image = $_POST['featured_image'];
		
		if( $term_image ) {
			 update_term_meta( $term_id, 'featured_image', $term_image );
		}
	} 
}