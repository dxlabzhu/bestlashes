<?php


	/**************************/
	/*       User Meta        */
	/**************************/
	
// User profile settings
add_action('user_new_form', 'bl_additional_user_options');
add_action('show_user_profile', 'bl_additional_user_options');
add_action('edit_user_profile', 'bl_additional_user_options');

function bl_additional_user_options($user) {
	$product_categories = get_terms(
		array(
			'taxonomy' => 'product_cat',
			'hide_empty' => false,
			'exclude' => array( BL_PRODUCT_CAT_ID_OTHERS, BL_PRODUCT_CAT_ID_TRAINING_COURSES, BL_PRODUCT_CAT_ID_DORCAY, BL_PRODUCT_CAT_ID_LASH_INC )
		)
	);
	$main_product_categories = get_terms(
		array(
			'taxonomy' => 'product_cat',
			'hide_empty' => false,
			'parent' => 0,
			'exclude' => array( BL_PRODUCT_CAT_ID_OTHERS, BL_PRODUCT_CAT_ID_TRAINING_COURSES, BL_PRODUCT_CAT_ID_DORCAY, BL_PRODUCT_CAT_ID_LASH_INC )
		)
	); ?>

	<hr />
	<h2><?php _e( 'BestLashes settings', 'bl' ); ?></h2>
	<table class="form-table additional">
		<tbody>
			<tr>
				<th><label for="activated_account"><?php _e( 'Activated account', 'bl' ); ?></label></th>
				<td><input type="checkbox" name="activated_account" id="activated_account" value="1" <?php checked('1', get_the_author_meta( 'activated_account', $user->ID )); ?> class="regular-text" /></td>
			</tr>
		</tbody>
	</table>
	<hr />
	<h2><?php _e( 'BestLashes fix user discount', 'bl' ); ?></h2>
	<table class="form-table additional">
		<tbody>
			<tr>
				<th>
					<label for="fix-user-discount"><?php _e( 'Fix user discount', 'bl' ); ?></label>
				</th>
				<td>
					<input type="text" id="fix-user-discount" name="fix-user-discount" value="<?php echo get_the_author_meta( 'fix-user-discount', $user->ID ) ?>" placeholder="" class="regular-text"> %
				</td>
			</tr>
		</tbody>
	</table>
	<hr>

	<!-- Product and category discount -->
	<h2><?php _e( 'BestLashes categories and products user discount', 'bl' ); ?></h2>
	<table class="form-table additional category-user-discount">
		<tbody>
			<tr>
				<th><label for="enable-category-user-discount"><?php _e( 'Enable category/product user discount', 'bl' ); ?></label></th>
				<td><input type="checkbox" name="enable-category-product-user-discount" id="enable-category-product-user-discount" value="1" <?php checked('1', get_the_author_meta( 'enable-category-product-user-discount', $user->ID )); ?> class="regular-text" /></td>
			</tr>
		</tbody>
	</table>
	<h3><?php _e('Category discount', 'bl'); ?></h3>
	<div class="form-table category-product-user-discount duplicatable">
		<?php $category_user_discount = maybe_unserialize( get_user_meta( $user->ID, 'category-user-discount', true ) );

		if( !empty( $category_user_discount ) ){
			foreach ( $category_user_discount as $category_discount ) {
				if( !empty( $category_discount ) ){ ?>
					<div class="duplicatable-element">
						<div class="flex-row">
			                <div class="category flex flex-1">
			                	<h5><?php _e('Category', 'bl'); ?></h5>
			                	<select name="category-user-discount-category[]">
			                		<option value="0"> - <?php _e( 'Please choose one', 'bl' ); ?> - </option>
			                		<?php if( !empty( $main_product_categories ) ){
			                			foreach ( $main_product_categories as $main_term ) { ?>
			                				<option value="<?php echo $main_term->term_id ?>" <?php selected( $main_term->term_id, $category_discount['id'] ); ?>><?php echo $main_term->name; ?></option>
			                				
			                				<?php if( !empty( $product_categories ) ){
												foreach ( $product_categories as $term ) {
													if( $term->parent == $main_term->term_id ){ ?>
														<option value="<?php echo $term->term_id; ?>" <?php selected( $term->term_id, $category_discount['id'] ); ?>> - <?php echo $term->name; ?></option>
													<?php }
												} 
											}
			                			}
			                		} ?>
			                	</select>
			                </div>
			                <div class="value flex flex-1">
			                	<h5><?php _e('Discount', 'bl'); ?></h5>
			                    <input type="text" id="category-user-discount" name="category-user-discount-value[]" value="<?php echo $category_discount['value']; ?>" placeholder="" class="regular-text"> %
			                </div>
			            </div>
			            <div class="remove-row">
			                <div class="remove">X</div>
			            </div>
					</div>
				<?php }
			}
		} ?>

		<div class="duplicatable-element prototype">
			<div class="flex-row">
                <div class="category flex flex-1">
                	<h5><?php _e('Category', 'bl'); ?></h5>
                	<select name="category-user-discount-category[]">
                		<option value="0"> - <?php _e( 'Please choose one', 'bl' ); ?> - </option>
                		<?php if( !empty( $main_product_categories ) ){
                			foreach ( $main_product_categories as $main_term ) { ?>
                				<option value="<?php echo $main_term->term_id ?>"><?php echo $main_term->name; ?></option>
                				
                				<?php if( !empty( $product_categories ) ){
									foreach ( $product_categories as $term ) {
										if( $term->parent == $main_term->term_id ){ ?>
											<option value="<?php echo $term->term_id; ?>"> - <?php echo $term->name; ?></option>
										<?php }
									} 
								}
                			}
                		} ?>
                	</select>
                </div>
                <div class="value flex flex-1">
                	<h5><?php _e('Discount', 'bl'); ?></h5>
                    <input type="text" id="category-user-discount" name="category-user-discount-value[]" value="" placeholder="" class="regular-text"> %
                </div>
            </div>
            <div class="remove-row">
                <div class="remove">X</div>
            </div>
		</div>
		<div class="add-new-row button"><?php _e( 'Add new row', 'bl' ); ?></div>
	</div>

	
	<h3><?php _e('Product discount', 'bl'); ?></h3>
	<div class="form-table category-product-user-discount duplicatable">
		<?php $product_user_discount = maybe_unserialize( get_user_meta( $user->ID, 'product-user-discount', true ) );

		$args = array();
	    $args['post_type'] = array( 'product' );
	    $args['posts_per_page'] = '-1';
	    $args['post_status'] = 'publish';
	    $args['fields'] = 'ids';

	    // Training courses excluded
	    $args['post__not_in'] = bl_get_training_courses_ids();

	    $products_loop = new WP_Query( $args );

		if( !empty( $product_user_discount ) ){
			foreach ( $product_user_discount as $product_discount ) {
				if( !empty( $product_discount ) ){ ?>
					<div class="duplicatable-element">
						<div class="flex-row">
			                <div class="product flex flex-1">
			                	<h5><?php _e('Product', 'bl'); ?></h5>
			                	<select class="select-2" name="product-user-discount-product[]">
			                		<option value="0"> - <?php _e( 'Please choose one', 'bl' ); ?> - </option>
			                		<?php if( $products_loop->have_posts() ){
									        while ( $products_loop->have_posts() ) {
								            $prod_id = $products_loop->next_post(); ?>

			                				<option value="<?php echo $prod_id ?>" <?php selected( $prod_id, $product_discount['id'] ); ?>><?php echo get_the_title( $prod_id ); ?></option>

			                			<?php }
			                		} ?>
			                	</select>
			                </div>
			                <div class="value flex flex-1">
			                	<h5><?php _e('Discount', 'bl'); ?></h5>
			                    <input type="text" id="category-user-discount" name="product-user-discount-value[]" value="<?php echo $product_discount['value']; ?>" placeholder="" class="regular-text"> %
			                </div>
			            </div>
			            <div class="remove-row">
			                <div class="remove">X</div>
			            </div>
					</div>
				<?php }
			}
		} ?>

		<div class="duplicatable-element prototype">
			<div class="flex-row">
                <div class="product flex flex-1">
                	<h5><?php _e('Product', 'bl'); ?></h5>
                	<select class="select-2-able" name="product-user-discount-product[]">
                		<option value="0"> - <?php _e( 'Please choose one', 'bl' ); ?> - </option>
                		<?php if( $products_loop->have_posts() ){
						        while ( $products_loop->have_posts() ) {
					            $prod_id = $products_loop->next_post(); ?>

                				<option value="<?php echo $prod_id ?>"><?php echo get_the_title( $prod_id ); ?></option>

                			<?php }
                		} ?>
                	</select>
                </div>
                <div class="value flex flex-1">
                	<h5><?php _e('Discount', 'bl'); ?></h5>
                    <input type="text" id="product-user-discount" name="product-user-discount-value[]" value="" placeholder="" class="regular-text"> %
                </div>
            </div>
            <div class="remove-row">
                <div class="remove">X</div>
            </div>
		</div>
		<div class="add-new-row button"><?php _e( 'Add new row', 'bl' ); ?></div>
	</div>
	<!-- /Product and category discount -->

	<hr>

	<!-- Disable products and category -->
	<h2><?php _e( 'BestLashes disable categories and products for user', 'bl' ); ?></h2>
	<table class="form-table additional category-user-discount">
		<tbody>
			<tr>
				<th><label for="disable-category-product-for-user"><?php _e( 'Disable category/product for user', 'bl' ); ?></label></th>
				<td><input type="checkbox" name="disable-category-product-for-user" id="disable-category-product-for-user" value="1" <?php checked('1', get_the_author_meta( 'disable-category-product-for-user', $user->ID )); ?> class="regular-text" /></td>
			</tr>
		</tbody>
	</table>
	<h3><?php _e('Disable Category products', 'bl'); ?></h3>
	<div class="form-table category-product-user-discount duplicatable">
		<?php $disabled_categories = maybe_unserialize( get_user_meta( $user->ID, 'disabled-categories', true ) );

		if( !empty( $disabled_categories ) ){
			foreach ( $disabled_categories as $disabled_category ) {
				if( !empty( $disabled_category ) ){ ?>
					<div class="duplicatable-element">
						<div class="flex-row">
			                <div class="category flex flex-1">
			                	<h5><?php _e('Category', 'bl'); ?></h5>
			                	<select name="disable-category-for-user[]">
			                		<option value="0"> - <?php _e( 'Please choose one', 'bl' ); ?> - </option>
			                		<?php if( !empty( $main_product_categories ) ){
			                			foreach ( $main_product_categories as $main_term ) { ?>
			                				<option value="<?php echo $main_term->term_id ?>" <?php selected( $main_term->term_id, $disabled_category ); ?>><?php echo $main_term->name; ?></option>
			                				
			                				<?php if( !empty( $product_categories ) ){
												foreach ( $product_categories as $term ) {
													if( $term->parent == $main_term->term_id ){ ?>
														<option value="<?php echo $term->term_id; ?>" <?php selected( $term->term_id, $disabled_category ); ?>> - <?php echo $term->name; ?></option>
													<?php }
												} 
											}
			                			}
			                		} ?>
			                	</select>
			                </div>
			            </div>
			            <div class="remove-row">
			                <div class="remove">X</div>
			            </div>
					</div>
				<?php }
			}
		} ?>

		<div class="duplicatable-element prototype">
			<div class="flex-row">
                <div class="category flex flex-1">
                	<h5><?php _e('Category', 'bl'); ?></h5>
                	<select name="disable-category-for-user[]">
                		<option value="0"> - <?php _e( 'Please choose one', 'bl' ); ?> - </option>
                		<?php if( !empty( $main_product_categories ) ){
                			foreach ( $main_product_categories as $main_term ) { ?>
                				<option value="<?php echo $main_term->term_id ?>"><?php echo $main_term->name; ?></option>
                				
                				<?php if( !empty( $product_categories ) ){
									foreach ( $product_categories as $term ) {
										if( $term->parent == $main_term->term_id ){ ?>
											<option value="<?php echo $term->term_id; ?>"> - <?php echo $term->name; ?></option>
										<?php }
									} 
								}
                			}
                		} ?>
                	</select>
                </div>
            </div>
            <div class="remove-row">
                <div class="remove">X</div>
            </div>
		</div>
		<div class="add-new-row button"><?php _e( 'Add new row', 'bl' ); ?></div>
	</div>

	
	<h3><?php _e('Disable product for user', 'bl'); ?></h3>
	<div class="form-table category-product-user-discount duplicatable">
		<?php $disabled_products = maybe_unserialize( get_user_meta( $user->ID, 'disabled-products', true ) );

		$args = array();
	    $args['post_type'] = array( 'product' );
	    $args['posts_per_page'] = '-1';
	    $args['post_status'] = 'publish';
	    $args['fields'] = 'ids';

	    // Training courses excluded
	    $args['post__not_in'] = bl_get_training_courses_ids();

	    $products_loop = new WP_Query( $args );

		if( !empty( $disabled_products ) ){
			foreach ( $disabled_products as $disabled_product ) {
				if( !empty( $disabled_product ) ){ ?>
					<div class="duplicatable-element">
						<div class="flex-row">
			                <div class="product flex flex-1">
			                	<h5><?php _e('Product', 'bl'); ?></h5>
			                	<select class="select-2" name="disable-product-for-user[]">
			                		<option value="0"> - <?php _e( 'Please choose one', 'bl' ); ?> - </option>
			                		<?php if( $products_loop->have_posts() ){
									        while ( $products_loop->have_posts() ) {
								            $prod_id = $products_loop->next_post(); ?>

			                				<option value="<?php echo $prod_id ?>" <?php selected( $prod_id, $disabled_product ); ?>><?php echo get_the_title( $prod_id ); ?></option>

			                			<?php }
			                		} ?>
			                	</select>
			                </div>
			            </div>
			            <div class="remove-row">
			                <div class="remove">X</div>
			            </div>
					</div>
				<?php }
			}
		} ?>

		<div class="duplicatable-element prototype">
			<div class="flex-row">
                <div class="product flex flex-1">
                	<h5><?php _e('Product', 'bl'); ?></h5>
                	<select class="select-2-able" name="disable-product-for-user[]">
                		<option value="0"> - <?php _e( 'Please choose one', 'bl' ); ?> - </option>
                		<?php if( $products_loop->have_posts() ){
						        while ( $products_loop->have_posts() ) {
					            $prod_id = $products_loop->next_post(); ?>

                				<option value="<?php echo $prod_id ?>"><?php echo get_the_title( $prod_id ); ?></option>

                			<?php }
                		} ?>
                	</select>
                </div>
            </div>
            <div class="remove-row">
                <div class="remove">X</div>
            </div>
		</div>
		<div class="add-new-row button"><?php _e( 'Add new row', 'bl' ); ?></div>
	</div>
	<!-- /Disable products and category -->

	<hr>
<?php 
}


// Save user Meta
add_action('user_register', 'bl_additional_user_options_update');
add_action('personal_options_update', 'bl_additional_user_options_update');
add_action('edit_user_profile_update', 'bl_additional_user_options_update');

function bl_additional_user_options_update($user_id) {
	
	// Activated user account
	update_user_meta( $user_id, 'activated_account', isset($_POST['activated_account']));

	// Fix user discount
	if( !empty( $_POST['fix-user-discount'] ) ){
		update_user_meta( $user_id, 'fix-user-discount', (int)$_POST['fix-user-discount'] );
	} else {
		delete_user_meta( $user_id, 'fix-user-discount' );
	}

	// Enable category-product user discount
	update_user_meta( $user_id, 'enable-category-product-user-discount', (int)$_POST['enable-category-product-user-discount'] );

	// Category user discount
	if( !empty( $_POST['category-user-discount-category'] ) && !empty( $_POST['category-user-discount-value'] ) ){
		$category_user_discount = array();

		foreach ( $_POST['category-user-discount-category'] as $key => $term_id ) {
			if( !empty( $term_id ) && ( !empty( $_POST['category-user-discount-value'][$key] || $_POST['category-user-discount-value'][$key] == '0' ) ) ){
				$category_user_discount[] = array(
					'id' => $term_id,
					'value' => (int)$_POST['category-user-discount-value'][$key]
				);
			}
		}

		if( !empty( $category_user_discount ) ){
			update_user_meta( $user_id, 'category-user-discount', $category_user_discount );
		}
	}

	// Product user discount
	if( !empty( $_POST['product-user-discount-product'] ) && !empty( $_POST['product-user-discount-value'] ) ){
		$product_user_discount = array();

		foreach ( $_POST['product-user-discount-product'] as $key => $term_id ) {
			if( !empty( $term_id ) && ( !empty( $_POST['product-user-discount-value'][$key] || $_POST['product-user-discount-value'][$key] == '0' ) ) ){
				$product_user_discount[] = array(
					'id' => $term_id,
					'value' => (int)$_POST['product-user-discount-value'][$key]
				);
			}
		}

		if( !empty( $product_user_discount ) ){
			update_user_meta( $user_id, 'product-user-discount', $product_user_discount );
		}
	}

	// Disable category-product for user
	update_user_meta( $user_id, 'disable-category-product-for-user', (int)$_POST['disable-category-product-for-user'] );

	// Disable categories for user
	if( !empty( $_POST['disable-category-for-user'] ) ){
		$disabled_categories = array();

		foreach ( $_POST['disable-category-for-user'] as $key => $term_id ) {
			if( !empty( $term_id ) ){
				$disabled_categories[] = $term_id;
			}
		}

		if( !empty( $disabled_categories ) ){
			update_user_meta( $user_id, 'disabled-categories', $disabled_categories );
		}
	}

	// Disable products for user
	if( !empty( $_POST['disable-product-for-user'] ) ){
		$disabled_products = array();

		foreach ( $_POST['disable-product-for-user'] as $key => $prod_id ) {
			if( !empty( $prod_id ) ){
				$disabled_products[] = $prod_id;
			}
		}

		if( !empty( $disabled_products ) ){
			update_user_meta( $user_id, 'disabled-products', $disabled_products );
		}
	}
}

?>