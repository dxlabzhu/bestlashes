<?php 

    /***************************************/
    /*          Admin menu item            */
    /***************************************/

add_action('admin_menu', 'bl_options_panel_page');
function bl_options_panel_page() {
	add_menu_page(
        __( 'Bestlashes settings', 'bl' ),
        __( 'Bestlashes settings', 'bl' ),
        'manage_options',
        'bl_options_panel_settings',
        'bl_options_panel_settings_page',
        'dashicons-admin-settings',
        41
    );
}


function bl_options_panel_settings_page(){
    $langs = icl_get_languages('skip_missing=0');

    // Save
    if(isset($_POST['bl_save_option_panel']) && $_POST['bl_save_option_panel'] == 1){
        $retrieved_nonce = $_REQUEST['_wpnonce'];
        if ( !wp_verify_nonce( $retrieved_nonce, 'bl_option_panel' ) ) die( 'Failed security check' );

        // Default tabs //
        // Social
        update_option('bl-right-side-menu-facebook-link', ( isset( $_POST['bl-right-side-menu-facebook-link'] ) ? wp_kses_post( stripslashes( $_POST['bl-right-side-menu-facebook-link'] ) ) : '' ) );
        update_option('bl-right-side-menu-instagram-link', ( isset( $_POST['bl-right-side-menu-instagram-link'] ) ? wp_kses_post( stripslashes( $_POST['bl-right-side-menu-instagram-link'] ) ) : '' ) );
        update_option('bl-right-side-menu-email', ( isset( $_POST['bl-right-side-menu-email'] ) ? wp_kses_post( stripslashes( $_POST['bl-right-side-menu-email'] ) ) : '' ) );

        // Newsletter
        update_option( 'bl-enable-newsletter-coupon', ( isset( $_POST['bl-enable-newsletter-coupon'] ) ? $_POST['bl-enable-newsletter-coupon'] : '0' ) );

        // Posts per page
        /*update_option('bl-posts-per-page-featured-products-on-frontpage', ( isset( $_POST['bl-posts-per-page-featured-products-on-frontpage'] ) ? wp_kses_post( stripslashes( $_POST['bl-posts-per-page-featured-products-on-frontpage'] ) ) : '' ) );
        update_option('bl-posts-per-page-webshop-products-view-limit', ( isset( $_POST['bl-posts-per-page-webshop-products-view-limit'] ) ? wp_kses_post( stripslashes( $_POST['bl-posts-per-page-webshop-products-view-limit'] ) ) : '' ) );
        update_option('bl-posts-per-page-related-products-on-single-products', ( isset( $_POST['bl-posts-per-page-related-products-on-single-products'] ) ? wp_kses_post( stripslashes( $_POST['bl-posts-per-page-related-products-on-single-products'] ) ) : '' ) );
        update_option('bl-posts-per-page-related-posts-on-single-post-sidebar', ( isset( $_POST['bl-posts-per-page-related-posts-on-single-post-sidebar'] ) ? wp_kses_post( stripslashes( $_POST['bl-posts-per-page-related-posts-on-single-post-sidebar'] ) ) : '' ) );
        update_option('bl-posts-per-page-related-products-on-single-post-sidebar', ( isset( $_POST['bl-posts-per-page-related-products-on-single-post-sidebar'] ) ? wp_kses_post( stripslashes( $_POST['bl-posts-per-page-related-products-on-single-post-sidebar'] ) ) : '' ) );*/

        // Other
        update_option( 'bl-enable-birthday-coupon', ( isset( $_POST['bl-enable-birthday-coupon'] ) ? $_POST['bl-enable-birthday-coupon'] : '0' ) );
        

        // Lang tabs //
        if( !empty( $langs ) ){
            foreach ( $langs as $code => $lang ) {
                
                // Shop
                update_option( 'bl-before-category-list-banner-content-' . $lang['code'], ( isset( $_POST['bl-before-category-list-banner-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-before-category-list-banner-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-before-category-list-banner-background-' . $lang['code'], ( isset( $_POST['bl-before-category-list-banner-background-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-before-category-list-banner-background-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-after-category-list-banner-content-' . $lang['code'], ( isset( $_POST['bl-after-category-list-banner-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-after-category-list-banner-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-after-category-list-banner-background-' . $lang['code'], ( isset( $_POST['bl-after-category-list-banner-background-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-after-category-list-banner-background-' . $lang['code']] ) ) : '' ) );
                $attributes = wc_get_attribute_taxonomies();
                if ( !empty( $attributes ) ){
                    foreach ( $attributes as $attribute ) {
                        update_option( 'bl-product-variation-label-' . $attribute->attribute_id . '-' . $lang['code'], ( isset( $_POST['bl-product-variation-label-' . $attribute->attribute_id . '-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-product-variation-label-' . $attribute->attribute_id . '-' . $lang['code']] ) ) : '' ) );
                    }
                }
                
                // Checkout
                update_option( 'bl-progress-bar-text-step-1-' . $lang['code'], ( isset( $_POST['bl-progress-bar-text-step-1-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-progress-bar-text-step-1-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-progress-bar-text-step-2-' . $lang['code'], ( isset( $_POST['bl-progress-bar-text-step-2-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-progress-bar-text-step-2-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-progress-bar-text-step-3-' . $lang['code'], ( isset( $_POST['bl-progress-bar-text-step-3-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-progress-bar-text-step-3-' . $lang['code']] ) ) : '' ) );
                
                // Woocommerce
                update_option( 'bl-woocommerce-email-text-only-product-' . $lang['code'], ( isset( $_POST['bl-woocommerce-email-text-only-product-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-woocommerce-email-text-only-product-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-woocommerce-email-text-only-online-courses-' . $lang['code'], ( isset( $_POST['bl-woocommerce-email-text-only-online-courses-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-woocommerce-email-text-only-online-courses-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-woocommerce-email-text-only-training-courses-' . $lang['code'], ( isset( $_POST['bl-woocommerce-email-text-only-training-courses-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-woocommerce-email-text-only-training-courses-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-woocommerce-email-text-product-and-training-courses-' . $lang['code'], ( isset( $_POST['bl-woocommerce-email-text-product-and-training-courses-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-woocommerce-email-text-product-and-training-courses-' . $lang['code']] ) ) : '' ) );

                // Registration
                update_option( 'bl-content-before-registration-form-' . $lang['code'], ( isset( $_POST['bl-content-before-registration-form-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-content-before-registration-form-' . $lang['code']] ) ) : '' ) );

                // Popups
                update_option( 'bl-popup-registration-verify-success-title-' . $lang['code'], ( isset( $_POST['bl-popup-registration-verify-success-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-registration-verify-success-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-registration-verify-success-content-' . $lang['code'], ( isset( $_POST['bl-popup-registration-verify-success-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-registration-verify-success-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-registration-verify-fail-title-' . $lang['code'], ( isset( $_POST['bl-popup-registration-verify-fail-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-registration-verify-fail-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-registration-verify-fail-content-' . $lang['code'], ( isset( $_POST['bl-popup-registration-verify-fail-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-registration-verify-fail-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-forgotten-password-password-change-fail-title-' . $lang['code'], ( isset( $_POST['bl-popup-forgotten-password-password-change-fail-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-forgotten-password-password-change-fail-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-forgotten-password-password-change-fail-content-' . $lang['code'], ( isset( $_POST['bl-popup-forgotten-password-password-change-fail-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-forgotten-password-password-change-fail-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-newsletter-subscription-verify-success-title-' . $lang['code'], ( isset( $_POST['bl-popup-newsletter-subscription-verify-success-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-newsletter-subscription-verify-success-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-newsletter-subscription-verify-success-content-' . $lang['code'], ( isset( $_POST['bl-popup-newsletter-subscription-verify-success-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-newsletter-subscription-verify-success-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-newsletter-subscription-verify-fail-title-' . $lang['code'], ( isset( $_POST['bl-popup-newsletter-subscription-verify-fail-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-newsletter-subscription-verify-fail-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-newsletter-subscription-verify-fail-content-' . $lang['code'], ( isset( $_POST['bl-popup-newsletter-subscription-verify-fail-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-newsletter-subscription-verify-fail-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-newsletter-unsubscription-verify-success-title-' . $lang['code'], ( isset( $_POST['bl-popup-newsletter-unsubscription-verify-success-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-newsletter-unsubscription-verify-success-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-newsletter-unsubscription-verify-success-content-' . $lang['code'], ( isset( $_POST['bl-popup-newsletter-unsubscription-verify-success-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-newsletter-unsubscription-verify-success-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-newsletter-unsubscription-verify-fail-title-' . $lang['code'], ( isset( $_POST['bl-popup-newsletter-unsubscription-verify-fail-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-newsletter-unsubscription-verify-fail-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-newsletter-unsubscription-verify-fail-content-' . $lang['code'], ( isset( $_POST['bl-popup-newsletter-unsubscription-verify-fail-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-newsletter-unsubscription-verify-fail-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-delete-user-confirmation-title-' . $lang['code'], ( isset( $_POST['bl-popup-delete-user-confirmation-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-delete-user-confirmation-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-delete-user-confirmation-content-' . $lang['code'], ( isset( $_POST['bl-popup-delete-user-confirmation-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-delete-user-confirmation-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-delete-user-success-title-' . $lang['code'], ( isset( $_POST['bl-popup-delete-user-success-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-delete-user-success-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-delete-user-success-content-' . $lang['code'], ( isset( $_POST['bl-popup-delete-user-success-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-delete-user-success-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-search-title-' . $lang['code'], ( isset( $_POST['bl-popup-search-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-search-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-popup-search-content-' . $lang['code'], ( isset( $_POST['bl-popup-search-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-popup-search-content-' . $lang['code']] ) ) : '' ) );

                // Footer
                update_option( 'bl-before-newsletter-subscription-content-' . $lang['code'], ( isset( $_POST['bl-before-newsletter-subscription-content-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-before-newsletter-subscription-content-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-newsletter-subscription-title-' . $lang['code'], ( isset( $_POST['bl-newsletter-subscription-title-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-newsletter-subscription-title-' . $lang['code']] ) ) : '' ) );
                update_option( 'bl-copyright-text-' . $lang['code'], ( isset( $_POST['bl-copyright-text-' . $lang['code']] ) ? wp_kses_post( stripslashes( $_POST['bl-copyright-text-' . $lang['code']] ) ) : '' ) );

                // Gift products
                update_option( 'bl-enable-gift-products-' . $lang['code'], ( isset( $_POST['bl-enable-gift-products-' . $lang['code']] ) ? $_POST['bl-enable-gift-products-' . $lang['code']] : '0' ) );
                if( !empty( $_POST['bl-gift-product-from-' . $lang['code']] ) ){
                    $gift_products_enabled_from_date = DateTime::createFromFormat( 'Y-m-d H:i', $_POST['bl-gift-product-from-' . $lang['code']] );
                    $gift_products_enabled_from_timestamp = $gift_products_enabled_from_date->format('U');

                    update_option( 'bl-gift-product-from-' . $lang['code'], $gift_products_enabled_from_timestamp );
                } else {
                    update_option( 'bl-gift-product-from-' . $lang['code'], '' );
                }
                if( !empty( $_POST['bl-gift-product-to-' . $lang['code']] ) ){
                    $gift_products_enabled_from_date = DateTime::createFromFormat( 'Y-m-d H:i', $_POST['bl-gift-product-to-' . $lang['code']] );
                    $gift_products_enabled_from_timestamp = $gift_products_enabled_from_date->format('U');

                    update_option( 'bl-gift-product-to-' . $lang['code'], $gift_products_enabled_from_timestamp );
                } else {
                    update_option( 'bl-gift-product-to-' . $lang['code'], '' );
                }
                if( !empty( $_POST['bl-gift-products-' . $lang['code']] ) && !empty( $_POST['bl-gift-products-cart-limit-' . $lang['code']] ) ) {
                    $bl_gift_product_discount = array();

                    foreach ( $_POST['bl-gift-products-' . $lang['code']] as $key => $product_id ) {
                        if( !empty( $_POST['bl-gift-products-cart-limit-' . $lang['code']][$key] ) ){
                            $bl_gift_product_discount[] = array(
                                'limit' => $_POST['bl-gift-products-cart-limit-' . $lang['code']][$key],
                                'product' => $product_id,
                                'variation' => ( !empty( $_POST['bl-gift-products-variation-' . $lang['code']][$key] ) ? $_POST['bl-gift-products-variation-' . $lang['code']][$key] : '' )
                            );
                        }
                    }

                    update_option( 'bl-gift-products-discount-' . $lang['code'], $bl_gift_product_discount );
                }
            }
        }

        do_action( 'bl-options-panel-save-fields', $_POST, $langs );
    } 

    // Tabs
    $tabs = array(
        array(
            'name' => __( 'Social settings', 'bl' ),
            'slug' => 'social',
            'template_file' => 'includes/options-panels/social.php'
        ),
        array(
            'name' => __( 'Newsletter settings', 'bl' ),
            'slug' => 'newsletter',
            'template_file' => 'includes/options-panels/newsletter.php'
        ),
        array(
            'name' => __( 'Other settings', 'bl' ),
            'slug' => 'other',
            'template_file' => 'includes/options-panels/other.php'
        )
    );
    $tabs = apply_filters( 'bl_options_panel_tabs', $tabs );

    $lang_tabs = array(
        array(
            'name' => __( 'Shop settings', 'bl' ),
            'slug' => 'shop',
            'template_file' => 'includes/options-panels/lang-shop.php'
        ),
        array(
            'name' => __( 'Checkout settings', 'bl' ),
            'slug' => 'checkout',
            'template_file' => 'includes/options-panels/lang-checkout.php'
        ),
        array(
            'name' => __( 'Woocommerce settings', 'bl' ),
            'slug' => 'woocommerce',
            'template_file' => 'includes/options-panels/lang-woocommerce.php'
        ),
        array(
            'name' => __( 'Registration settings', 'bl' ),
            'slug' => 'registration',
            'template_file' => 'includes/options-panels/lang-registration.php'
        ),
        array(
            'name' => __( 'Popups settings', 'bl' ),
            'slug' => 'popups',
            'template_file' => 'includes/options-panels/lang-popups.php'
        ),
        array(
            'name' => __( 'Footer settings', 'bl' ),
            'slug' => 'footer',
            'template_file' => 'includes/options-panels/lang-footer.php'
        ),
        array(
            'name' => __( 'Gift products', 'bl' ),
            'slug' => 'gift-products',
            'template_file' => 'includes/options-panels/lang-gift-products.php'
        )
    );
    $lang_tabs = apply_filters( 'bl_options_panel_lang_tabs', $lang_tabs );


    // Products for Gift products
    $gift_products = array();
    
    $args = array();
    $args['post_type'] = array( 'product' );
    $args['posts_per_page'] = '-1';
    $args['post_status'] = 'publish';
    $args['fields'] = 'ids';

    // Training courses excluded
    $args['post__not_in'] = bl_get_training_courses_ids();

    $products_loop = new WP_Query( $args );

    if( $products_loop->have_posts() ){
        while ( $products_loop->have_posts() ) {
            $prod_id = $products_loop->next_post();

            $gift_products[] = array(
                'id' => $prod_id,
                'name' => get_the_title( $prod_id )
            );
        }
    }

    ?>
    <div class="wrap inside aw-settings-container">
        <h2><?php _e('Settings page', 'bl') ?></h2>
        <br><hr><br>
        
        <form method="post" action="" autocomplete="off">
            <div class="aw-settings-section">
                <div class="menu-wrapper">
                    <ul class="menu">

                        <?php
                        // Default tabs
                        if( !empty( $tabs ) ){
                            $i = 0;

                            foreach ( $tabs as $tab ) { ?>

                                <li data-menu-item="<?php echo $tab['slug'] ?>-settings" class="<?php echo ( $i == 0 ? 'active' : '' ); ?>">
                                    <span><?php echo $tab['name']; ?></span>
                                </li>

                                <?php $i++;
                            }
                        }

                        // Lang tabs
                        if( !empty( $langs ) ){
                            $i = 0;

                            foreach ( $langs as $code => $lang ) { ?>

                                <h3><?php echo $lang['native_name']; ?></h3>

                                <?php if( !empty( $lang_tabs ) ){
                                    foreach ( $lang_tabs as $tab ) { ?>

                                        <li data-menu-item="<?php echo $tab['slug'] ?>-settings-<?php echo $lang['code']; ?>" class="">
                                            <span><?php echo $tab['name']; ?></span>
                                        </li>
                                        
                                    <?php }
                                }
                            }
                        } ?>

                    </ul>
                    
                    <input type="hidden" name="bl_save_option_panel" value="1">
                    <input type="submit" value="<?php _e('Save', 'bl'); ?>" name="bl_save_option_panel_save" class="button button-primary button-large" />
                </div>

                <div class="content-wrapper">
                    <?php 
                    $i = 0;

                    // Default tabs
                    if( !empty( $tabs ) ){
                        foreach ( $tabs as $tab ) {
                            $eclass = array();
                            
                            if( $i == 0 ){
                                $eclass[] = 'active';
                            }

                            include( locate_template( $tab['template_file'], false, false ) );

                            $i++;
                        }
                    }

                    // Lang tabs
                    if( !empty( $langs ) ){
                        foreach ( $langs as $code => $lang ) {
                            if( !empty( $lang_tabs ) ){
                                foreach ( $lang_tabs as $tab ) {
                                    include( locate_template( $tab['template_file'], false, false ) );
                                }
                            }
                        }
                    } ?>
                </div>

            <?php wp_nonce_field('bl_option_panel'); ?>
        </form>
    </div>
<?php }