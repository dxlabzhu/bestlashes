<?php

add_shortcode('gold_handwrite', 'gold_handwrite_shortcode');
function gold_handwrite_shortcode($atts, $content = null) {
	return '<span class="script">'. do_shortcode( $content ) .'</span>';
}


add_shortcode('button', 'button_shortcode');
function button_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'label' => '',
		'link' => '',
		'eclass' => ''
	), $atts));

	return '<a href="'. $link .'" class="underline-button '. $eclass .'"><div class="underline-button-text">'. $label .' →</div><div class="link-underline"></div></a>';
}


add_shortcode( 'breadcrumb', 'breadcrumb_shorcode' );
function breadcrumb_shorcode( $atts, $content = null ){
	ob_start();
	bl_breadcrumb();
	return ob_get_clean();
}


add_shortcode('icon', 'icon_shortcode');
function icon_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'type' => '',
	), $atts));
	
	$reutrn = '';

	if( !empty( $type ) ){
		$reutrn = '<span class="icon '. $type .'"></span>';
	}

	return $reutrn;
}


add_shortcode('vertical-line', 'vertical_line_shortcode');
function vertical_line_shortcode($atts, $content = null) {
	return '<span class="vertical-line"></span>';
}

add_shortcode('lash-inc-logo', 'lash_inc_logo_shortcode');
function lash_inc_logo_shortcode($atts, $content = null) {
	return '<img src="/wp-content/uploads/2018/12/slider-logo-lashinc.png" class="slider-lash-inc-logo">';
}


add_shortcode( 'bl_map', 'map_shortcode' );
function map_shortcode( $atts, $content = null ){
	extract( shortcode_atts (
		array(
			'lat' => '',
			'long' => ''
		),
		$atts )
	);

	$return = '';

	$return .= '<div class="bl-map" id="bl-map" data-lat="'. $lat .'" data-long="'. $long .'"></div>';

	return $return;
}


add_shortcode( 'bestbrows-text-logo', 'bestbrows_text_logo' );
function bestbrows_text_logo( $atts, $content = null ){
	return '<span class="text-logo">'. do_shortcode( $content ) .'</span>';
}