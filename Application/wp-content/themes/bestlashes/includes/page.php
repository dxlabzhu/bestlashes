<?php

	/**************************/
	/*      Custom Fields     */
	/**************************/

add_action('add_meta_boxes', 'bl_add_custom_meta_boxes_to_page');
function bl_add_custom_meta_boxes_to_page() {
	
	// Beállítások
	add_meta_box("page_settings", __('Page settings', 'bl'), "page_settings_meta", "page", "normal", "default" );

}


function page_settings_meta() {
	global $post;
	$custom = get_post_custom( $post->ID ); ?>
	
	<div class="aw-settings-section">
		<div class="menu-wrapper">
			<ul class="menu">
				<li data-menu-item="main-settings" class="active">
					<span><?php _e( 'Main settings', 'bl' ); ?></span>
				</li>
			</ul>
		</div>

		<div class="content-wrapper">
			<div class="content-box main-settings active">
				<h3><?php _e( 'Breadcrumb', 'bl' ); ?></h3>
				<div class="chckbox-box">
					<label><input type="checkbox" name="need-breadcrumb" value="1" <?php checked( $custom['need-breadcrumb'][0], '1' ); ?>><?php _e('Force breadcrumb to show', 'bl') ?></label>
				</div>
			</div>
			
		</div>
	</div>

<?php }

///////////////////////////// Save CFs

add_action('save_post', 'bl_page_save_custom_postdata');
function bl_page_save_custom_postdata( $post_id ) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

	if( isset( $_POST['post_type'] ) ){
		if ($_POST['post_type'] == "page") {
			
			// Main settings
			update_post_meta( $post_id, 'need-breadcrumb', ( isset( $_POST['need-breadcrumb'] ) ? $_POST['need-breadcrumb'] : '0' ) );
			
		}
	}
}