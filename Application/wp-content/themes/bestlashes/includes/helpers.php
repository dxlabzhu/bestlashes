<?php


//////////////////////// Get Excerpt By ID

function get_the_excerpt_by_id($post_id, $word_count = 55) {
    global $post;
    $save_post = $post;
    $post = get_post($post_id);
    $output = $post->post_excerpt;
    $post = $save_post;

    if($output == ''){
        $output = get_the_content_by_id($post_id);
        $output = strip_shortcodes( $output );
        $output = apply_filters('the_content', $output);
        $output = str_replace(']]>', ']]>', $output);
        $output = strip_tags($output);
        $output = nl2br($output);
        $excerpt_length = apply_filters('excerpt_length', $word_count);
        $words = explode(' ', $output, $excerpt_length + 1);
        if (count($words) > $excerpt_length) {
            array_pop($words);
            array_push($words, '..');
            $output = implode(' ', $words);
        }
    }
    
    // Language
    $output = str_replace('[:hu]', '', $output);
    $output = str_replace('[:en]', '', $output);
    $output = str_replace('[:]', '', $output);
    $output = strip_tags( $output );
    return do_shortcode($output);
}


//////////////////////// Get Filtered Content By ID

function get_the_content_by_id( $post_id ) {
    $page_data = get_page( $post_id );
    if ( $page_data ){
        return apply_filters( 'the_content', $page_data->post_content );
    }
    
    return false;
}


//////////////////////// Get Excerpt By ID for products

function get_the_excerpt_by_id_for_products($post_id, $word_count = 55) {
    global $post;
    $save_post = $post;
    $post = get_post($post_id);
    $output = $post->post_excerpt;
    
    // Language
    $output = str_replace('[:hu]', '', $output);
    $output = str_replace('[:en]', '', $output);
    $output = str_replace('[:]', '', $output);
    $output = strip_tags( $output );
    return do_shortcode($output);
}


// Bestlashes options by language
function bl_get_option_lang( $option_name = '', $lang_code = '' ){
    if( !empty( $option_name ) ){
        if( empty( $lang_code ) ){
            $lang_code = ICL_LANGUAGE_CODE;
        }

        return get_option( $option_name . '-' . $lang_code ); 
    }

    return false;
}


function get_current_ip_address(){
    $ip = '';

    if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
        //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
        //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    $ip = apply_filters( 'wpb_get_ip', $ip );

    return $ip;
}


function is_dev_server(){
    $dev_server = false;

    $base_url = $_SERVER['HTTP_HOST'];
    $dev_server_url = 'auretto.works';

    if( strpos($base_url, $dev_server_url) !== false ){
        $dev_server = true;
    }

    return $dev_server;
}