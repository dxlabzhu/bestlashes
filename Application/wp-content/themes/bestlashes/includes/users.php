<?php

// Create VIP roles
add_action( 'init', 'bl_create_vip_roles' );
function bl_create_vip_roles(){
	// VIP 1
	add_role( 
		'vip_1', 
		__('VIP 1', 'bl'), 
		array(
		    'read' => true,
		    'edit_posts' => false,
		    'delete_posts' => false,
		    'vip' => true
		)
	);

	// VIP 2
	add_role( 
		'vip_2', 
		__('VIP 2', 'bl'), 
		array(
		    'read' => true,
		    'edit_posts' => false,
		    'delete_posts' => false,
		    'vip' => true
		)
	);
}


function is_vip_user(){
	if( is_user_logged_in() ){
		if( current_user_can('vip') ){
			return true;
		}
	}

	return false;
}

function bl_get_vip_roles(){
	$vip_roles = array(
		'vip_1' => get_role( 'vip_1' ),
		'vip_2' => get_role( 'vip_2' )
	);

	return $vip_roles;
}

function bl_get_user_role_slug(){
	if( is_user_logged_in() ){
		$user = wp_get_current_user();
		$roles = ( array ) $user->roles;
		$role_slug = array_shift( $roles );

		return $role_slug;
	}

	return false;
}

function bl_get_user_vip_role( $user_id = null ){
	$vip_slug = '';

	if( is_user_logged_in() ){
		if( empty( $user_id ) ){
			$user_id = get_current_user_id();
		}

		if( !empty( $user_id ) ){
			$user_data = get_userdata( $user_id );

			// Get all the user roles as an array.
			$user_roles = $user_data->roles;
			$vip_roles = bl_get_vip_roles();
			
			if( !empty( $vip_roles ) ){
				foreach ( $vip_roles as $role_key => $role ) {
					if( in_array( $role_key, $user_data->roles ) ){
						$vip_slug = $role_key;
					}
				}
			}
		}
	}

	return $vip_slug;
}


// Redirect user from login and registration page if user is logged in
// Redirect user from WC login to BL login
add_action('template_redirect', 'bl_login_registration_redirect');
function bl_login_registration_redirect(){
	if( is_page( BL_PAGE_LOGIN ) || is_page( BL_PAGE_REGISTRATION ) || is_page( BL_PAGE_FORGOTTEN_PASSWORD_EMAIL_VERIFICATION ) || is_page( BL_PAGE_FORGOTTEN_PASSWORD_CHANGE_PASSWORD ) ){
		if( is_user_logged_in() ){
			wp_redirect( get_permalink( wc_get_page_id( 'myaccount' ) ) );
			die();
		}
	}

	if( is_page( wc_get_page_id( 'myaccount' ) ) && !is_user_logged_in() ){
		wp_redirect( get_permalink( BL_PAGE_LOGIN ) );
		die();
	}
}


	/***************************/
	/*	   		Login		   */
	/***************************/

add_action( 'wp_ajax_bl_login', 'bl_login' );
add_action( 'wp_ajax_nopriv_bl_login', 'bl_login' );
function bl_login() {
	if ( isset( $_POST['form'] ) ) {
		$postData = array();
		parse_str( $_POST['form'], $postData );
		$email = $postData['login-email'];
		$pass = $postData['login-password'];
		$errors = array();

		$user = get_user_by('email', $email);

		if ( !empty( $user->user_login ) ) {

			$creds = array();
			$creds['user_login'] = $user->user_login;
			$creds['user_password'] = $pass;
			$creds['remember'] = true;
			
			// Rossz jelszó
			if (! wp_check_password( $pass, $user->data->user_pass, $user->ID ) ) {
				$errors = array('success' => false, 'error' => 'wrong-password');
				
			} else {
				$user_activated = get_user_meta( $user->ID, 'activated_account', true );

				if ($user_activated == '1') { // Felhasználói fiók aktiválva lett-e már
					// Jelszóemlékeztető linkre csak addig tudjon rákattintani, amíg be nem jelentkezik
					update_user_meta( $user->ID, 'password-changed', '0' );

					$login_user = wp_signon( $creds, false );
					// Minden ok, beengedjük.
					$errors = array('success' => true);
				} else {
					// Még nem aktiválták a felhasználói fiókot
					$errors = array('success' => false, 'error' => 'user-not-activated');
				}
			}
		} else {
			// Nem létezik felhasználó ilyen email címmel
			$errors = array('success' => false, 'error' => 'no-user-found');
		}

		wp_send_json( $errors );
	}
}


	/***************************/
	/*	   	Registration	   */
	/***************************/

add_action( 'wp_ajax_bl_registration', 'bl_registration' );
add_action( 'wp_ajax_nopriv_bl_registration', 'bl_registration' );
function bl_registration() {
	$errors = array();

	if ( isset( $_POST['form'] ) ) {
		$postData = array();
		parse_str( $_POST['form'], $postData );

		if( email_exists( $postData['reg-email'] ) == false ){
			$ip = get_current_ip_address();
			$current_timestamp = current_time( 'timestamp' );

			// Create user
			$new_member = wp_create_user( $postData['reg-email'], $postData['reg-password'], $postData['reg-email'] );

			// Update user data
			wp_update_user( array(
					'ID' => $new_member,
					'first_name' => $postData['reg-shipping-first-name'],
					'last_name' => $postData['reg-shipping-last-name'],
				)
			);

			$rand_pass_for_hash = wp_generate_password();
			$hash = md5( $postData['reg-email'] . $rand_pass_for_hash );
			$link = home_url() . '?verify-registration=1&uid=' . $new_member . '&hash=' . $hash;
			$clickable_link = '<a href="'. $link .'">' . $link . '</a>';

			// Billing
			update_user_meta( $new_member, 'billing_first_name', esc_attr( $postData['reg-billing-first-name'] ) );
			update_user_meta( $new_member, 'billing_last_name', esc_attr( $postData['reg-billing-last-name'] ) );
			update_user_meta( $new_member, 'billing_company', esc_attr( $postData['reg-billing-company'] ) );
			update_user_meta( $new_member, 'billing_country', esc_attr( $postData['reg-billing-country'] ) );
			update_user_meta( $new_member, 'billing_address_1', esc_attr( $postData['reg-billing-address'] ) );
			update_user_meta( $new_member, 'billing_city', esc_attr( $postData['reg-billing-city'] ) );
			update_user_meta( $new_member, 'billing_postcode', esc_attr( $postData['reg-billing-zip'] ) );
				
			// Shipping
			update_user_meta( $new_member, 'shipping_first_name', esc_attr( $postData['reg-shipping-first-name'] ) );
			update_user_meta( $new_member, 'shipping_last_name', esc_attr( $postData['reg-shipping-last-name'] ) );
			update_user_meta( $new_member, 'shipping_company', '' );
			update_user_meta( $new_member, 'shipping_country', esc_attr( $postData['reg-shipping-country'] ) );
			update_user_meta( $new_member, 'shipping_address_1', esc_attr( $postData['reg-shipping-address'] ) );
			update_user_meta( $new_member, 'shipping_city', esc_attr( $postData['reg-shipping-city'] ) );
			update_user_meta( $new_member, 'shipping_postcode', esc_attr( $postData['reg-shipping-zip'] ) );
			
			// Other
			update_user_meta( $new_member, 'billing_phone', esc_attr( $postData['reg-phone'] ) );
			update_user_meta( $new_member, 'billing_email', esc_attr( $postData['reg-email'] ) );
			update_user_meta( $new_member, 'tax-number', esc_attr( $postData['reg-billing-tax-number'] ) );
			update_user_meta( $new_member, 'activated_account', '0' );
			update_user_meta( $new_member, 'registration_timestamp', $current_timestamp );
			update_user_meta( $new_member, 'registration_hash', $hash );

			// Birthday
			if( isset( $postData['reg-birthday'] ) ){
				if( !empty( $postData['reg-birthday'] ) ){
					$birthday_raw = $postData['reg-birthday'];
					$birthday_datetime = DateTime::createFromFormat('Y.m.d', $birthday_raw);
					$birthday = $birthday_datetime->format('Y-m-d');
					$birthday_month_day = $birthday_datetime->format('m-d');

					if( !empty( $birthday ) && !empty( $birthday_month_day ) ){
						update_user_meta( $new_member, 'birthday', $birthday );
						update_user_meta( $new_member, 'birthday_month_day', $birthday_month_day );
					}
				}
			}

			// Newsletter
			if( isset( $postData['newsletter'] ) ){
				$unsubscribe_hash = md5( $postData['reg-email'] . $name . BL_NEWSLETTER_SUBSCRIPTION_FIX_UNSUBSCRIBE_HASH );

				$subsription_row = array(
					'email' => $postData['reg-email'],
					'name' => $postData['reg-shipping-first-name'] . ' ' . $postData['reg-shipping-last-name'],
					'hash' => $hash,
					'subscribe_time' => $current_timestamp,
					'subscribe_ip' => $ip,
					'unsubscribe_hash' => $unsubscribe_hash
			 	);

			 	$insert_answer = insert_newsletter_subscription( $subsription_row );

			 	if( $insert_answer['error'] == false ){
				 	update_user_meta( $new_member, 'newsletter_subscription_id', $insert_answer['id'] );
				}
			}

			// Document
			/*if( !empty( $_FILES["document"] ) ){
				$file_raw = $_FILES["document"];

                if( $file_raw['name'] != '' ){

                	$filename = sanitize_file_name(rand() .'-'. $file_raw['name']);
                    $tmp_name = $file_raw['tmp_name'];
                    $filesize = $file_raw['size'];
                        
                    if ( !empty( $filename ) ) {
                        $wp_filetype = wp_check_filetype( basename( $filename ), null );
                        $wp_upload_dir = wp_upload_dir();

                        // Move the uploaded file into the WordPress uploads directory
                        move_uploaded_file( $tmp_name, $wp_upload_dir['path']  . '/' . $filename );

                        $attachment = array(
                            'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
                            'post_mime_type' => $wp_filetype['type'],
                            'post_title' => $filename,
                            'post_content' => '',
                            'post_status' => 'inherit'
                        );

                        $filename = $wp_upload_dir['path']  . '/' . $filename;
                            
                        $attach_id = wp_insert_attachment( $attachment, $filename );
                        require_once( ABSPATH . 'wp-admin/includes/image.php' );
                        $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );

                        wp_update_attachment_metadata( $attach_id, array_merge( $attach_data, $attachment ) );

                        add_user_meta( $new_member, 'jogosultsag_igazolas', $attach_id, true );
                    }
                }
            }*/

			// Email to user
			if( class_exists( 'Auretto_Email_Editor' ) ){
				$AWEmail = new Auretto_Email_Editor();
				$extra_datas = array(
					'receiver_email' => $postData['reg-email'],
					'replaceable_content_words' => array(
						'%name%' => $postData['reg-shipping-first-name'] . ' ' . $postData['reg-shipping-last-name'],
						'%link%' => $clickable_link
					)
				);
				$AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_REGISTRATION_VERIFICATION, $extra_datas );
			}
			$errors = array( 'success' => true, 'redirect' => get_permalink( BL_PAGE_REGISTRATION_SUCCESSFULL ) );
		} else {
			// Már létezik felhasználó ilyen email címmel
			$errors = array( 'success' => false, 'error' => 'user-already-registred' );
		}
	} 

	wp_send_json( $errors );
}


	/****************************/
	/*	 Verify Registration	*/
	/****************************/

add_action('init', 'bl_verify_registration');
function bl_verify_registration(){
	if( isset( $_GET['verify-registration'] ) && $_GET['verify-registration'] == '1' ){
		$user_id = $_GET['uid'];
		$hash = $_GET['hash'];

        $user_hash = get_the_author_meta( 'registration_hash', $user_id );
        $activated_account = get_the_author_meta( 'activated_account', $user_id );

        if( $activated_account != '1' ){
	        if( $hash != $user_hash ){
	           add_action( 'wp_footer', 'bl_open_user_popup_failed_verify_hash', 1000 );
	        } else {
	        	update_user_meta( $user_id, 'activated_account', '1' );
	            add_action( 'wp_footer', 'bl_open_user_popup_success_verify_hash', 1000 );
	        }
		}

		// Newsletter subscription check
		$newsletter_subscription_id = get_the_author_meta( 'newsletter_subscription_id', $user_id );
		
		if( !empty( $newsletter_subscription_id ) ){
			global $wpdb;
			
			$table = $wpdb->prefix . BL_NEWSLETTER_SUBSCRIPTION_TABLE;

	        $row = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table WHERE id = %d", array( $newsletter_subscription_id ) ) );
	        
	        if( !empty( $row ) ){
	        	if( $row->hash == $hash ){
					if( $row->accept_success != '1' ){
						$where = array( 'id' => (int)$row->id );
						$current_timestamp = current_time( 'timestamp' );
						$ip = get_current_ip_address();

						$update_query = array(
							'accept_time' => $current_timestamp,
							'accept_ip' => $ip,
							'accept_success' => 1,
						);

						// Update row
						$wpdb->update(
							$table,
							$update_query,
							$where
						);
					}
				}
			}
		}
	}
}



	/***************************/
	/*	  Forgotten password   */
	/***************************/

add_action( 'wp_ajax_bl_forgotten_password_email_verification', 'bl_forgotten_password_email_verification' );
add_action( 'wp_ajax_nopriv_bl_forgotten_password_email_verification', 'bl_forgotten_password_email_verification' );
function bl_forgotten_password_email_verification() {
	if (isset($_POST['form'])) {
		$postData = array();
		parse_str( $_POST['form'], $postData );
		$email = $postData['forgotten-password-email'];
		$errors = array();

		$user = get_user_by( 'email', $email );

		if ( !empty( $user->user_login ) ) {
			$userMeta = get_user_meta( $user->ID );
			$rand_pass_for_hash = wp_generate_password();
	        $hash = md5( $email . $rand_pass_for_hash);
	        $link = home_url() . '?forgotten-password-check=1&uid=' . $user->ID . '&hash=' . $hash;

	        // Save hash
	        update_user_meta( $user->ID, 'forgotten-password-hash', $hash );
	        update_user_meta( $user->ID, 'password-change-in-progress', '1' );

	        // Email to user
			if( class_exists( 'Auretto_Email_Editor' ) ){
				$AWEmail = new Auretto_Email_Editor();
				$extra_datas = array(
					'receiver_email' => $email,
					'replaceable_content_words' => array(
						'%name%' => $userMeta['first_name'][0] . ' ' . $userMeta['last_name'][0],
						'%link%' => $link
					)
				);
				$AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_FORGOTTEN_PASSWORD_EMAIL_VERIFICATION, $extra_datas );
			}
	        

			$errors = array( 'success' => true , 'redirect' => get_permalink( BL_PAGE_FORGOTTEN_PASSWORD_EMAIL_VERIFICATION_SUCCESS ) );
		} else {
			// Nem létezik felhasználó ilyen email címmel
			$errors = array('success' => false, 'error' => 'no-user-found');
		}

		wp_send_json( $errors );
	}
}

	/****************************/
	/* Forgotten password check */
	/****************************/

add_action('template_redirect', 'bl_forgotten_password_link_validation');
function bl_forgotten_password_link_validation() {
	// First verification
	if( isset( $_GET['forgotten-password-check'] ) && $_GET['forgotten-password-check'] == '1' ){
		$user_id = $_GET['uid'];
		$hash = $_GET['hash'];

        $user_hash = get_the_author_meta( 'forgotten-password-hash', $user_id );
        $password_change_in_progress = get_the_author_meta( 'password-change-in-progress', $user_id );

        if( $password_change_in_progress == '1' ){
        	if( $hash == $user_hash ){
        		$link = get_permalink( BL_PAGE_FORGOTTEN_PASSWORD_CHANGE_PASSWORD ) . '?uid=' . $user_id . '&hash=' . $hash;
        		wp_redirect( $link );
        		die();
        	} else {
        		add_action( 'wp_footer', 'bl_open_user_popup_failed_verify_password_change_hash', 1000 );
        	}
		}
	}
}


	/********************************/
	/*		Change password 		*/
	/********************************/

add_action( 'wp_ajax_bl_change_password', 'bl_change_password' );
add_action( 'wp_ajax_nopriv_bl_change_password', 'bl_change_password' );
function bl_change_password() {
	if (isset($_POST['form'])) {
		$postData = array();
		parse_str( $_POST['form'], $postData );
		$pass = $postData['change-password'];
		$user_id = $postData['uid'];
		$hash = $postData['hash'];
		$errors = array();

		$user_hash = get_the_author_meta( 'forgotten-password-hash', $user_id );
        $password_change_in_progress = get_the_author_meta( 'password-change-in-progress', $user_id );

        if( $password_change_in_progress == '1' ){
        	if( $hash == $user_hash ){
        		wp_set_password( $pass, $user_id );

        		$errors = array('success' => true, 'redirect' => get_permalink( BL_PAGE_FORGOTTEN_PASSWORD_CHANGE_PASSWORD_SUCCESS ) );
        	} else {
        		$errors = array('success' => false, 'error' => 'hash-not-match');
        	}
        } else {
        	$errors = array('success' => false, 'error' => 'wrong-link');
        }

		wp_send_json( $errors );
	}
}



add_action('init', 'bl_delete_account');
function bl_delete_account(){
	if( isset( $_POST['bl_delete_account'] ) && $_POST['bl_delete_account'] ){
		$user_id = get_current_user_id();

		$retrieved_nonce = $_REQUEST['bl_delete_account_nonce'];
		if (!wp_verify_nonce( $_REQUEST['user_delete_account_from_frontend'], 'bl_delete_account_nonce' ) ) die( 'Failed security check' );

		if( !empty( $user_id ) ){
			if( bl_delete_user_by_id( $user_id ) ){
				wp_logout();
				wp_redirect( add_query_arg('user_deleted', 'true', home_url() ) );
				exit;
			}
		}
	}
}


add_action('init', 'bl_user_deleted_show_popup');
function bl_user_deleted_show_popup(){
	if( isset( $_GET['user_deleted'] ) ){
		add_action( 'wp_footer', 'bl_open_user_popup_user_deleted', 1000 );
	}
}

function bl_delete_user_by_id( $user_id = null ){
	if( !empty( $user_id ) ){
		global $wpdb;
		$hash = wp_generate_password( 8, false, false );
		$user = get_user_by( 'id', $user_id );
		$user_email = $user->user_email;

		// Order data rewrite
		$args['posts_per_page'] = -1;
		$args['post_type'] = wc_get_order_types();
		$args['post_status'] = array_keys( wc_get_order_statuses() );
		$args['meta_query'] = array();
		$args['meta_query'][] = array(
			'key' => '_customer_user',
			'value' => $user_id
		);
		$args['fields'] = 'ids';
		
		$user_orders_loop = new WP_Query( $args );

		if( $user_orders_loop->have_posts() ){
			while ( $user_orders_loop->have_posts() ) {
				$order_id = $user_orders_loop->next_post();

				// Update meta fields
				update_post_meta( $order_id, '_billing_first_name', $hash );
				update_post_meta( $order_id, '_billing_last_name', $hash );
				update_post_meta( $order_id, '_billing_company', $hash );
				update_post_meta( $order_id, '_billing_email', $hash );
				update_post_meta( $order_id, '_billing_phone', $hash );
				update_post_meta( $order_id, '_billing_country', $hash );
				update_post_meta( $order_id, '_billing_address_1', $hash );
				update_post_meta( $order_id, '_billing_address_2', $hash );
				update_post_meta( $order_id, '_billing_city', $hash );
				update_post_meta( $order_id, '_billing_state', $hash );
				update_post_meta( $order_id, '_billing_postcode', $hash );
				update_post_meta( $order_id, '_billing_address_index', $hash );


				update_post_meta( $order_id, '_shipping_first_name', $hash );
				update_post_meta( $order_id, '_shipping_last_name', $hash );
				update_post_meta( $order_id, '_shipping_company', $hash );
				update_post_meta( $order_id, '_shipping_country', $hash );
				update_post_meta( $order_id, '_shipping_address_1', $hash );
				update_post_meta( $order_id, '_shipping_address_2', $hash );
				update_post_meta( $order_id, '_shipping_city', $hash );
				update_post_meta( $order_id, '_shipping_state', $hash );
				update_post_meta( $order_id, '_shipping_postcode', $hash );
				update_post_meta( $order_id, '_shipping_address_index', $hash );

				update_post_meta( $order_id, '_customer_ip_address', $hash );
				update_post_meta( $order_id, 'tax-number', $hash );
			}
		}

		if ( wp_delete_user( $user_id ) ) {
			return true;
		}

	}

	return false;
}


// Add capabilities to 'Editor' role for woocommerce
add_action( 'init', 'bl_edit_editor_role_capabilities' );
function bl_edit_editor_role_capabilities(){
	$role = get_role( 'editor' );

	if( !empty( $role ) ){
		$role->add_cap("manage_woocommerce");
		$role->add_cap("view_woocommerce_reports");
		$role->add_cap("edit_product");
		$role->add_cap("read_product");
		$role->add_cap("delete_product");
		$role->add_cap("edit_products");
		$role->add_cap("edit_others_products");
		$role->add_cap("publish_products");
		$role->add_cap("read_private_products");
		$role->add_cap("delete_products");
		$role->add_cap("delete_private_products");
		$role->add_cap("delete_published_products");
		$role->add_cap("delete_others_products");
		$role->add_cap("edit_private_products");
		$role->add_cap("edit_published_products");
		$role->add_cap("manage_product_terms");
		$role->add_cap("edit_product_terms");
		$role->add_cap("delete_product_terms");
		$role->add_cap("assign_product_terms");
		$role->add_cap("edit_shop_order");
		$role->add_cap("read_shop_order");
		$role->add_cap("delete_shop_order");
		$role->add_cap("edit_shop_orders");
		$role->add_cap("edit_others_shop_orders");
		$role->add_cap("publish_shop_orders");
		$role->add_cap("read_private_shop_orders");
		$role->add_cap("delete_shop_orders");
		$role->add_cap("delete_private_shop_orders");
		$role->add_cap("delete_published_shop_orders");
		$role->add_cap("delete_others_shop_orders");
		$role->add_cap("edit_private_shop_orders");
		$role->add_cap("edit_published_shop_orders");
		$role->add_cap("manage_shop_order_terms");
		$role->add_cap("edit_shop_order_terms");
		$role->add_cap("delete_shop_order_terms");
		$role->add_cap("assign_shop_order_terms");
		$role->add_cap("edit_shop_coupon");
		$role->add_cap("read_shop_coupon");
		$role->add_cap("delete_shop_coupon");
		$role->add_cap("edit_shop_coupons");
		$role->add_cap("edit_others_shop_coupons");
		$role->add_cap("publish_shop_coupons");
		$role->add_cap("read_private_shop_coupons");
		$role->add_cap("delete_shop_coupons");
		$role->add_cap("delete_private_shop_coupons");
		$role->add_cap("delete_published_shop_coupons");
		$role->add_cap("delete_others_shop_coupons");
		$role->add_cap("edit_private_shop_coupons");
		$role->add_cap("edit_published_shop_coupons");
		$role->add_cap("manage_shop_coupon_terms");
		$role->add_cap("edit_shop_coupon_terms");
		$role->add_cap("delete_shop_coupon_terms");
		$role->add_cap("assign_shop_coupon_terms");
		$role->add_cap("edit_shop_webhook");
		$role->add_cap("read_shop_webhook");
		$role->add_cap("delete_shop_webhook");
		$role->add_cap("edit_shop_webhooks");
		$role->add_cap("edit_others_shop_webhooks");
		$role->add_cap("publish_shop_webhooks");
		$role->add_cap("read_private_shop_webhooks");
		$role->add_cap("delete_shop_webhooks");
		$role->add_cap("delete_private_shop_webhooks");
		$role->add_cap("delete_published_shop_webhooks");
		$role->add_cap("delete_others_shop_webhooks");
		$role->add_cap("edit_private_shop_webhooks");
		$role->add_cap("edit_published_shop_webhooks");
		$role->add_cap("manage_shop_webhook_terms");
		$role->add_cap("edit_shop_webhook_terms");
		$role->add_cap("delete_shop_webhook_terms");
		$role->add_cap("assign_shop_webhook_terms");
	}
}