<?php

class Bestlashes_Main_Menu_Walker extends Walker_Nav_Menu {
	static $open_elems = 0;
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "\n";
		if ($depth == 0) {
			$output .= '<div class="nav-dropdown-toggle">';
		}
	}
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		if ($depth == 0) {
			$output .= "\n";
			$output .= '</div>';
		}
	}
	function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {
		$output .= "\n";
		$classes = empty( $object->classes ) ? array() : (array) $object->classes;
		
		if ($args->walker->has_children) {
			self::$open_elems ++;
			$current = in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes) || in_array('current-menu-ancestor', $classes) ? 'current' : '';
			
			$output .= '<div class="nav-dropdown">';
			$output .= '<div class="nav-text '. $current .'">' . $object->title . '</div>';
			$output .= '<div class="link-underline"></div>';
			
		} else {
			$current = in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes) || in_array('current-menu-ancestor', $classes) ? 'current' : '';
			if ($depth > 0) {
				$output .= '<a href="'. $object->url .'" class="nav-dropdown-item '. $current .' '. implode( ' ', $classes ) .'"><div class="nav-text">'. $object->title .'</div><div class="link-underline"></div></a>';
			} else {
				$output .= '<a href="'. $object->url .'" class="nav-item w-inline-block '. $current .' '. implode( ' ', $classes ) .'"><div class="nav-text">'. $object->title .' '. ( $object->object_id == BL_PAGE_BESTBROWS ? '<span class="bestbrows-menuitem-new-span '. ICL_LANGUAGE_CODE .'">'. __( 'new', 'bl' ) .'</span>' : '' ) .'</div><div class="link-underline"></div></a>';
			}
		}
	}
	function end_el( &$output, $object, $depth = 0, $args = array() ) {
		if ($depth == 0 && self::$open_elems > 0) {
			$output .= '</div>' . "\n";
			self::$open_elems --;
		}
	}
}