<?php

add_filter( 'woocommerce_email_styles', 'bl_change_woocommerce_email_header_font_color' );
function bl_change_woocommerce_email_header_font_color( $css ) {
	$css .= "#template_header { background-color: #f7f1e8; }";
	$css .= "#template_header h1 { text-shadow: none; }";
	$css .= '#template_header_image img { max-width: 400px; }';
	
	return $css;
}


add_action( 'bl_email_content', 'bl_woocommerce_email_content', 10, 5 );
function bl_woocommerce_email_content( $order = array(), $sent_to_admin = false, $plain_text = false, $email = array(), $type = '' ){
	$text = '';

	if( !empty( $order ) && !$sent_to_admin ){
		$count_products = 0;
		$count_training_courses = 0;
		$order_data = $order->get_data();
		$user_name = $order_data['billing']['first_name'] . ' '. $order_data['billing']['last_name'];

		foreach ($order->get_items() as $item_id => $item_product) {
			$training_course = get_post_meta( $item_product->get_product_id(), 'training_course', true );

			if( $training_course == '1' ){
				$count_training_courses++;
			} else {
				$count_products++;
			}
		}

		
		if( $count_products == 0 ){
			// Only training courses
			$text = bl_get_option_lang( 'bl-woocommerce-email-text-only-training-courses' );

			$text = str_replace( '%name%', $user_name, $text );

		} else if( $count_training_courses == 0 ) {
			// Only products
			$text = bl_get_option_lang( 'bl-woocommerce-email-text-only-product' );

			$text = str_replace( '%name%', $user_name, $text );
		} else {
			// Products and training courses
			$text = bl_get_option_lang( 'bl-woocommerce-email-text-product-and-training-courses' );

			$text = str_replace( '%name%', $user_name, $text );
		}
	}

	if( empty( $text ) ){
		if( $type == 'processing' ) {
			$text = __( "Your order has been received and is now being processed. Your order details are shown below for your reference:", 'woocommerce' );
		} else if( $type == 'on-hold' ){
			$text = __( "Your order is on-hold until we confirm payment has been received. Your order details are shown below for your reference:", 'woocommerce' );
		}
	}

	if( $plain_text ){
		echo $text;
	} else {
		echo wpautop( $text );
	}
}