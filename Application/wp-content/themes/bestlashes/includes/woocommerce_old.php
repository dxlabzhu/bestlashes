<?php
function sv_wc_cod_order_status( $status, $order ) {
	if ( bl_has_online_course_in_order($order) ) {		
		return 'on-hold';
	}
	
	return $status;
}
add_filter( 'woocommerce_cod_process_payment_order_status', 'sv_wc_cod_order_status', 15, 2 );

function bl_has_online_course_in_order( $order ) {
    $has_online_courses = false;
    $category_checks = array();

	foreach ( $order->get_items() as $item_id => $line_item ) {
		$product = $line_item->get_product();
        $product_in_cat = has_term( 'online-kepzes', 'product_cat', $product->id );
        array_push( $category_checks, $product_in_cat ); 
    }

    if ( in_array( true, $category_checks, true ) ) {
        $has_online_courses = true;
    }

    return $has_online_courses;
}

function bl_has_training_course_in_order(){
	$has_training_courses = false;

	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
    	$training_course = get_post_meta( $cart_item['product_id'], 'training_course', true );

    	if( $training_course == '1' ){
    		$has_training_courses = true;
    	}
    }

    return $has_training_courses;
}

function bl_all_product_is_training_course_in_cart(){
	$all_prodcut_is_training_cours = true;
		
	if( !WC()->cart ){
		return false;
	}
	
	if( !is_admin() ){
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
	    	$training_course = get_post_meta( $cart_item['product_id'], 'training_course', true );

	    	if( $training_course != '1' ){
	    		$all_prodcut_is_training_cours = false;
	    	}
	    }
	}

    return $all_prodcut_is_training_cours;
}

function bl_all_product_is_online_course_in_cart(){
    $all_product_is_online_course = true;

    if( !WC()->cart ){
        return false;
    }

    if( !is_admin() ){
        $category_checks= array();

        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $product = $cart_item['data'];
            $product_in_cat = has_term( 'online-kepzes', 'product_cat', $product->id );
            array_push( $category_checks, $product_in_cat );

            if ( ! in_array( false, $category_checks, true ) ) {
                $all_product_is_online_course = true;
            } else {
                $all_product_is_online_course = false;
            }
        }
    }

    return $all_product_is_online_course;
}

function bl_need_to_upload_certificate(){
	$need_to_upload_certificate = false;

	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
    	$training_course = get_post_meta( $cart_item['product_id'], 'training_course', true );

    	if( $training_course == '1' ){
    		$certificate_must_upload = get_post_meta( $cart_item['product_id'], 'training_course_certificate_must_upload', true );

    		if( $certificate_must_upload == '1' ){
	    		$need_to_upload_certificate = true;
	    	}
    	}
    }

    return $need_to_upload_certificate;
}


add_action( 'wp_ajax_bl_get_minicart', 'bl_get_minicart' );
add_action( 'wp_ajax_nopriv_bl_get_minicart', 'bl_get_minicart' );
function bl_get_minicart() {
	ob_start();
	wc_get_template( 'minicart.php' );

	wp_send_json_success( ob_get_clean() );
}

add_action( 'template_redirect', 'bl_woocommerce_shop_to_bl_shop_redirect' );
function bl_woocommerce_shop_to_bl_shop_redirect(){
	if( is_shop() ){
		wp_redirect( get_permalink( BL_PAGE_WEBSHOP ) );
		die();
	}
}


add_filter( 'woocommerce_cart_totals_order_total_html', 'bl_edit_cart_totals_order_html' );
function bl_edit_cart_totals_order_html( $value ){
	$value = str_replace( '<small class="includes_tax">', '<br /><span class="net-price">', $value );
	$value = str_replace( '</small>', '</span>', $value );

	return $value;
}


/* Remove cross sells from cart */
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');	

/* Replace payment on checkout */
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
add_action( 'woocommerce_checkout_after_customer_details', 'woocommerce_checkout_payment', 20 );

/* Remove cart notices from the begining of the page */
add_action( 'init', 'bl_remove_notices_from_the_begining_of_the_page' );
function bl_remove_notices_from_the_begining_of_the_page(){
	remove_action( 'woocommerce_before_cart', 'woocommerce_output_all_notices' );
}

/* Woocommerce form field remake */
function woocommerce_form_field( $key, $args, $value = null ) {
	$defaults = array(
		'type'              => 'text',
		'label'             => '',
		'description'       => '',
		'placeholder'       => '',
		'maxlength'         => false,
		'required'          => false,
		'autocomplete'      => false,
		'id'                => $key,
		'class'             => array(),
		'label_class'       => array(),
		'input_class'       => array(),
		'return'            => false,
		'options'           => array(),
		'custom_attributes' => array(),
		'validate'          => array(),
		'default'           => '',
		'autofocus'         => '',
		'priority'          => '',
	);
	$args = wp_parse_args( $args, $defaults );
	$args = apply_filters( 'woocommerce_form_field_args', $args, $key, $value );
	if ( $args['required'] ) {
		$args['class'][] = 'validate-required';
		$required        = '&nbsp;<abbr class="required" title="' . esc_attr__( 'required', 'woocommerce' ) . '">*</abbr>';
	} else {
		$required = '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>';
	}
	if ( is_string( $args['label_class'] ) ) {
		$args['label_class'] = array( $args['label_class'] );
	}
	if ( is_null( $value ) ) {
		$value = $args['default'];
	}
	// Custom attribute handling.
	$custom_attributes         = array();
	$args['custom_attributes'] = array_filter( (array) $args['custom_attributes'], 'strlen' );
	if ( $args['maxlength'] ) {
		$args['custom_attributes']['maxlength'] = absint( $args['maxlength'] );
	}
	if ( ! empty( $args['autocomplete'] ) ) {
		$args['custom_attributes']['autocomplete'] = $args['autocomplete'];
	}
	if ( true === $args['autofocus'] ) {
		$args['custom_attributes']['autofocus'] = 'autofocus';
	}
	if ( $args['description'] ) {
		$args['custom_attributes']['aria-describedby'] = $args['id'] . '-description';
	}
	if ( ! empty( $args['custom_attributes'] ) && is_array( $args['custom_attributes'] ) ) {
		foreach ( $args['custom_attributes'] as $attribute => $attribute_value ) {
			$custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
		}
	}
	if ( ! empty( $args['validate'] ) ) {
		foreach ( $args['validate'] as $validate ) {
			$args['class'][] = 'validate-' . $validate;
		}
	}
	$field           = '';
	$label_id        = $args['id'];
	$sort            = $args['priority'] ? $args['priority'] : '';
	$field_container = '<div class="table-row _2col form-row %1$s" id="%2$s" data-priority="' . esc_attr( $sort ) . '">%3$s</div>';
	switch ( $args['type'] ) {
		case 'country':
			$countries = 'shipping_country' === $key ? WC()->countries->get_shipping_countries() : WC()->countries->get_allowed_countries();
			if ( 1 === count( $countries ) ) {
				$field .= '<strong>' . current( array_values( $countries ) ) . '</strong>';
				$field .= '<input type="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="' . current( array_keys( $countries ) ) . '" ' . implode( ' ', $custom_attributes ) . ' class="country_to_state" readonly="readonly" />';
			} else {
				$field = '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="country_to_state country_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . '><option value="">' . esc_html__( 'Select a country&hellip;', 'woocommerce' ) . '</option>';
				foreach ( $countries as $ckey => $cvalue ) {
					$field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';
				}
				$field .= '</select>';
				$field .= '<noscript><button type="submit" name="woocommerce_checkout_update_totals" value="' . esc_attr__( 'Update country', 'woocommerce' ) . '">' . esc_html__( 'Update country', 'woocommerce' ) . '</button></noscript>';
			}
			break;
		case 'state':
			/* Get country this state field is representing */
			$for_country = isset( $args['country'] ) ? $args['country'] : WC()->checkout->get_value( 'billing_state' === $key ? 'billing_country' : 'shipping_country' );
			$states      = WC()->countries->get_states( $for_country );
			if ( is_array( $states ) && empty( $states ) ) {
				$field_container = '<div class="table-row _2col"><p class="form-row %1$s" id="%2$s" style="display: none">%3$s</p></div>';
				$field .= '<input type="hidden" class="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="" ' . implode( ' ', $custom_attributes ) . ' placeholder="' . esc_attr( $args['placeholder'] ) . '" readonly="readonly" />';
			} elseif ( ! is_null( $for_country ) && is_array( $states ) ) {
				$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="state_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">
					<option value="">' . esc_html__( 'Select a state&hellip;', 'woocommerce' ) . '</option>';
				foreach ( $states as $ckey => $cvalue ) {
					$field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';
				}
				$field .= '</select>';
			} else {
				$field .= '<input type="text" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $value ) . '"  placeholder="' . esc_attr( $args['placeholder'] ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" ' . implode( ' ', $custom_attributes ) . ' />';
			}
			break;
		case 'textarea':
			$field .= '<textarea name="' . esc_attr( $key ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '" ' . ( empty( $args['custom_attributes']['rows'] ) ? ' rows="2"' : '' ) . ( empty( $args['custom_attributes']['cols'] ) ? ' cols="5"' : '' ) . implode( ' ', $custom_attributes ) . '>' . esc_textarea( $value ) . '</textarea>';
			break;
		case 'checkbox':
			$field = '<label class="checkbox ' . implode( ' ', $args['label_class'] ) . '" ' . implode( ' ', $custom_attributes ) . '>
					<input type="' . esc_attr( $args['type'] ) . '" class="input-checkbox ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="1" ' . checked( $value, 1, false ) . ' /> ' . $args['label'] . $required . '</label>';
			break;
		case 'text':
		case 'password':
		case 'datetime':
		case 'datetime-local':
		case 'date':
		case 'month':
		case 'time':
		case 'week':
		case 'number':
		case 'email':
		case 'url':
		case 'tel':
			$field .= '<input type="' . esc_attr( $args['type'] ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '"  value="' . esc_attr( $value ) . '" ' . implode( ' ', $custom_attributes ) . ' />';
			break;
		case 'select':
			$field   = '';
			$options = '';
			if ( ! empty( $args['options'] ) ) {
				foreach ( $args['options'] as $option_key => $option_text ) {
					if ( '' === $option_key ) {
						// If we have a blank option, select2 needs a placeholder.
						if ( empty( $args['placeholder'] ) ) {
							$args['placeholder'] = $option_text ? $option_text : __( 'Choose an option', 'woocommerce' );
						}
						$custom_attributes[] = 'data-allow_clear="true"';
					}
					$options .= '<option value="' . esc_attr( $option_key ) . '" ' . selected( $value, $option_key, false ) . '>' . esc_attr( $option_text ) . '</option>';
				}
				$field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">
						' . $options . '
					</select>';
			}
			break;
		case 'radio':
			$label_id = current( array_keys( $args['options'] ) );
			if ( ! empty( $args['options'] ) ) {
				foreach ( $args['options'] as $option_key => $option_text ) {
					$field .= '<input type="radio" class="input-radio ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $option_key ) . '" name="' . esc_attr( $key ) . '" ' . implode( ' ', $custom_attributes ) . ' id="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '"' . checked( $value, $option_key, false ) . ' />';
					$field .= '<label for="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '" class="radio ' . implode( ' ', $args['label_class'] ) . '">' . $option_text . '</label>';
				}
			}
			break;
		case 'bl-file':
			$field .= '<div class="file-attachment-container">
				<div class="document-name">
					<strong></strong>
				</div>
				<div class="input-container" data-old-document-id="">
					<input type="file" name="document" value="" id="document" accept="application/pdf, image/*" style="display: none;"/>
					<a href="#" class="rounded-button w-button file-attachment loading-animation-button">
						' . __('File', 'bl' ) .'<span class="chart-button-icon">↑</span>
						<span class="loading-button"></span>
					</a>
					<input type="hidden" name="document-id" value="">
				</div>
			</div>';
			$required = '&nbsp;<abbr class="required" title="' . esc_attr__( 'required', 'woocommerce' ) . '">*</abbr>';
			break;
	}
	if ( ! empty( $field ) ) {
		$field_html = '';
		if ( $args['label'] && 'checkbox' !== $args['type'] ) {
			$field_html .= '<div class="table-column _30percent"><div><label for="' . esc_attr( $label_id ) . '" class="' . esc_attr( implode( ' ', $args['label_class'] ) ) . '">' . $args['label'] . $required . '</label></div></div>';
		}
		$field_html .= '<div class="table-column _70percent"><div class="table-form _100percent w-form"><span class="woocommerce-input-wrapper">' . $field;
		if ( $args['description'] ) {
			$field_html .= '<span class="description" id="' . esc_attr( $args['id'] ) . '-description" aria-hidden="true">' . wp_kses_post( $args['description'] ) . '</span>';
		}
		$field_html .= '</span></div></div>';
		$container_class = esc_attr( implode( ' ', $args['class'] ) );
		$container_id    = esc_attr( $args['id'] ) . '_field';
		$field           = sprintf( $field_container, $container_class, $container_id, $field_html );
	}
	/**
	 * Filter by type.
	 */
	$field = apply_filters( 'woocommerce_form_field_' . $args['type'], $field, $key, $args, $value );
	/**
	 * General filter on form fields.
	 *
	 * @since 3.4.0
	 */
	$field = apply_filters( 'woocommerce_form_field', $field, $key, $args, $value );
	if ( $args['return'] ) {
		return $field;
	} else {
		echo $field; // WPCS: XSS ok.
	}
}



add_filter('wc_add_to_cart_message_html', 'bl_modify_add_to_cart_messeges_for_courses', 10, 2);
function bl_modify_add_to_cart_messeges_for_courses( $message, $products ) {
	
	if ( sizeof( $products ) == 1 ) {
		$product_id = 0;
		foreach ( $products as $prod_id => $qty) {
			$product_id = $prod_id;
			$training_course = get_post_meta( $prod_id, 'training_course', true );
			$product_video_id = get_post_meta( $prod_id, 'product-after-add-to-cart-notice-video-id', true );
		}

		if( $training_course == '1' ){
			$message = sprintf( __( '“%s“ training course application has been added to your cart.', 'bl' ), get_the_title( $product_id ) );
	   	}

	   	if( !empty( $product_video_id ) ){
			$message .= '<br>';
			$message .= sprintf( __('%1$sIMPORTANT: Please view %2$sthis video%3$s!%4$s', 'bl' ), '<span class="video_link">', '<a href="'. $product_video_id .'" class="cart-show-popup">', '</a>', '</span>' );

			$message .= sprintf( '<a href="%s" class="button wc-forward">%s</a>', esc_url( wc_get_page_permalink( 'cart' ) ), esc_html__( 'View cart', 'woocommerce' ) );

			// Add popup to footer
			add_action( 'wp_footer', 'bl_open_add_to_cart_video_popup', 1000 );
		}

   		return $message;
   	}

   	return $message;
}


// Set product list filter
add_action( 'init', 'bl_set_product_list_filter');
function bl_set_product_list_filter(){
	if( isset( $_POST['set-product-list-filter'] ) && $_POST['set-product-list-filter'] == '1' ){
		if( !empty( $_POST['view'] ) ){
			$_COOKIE['product-list']['view'] = $_POST['view'];
		}

		if( !empty( $_POST['order'] ) ){
			$_COOKIE['product-list']['order'] = $_POST['order'];
		}
	} else {
		if( empty( $_COOKIE['product-list']['order'] ) ){
			$_COOKIE['product-list']['order'] = BL_PRODUCT_LIST_DEFAULT_ORDER;
		}
	}
}

// Stock log
add_action( 'woocommerce_product_set_stock', 'bl_product_stock_change' );
add_action( 'woocommerce_variation_set_stock', 'bl_product_stock_change' );
function bl_product_stock_change( $product = null, $old_stock = null, $new_stock = null, $label = '' ){
	global $wpdb, $post;
	$table = $wpdb->prefix . BL_STOCK_LOG_TABLE;
	$reason_for_change = '';
	$current_timestamp = current_time( 'timestamp' );

	if( $product->get_type() == 'variation' ){
		$variation_id = $product->get_id();
    	$product_id = $product->get_parent_id();
	} else {
		$variation_id = '';
		$product_id = $product->get_id();
	}
	
	if( !empty( $label ) ){
		// Admin - order refunded/cancelled
		$reason_for_change = $label;

	} else {
		// Debug backtrace
		$backtrace = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS );

		if( !empty( $backtrace ) ){
			foreach ( $backtrace as $func ) {
				if( !empty( $func['class'] ) ){
					// Buying
					if( $func['class'] == 'WC_Checkout' ){
						$reason_for_change = 'buying';
					}

					// Admin
					if( $func['class'] == 'WC_Meta_Box_Product_Data' ){
						$reason_for_change = 'admin - ' . get_current_user_id();
					}
					if( $func['class'] == 'WC_Admin_Meta_Boxes' ){
						$reason_for_change = 'admin - ' . get_current_user_id();
					}
				}
			}
		}

		// Stock
		$old_stock = $product->get_data()['stock_quantity'];
		$new_stock = $product->get_stock_quantity();
	}

	if( $old_stock != $new_stock ){
		$stock_log_row = array(
			'product_id' => $product_id,
			'variation_id' => $variation_id,
			'timestamp' => $current_timestamp,
			'old_stock' => $old_stock,
			'new_stock' => $new_stock,
			'reason_for_change' => $reason_for_change,
	 	);

	 	$rows = $wpdb->insert(
			$table,
			$stock_log_row
		);
	}
}

/* Auto restore stock when orders are cancelled */
//add_action( 'woocommerce_order_status_refunded', 'bl_restore_order_stock', 10, 1 );
//add_action( 'woocommerce_order_status_cancelled', 'bl_restore_order_stock', 10, 1 );
function bl_restore_order_stock( $order_id ){
	$order = new WC_Order( $order_id );

	if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
		return;
	}

	foreach ( $order->get_items() as $item ) {

		if ( $item['product_id'] > 0 ) {
			$_product = $order->get_product_from_item( $item );

			if ( $_product && $_product->exists() && $_product->managing_stock() ) {

				$new_stock = $_product->get_stock_quantity();

				$qty = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $order, $item );

				$old_stock = $new_stock - $qty;

				//$new_quantity = $_product->increase_stock( $qty );

				//var_dump($old_stock, $new_quantity, $qty); die();

				//do_action( 'woocommerce_auto_stock_restored', $_product, $item );

				//$order->add_order_note( sprintf( __( 'Item #%s stock incremented from %s to %s.', 'bl' ), $item['product_id'], $old_stock, $new_quantity ) );

				//$order->send_stock_notifications( $_product, $new_quantity, $item['qty'] );

				// Log stock
				bl_product_stock_change( $_product, (int)$old_stock, (int)$new_stock, 'order-refunded-cancelled' );
			}
		}
	}
}

// New order - stock log
add_action('woocommerce_checkout_update_order_meta', 'bl_new_order_save_stock_log');
function bl_new_order_save_stock_log( $order_id = 0, $data = array() ){
	if( !empty( $order_id ) ){
		$order = wc_get_order( $order_id );
		
		if( !empty( $order ) ){
			foreach ( $order->get_items() as $items_key => $item_product ) { 
				$product = $item_product->get_product();
				$order_quantity = $item_product->get_quantity();
				$old_stock = $product->get_stock_quantity();
				$new_stock = $old_stock - $order_quantity;

				bl_product_stock_change( $product, (int)$old_stock, (int)$new_stock, 'user-new-order - ' . $order->get_id() );
			}
		}
	}
}


// User menu items - end-points
add_filter ( 'woocommerce_account_menu_items', 'bl_modify_my_account_menu' );
function bl_modify_my_account_menu( $menu_links ){
 	// Remove pages
 	unset( $menu_links['downloads'] );

 	// Add pages
 	$menu_links = array_merge( array_slice( $menu_links, 0, 2 ), array( 'my-training-courses' =>  __('My training courses', 'bl') ), array_slice( $menu_links, 2 ) );

 	if( BL_LANG == 'HU' ){
	 	//$menu_links = array_merge( array_slice( $menu_links, 0, 3 ), array( 'my-videos' =>  __('My videos', 'bl') ), array_slice( $menu_links, 3 ) );
	}
 	$menu_links = array_merge( array_slice( $menu_links, 0, 5 ), array( 'delete-account' =>  __('Delete account', 'bl') ), array_slice( $menu_links, 5 ) );


	return $menu_links;
}

add_action( 'init', 'bl_add_woocommerce_my_account_endpoint' );
function bl_add_woocommerce_my_account_endpoint() {
	// Delete account
	add_rewrite_endpoint( 'my-training-courses', EP_PAGES );
	//add_rewrite_endpoint( 'my-videos', EP_PAGES );
	add_rewrite_endpoint( 'delete-account', EP_PAGES );
}

add_action( 'woocommerce_account_my-training-courses_endpoint', 'bl_my_training_courses_endpoint_content' );
function bl_my_training_courses_endpoint_content() {
 	
	include( locate_template( 'template-parts/my-account-my-training-courses.php', false, false ) );
}

add_action( 'woocommerce_account_delete-account_endpoint', 'bl_delete_account_endpoint_content' );
function bl_delete_account_endpoint_content() {
 	
	include( locate_template( 'template-parts/my-account-delete-account.php', false, false ) );
	add_action( 'wp_footer', 'bl_open_user_popup_delete_confirmation', 1000 );
}

/* Edit payment methods */
add_filter('woocommerce_available_payment_gateways', 'bl_edit_payment_gateways', 10, 1);
function bl_edit_payment_gateways( $available_gateways = array() ) {
	// Only Hungarian site
	if( BL_LANG == 'HU' ){
		global $woocommerce;
		if ( !is_admin() && $woocommerce->customer ){
			$lang = ICL_LANGUAGE_CODE;
			
			// Disable Paypal in Hungary
			if( $lang == 'hu' ){
				unset( $available_gateways['paypal'] );
			}

			// Disable COD except Hungary
			if( $woocommerce->customer->get_shipping_country() != 'HU' ){
				unset( $available_gateways['cod'] );
			}
		}
	}
    
    return $available_gateways;
}

add_filter( 'bl_available_payment_gateways_on_checkout', 'bl_modify_available_payment_gateways_on_checkout', 10, 1 );
function bl_modify_available_payment_gateways_on_checkout( $available_gateways = array() ){
	// Only Hungarian site
	if( BL_LANG == 'HU' ){
		global $woocommerce;
		if ( !is_admin() && $woocommerce->customer ){
			$all_training_courses = bl_all_product_is_training_course_in_cart();
			$all_online_courses = bl_all_product_is_online_course_in_cart();
			$lang = ICL_LANGUAGE_CODE;
            //var_dump($all_online_courses);
			// Disable COD if all product in cart is training course or online course
		    if( $all_training_courses || $all_online_courses ){
			    unset( $available_gateways['cod'] );
			} else {
                unset( $available_gateways['bacs'] );
            }

			// Disable BACS if all product in cart is not training course or online course
		    //if( !$all_training_courses || !$all_online_courses ){
			    //unset( $available_gateways['bacs'] );
			//}
		}
	}
    
    return $available_gateways;
}


/* Hide shipping rates when free shipping is available. */
add_filter( 'woocommerce_package_rates', 'bl_hide_shipping_when_free_is_available', 100 );
function bl_hide_shipping_when_free_is_available( $rates ) {
	$free = array();
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}

	// Enable local pickup on AT site
	if( BL_LANG == 'AT' ){
		if( !empty( $free ) ){
			foreach ( $rates as $rate_id => $rate ) {
				if ( 'local_pickup' === $rate->method_id ) {
					$free[ $rate_id ] = $rate;
					break;
				}
			}
		}
	}

	// Add new price for SK site
	if( BL_LANG == 'SK' ){
		$discount_amount = 50;
		
		foreach ( $rates as $rate_id => $rate ) {
			if( WC()->cart->subtotal >= 70 ){
				$rates[$rate_id]->cost = $rates[$rate_id]->cost - ( $rates[$rate_id]->cost * ( $discount_amount / 100 ) );
			}
		}
	}

	return ! empty( $free ) ? $free : $rates;
}

/* Add delivery days after shipping label */
add_action('woocommerce_after_shipping_rate', 'bl_add_delivery_days_after_label');
function bl_add_delivery_days_after_label( $method, $index = '' ){
	
	// Only flat_rate method
	if( $method->get_method_id() == 'flat_rate' ){
		$instance_id = $method->get_instance_id();
		$delivery_days = '';
		$days_to_instance_ids = array(
			21	=> '1-2',
			23	=> '1-2',
			24	=> '3-5',
			25	=> '2-7',
			26	=> '1-3',
			27	=> '2-5',
			28	=> '3-6',
			29	=> '3-6',
		);

		if( !empty( $days_to_instance_ids[ $instance_id ] ) ){
			$delivery_days = $days_to_instance_ids[ $instance_id ];
		}

		if( !empty( $delivery_days ) ){
			echo '<div class="delivery-days">('. sprintf( __( '%s workdays', 'bl' ), $delivery_days ) .')</div>';
		}
	}
}

/* Filter Shipping Zones by language */
/* Fix prefix for Zones: 'EN-' & 'HU-' */
add_filter( 'bl_modify_shipping_zones', 'bl_modify_shipping_zones_by_language', 10, 1 );
function bl_modify_shipping_zones_by_language( $zones = array() ){
	// Only Hungarian site
	if( BL_LANG == 'HU' ){
		if( !empty( $zones ) && !is_admin() ){
			$lang = ICL_LANGUAGE_CODE;
			$new_zones = array();

			if( $lang == 'hu' ){
				foreach ( $zones as $zone ) {
					if( strpos( $zone->zone_name, 'HU-') !== false ){
						$new_zones[] = $zone;
					}
				}
			} else if( $lang == 'en' ) {
				foreach ( $zones as $zone ) {
					if( strpos( $zone->zone_name, 'EN-') !== false ){
						$new_zones[] = $zone;
					}
				}
			}

			if( !empty( $new_zones ) ){
				$zones = $new_zones;
			}
		}
	}

	return $zones;
}

/* Filter Shipping Zones for packages by language */
/* Fix prefix for Zones: 'EN-' & 'HU-' */
add_filter( 'bl_modify_shipping_zones_for_package', 'bl_modify_shipping_zones_for_package_by_language', 10, 1 );
function bl_modify_shipping_zones_for_package_by_language( $zones = array() ){
	// Fix modified_query
	if( !empty( $zones ) ){
		$fixed_zones = array();
		foreach ( $zones as $zone ) {
			$fixed_zones[] = $zone->zone_id;
		}

		$zones = $fixed_zones;
	}

	if( !empty( $zones ) && !is_admin() ){
		$lang = ICL_LANGUAGE_CODE;
		$new_zones = array();

		if( $lang == 'hu' ){
			foreach ( $zones as $zone_id ) {
				$zone = WC_Shipping_Zones::get_zone( $zone_id );

				if( !empty( $zone ) ){
					if( strpos( $zone->get_zone_name(), 'HU-') !== false ){
						$new_zones[] = $zone_id;
					}
				}
			}
		} else if( $lang == 'en' ) {
			foreach ( $zones as $zone_id ) {
				$zone = WC_Shipping_Zones::get_zone( $zone_id );
				
				if( !empty( $zone ) ){
					if( strpos( $zone->get_zone_name(), 'EN-') !== false ){
						$new_zones[] = $zone_id;
					}
				}
			}
		}

		if( !empty( $new_zones ) ){
			$zones = $new_zones;
		}
	}

	return $zones[0];
}


/* Fixing country list on checkout page */
add_filter( 'woocommerce_countries', 'wc_remove_specific_country', 10, 1 );
function wc_remove_specific_country( $countries = array() ){
	// Only in HU site
	if( BL_LANG == 'HU' ){
		if( !is_admin() && !is_page(BL_PAGE_REGISTRATION) ){
			$lang = ICL_LANGUAGE_CODE;
			$hun_country_list = array( 'HU', 'AT', 'DE', 'CH', 'SK', 'RO', 'GB', 'NL', 'SE' );
			$en_country_list = array( 'AT', 'CZ', 'DE', 'RO', 'SK', 'SI', 'AL', 'BE', 'BA', 'BG', 'DK', 'GB', 'FR', 'GI', 'NL', 'HR', 'CY', 'PL', 'LV', 'LU', 'MC', 'MT', 'NO', 'IT', 'PT', 'SM', 'ES', 'SE', 'RS', 'VA', 'EE', 'IE', 'US', 'CA', 'DZ', 'ZA', 'EG', 'LR', 'LY', 'ML', 'MA', 'MU', 'SC', 'TN', 'KR', 'AE', 'HK', 'IN' ,'IL', 'JP','CN', 'LB', 'MY', 'MV', 'MN', 'SA', 'SG', 'TW', 'TH', 'VN', 'BY', 'MK', 'MD', 'ME', 'TR', 'UA', 'VI', 'BS', 'VG', 'CR', 'DM', 'DO', 'GL', 'JM', 'CU', 'MX', 'PA', 'PR', 'AU', 'AR', 'CL', 'EC', 'PE', 'VE', 'GR' );
			

			if( $lang == 'hu' ){
				foreach ( $countries as $country_code => $country_name ) {
					if( !in_array( $country_code, $hun_country_list ) ){
						unset( $countries[$country_code] );
					}
				}
			}
			if( $lang == 'en' ){
				foreach ( $countries as $country_code => $country_name ) {
					if( !in_array( $country_code, $en_country_list ) ){
						unset( $countries[$country_code] );
					}
				}
			}
		}
	} else if( BL_LANG == 'SK' ){
		// Only in SK site
		if( !is_admin() && !is_page(BL_PAGE_REGISTRATION) ){
			$sk_country_list = array( 'SK', 'PL', 'CZ' );

			foreach ( $countries as $country_code => $country_name ) {
				if( !in_array( $country_code, $sk_country_list ) ){
					unset( $countries[$country_code] );
				}
			}
		}
	} else if( BL_LANG == 'AT' ){
		// Only in AT site
		if( !is_admin() && !is_page(BL_PAGE_REGISTRATION) ){
			$sk_country_list = array( 'AT', 'DE' );

			foreach ( $countries as $country_code => $country_name ) {
				if( !in_array( $country_code, $sk_country_list ) ){
					unset( $countries[$country_code] );
				}
			}
		}
	}

	return $countries; 
}


// Privacy policy checkout
add_action('woocommerce_checkout_process', 'bl_checkout_check_privacy_policy');
function bl_checkout_check_privacy_policy(){
	// Privacy policy
	if( isset( $_POST['privacy-policy-field'] ) && $_POST['privacy-policy-field'] == '1' ){
		if( !isset( $_POST['privacy-policy'] ) && $_POST['privacy-policy'] != '1' ){
			wc_add_notice( __( 'You have to agree to our Privacy policy in order to proceed', 'bl' ), 'error' );
		}
	}
}


/* Add file upload for checkout if training courses is in cart */
add_filter('woocommerce_billing_fields', 'bl_add_certificate_to_checkout_if_training_courses_in_cart');
function bl_add_certificate_to_checkout_if_training_courses_in_cart( $fields ) {
	$need_to_upload_certificate = bl_need_to_upload_certificate();

    if( $need_to_upload_certificate ){

	    $fields['billing_options'] = array(
	        'label' => __( 'Certificate of qualification', 'bl' ), // Add custom field label
	        'type' => 'bl-file', // add field type
	    );
	}

    return $fields;
}

// File upload check for training courses
add_action('woocommerce_checkout_process', 'bl_checkout_check_certificate_of_qualification');
function bl_checkout_check_certificate_of_qualification(){
	$need_to_upload_certificate = bl_need_to_upload_certificate();

    if( $need_to_upload_certificate ){
    	if( empty( $_POST['document-id'] ) ){
    		wc_add_notice( __( 'Please upload your certificate.', 'bl' ), 'error' );
    	}
    }
}

// File uplod for chekout
add_action( 'wp_ajax_bl_upload_certificate_from_checkout', 'bl_upload_certificate_from_checkout' );
add_action( 'wp_ajax_nopriv_bl_upload_certificate_from_checkout', 'bl_upload_certificate_from_checkout' );
function bl_upload_certificate_from_checkout(){
	$return = array();

	// Upload new document
	if( !empty( $_FILES["document"] ) ){
		$file_raw = $_FILES["document"];

        if( $file_raw['name'] != '' ){

        	$filename = sanitize_file_name('Certificate - '. rand() .'-'. $file_raw['name'] );
            $tmp_name = $file_raw['tmp_name'];
            $filesize = $file_raw['size'];
                
            if ( !empty( $filename ) ) {
                $wp_filetype = wp_check_filetype( basename( $filename ), null );
                $wp_upload_dir = wp_upload_dir();

                // Move the uploaded file into the WordPress uploads directory
                move_uploaded_file( $tmp_name, $wp_upload_dir['path']  . '/' . $filename );

                $attachment = array(
                    'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
                    'post_mime_type' => $wp_filetype['type'],
                    'post_title' => $filename,
                    'post_content' => '',
                    'post_status' => 'inherit'
                );

                $filename = $wp_upload_dir['path']  . '/' . $filename;
                
                $attach_id = wp_insert_attachment( $attachment, $filename );
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );

                wp_update_attachment_metadata( $attach_id, array_merge( $attach_data, $attachment ) );

                $return = array( 'success' => true, 'attachemnt_id' => $attach_id );
            }
        }
    }


    // Delete old document
    if( !empty( $_POST['old-document-id'] ) ){
    	wp_delete_attachment( $_POST['old-document-id'], true );
    }

    wp_send_json( $return );
}

// Save Certificate
add_action('woocommerce_checkout_create_order', 'bl_save_certificate_id_to_order');
function bl_save_certificate_id_to_order( $order = array(), $data = array() ){
	if( !empty( $_POST['document-id'] ) ){
		$order->update_meta_data( '_certificate-of-qualification-document-id', $_POST['document-id'] );
	}
}


add_action('woocommerce_checkout_create_order', 'bl_new_order_regenerate_stock');
function bl_new_order_regenerate_stock( $order = array(), $data = array() ){
	foreach ( $order->get_items() as $items_key => $item_product ) { 
		$product = $item_product->get_product();

		bl_generate_variations_data( $product );
	}
}


// Order pay endpoint
add_action( 'before_woocommerce_pay', 'bl_before_order_pay_content' );
function bl_before_order_pay_content(){
	if( is_wc_endpoint_url( 'order-pay' ) ){
		echo '<div class="subpage-wrapper">
				<section class="section top">
					<div class="container woocommerce-order-pay">';
		bl_breadcrumb();

		wc_get_template( 'order-progress-bar.php' );
	}
}

add_action( 'after_woocommerce_pay', 'bl_after_order_pay_content' );
function bl_after_order_pay_content(){
	if( is_wc_endpoint_url( 'order-pay' ) ){
		echo '</div>
			</section>
		</div>';
	}
}


// Change placeholder image for products
add_filter('woocommerce_placeholder_img_src', 'bl_change_placeholder_img_src');
function bl_change_placeholder_img_src( $src ) {
	$src = BL_DEFAULT_PRODUCT_IMAGE_URL;
	 
	return $src;
}


/* Checkout - Newsletter subscribe */
add_action( 'woocommerce_checkout_create_order', 'bl_after_checkout_subscribe_user_to_newsletter');
function bl_after_checkout_subscribe_user_to_newsletter( $order = array(), $data = array() ){
	
	if( isset( $_POST['newsletter'] ) ){
		$name = $_POST['billing_first_name'] . ' ' . $_POST['billing_last_name'];
		$email = $_POST['billing_email'];
		$ip = get_current_ip_address();
		$current_timestamp = current_time( 'timestamp' );

		// Hashes
		$hash = md5( $email . $name . BL_NEWSLETTER_SUBSCRIPTION_FIX_HASH );
		$unsubscribe_hash = md5( $email . $name . BL_NEWSLETTER_SUBSCRIPTION_FIX_UNSUBSCRIBE_HASH );

		$subsription_row = array(
			'email' => $email,
			'name' => $name,
			'hash' => $hash,
			'subscribe_time' => $current_timestamp,
			'subscribe_ip' => $ip,
			'unsubscribe_hash' => $unsubscribe_hash
	 	);

	 	$insert_answer = insert_newsletter_subscription( $subsription_row );

	 	if( $insert_answer['error'] != true ){
	 		$link = home_url() . '?newsletter-subscription=1&id=' . $insert_answer['id'] . '&hash=' . $hash;

	 		// Email to user
			if( class_exists( 'Auretto_Email_Editor' ) ){
				$AWEmail = new Auretto_Email_Editor();
				$extra_datas = array(
					'receiver_email' => $email,
					'replaceable_content_words' => array(
						'%name%' => $name,
						'%link%' => $link
					)
				);
				$AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_NEWSLETTER_SUBSCRIPTION_EMAIL_VERIFICATION, $extra_datas );
			}
	 	}
	}
}

/* Add order action */
add_action( 'woocommerce_order_actions', 'sv_wc_add_order_meta_box_action' );
function sv_wc_add_order_meta_box_action( $actions = array() ) {
	if( BL_LANG == 'HU' ){
		global $theorder;

		// Remove woocommerce invoice send
		unset( $actions['send_order_details'] );

		// Add order action
		$actions['bl_send_invoice_to_customer'] = __( 'Send invoice to customer', 'bl' );
	}
	return $actions;
}

add_action( 'woocommerce_order_action_bl_send_invoice_to_customer', 'bl_send_invoice_to_customer_order_action' );
function bl_send_invoice_to_customer_order_action( $order = array() ) {
	if( !empty( $order ) ){
		$email = $order->billing_email;
		$name = $order->billing_first_name .' '. $order->billing_last_name;
		$invoice_id = get_post_meta( $order->get_id(), '_invoice-id', true );

		if( !empty( $invoice_id ) ){
			if( !empty( $email ) && !empty( $name ) ){
				if( class_exists( 'Auretto_Email_Editor' ) ){
					$AWEmail = new Auretto_Email_Editor();
					$extra_datas = array(
						'receiver_email' => $email,
						'replaceable_content_words' => array(
							'%name%' => $name
						),
						'added_attachments' => array( $invoice_id )
					);
					$email_sent = $AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_SEND_INVOICE_TO_CUSTOMER, $extra_datas );

					if( $email_sent ){
						set_transient( 'bl_send_invoice_to_customer_notice', __( 'The invoice has been sent to customer!', 'bl' ) );
					} else {
						set_transient( 'bl_send_invoice_to_customer_error', __( 'Server error. Please try it again later!', 'bl' ) );
					}

				} else {
					set_transient( 'bl_send_invoice_to_customer_error', __( 'Server error. Please try it again later!', 'bl' ) );
				}
			} else {
				set_transient( 'bl_send_invoice_to_customer_error', __( 'Billing email and/or billing name is missing for this order. Please fill the billing email and billing name before you send it to customer!', 'bl' ) );
			}
		} else {
			set_transient( 'bl_send_invoice_to_customer_error', __( 'No Invoce for this order. Please regenerate it before you send it to customer!', 'bl' ) );
		}
	}
}

add_action( 'admin_notices', 'bl_send_invoice_to_customer_error_notices' );
function bl_send_invoice_to_customer_error_notices() {
	if ( $error_message = get_transient( 'bl_send_invoice_to_customer_error' ) ) { ?>
		
		<div class="updated error">
			<p><?php echo $error_message; ?></p>
		</div>

		<?php delete_transient( 'bl_send_invoice_to_customer_error' );
	}

	if ( $updated_message = get_transient( 'bl_send_invoice_to_customer_notice' ) ) { ?>
		
		<div class="updated notice notice-success is-dismissible">
			<p><?php echo $updated_message; ?></p>
		</div>

		<?php delete_transient( 'bl_send_invoice_to_customer_notice' );
	}
}


// Add Custom fields to checkout
add_filter( 'woocommerce_checkout_fields' , 'bl_add_custom_fields_to_checkout' );
function bl_add_custom_fields_to_checkout( $fields = array() ) {
	if( is_user_logged_in() ){
		$tax_number = get_user_meta( get_current_user_id(), 'tax-number', true );
    } else {
    	$tax_number = '';
    }

    $fields['billing']['tax_number'] = array(
    	'label' => __('Tax number', 'bl'),
    	'placeholder' => __('Tax number', 'bl'),
    	'required' => false,
    	'clear' => false,
    	'type' => 'text',
    	'class' => array( 'form-row-first' ),
    	'value' => $tax_number
    );
    return $fields;
}

/* Save Custom fields from checkout to order and user */
add_action('woocommerce_checkout_update_order_meta', 'bl_save_custom_fields_to_order_from_checkout');
function bl_save_custom_fields_to_order_from_checkout( $order_id = 0 ) {
	if( !empty( $order_id ) && $order_id != 0 ){
		if ( !empty( $_POST['tax_number'] ) ) {
			update_post_meta( $order_id, '_billing_tax_number', sanitize_text_field( $_POST['tax_number'] ) );

			if( is_user_logged_in() ){
				update_user_meta( get_current_user_id(), 'tax-number', sanitize_text_field( $_POST['tax_number'] ) );
			}
		}
	}
}

/* Show Tax number on order page */
//add_action( 'woocommerce_admin_order_data_after_billing_address', 'bl_display_tax_number_on_order_admin_page', 10, 1 );
function bl_display_tax_number_on_order_admin_page( $order = array() ){
	if( !empty( $order ) ){
		$user_tax_number = get_post_meta( $order->get_id(), '_billing_tax_number', true );

		if( empty( $user_tax_number ) ){
			$user_id = get_post_meta( $order->get_id(), '_customer_user', true );

			if( !empty( $user_id ) ){
				$user_tax_number = get_user_meta( $user_id, 'tax-number', true );
			}
		}

		if( !empty( $user_tax_number ) ){
			echo '<p>
				<strong>' . __('Tax number', 'bl' ) . ':</strong>
				<br/>' . $user_tax_number . 
			'</p>';
		}
	}   
}


/* Fix WPML item metadatas after order created */
add_action('woocommerce_checkout_update_order_meta', 'bl_fix_wmpl_item_metadatas_after_checkout');
function bl_fix_wmpl_item_metadatas_after_checkout( $order_id = 0 ) {
	if( !empty( $order_id ) && $order_id != 0 ){
		$order = wc_get_order( $order_id );

		foreach ( $order->get_items() as $item_id => $item_data ) {
			$_product = $item_data->get_product();
			$formatted_net_price = number_format( wc_get_price_excluding_tax( $_product ), 6, '.', '' );

			wc_update_order_item_meta( $item_id, '_wcml_converted_subtotal', $formatted_net_price);
			wc_update_order_item_meta( $item_id, '_wcml_converted_total', $formatted_net_price);
		}
	}
}

/* Modify training course meta infos on cart */
add_filter( 'woocommerce_get_item_data', 'bl_modify_training_course_meta_infos', 10, 2 );
function bl_modify_training_course_meta_infos( $item_data, $cart_item ){
	// Training course
	$training_course = get_post_meta( $cart_item['product_id'], 'training_course', true );

	if( $training_course == '1' ){
		$training_course_timestamp = get_post_meta( $cart_item['product_id'], 'training_course_date_time', true );
		$training_course_formatted_date_time = date_i18n( 'Y.m.d. H:i', (int)$training_course_timestamp );
		$city = get_post_meta( $cart_item['product_id'], 'training_course_city', true );

		$item_data[] = array(
			'key' => __('City', 'bl'),
			'value' => $city
		);
		$item_data[] = array(
			'key' => __('Date', 'bl'),
			'value' => $training_course_formatted_date_time
		);
	}

	// Products with one attribute
	if ( $cart_item['data']->is_type( 'variation' ) && !wc_is_attribute_in_product_name( $value, $cart_item['data']->get_name() ) && empty( $item_data ) ) {
        foreach ( $cart_item['variation'] as $name => $value ) {
            $taxonomy = wc_attribute_taxonomy_name( str_replace( 'attribute_pa_', '', urldecode( $name ) ) );

            if ( taxonomy_exists( $taxonomy ) ) {
                // If this is a term slug, get the term's nice name.
                $term = get_term_by( 'slug', $value, $taxonomy );

                if ( ! is_wp_error( $term ) && $term && $term->name ) {
                    $value = $term->name;
                }
                $label = wc_attribute_label( $taxonomy );
            } else {
                // If this is a custom option slug, get the options name.
                $value = apply_filters( 'woocommerce_variation_option_name', $value );
                $label = wc_attribute_label( str_replace( 'attribute_', '', $name ), $cart_item['data'] );
            }

            $item_data[] = array(
                'key'   => $label,
                'value' => $value,
            );
        }
    }

	return $item_data;
}

// Modify cart item name
add_filter( 'bl_cart_item_name', 'bl_modify_cart_item_name', 10, 4 );
function bl_modify_cart_item_name( $product = array(), $product_name = '', $cart_item = '', $cart_item_key = '' ){

	if( !empty( $product ) ){
		if( $product->is_type( 'variation' ) ){
			$product_name = get_the_title( $cart_item['product_id'] );
		}
	}

	return $product_name;
}

// Modify variation product name
add_filter( 'bl_product_variation_name', 'bl_modify_product_variation_name', 10, 4 );
function bl_modify_product_variation_name( $product = array(), $product_name = '' ){

	if( !empty( $product ) ){
		if( $product->is_type( 'variation' ) ){
			$product_name = get_the_title( $product->get_parent_id() );
		}
	}

	return $product_name;
}

add_filter( 'woocommerce_add_to_cart_redirect', 'bl_redirect_to_checkout_if_training_course' );
function bl_redirect_to_checkout_if_training_course( $url = '' ) {

	if( !empty( $_REQUEST['add-to-cart'] ) ){
		$product_id = $_REQUEST['add-to-cart'];
		$training_course = get_post_meta( $product_id, 'training_course', true );

		if( $training_course == '1' ){
			$url = wc_get_checkout_url();
		}
	}

	return $url;
}

// Modify VIP user tax to 0
add_filter( 'woocommerce_calc_tax', 'bl_tax_test', 10, 5 );
function bl_tax_test( $taxes = array() ){
	$need_zero_tax = false;

	// For frontpage
	if( is_vip_user() ){
		$need_zero_tax = true;
	}

	if( is_ajax() ){
		// Admin order update
		if( !empty( $_POST ) ){
			if( isset( $_POST['action'] ) && $_POST['action'] == 'woocommerce_calc_line_taxes' ){
				$order_id = $_POST['order_id'];

				if( !empty( $order_id ) ){
					$is_vip_order = get_post_meta( $order_id, '_is_vip_order', true );

					if( $is_vip_order == '1' ){
						$need_zero_tax = true;
					}
				}
			}
		}

		// Checkout update
//		if( !empty( $_GET['wc-ajax'] ) && $_GET['wc-ajax'] == 'update_order_review' ){
//			$need_zero_tax = true;
//		}

		// Cart update
		if( !empty( $_GET['wc-ajax'] ) && $_GET['wc-ajax'] == 'get_refreshed_fragments' ){
			$need_zero_tax = true;
		}
	}

	if( $need_zero_tax ){
		if( !empty( $taxes ) ){
			foreach ( $taxes as $key => $tax ) {
				$taxes[$key] = 0;
			}
		}
	}


	return $taxes;
}

add_action('woocommerce_checkout_create_order', 'bl_new_order_is_vip_user');
function bl_new_order_is_vip_user( $order = array(), $data = array() ){
	if( is_vip_user() ){
		$order->update_meta_data( '_is_vip_order', '1' );
	}
}


/* Fix WPML - add item to order via ajax */
add_action( 'woocommerce_ajax_add_order_item_meta', 'bl_fix_wmpl_item_metadatas_after_add_item_via_ajax', 10, 2 );
function bl_fix_wmpl_item_metadatas_after_add_item_via_ajax( $item_id, $item ){
	if ( 'line_item' === $item->get_type() ) {
		if( $item->meta_exists( '_wcml_converted_subtotal' ) && $item->meta_exists( '_wcml_converted_total' ) ){
			$_product = $item->get_product();
			$formatted_net_price = number_format( wc_get_price_excluding_tax( $_product ), 6, '.', '' );

			wc_update_order_item_meta( $item_id, '_wcml_converted_subtotal', $formatted_net_price);
			wc_update_order_item_meta( $item_id, '_wcml_converted_total', $formatted_net_price);
		}
	}
}


// Remove unavaiable products from cart for VIP useres
add_action( 'wp', 'bl_remove_products_from_cart_for_vip_users' );
function bl_remove_products_from_cart_for_vip_users(){
	global $woocommerce;

	if( is_user_logged_in() ){
		$disable_category_products_for_user = get_user_meta( get_current_user_id(), 'disable-category-product-for-user', true );

		if( '1' === $disable_category_products_for_user ){
			$disabled_categories = maybe_unserialize( get_user_meta( get_current_user_id(), 'disabled-categories', true ) );
			$disabled_products = maybe_unserialize( get_user_meta( get_current_user_id(), 'disabled-products', true ) );

			if( !empty( $disabled_categories ) || !empty( $disabled_products ) ){
				if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
					foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $value ) {
						$_product = wc_get_product( $value['product_id'] );
						$product_cat_ids = $_product->get_category_ids();
						$need_to_remove = false;

						// Category check
						if( !empty( $disabled_categories ) ){
							foreach ( $disabled_categories as $disabled_category ) {
								if( in_array( apply_filters( 'wpml_object_id', $disabled_category, 'product_cat', TRUE ), $product_cat_ids ) ){
									$need_to_remove = true;
								}
							}
						}

						// Product check
						if( !empty( $disabled_products ) ){
							foreach ( $disabled_products as $disabled_product ) {
								if( apply_filters( 'wpml_object_id', $disabled_product, 'product', TRUE ) == $value['product_id'] ){
									$need_to_remove = true;
								}
							}
						}

						if( $need_to_remove ){
							$woocommerce->cart->remove_cart_item( $cart_item_key );
							wc_add_notice( sprintf( __('“%s“ has been removed from cart. You are not eligible to buy that product.', 'bl'), get_the_title( $value['product_id'] ) ), 'error');
						}
					}
				}
			}
		}
	}
}

/* Check if user eligible to buy the product */
function bl_is_user_eligible_to_buy_product( $product_id = 0 ){
	if( !empty( $product_id ) ){
		if( is_user_logged_in() ){
			$disable_category_products_for_user = get_user_meta( get_current_user_id(), 'disable-category-product-for-user', true );

			if( '1' === $disable_category_products_for_user ){
				$disabled_categories = maybe_unserialize( get_user_meta( get_current_user_id(), 'disabled-categories', true ) );
				$disabled_products = maybe_unserialize( get_user_meta( get_current_user_id(), 'disabled-products', true ) );
				
				if( !empty( $disabled_categories ) || !empty( $disabled_products ) ){
					$_product = wc_get_product( $product_id );
					$product_cat_ids = $_product->get_category_ids();
					$need_to_remove = false;

					// Category check
					if( !empty( $disabled_categories ) ){
						foreach ( $disabled_categories as $disabled_category ) {
							if( in_array( apply_filters( 'wpml_object_id', $disabled_category, 'product_cat', TRUE ), $product_cat_ids ) ){
								$need_to_remove = true;
							}
						}
					}

					// Product check
					if( !empty( $disabled_products ) ){
						foreach ( $disabled_products as $disabled_product ) {
							if( apply_filters( 'wpml_object_id', $disabled_product, 'product', TRUE ) == $product_id ){
								$need_to_remove = true;
							}
						}
					}

					if( $need_to_remove ){
						return false;
					}
				}
			}
		}
	}

	return true;
}

add_filter( 'bl_shop_category_list_exlude', 'bl_shop_category_list_exlude_by_lang', 10, 1 );
function bl_shop_category_list_exlude_by_lang( $exclude_list = array() ){
	if( BL_LANG == 'SK' ){
		$exclude_list[] = BL_PRODUCT_CAT_ID_GLAMCOR_LAPMS;
	}

	return $exclude_list;
}

add_filter( 'bl_shop_sidebar_category_list_exlude', 'bl_shop_sidebar_category_list_exlude_by_lang', 10, 1 );
function bl_shop_sidebar_category_list_exlude_by_lang( $exclude_list = array() ){
	if( BL_LANG == 'SK' ){
		$exclude_list[] = BL_PRODUCT_CAT_ID_GLAMCOR_LAPMS;
		$exclude_list[] = BL_PRODUCT_CAT_ID_REFLECTOCIL;
	}

	return $exclude_list;
}

function bl_get_fixed_lang_sale_products(){
	$sale_products = array();
	$products_on_sale = array_merge( array( 0 ), wc_get_product_ids_on_sale() );

    // Exclude training courses
    $products_on_sale = array_diff( $products_on_sale, bl_get_training_courses_ids() );

    // Sort simple products
    $simple_products_on_sale = array();
    if( !empty( $products_on_sale ) ){
        foreach ( $products_on_sale as $prod_id ) {
            $prod = wc_get_product( $prod_id );

            if( $prod ){
                if( $prod->get_type() === 'simple' ){
                    $simple_products_on_sale[] = $prod->get_id();
                }
            }
        }
    }

    $sale_products = bl_get_product_variations_on_sale( $products_on_sale );

    // Merge variations and simple products
    if( !empty( $simple_products_on_sale ) ){
        $sale_products = array_merge( $simple_products_on_sale, $sale_products );
    }

    // Fix language sorting
    $fixed_lang_product_variations_on_sale = array();

    if( !empty( $sale_products ) ){
        foreach ( $sale_products as $id ) {
            $prod = wc_get_product( $id );

            if( $prod ){
                if( $prod->get_type() === 'simple' ){
                    $product_lang_id = apply_filters( 'wpml_object_id', $prod->get_id(), 'product', FALSE );
                }
                if( $prod->get_type() === 'variation' ){
                    $product_lang_id = apply_filters( 'wpml_object_id', $prod->get_id(), 'product_variation', FALSE );
                }
                    
                if( !in_array( $product_lang_id, $fixed_lang_product_variations_on_sale ) ){
                    $fixed_lang_product_variations_on_sale[] = $product_lang_id;
                }
            }
        }

        $sale_products = $fixed_lang_product_variations_on_sale;
    }

    return $sale_products;
}


// Stock update on woocommerce order status change
add_action( 'woocommerce_order_status_completed', 'bl_maybe_reduce_stock_levels', 100 );
add_action( 'woocommerce_order_status_processing', 'bl_maybe_reduce_stock_levels', 100 );
add_action( 'woocommerce_order_status_on-hold', 'bl_maybe_reduce_stock_levels', 100 );
function bl_maybe_reduce_stock_levels( $order_id ) {
	$order = wc_get_order( $order_id );

	if ( ! $order ) {
		return;
	}

	// Loop over all items.
	foreach ( $order->get_items() as $item ) {
		if ( ! $item->is_type( 'line_item' ) ) {
			continue;
		}

		$product = $item->get_product();

		if ( ! $product || ! $product->managing_stock() ) {
			continue;
		}

		$qty       = apply_filters( 'woocommerce_order_item_quantity', $item->get_quantity(), $order, $item );
		$new_stock = $product->get_stock_quantity();
		$old_stock = $new_stock + $qty;

		$order_status = $order->get_status( 'edit' );

		// Log stock
		bl_product_stock_change( $product, (int)$old_stock, (int)$new_stock, 'order-status-changed-to-'. $order_status .' - ' . $order_id );
	}
}

// Stock update on woocommerce order status change
add_action( 'woocommerce_order_status_refunded', 'bl_maybe_increase_stock_levels', 100 );
add_action( 'woocommerce_order_status_cancelled', 'bl_maybe_increase_stock_levels', 100 );
add_action( 'woocommerce_order_status_pending', 'bl_maybe_increase_stock_levels', 100 );
function bl_maybe_increase_stock_levels( $order_id ) {
	$order = wc_get_order( $order_id );

	if ( ! $order ) {
		return;
	}
	// Loop over all items.
	foreach ( $order->get_items() as $item ) {
		if ( ! $item->is_type( 'line_item' ) ) {
			continue;
		}

		$product = $item->get_product();

		if ( ! $product || ! $product->managing_stock() ) {
			continue;
		}

		$qty       = apply_filters( 'woocommerce_order_item_quantity', $item->get_quantity(), $order, $item );
		$new_stock = $product->get_stock_quantity();
		$old_stock = $new_stock - $qty;

		$order_status = $order->get_status( 'edit' );

		// Log stock
		bl_product_stock_change( $product, (int)$old_stock, (int)$new_stock, 'order-status-changed-to-'. $order_status .' - ' . $order_id );
	}
}

// WC has problem translating the single item allowed cart notice so we override it
add_filter('woocommerce_add_error', 'bl_override_single_item_allowed_in_cart_notice');
function bl_override_single_item_allowed_in_cart_notice($message) {
    if (get_locale() == 'hu_HU' && strpos($message, 'You cannot add another') !== false) {
        $message_parts = explode('&quot;', $message);
        $message_parts[0] = explode('</a>', $message_parts[0]);
        $message_parts[0][1] = 'A ';
        $message_parts[0] = implode('</a>', $message_parts[0]);
        $message_parts[2] = ' már a kosaradban van. Ebből a termékből nem tehetsz be két egyformát a kosaradba.';
        $message = implode('&quot;', $message_parts);
    }

    return $message;
}