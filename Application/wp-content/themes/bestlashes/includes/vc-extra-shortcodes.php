<?php

$path = get_stylesheet_directory() . '/includes/vc-extra-shortcodes/';

if ( function_exists('vc_map') ) {
	// Frontpage
	include $path . 'frontpage-slider.php';
	include $path . 'frontpage-intro.php';
	include $path . 'frontpage-separator-heading.php';
	include $path . 'frontpage-featured-products.php';
	include $path . 'subpage-highlighted-section.php';
	include $path . 'subpage-half-img-text.php';
	include $path . 'subpage-big-text.php';
	include $path . 'subpage-center-separator-heading.php';
	include $path . 'subpage-simple-page-title.php';
	include $path . 'subpage-course-list.php';
	include $path . 'simple-button.php';
	include $path . 'subpage-bestbrows-hero.php';
	include $path . 'subpage-bestbrows-underline-button.php';
	include $path . 'subpage-bestbrows-image-slider.php';
	include $path . 'subpage-bestbrows-social.php';
}