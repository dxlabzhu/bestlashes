<?php

add_action( 'vc_before_init', 'bl_vc_subpage_bestbrows_image_slider_box');
function bl_vc_subpage_bestbrows_image_slider_box() {
	vc_map( 
		array (
			'name' => __( 'Subpage - BestBrows - Image slider', 'bl' ),
			'base' => 'subpage_bestbrows_image_slider',
			'icon' => 'of-icon-for-vc',
			'category' => __( 'Bestlashes', 'bl' ),
			'params' => array (
				// params group
	            array(
	                'type' => 'param_group',
	                'value' => '',
	                'heading' => __( 'Slides', 'bl' ),
	                'param_name' => 'slides',
	                'params' => array(
						array (
							'type' => 'attach_image',
							'heading' => __( 'Main image', 'bl' ),
							'param_name' => 'main_image',
						),
	                )
	            ),
				array (
					'type' => 'textfield',
					'heading' => __ ( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __ ( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			),
			'admin_enqueue_js' => array(
				esc_url( 'cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.4.5/jquery-ui-timepicker-addon.min.js' ),
			),
			'admin_enqueue_css' => array(
				esc_url( 'ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' ),
			)
		)
	);
}

add_shortcode('subpage_bestbrows_image_slider', 'subpage_bestbrows_image_slider_shortcode');
function subpage_bestbrows_image_slider_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'slides' => array(),
		'eclass' => '',
	), $atts));

	$slides = vc_param_group_parse_atts( $atts['slides'] );
	$current_timestamp = current_time( 'timestamp' );

	ob_start(); ?>

	<section class="section bestbrows-slider-container <?php echo $atts['eclass']; ?>">
		<div class="container">
			<div class="columns">
				<div class="margin">
					<div data-delay="4000" data-animation="cross" data-autoplay="1" data-nav-spacing="10" data-duration="1000" data-infinite="1" class="bestbrows-slider w-slider">
						<div class="w-slider-mask">
							<?php if( !empty( $slides ) ){
								$i = 1;

								foreach ( $slides as $slide ) { ?>

									<div class="bestbrows-slide-<?php echo $i; ?> w-slide"></div>

									<?php $i++;
								}
							} ?>
						</div>
						<div class="bestbrows-slider-left-arrow w-slider-arrow-left">
							<div class="w-icon-slider-left"></div>
						</div>
						<div class="bestbrows-slider-right-arrow w-slider-arrow-right">
							<div class="w-icon-slider-right"></div>
						</div>
						<div class="bestbrows-slider-nav w-slider-nav w-round"></div>
					</div>
					<style type="text/css" media="screen">
						<?php if( !empty( $slides ) ){
							$i = 1;

							foreach ( $slides as $slide ) {
								$main_image_full_url = wp_get_attachment_image_src( $slide['main_image'], 'full' );
								$main_image_small_url = wp_get_attachment_image_src( $slide['main_image'], 'textbox-image' ); ?>

								.bestbrows-slide-<?php echo $i; ?> {
								    background-image: url(<?php echo $main_image_full_url[0]; ?>);
								}
								@media (-webkit-min-device-pixel-ratio: 1.25), (min-resolution: 120dpi) {
									.bestbrows-slide-<?php echo $i; ?> {
									    background-image: url(<?php echo $main_image_small_url[0]; ?>);
									}
								}

								<?php $i++;
							}
						} ?>
					</style>
				</div>
			</div>
		</div>
	</section>
	
	
	
    <?php 
    return ob_get_clean();
}