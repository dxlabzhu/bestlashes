<?php

add_action( 'vc_before_init', 'bl_vc_frontpage_slider_box');
function bl_vc_frontpage_slider_box() {
	vc_map( 
		array (
			'name' => __( 'Frontpage - Slider', 'bl' ),
			'base' => 'frontpage_slider',
			'icon' => 'of-icon-for-vc',
			'category' => __( 'Bestlashes', 'bl' ),
			'params' => array (
				// params group
	            array(
	                'type' => 'param_group',
	                'value' => '',
	                'heading' => __( 'Slides', 'bl' ),
	                'param_name' => 'slides',
	                'params' => array(
						array (
							'type' => 'textfield',
							'heading' => __( 'Slider title', 'bl' ),
							'param_name' => 'title',
							'admin_label' => true
						),
						array (
							'type' => 'textfield',
							'heading' => __( 'Button label', 'bl' ),
							'param_name' => 'button_label',
						),
						array (
							'type' => 'vc_link',
							'heading' => __ ( 'Link', 'bl' ),
							'param_name' => 'button_link',
						),
						array (
							'type' => 'attach_image',
							'heading' => __( 'Main image', 'bl' ),
							'param_name' => 'main_image',
						),
						array (
							'type' => 'attach_image',
							'heading' => __( 'Background', 'bl' ),
							'param_name' => 'background',
						),
			           	array(
							'type'        => 'wpc_date',
							'heading'     => __( 'From date', 'bl' ),
							'param_name'  => 'from_date',
							'value'       => ''
						),
						array(
							'type'        => 'wpc_date',
							'heading'     => __( 'To date', 'bl' ),
							'param_name'  => 'to_date',
							'value'       => ''
						),
	                )
	            ),
	            array (
						'type' => 'vc_link',
						'heading' => __ ( 'Scroll down link', 'bl' ),
						'param_name' => 'scroll_down_link',
					),
				array (
					'type' => 'textfield',
					'heading' => __ ( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __ ( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			),
			'admin_enqueue_js' => array(
				esc_url( 'cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.4.5/jquery-ui-timepicker-addon.min.js' ),
			),
			'admin_enqueue_css' => array(
				esc_url( 'ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' ),
			)
		)
	);
}

add_shortcode('frontpage_slider', 'frontpage_slider_shortcode');
function frontpage_slider_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'slides' => array(),
		'scroll_down_link' => '',
		'eclass' => '',
	), $atts));

	$slides = vc_param_group_parse_atts( $atts['slides'] );
	$scroll_down_link = vc_build_link( $atts['scroll_down_link'] );
	$current_timestamp = current_time( 'timestamp' );

	// Filter slides by date
	if( !empty( $slides ) ){
		$filtered_slides = array();

		foreach ( $slides as $slide ) {
			$from_date = $slide['from_date'];
			$to_date = $slide['to_date'];

			if( !empty( $from_date ) && !empty( $to_date ) ){
				$from_date_timestamp = strtotime( $from_date );
				$to_date_timestamp = strtotime( $to_date );

				if( !empty( $from_date_timestamp ) && !empty( $to_date_timestamp ) ){
					// From -> To
					if( $from_date_timestamp < $to_date_timestamp ){
						if( $from_date_timestamp < $current_timestamp && $to_date_timestamp > $current_timestamp ){
							$filtered_slides[] = $slide;
						}
					}

					// To -> From
					if( $to_date_timestamp < $from_date_timestamp ){
						if( $from_date_timestamp < $current_timestamp || $to_date_timestamp > $current_timestamp ){
							$filtered_slides[] = $slide;
						}
					}
				}

			} else {
				$filtered_slides[] = $slide;
			}
		}

		if( !empty( $filtered_slides ) ){
			$slides = $filtered_slides;
		}
	}

	ob_start(); ?>
	
	<section class="home-hero w-clearfix <?php echo $atts['eclass']; ?>">
		<div class="container">
			<div class="bl-breadcrumb-placeholder"></div>
		</div>
		<!-- Slider text, button -->
		<div class="home-slider-text">
			<div class="arrow-container-right mobilehide"><a class="round-button w-button">→</a></div>
			<div class="arrow-container-left mobilehide"><a class="round-button w-button">←</a></div>
			<div class="home-slider-text-mask">
				<div class="home-slider-text-container" data-slide="0">
					<?php if( !empty( $slides ) ){
						foreach ( $slides as $slide ) { 
							$link = vc_build_link( $slide['button_link'] ); ?>

                            <a href="<?php echo $link['url']; ?>">
                                <div class="home-slider-text-box w-clearfix">
                                    <h1 class="home-hero-heading"><?php echo do_shortcode( $slide['title'] ); ?></h1>
                                    <div class="underline-button w-inline-block" title="<?php echo $link['title']; ?>" <?php if( !empty( $link['target']) ){ echo 'target="'. $link['target'] .'"'; } ?> <?php if( !empty( $link['rel']) ){ echo 'rel="'. $link['rel'] .'"'; } ?>">
                                        <div class="underline-button-text"><?php echo do_shortcode( $slide['button_label'] ); ?></div>
                                        <div class="link-underline"></div>
                                    </div>
                                </div>
                            </a>
						<?php }
					} ?>
				</div>
			</div>
		</div>
		<!-- /Slider text, button -->
		
		<!-- Navigation -->
		<div class="home-slider-button-container" data-slide="0">
			<div class="home-slider-buttons" data-slides="<?php echo sizeof($slides); ?>">
				<?php if( !empty( $slides ) ){ 
					for ( $i = 1; $i <= sizeof( $slides ); $i++ ) { ?>

						<a href="#" class="home-slider-button" data-slide="<?php echo $i-1; ?>"><?php printf("%02d", $i ); ?></a>
					
					<?php } ?>
				<?php } ?>
				<div class="home-slider-separator">–</div>
				<a href="<?php echo $scroll_down_link['url']; ?>" class="round-button w-button" title="<?php echo $scroll_down_link['title']; ?>" <?php if( !empty( $scroll_down_link['target'] ) ){ echo 'target="'. $scroll_down_link['target'] .'"'; } ?> <?php if( !empty( $scroll_down_link['rel']) ){ echo 'rel="'. $scroll_down_link['rel'] .'"'; } ?>">↓</a>
				<div class="home-slider-button-circle"></div>
			</div>
		</div>
    	<!-- /Navigation -->

    	<!-- Main image -->
    	<div class="home-hero-parallax-slider" data-slide="0" data-slides="<?php echo sizeof($slides); ?>">
			<div class="home-hero-parallax-slider-mask">
				<div class="home-hero-parallax-slider-container">
					<?php if( !empty( $slides ) ){
						$i = 0;

						foreach ( $slides as $slide ) {
                            $link = vc_build_link( $slide['button_link'] );
							$main_image_small_url = wp_get_attachment_image_src( $slide['main_image'], 'slider-hero' );
							$main_image_url = wp_get_attachment_image_src( $slide['main_image'], 'large' ); ?>
							
							<div class="home-hero-parallax-slider-image-mask home-hero-parallax-slider-image-<?php echo $i; ?>-mask">
                                <a href="<?php echo $link['url']; ?>" class="home-hero-parallax-slider-image home-hero-parallax-slider-image-<?php echo $i; ?>"></a>
							</div>

							<style>
								.home-hero-parallax-slider-image-<?php echo $i; ?> {
								    background-image: url(<?php echo $main_image_small_url[0]; ?>);
								}
								@media (-webkit-min-device-pixel-ratio: 1.25), (min-resolution: 120dpi){
									.home-hero-parallax-slider-image-<?php echo $i; ?> {
								    	background-image: url(<?php echo $main_image_url[0]; ?>);
									}
								}
							</style>

							<?php $i++; 
						}
					} ?>
				</div>
			</div>
		</div>
    	<!-- /Main image -->
	
		<!-- Background -->
		<div class="home-hero-background-slider" data-slide="0">
			<div class="home-hero-background-slider-mask">
				<?php if( !empty( $slides ) ){
					$i = 0;

					foreach ( $slides as $slide ) { 
						$background_image_url = wp_get_attachment_image_src( $slide['background'], 'textbox-image' ); ?>
						
						<div class="home-hero-background-slider-image home-hero-background-slider-image-<?php echo $i; ?>" style="background-image: url(<?php echo $background_image_url[0]; ?>);"></div>

						<?php $i++; 
					}
				} ?>
			</div>
		</div>
		<!-- /Background -->


	</section>
	
    <?php 
    return ob_get_clean();
}



function wpc_date($settings, $value) {
    return '<div class="date-group">'
           . '<input name="' . $settings['param_name'] . '" class="wpb_vc_param_value wpb-date ' . $settings['param_name'] . ' ' . $settings['type'] . '_field" type="text" value="' . $value . '"/>'
           . '</div>';
}
vc_add_shortcode_param('wpc_date', 'wpc_date', get_template_directory_uri() . '/js/admin-vc.js');
 
function wpc_date_style() {
    wp_enqueue_script('jquery-ui-datepicker' );
}
add_action( 'admin_enqueue_scripts', 'wpc_date_style' );