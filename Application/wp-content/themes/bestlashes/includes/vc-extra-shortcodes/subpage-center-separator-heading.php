<?php
// Prob
add_action( 'vc_before_init', 'bl_vc_subpage_center_separator_heading_box');
function bl_vc_subpage_center_separator_heading_box() {
	vc_map ( 
		array (
			'name' => __( 'Subpage Center Separator Heading', 'bl' ),
			'base' => 'subpage_center_separator_heading',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Title', 'bl' ),
					'param_name' => 'title',
					'admin_label' => true
				),
				array (
					'type' => 'dropdown',
					'heading' => __( 'Title type', 'bl' ),
					'param_name' => 'title_type',
					'value' => array(
						'H1' => '1',
						'H2' => '2',
						'H3' => '3',
						'H4' => '4',
						'H5' => '5',
						'H6' => '6',
					)
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('subpage_center_separator_heading', 'subpage_center_separator_heading_shortcode');
function subpage_center_separator_heading_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'title_type' => 'h1',
		'eclass' => ''
	), $atts));
	
	ob_start(); ?>
	
	
    <div class="separator-heading <?php echo $atts['eclass']; ?>">
		<?php if( !empty( $atts['title_type'] ) ){ ?>
			<h<?php echo $atts['title_type']; ?>>
				<?php echo $atts['title']; ?>
			</h<?php echo $atts['title_type']; ?>>
		<?php } else { ?>
			<h3><?php echo $atts['titlte']; ?></h3>
		<?php } ?>
    </div>
	
    <?php 
    return ob_get_clean();
}