<?php

add_action( 'vc_before_init', 'bl_vc_subpage_bestbrows_social_box');
function bl_vc_subpage_bestbrows_social_box() {
	vc_map ( 
		array (
			'name' => __( 'Subpage - BestBrows - Social', 'bl' ),
			'base' => 'subpage_bestbrows_social',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Title', 'bl' ),
					'param_name' => 'title',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Facebook', 'bl' ),
					'param_name' => 'facebook_link',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Instagram', 'bl' ),
					'param_name' => 'instagram_link',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('subpage_bestbrows_social', 'subpage_bestbrows_social_shortcode');
function subpage_bestbrows_social_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'facebook_link' => '',
		'instagram_link' => '',
		'eclass' => ''
	), $atts));

	ob_start(); ?>

	<section class="section <?php echo $atts['eclass']; ?>">
		<div class="container">
			<div class="columns">
				<div class="margin center">
					<div class="bestbrowsocialflex">
						<h3 class="heading-03 bestbrowssocial"><?php echo $atts['title']; ?><br/></h3>
						
						<?php if( !empty( $atts['facebook_link'] ) ){ ?>
							<a href="<?php echo $atts['facebook_link']; ?>" target="_blank" class="nav-item-circle white w-inline-block">
								<div class="fa-brands"></div>
							</a>
						<?php } ?>

						<?php if( !empty( $atts['instagram_link'] ) ){ ?>
							<a href="<?php echo $atts['instagram_link']; ?>" target="_blank" class="nav-item-circle white w-inline-block">
								<div class="fa-brands"></div>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	
    <?php 
    return ob_get_clean();
}