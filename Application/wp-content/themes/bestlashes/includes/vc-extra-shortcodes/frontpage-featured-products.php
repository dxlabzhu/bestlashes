<?php

add_action( 'vc_before_init', 'bl_vc_frontpage_featured_products_box');
function bl_vc_frontpage_featured_products_box() {
	vc_map ( 
		array (
			'name' => __( 'Frontpage Featured products', 'bl' ),
			'base' => 'frontpage_featured_products',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('frontpage_featured_products', 'frontpage_featured_products_shortcode');
function frontpage_featured_products_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'eclass' => ''
	), $atts));

	global $sitepress;

	$featured_products = array();

	// Query
	$args = array();
	$args['post_type'] = 'product';
	$args['posts_per_page'] = '-1';
	$args['post_status'] = 'publish';
	$args['fields'] = 'ids';
	$args['tax_query'] = array();
	$args['tax_query'][] = array(
        'taxonomy' => 'product_visibility',
        'field'    => 'name',
        'terms'    => 'featured',
        'operator' => 'IN',
    );

	$featured_products_loop = new WP_Query( $args );
	
	if( $featured_products_loop->have_posts() ){
		$featured_products = $featured_products_loop->posts;
	}

	//$featured_products = wc_get_featured_product_ids();

	// Exclude training courses
	if( !empty( $featured_products ) ){
		$featured_products = array_diff( $featured_products, bl_get_training_courses_ids() );
	}

	// Query
	$args = array();
	$args['post_type'] = 'product';
	$args['posts_per_page'] = BL_FRONTPAGE_FEATURED_PRODUCTS_POSTS_PER_PAGE;
	$args['post_status'] = 'publish';
	$args['fields'] = 'ids';
	$args['post__in'] = $featured_products;

	$products_loop = new WP_Query( $args );
	
	ob_start(); ?>
	
	<section class="home-product-slider-section">
		<div class="arrow-container-right mobilehide"><a class="round-button w-button">→</a></div>
		<div class="arrow-container-left mobilehide"><a class="round-button w-button">←</a></div>
		<div class="container">
			<div class="bl-breadcrumb-placeholder"></div>
			<div class="product-slider-container">
				<div class="product-slider-mask">
					<div class="row w-row">
						<?php if( $products_loop->have_posts() ){
							while ( $products_loop->have_posts() ) {
								$prod_id = $products_loop->next_post(); 

								$_product = wc_get_product( $prod_id );

								// Image
								$image = $_product->get_image( 'product-featured-small', array( 'class' => 'product-image' ) );
								
								// Price
								$prices = bl_get_product_prices( $_product );

								// Label
								$label_text = get_post_meta( $prod_id, 'product-label-text', true );
								
								if( empty( $label_text ) && $_product->is_on_sale() ){
									if( !empty( $prices['gross_regular_price'] ) ){
										$price_percentage = round( ( 1 - ( $prices['gross_price'] / $prices['gross_regular_price'] ) ) * 100 );

										$label_text = -1 * abs( $price_percentage ) . '%';
									}
								}

								$container_classes = 'col w-col w-col-2 w-col-medium-4 w-col-small-6 w-col-tiny-6';

								include( locate_template( 'template-parts/single-product-list-view.php', false, false ) );

							}
						} ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	
    <?php 
    return ob_get_clean();
}