<?php
// Prob
add_action( 'vc_before_init', 'bl_vc_subpage_simple_page_title_box');
function bl_vc_subpage_simple_page_title_box() {
	vc_map ( 
		array (
			'name' => __( 'Subpage simple page title', 'bl' ),
			'base' => 'subpage_simple_page_title',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Title', 'bl' ),
					'param_name' => 'title',
					'admin_label' => true
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('subpage_simple_page_title', 'subpage_simple_page_title_shortcode');
function subpage_simple_page_title_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'eclass' => ''
	), $atts));
	
	ob_start(); ?>
	
	
    <div class="bl-page-title <?php echo $atts['eclass']; ?>">
		<h1 class="heading-01">
			<?php echo $atts['title']; ?>
		</h1>
		<div class="separator-line"></div>
    </div>
	
    <?php 
    return ob_get_clean();
}