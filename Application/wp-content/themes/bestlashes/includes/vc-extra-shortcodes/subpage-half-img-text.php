<?php
//prob
add_action( 'vc_before_init', 'bl_vc_subpage_half_img_text_box');
function bl_vc_subpage_half_img_text_box() {
	vc_map ( 
		array (
			'name' => __( 'Subpage Half Image Text', 'bl' ),
			'base' => 'subpage_half_img_text',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Title', 'bl' ),
					'param_name' => 'title',
				),
				array (
					'type' => 'textarea_html',
					'heading' => __( 'Content', 'bl' ),
					'param_name' => 'content',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Button label', 'bl' ),
					'param_name' => 'button_label',
				),
				array (
					'type' => 'vc_link',
					'heading' => __ ( 'Link', 'bl' ),
					'param_name' => 'button_link',
				),
				array (
					'type' => 'attach_image',
					'heading' => __( 'Left image', 'bl' ),
					'param_name' => 'left_image',
				),
				array (
					'type' => 'checkbox',
					'heading' => __( 'Top-bottom padding', 'bl' ),
					'param_name' => 'top_bottom_padding',
					'value' => array(
						'Top-bottom padding' => '1'
					)
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('subpage_half_img_text', 'subpage_half_img_text_shortcode');
function subpage_half_img_text_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'button_label' => '',
		'button_link' => '',
		'left_image' => '0',
		'top_bottom_padding' => '',
		'eclass' => ''
	), $atts));

	$left_image = wp_get_attachment_image( $atts['left_image'], 'textbox-image', false, array( 'class' => 'floating-image' ) );
	$link = vc_build_link( $atts['button_link'] );
	
	$box_classes = array();
	
	if( $atts['top_bottom_padding'] == '1' ){
		$box_classes[] = 'highlighted-section';
	} else {
		$box_classes[] = 'subpage-hero';
	}
	
	ob_start(); ?>
	
	<section class="<?php echo implode( ' ', $box_classes ); ?> <?php echo $atts['eclass']; ?>">
		<div class="container">
			<div class="bl-breadcrumb-placeholder"></div>
			<div class="floating-boxes-content">
				<div class="whtie-floating-textbox w-clearfix">
					<h1 class="whitebox-heading"><?php echo $atts['title']; ?><br></h1>
					<div class="paragraph"><?php echo wpautop( $content ); ?></div>
					<a href="<?php echo $link['url']; ?>" class="underline-button w-inline-block" title="<?php echo $link['title']; ?>" <?php if( !empty( $link['target']) ){ echo 'target="'.$link['target'] .'"'; } ?> <?php if( !empty( $link['rel']) ){ echo 'rel="'.$link['rel'] .'"'; } ?>">
						<div class="underline-button-text"><?php echo $atts['button_label']; ?> <span class="underline-button-icon">↓</span></div>
						<div class="link-underline"></div>
					</a>
				</div>
				<?php echo $left_image; ?>
			</div>
		</div>
    </section>
	
    <?php 
    return ob_get_clean();
}