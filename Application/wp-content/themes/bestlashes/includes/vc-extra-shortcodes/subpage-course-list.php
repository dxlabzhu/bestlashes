<?php
//prob
add_action( 'vc_before_init', 'bl_vc_subpage_course_list_box');
function bl_vc_subpage_course_list_box() {
	$course_dropdown_list = array();
	$course_dropdown_list[ __('All', 'bl') ] = 0;

	$training_courses_categories = get_terms( 
		array(
			'taxonomy' => 'product_cat',
			'hide_empty' => false,
			'child_of' => BL_PRODUCT_CAT_ID_TRAINING_COURSES
		)
	);

	if( !empty( $training_courses_categories ) ){
		foreach ( $training_courses_categories as $term ) {
			$course_dropdown_list[ $term->name ] = $term->term_id;
		}
	}

	vc_map (
		array (
			'name' => __( 'Subpage course list', 'bl' ),
			'base' => 'subpage_course_list',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Title', 'bl' ),
					'param_name' => 'title',
				),
				array(
					'type'        => 'dropdown',
					'heading'     => __('Course category', 'bl'),
					'param_name'  => 'category',
					'admin_label' => true,
					'value'       => $course_dropdown_list,
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('subpage_course_list', 'subpage_subpage_course_list');
function subpage_subpage_course_list($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'category' => 0,
		'eclass' => ''
	), $atts));

	$current_timestamp = current_time( 'timestamp' );
	$filter = array();
	$training_courses = array();

	if( !empty( $category ) ){
		$filter['tax_query'][] = array(
			'taxonomy' => 'product_cat',
			'field' => 'id',
			'terms' => $category
		);
	}

	$training_courses_ids = bl_get_training_courses_ids( $filter );

	// Remove past courses
	if( !empty( $training_courses_ids ) ){
		$filtered_training_courses_ids = array();

		foreach ( $training_courses_ids as $post_id ) {
			$training_course_timestamp = get_post_meta( $post_id, 'training_course_date_time', true );

			if( $current_timestamp <= (float)$training_course_timestamp ){
				$filtered_training_courses_ids[] = $post_id;
			}
		}

		$training_courses_ids = $filtered_training_courses_ids;
	}

	if( !empty( $training_courses_ids ) ){
		foreach ( $training_courses_ids as $post_id ) {
			$training_course_timestamp = get_post_meta( $post_id, 'training_course_date_time', true );

			if( !empty( $training_course_timestamp ) ){
				if( $current_timestamp <= (float)$training_course_timestamp ){
					$training_course_formatted_date_time = date_i18n( 'Y.m.d. H:i', $training_course_timestamp );
					$training_course_key = date_i18n( 'Y-m', $training_course_timestamp );
					$city = get_post_meta( $post_id, 'training_course_city', true );
					$_product = wc_get_product( $post_id );

					$training_courses[ $training_course_key ][] = array(
						'ID' => $post_id,
						'title' => get_the_title( $post_id ),
						'date-day' => date_i18n( 'd', $training_course_timestamp ),
						'date-weekday' => date_i18n( 'l', $training_course_timestamp ),
						'date-time' => date_i18n( 'H:i', $training_course_timestamp ),
						'city' => $city,
						'address' => get_post_meta( $post_id, 'training_course_address', true ),
						'prices' => bl_get_product_prices( $post_id ),
						'is_on_sale' => $_product->is_on_sale(),
						'stock_quantity' => $_product->get_stock_quantity(),
						'content' => get_the_content_by_id( $post_id ),
						'timestamp' => $training_course_timestamp
					);
				}
			}
		}
	} 

	// Order courses
	if( !empty( $training_courses ) ){
		ksort( $training_courses );

		foreach ( $training_courses as $key => $courses ) {
			$ordered_courses = $courses;
			usort( $ordered_courses, 'bl_order_training_courses' );
			$training_courses[ $key ] = $ordered_courses;
		}
		
	}
	
	ob_start(); ?>

	<div class="table-wrapper training-courses embed-list <?php echo $atts['eclass']; ?>">
		<div class="table-content">
			<?php if( !empty( $training_courses ) ){
				foreach ( $training_courses as $date => $courses ) {
					$month_start = $date . '-01 00:00';
					$month_start_date = DateTime::createFromFormat( 'Y-m-d H:i', $month_start );
					$month_start_timestamp = $month_start_date->format('U');
					$month_name = date_i18n( 'F', $month_start_timestamp );
					$year = date_i18n( 'Y', $month_start_timestamp ); ?>

					<div class="table-header big">
						<div class="table-column _100percent">
							<div class="heading-03 nopadding">
								<?php echo $month_name; ?> 
								<span class="heading-span">| <?php echo $year; ?></span>
							</div>
						</div>
					</div>

					<?php if( !empty( $courses ) ){
						foreach ( $courses as $course ) { ?>
							
							<div class="table-row big">
								<div class="table-column training25">
									<div class="table-column _40percent training">
										<div class="circle-counter">
											<?php echo $course['date-day']; ?><br>
											<span class="circle-counter-span"><?php echo $course['date-weekday']; ?></span>
										</div>
									</div>
									<div class="table-column _60percent">
										<div>
											<?php echo $course['city'] . ' / ' . $course['date-time']; ?><br>
											<span class="table-small-text"><?php echo $course['address']; ?></span>
										</div>
									</div>
								</div>
								<div class="table-column training30">
									<div class="table-column _100percent center">
										<div class="table-link"><?php echo $course['title']; ?></div>
									</div>
								</div>
								<div class="table-column training45">
									<div class="table-column _30percent center">
										<div class="table-price">
											<?php if( $course['is_on_sale'] ){ ?>
												<div class="old-price"><?php echo $course['prices']['gross_regular_price_formatted']; ?></div>
												<div class="price"><?php echo $course['prices']['gross_price_formatted']; ?></div>
												<div class="net-price"><?php echo $course['prices']['net_price_formatted']; ?></div>
												<div class="booking"><?php _e('Booking only', 'bl'); ?></div>
											<?php } else { ?>
												<div class="price"><?php echo $course['prices']['gross_price_formatted']; ?></div>
													<div class="net-price"><?php echo $course['prices']['net_price_formatted']; ?></div>
													<div class="booking"><?php _e('Booking only', 'bl'); ?></div>
											<?php } ?>
										</div>
									</div>
									<div class="table-column _30percent center">
										<?php if( $course['stock_quantity'] == 0 ){ ?>
											<div class="courses-status"><?php _e( 'Full', 'bl' ); ?></div>
										<?php } else { ?>
											<div class="courses-status active"><?php echo sprintf( __( '%1$d place', 'bl' ), $course['stock_quantity'] ); ?></div>
										<?php } ?>
									</div>
									<div class="table-column _70percent mobilecenter">
										<a href="#welcome" class="information-button w-button" data-ix="training-dropdown">𝔦</a>
										<?php if( $course['stock_quantity'] != 0 ){ ?>
											<form action="" method="post" accept-charset="utf-8" class="add-to-cart-course">
												<a href="#" class="rounded-button add-to-cart-button w-button">
													<?php _e( 'Apply for the course', 'bl' ); ?>
													<span class="chart-button-icon">→</span>
													<div class="loading-button"></div>
												</a>
												<input type="hidden" name="add-to-cart" value="<?php echo $course['ID']; ?>">
											</form>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="training-information">
								<div class="training-information-text">
									<?php echo $course['content']; ?>
								</div>
								<a href="#" class="traning-information-close w-button">x</a>
							</div>

						<?php }
					} ?>

				<?php }
			} else { ?>

			<?php } ?>
			
		</div>
	</div>
	
    <?php 
    return ob_get_clean();
}