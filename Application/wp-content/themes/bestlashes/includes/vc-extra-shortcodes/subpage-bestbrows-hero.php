<?php

add_action( 'vc_before_init', 'bl_vc_subpage_bestbrows_hero_box');
function bl_vc_subpage_bestbrows_hero_box() {
	vc_map ( 
		array (
			'name' => __( 'Subpage - BestBrows - Hero', 'bl' ),
			'base' => 'subpage_bestbrows_hero',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Title', 'bl' ),
					'param_name' => 'title',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Button label', 'bl' ),
					'param_name' => 'button_label',
				),
				array (
					'type' => 'vc_link',
					'heading' => __ ( 'Link', 'bl' ),
					'param_name' => 'button_link',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('subpage_bestbrows_hero', 'subpage_bestbrows_hero_shortcode');
function subpage_bestbrows_hero_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'button_label' => '',
		'button_link' => '',
		'eclass' => ''
	), $atts));

	$link = vc_build_link( $atts['button_link'] );
	ob_start(); ?>

	<section class="subpage-hero bestbrows <?php echo $atts['eclass']; ?>">
		<div class="container">
			<div class="floating-boxes-content bestbrows">
				<h1 class="heading-01 bestbrows"><?php echo $atts['title']; ?></h1>
			</div>
		</div>
		<div class="bestbrows-hero-packshot">
			<a href="<?php echo $link['url']; ?>" class="bestbrows-hero-readmore" title="<?php echo $link['title']; ?>" <?php if( !empty( $link['target']) ){ echo 'target="'.$link['target'] .'"'; } ?> <?php if( !empty( $link['rel']) ){ echo 'rel="'.$link['rel'] .'"'; } ?>">
				<?php echo $atts['button_label']; ?>
			</a>
            <?php
            $lamination_template = array('szemoldok-laminalas', 'brow-lamination');
            $page_slug = get_post_field( 'post_name' );
            ?>
            <?php if (in_array($page_slug, $lamination_template)) : ?>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-products.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-products-p-500.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-products-p-800.png 800w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-products-p-1080.png 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-products-p-1600.png 1600w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-products-p-2000.png 2000w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-products.png 2200w" sizes="100vw" alt="" class="bestbrows-hero-packages">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-stuff.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-stuff-p-500.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-stuff-p-800.png 800w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-stuff-p-1080.png 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-stuff-p-1600.png 1600w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-stuff-p-2000.png 2000w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-stuff.png 2200w" sizes="100vw" alt="" class="bestbrows-hero-stuff">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-background.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-background-p-500.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-background-p-800.png 800w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-background-p-1080.png 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-background-p-1600.png 1600w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-background-p-2000.png 2000w, <?php echo get_stylesheet_directory_uri() ?>/images/bl-hero-background.png 2200w" sizes="100vw" alt="" class="bestbrows-hero-background">
            <?php else : ?>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-500.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-800.png 800w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-1080.png 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-1600.png 1600w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products-p-2000.png 2000w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-products.png 2200w" sizes="100vw" alt="" class="bestbrows-hero-packages">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-500.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-800.png 800w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-1080.png 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-1600.png 1600w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff-p-2000.png 2000w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-stuff.png 2200w" sizes="100vw" alt="" class="bestbrows-hero-stuff">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background.png" srcset="<?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-500.png 500w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-800.png 800w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-1080.png 1080w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-1600.png 1600w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background-p-2000.png 2000w, <?php echo get_stylesheet_directory_uri() ?>/images/bb-hero-background.png 2200w" sizes="100vw" alt="" class="bestbrows-hero-background">

            <?php endif; ?>
        </div>
	</section>
	
    <?php 
    return ob_get_clean();
}