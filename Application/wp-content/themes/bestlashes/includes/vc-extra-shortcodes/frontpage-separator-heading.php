<?php

add_action( 'vc_before_init', 'bl_vc_frontpage_separator_heading_box');
function bl_vc_frontpage_separator_heading_box() {
	vc_map ( 
		array (
			'name' => __( 'Frontpage Separator heading', 'bl' ),
			'base' => 'frontpage_separator_heading',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Title', 'bl' ),
					'param_name' => 'title',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('frontpage_separator_heading', 'frontpage_separator_heading_shortcode');
function frontpage_separator_heading_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'eclass' => ''
	), $atts));
	ob_start(); ?>
	
	<div class="bl-breadcrumb-placeholder"></div>
	<section class="home-separator-section <?php echo $atts['eclass']; ?>">
		<div class="container">
			<div class="separator-heading">
				<h3 class="heading-03"><?php echo $atts['title']; ?></h3>
			</div>
		</div>
	</section>
	
    <?php 
    return ob_get_clean();
}