<?php

add_action( 'vc_before_init', 'bl_vc_simple_button_box');
function bl_vc_simple_button_box() {
	vc_map ( 
		array (
			'name' => __( 'Simple button', 'bl' ),
			'base' => 'simple_button',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Button label', 'bl' ),
					'param_name' => 'button_label',
					'admin_label' => true,
				),
				array (
					'type' => 'vc_link',
					'heading' => __ ( 'Link', 'bl' ),
					'param_name' => 'button_link',
					'admin_label' => true,
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('simple_button', 'simple_button_shortcode');
function simple_button_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'button_label' => '',
		'button_link' => '',
		'eclass' => ''
	), $atts));

	$link = vc_build_link( $atts['button_link'] );
	ob_start(); ?>
	
	<div class="simple-button-container">
		<a href="<?php echo $link['url']; ?>" class="rounded-button w-button <?php echo $atts['eclass']; ?>" title="<?php echo $link['title']; ?>" <?php if( !empty( $link['target']) ){ echo 'target="'.$link['target'] .'"'; } ?> <?php if( !empty( $link['rel']) ){ echo 'rel="'.$link['rel'] .'"'; } ?>"><?php echo $atts['button_label']; ?></a>
	</div>

    <?php 
    return ob_get_clean();
}