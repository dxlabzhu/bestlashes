<?php

add_action( 'vc_before_init', 'bl_vc_subpage_bestbrows_underline_button');
function bl_vc_subpage_bestbrows_underline_button() {
	vc_map ( 
		array (
			'name' => __( 'Subpage - BestBrows - Underline_button', 'bl' ),
			'base' => 'subpage_bestbrows_underline_button',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Button label', 'bl' ),
					'param_name' => 'button_label',
				),
				array (
					'type' => 'vc_link',
					'heading' => __ ( 'Link', 'bl' ),
					'param_name' => 'button_link',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('subpage_bestbrows_underline_button', 'subpage_bestbrows_underline_button_shortcode');
function subpage_bestbrows_underline_button_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'button_label' => '',
		'button_link' => '',
		'eclass' => ''
	), $atts));

	$link = vc_build_link( $atts['button_link'] );
	
	ob_start(); ?>

	<a href="<?php echo $link['url']; ?>" class="underline-button bestbrows w-inline-block <?php echo $atts['eclass']; ?>" title="<?php echo $link['title']; ?>" <?php if( !empty( $link['target']) ){ echo 'target="'.$link['target'] .'"'; } ?> <?php if( !empty( $link['rel']) ){ echo 'rel="'.$link['rel'] .'"'; } ?>">
		<div class="underline-button-text"><?php echo $atts['button_label']; ?></div>
		<div class="link-underline bestbrows"></div>
	</a>
	
    <?php 
    return ob_get_clean();
}