<?php

add_action( 'vc_before_init', 'bl_vc_subpage_background_image_half_text_section_box');
function bl_vc_subpage_background_image_half_text_section_box() {
	vc_map ( 
		array (
			'name' => __( 'Subpage Background Image Half Text', 'bl' ),
			'base' => 'subpage_background_image_half_text_section',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Title', 'bl' ),
					'param_name' => 'title',
				),
				array (
					'type' => 'textarea_html',
					'heading' => __( 'Content', 'bl' ),
					'param_name' => 'content',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Button label', 'bl' ),
					'param_name' => 'button_label',
				),
				array (
					'type' => 'vc_link',
					'heading' => __ ( 'Link', 'bl' ),
					'param_name' => 'button_link',
				),
				array (
					'type' => 'attach_image',
					'heading' => __( 'Left image', 'bl' ),
					'param_name' => 'left_image',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('subpage_background_image_half_text_section', 'subpage_background_image_half_text_section_shortcode');
function subpage_background_image_half_text_section_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'button_label' => '',
		'button_link' => '',
		'left_image' => '0',
		'eclass' => ''
	), $atts));

	$left_image = wp_get_attachment_image( $atts['left_image'], 'full', false, array( 'class' => 'floating-image' ) );
	$link = vc_build_link( $atts['button_link'] );
	
	ob_start(); ?>

	<section class="highlighted-section <?php echo $atts['eclass']; ?>">
		<div class="container">
			<div class="bl-breadcrumb-placeholder"></div>
			<div class="highlighted-content">
				<div class="highligted-textbox w-clearfix">
					<h1 class="whitebox-heading"><?php echo $atts['title']; ?><br></h1>
					<div class="paragraph"><?php echo wpautop( $content ); ?></div>
					<a href="<?php echo $link['url']; ?>" class="underline-button w-inline-block" title="<?php echo $link['title']; ?>" <?php if( !empty( $link['target']) ){ echo 'target="'.$link['target'] .'"'; } ?> <?php if( !empty( $link['rel']) ){ echo 'rel="'.$link['rel'] .'"'; } ?>">
						<div class="underline-button-text"><?php echo $atts['button_label']; ?> <span class="underline-button-icon">→</span></div>
						<div class="link-underline"></div>
					</a>
				</div>

				<?php echo $left_image; ?>
			</div>
		</div>
	</section>
	
    <?php 
    return ob_get_clean();
}