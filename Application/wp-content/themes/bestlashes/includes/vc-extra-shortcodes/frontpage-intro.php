<?php

add_action( 'vc_before_init', 'bl_vc_frontpage_intro_box');
function bl_vc_frontpage_intro_box() {
	vc_map ( 
		array (
			'name' => __( 'Frontpage Intro', 'bl' ),
			'base' => 'frontpage_intro',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textfield',
					'heading' => __( 'Title', 'bl' ),
					'param_name' => 'title',
				),
				array (
					'type' => 'textarea_html',
					'heading' => __( 'Content', 'bl' ),
					'param_name' => 'content',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Button label', 'bl' ),
					'param_name' => 'button_label',
				),
				array (
					'type' => 'vc_link',
					'heading' => __ ( 'Link', 'bl' ),
					'param_name' => 'button_link',
				),
				array (
					'type' => 'attach_image',
					'heading' => __( 'Left image', 'bl' ),
					'param_name' => 'left_image',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('frontpage_intro', 'frontpage_intro_shortcode');
function frontpage_intro_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'button_label' => '',
		'button_link' => '',
		'left_image' => '0',
		'eclass' => ''
	), $atts));

	$left_image_url = wp_get_attachment_image_src( $atts['left_image'], 'full' );
	$link = vc_build_link( $atts['button_link'] );
	ob_start(); ?>
	
	<section class="home-intro <?php echo $atts['eclass']; ?>">
		<div class="container">
			<div class="bl-breadcrumb-placeholder"></div>
			<div class="content">
				<div class="w-row">
					<div class="col w-col w-col-6 w-col-stack"></div>
					<div class="col w-clearfix w-col w-col-6 w-col-stack">
						<h2 class="heading-02"><?php echo $atts['title']; ?><br></h2>
						<div class="home-text"><?php echo wpautop( $content ); ?></div>
						<a href="<?php echo $link['url']; ?>" class="underline-button w-inline-block" title="<?php echo $link['title']; ?>" <?php if( !empty( $link['target']) ){ echo 'target="'.$link['target'] .'"'; } ?> <?php if( !empty( $link['rel']) ){ echo 'rel="'.$link['rel'] .'"'; } ?>">
							<div class="underline-button-text"><?php echo $atts['button_label']; ?> <span class="underline-button-icon">→</span></div>
							<div class="link-underline"></div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="home-intro-parallax-image-container">
			<div class="home-intro-parallax-image" style="background-image: url(<?php echo $left_image_url[0]; ?>);"></div>
		</div>
	</section>
	
    <?php 
    return ob_get_clean();
}