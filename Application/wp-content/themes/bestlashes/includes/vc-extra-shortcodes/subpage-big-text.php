<?php

add_action( 'vc_before_init', 'bl_vc_subpage_big_text');
function bl_vc_subpage_big_text() {
	vc_map ( 
		array (
			'name' => __( 'Subpage Big text', 'bl' ),
			'base' => 'subpage_big_text',
			'icon' => 'of-icon-for-vc',
			'category' => __ ( 'Bestlashes', 'bl' ),
			'params' => array (
				array (
					'type' => 'textarea_html',
					'heading' => __( 'Content', 'bl' ),
					'param_name' => 'content',
				),
				array (
					'type' => 'textfield',
					'heading' => __( 'Extra class name', 'bl' ),
					'param_name' => 'eclass',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bl' )
				),
			)
		)
	);
}


add_shortcode('subpage_big_text', 'subpage_big_text_shortcode');
function subpage_big_text_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'eclass' => ''
	), $atts));
	
	ob_start(); ?>

	<div class="paragraph-big <?php echo $atts['eclass']; ?>">
		<?php echo do_shortcode( wpautop( $content ) ); ?>
	</div>
	
    <?php 
    return ob_get_clean();
}