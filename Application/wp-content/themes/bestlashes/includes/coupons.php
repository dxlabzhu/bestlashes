<?php

// Generate coupon
function bl_generate_coupon( $args = array() ){
	if( !empty( $args ) ){
		$default_args = array(
			'prefix' => 'BL',
			'code' => '',
			'content' => '',
			'discount_type' => 'percent',
			'coupon_amount' => 0,
			'individual_use' => 'yes',
			'product_ids' => '',
			'exclude_product_ids' => '',
			'usage_limit' => '',
			'usage_limit_per_user' => '',
			'expiry_date' => '',
			'free_shipping' => 'no',
			'customer_email' => '',
			'exclude_sale_items' => 'no',
			'minimum_amount' => '',
			'maximum_amount' => '',
			'exclude_product_categories' => ''
		);

		$args = wp_parse_args( $args, $default_args );

		if( !empty( $args['coupon_amount'] ) ){
			// Coupon code
			$coupon_code = '';

			if( empty( $args['code'] ) ){
				$args['code'] = wp_generate_password( 10, false, false );
			}

			if( !empty( $args['prefix'] ) ){
				$coupon_code = $args['prefix'] . '-';
			}

			$coupon_code .= $args['code'];

			if( !empty( $coupon_code ) ){
				// Generate coupon
				$coupon = array(
					'post_title' => $coupon_code,
					'post_content' => ( isset( $args['content'] ) ? $args['content'] : '' ),
					'post_status' => 'publish',
					'post_author' => 1,
					'post_type'		=> 'shop_coupon'
				);

				$new_coupon_id = wp_insert_post( $coupon );

				// Add meta fields
				update_post_meta( $new_coupon_id, 'discount_type', $args['discount_type'] );
				update_post_meta( $new_coupon_id, 'coupon_amount', $args['coupon_amount'] );
				update_post_meta( $new_coupon_id, 'individual_use', $args['individual_use'] );
				update_post_meta( $new_coupon_id, 'product_ids', $args['product_ids'] );
				update_post_meta( $new_coupon_id, 'exclude_product_ids', $args['exclude_product_ids'] );
				update_post_meta( $new_coupon_id, 'usage_limit', $args['usage_limit'] );
				update_post_meta( $new_coupon_id, 'usage_limit_per_user', $args['usage_limit_per_user'] );
				update_post_meta( $new_coupon_id, 'expiry_date', $args['expiry_date'] );
				update_post_meta( $new_coupon_id, 'free_shipping', $args['free_shipping'] );
				update_post_meta( $new_coupon_id, 'customer_email', $args['customer_email'] );
				update_post_meta( $new_coupon_id, 'exclude_sale_items', $args['exclude_sale_items'] );
				update_post_meta( $new_coupon_id, 'minimum_amount', $args['minimum_amount'] );
				update_post_meta( $new_coupon_id, 'maximum_amount', $args['maximum_amount'] );
				update_post_meta( $new_coupon_id, 'exclude_product_categories', $args['exclude_product_categories'] );

				return array( 'coupon-id' => $new_coupon_id, 'coupon-code' => $coupon_code );
			}
		} else {
			return false;
		}
	}
}