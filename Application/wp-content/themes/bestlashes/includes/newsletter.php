<?php

/* Newsletter subscription - ajax from footer */
add_action( 'wp_ajax_bl_newsletter_subscription', 'bl_newsletter_subscription' );
add_action( 'wp_ajax_nopriv_bl_newsletter_subscription', 'bl_newsletter_subscription' );
function bl_newsletter_subscription(){
	$return = array();

	if ( isset($_POST['form'] ) ) {
		$postData = array();
		parse_str( $_POST['form'], $postData );
		
		$name = $postData['newsletter-name'];
		$email = $postData['newsletter-email'];
		$list_id = $postData['newsletter-topic'];
		$ip = get_current_ip_address();
		$current_timestamp = current_time( 'timestamp' );

		// Hashes
		$hash = md5( $email . $name . BL_NEWSLETTER_SUBSCRIPTION_FIX_HASH );
		$unsubscribe_hash = md5( $email . $name . BL_NEWSLETTER_SUBSCRIPTION_FIX_UNSUBSCRIBE_HASH );

		$subsription_row = array(
			'email' => $email,
			'name' => $name,
			'hash' => $hash,
			'list_id' => $list_id,
			'subscribe_time' => $current_timestamp,
			'subscribe_ip' => $ip,
			'unsubscribe_hash' => $unsubscribe_hash
	 	);

	 	$insert_answer = insert_newsletter_subscription( $subsription_row );

	 	if( $insert_answer['error'] == true ){
	 		$return = array(
	 			'success' => false,
	 			'error' => $insert_answer['description']
	 		);
	 	} else {
	 		$link = home_url() . '?newsletter-subscription=1&id=' . $insert_answer['id'] . '&hash=' . $hash;

	 		// Email to user
			if( class_exists( 'Auretto_Email_Editor' ) ){
				$AWEmail = new Auretto_Email_Editor();
				$extra_datas = array(
					'receiver_email' => $email,
					'replaceable_content_words' => array(
						'%name%' => $name,
						'%link%' => $link
					)
				);
				$AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_NEWSLETTER_SUBSCRIPTION_EMAIL_VERIFICATION, $extra_datas );
			}

	 		$return = array(
	 			'success' => true,
	 			'text' => __( 'Successfully subscribed for our newsletter. Please check your inbox!', 'bl' )
	 		);
	 	}
	}


	wp_send_json( $return );
}

function subscribe_emailmarketer($row) {
	$xml = '<xmlrequest>
		<username>' . MARKETER_USERNAME . '</username>
		<usertoken>' . MARKETER_API_KEY . '</usertoken>
		<requesttype>subscribers</requesttype>
		<requestmethod>AddSubscriberToList</requestmethod>
		<details>
			<emailaddress>' . $row['email'] . '</emailaddress>
			<mailinglist>' . $row['list_id'] . '</mailinglist>
			<format>html</format>
			<confirmed>yes</confirmed>
		</details>
	</xmlrequest>';

	$ch = curl_init('http://emailmarketer.hu/xml.php');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	curl_setopt($ch, CURLOPT_TIMEOUT, 3);
	@curl_exec($ch);
	curl_close($ch);
} 

function insert_newsletter_subscription( $row = array() ){
	if( !empty( $row ) ){
		global $wpdb;
		$table = $wpdb->prefix . BL_NEWSLETTER_SUBSCRIPTION_TABLE;
		
		// Check already exists
		$check = $wpdb->get_results( 'SELECT * FROM '. $table .' WHERE email = "'. $row['email'] .'"' );
		
		if( empty( $check ) ){
			$rows = $wpdb->insert(
				$table,
				$row
			);

			$inserted_id = $wpdb->insert_id;

			if( !empty( $inserted_id ) ){
				return array( 'error' => false, 'id' => $inserted_id );
			} else {
				return array( 'error' => true, 'description' => __( "We can't subscribe now. Please try again later", 'bl' ) );
			}
		} else {
			return array( 'error' => true, 'description' => __( "Already subscribed e-mail address", 'bl' ) );
		}
	}

	return array( 'error' => true, 'description' => __( "No data provided", 'bl' ) );;
}


add_action('init', 'bl_verify_newsletter_subscription');
function bl_verify_newsletter_subscription(){
	if( isset( $_GET['newsletter-subscription'] ) && $_GET['newsletter-subscription'] == '1' ){
		global $wpdb;
		
		$table = $wpdb->prefix . BL_NEWSLETTER_SUBSCRIPTION_TABLE;
		$id = $_GET['id'];
		$hash = $_GET['hash'];

        $row = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table WHERE id = %d", array( $id ) ) );
        if( !empty( $row ) ){
			if( $row->hash == $hash ){
				if( $row->accept_success != '1' ){
					$where = array( 'id' => (int)$row->id );
					$current_timestamp = current_time( 'timestamp' );
					$ip = get_current_ip_address();

					$update_query = array(
						'accept_time' => $current_timestamp,
						'accept_ip' => $ip,
						'accept_success' => 1,
					);

					// Update row
					$wpdb->update(
						$table,
						$update_query,
						$where
					);

					// Send to EmailMarketer - only HU
					if( BL_LANG == 'HU' ){
						$list_ids = array();

						if( empty( $row->list_id ) ){
							$list_ids = array( MARKETER_LIST_BESTLASHES_ID );
						} else {
							$list_ids = explode( ',', $row->list_id );
						}

						if( !empty( $list_ids ) ){
							foreach ( $list_ids as $list_id ) {
								subscribe_emailmarketer( array( 'email' => $row->email, 'list_id' => $list_id ) );
							}
						}
					}

					add_action( 'wp_footer', 'bl_open_newsletter_subscription_popup_success_verify_hash', 1000 );

					// Newsletter coupon
					if( get_option( 'bl-enable-newsletter-coupon' ) == '1' ){

						// Check coupons
		        		$args = array();
		        		$args['post_type'] = 'shop_coupon';
		        		$args['posts_per_page'] = '-1';
		        		$args['post_status'] = 'publish';
		        		$args['meta_query'] = array();
						$args['meta_query'][] = array(
							'key' => '_newsletter_coupon_owner_email',
							'value' => $row->email,
						);
		        		$args['fields'] = 'ids';
		        		
		        		$email_newsletter_coupon_loop = new WP_Query( $args );
		        		
		        		if( !$email_newsletter_coupon_loop->have_posts() ){
							$user_email = $row->email;
							$user_name = $row->name;
							$current_timestamp = current_time( 'timestamp' );
							$expiry_date = date( 'Y-m-d', strtotime( '+'. BL_COUPON_BITHDAY_EXPIRY_DAYS .' days', $current_timestamp ) );

							// Generate coupon
							$args = array(
								'prefix' => BL_COUPON_PREFIX_NEWSLETTER_SUBSCRIPTION,
								'code' => wp_generate_password( 10, false, false ),
								'content' => 'Newsletter coupon - ' . $user_email,
								'discount_type' => 'percent',
								'coupon_amount' => BL_COUPON_NEWSLETTER_AMOUNT,
								'individual_use' => 'yes',
								'usage_limit' => 1,
								'usage_limit_per_user' => 1,
								'free_shipping' => 'no',
								'customer_email' => array( $user_email ),
								'exclude_sale_items' => 'yes',
								'exclude_product_categories' => array( BL_PRODUCT_CAT_ID_TRAINING_COURSES ),
								'expiry_date' => $expiry_date
							);

							$coupon = bl_generate_coupon( $args );

							if( $coupon ){
								// Email to user
								if( class_exists( 'Auretto_Email_Editor' ) ){
									$AWEmail = new Auretto_Email_Editor();
									$extra_datas = array(
										'receiver_email' => $user_email,
										'replaceable_content_words' => array(
											'%name%' => $user_name,
											'%coupon-code%' => $coupon['coupon-code']
										)
									);

									$response = $AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_NEWSLETTER_SUBSCRIPTION_WITH_COUPON, $extra_datas );
								}
							}

							update_post_meta( $coupon['coupon-id'], '_newsletter_coupon_owner_email', $user_email );
						}
					}
				}
			} else {
				add_action( 'wp_footer', 'bl_open_newsletter_subscription_popup_failed_verify_hash', 1000 );
			}
		} else {
			add_action( 'wp_footer', 'bl_open_newsletter_subscription_popup_failed_verify_hash', 1000 );
		}
	}
}


add_action('init', 'bl_verify_newsletter_unsubscription');
function bl_verify_newsletter_unsubscription(){
	if( isset( $_GET['newsletter-unsubscription'] ) && $_GET['newsletter-unsubscription'] == '1' ){
		global $wpdb;
		
		$table = $wpdb->prefix . BL_NEWSLETTER_SUBSCRIPTION_TABLE;
		$id = $_GET['id'];
		$hash = $_GET['hash'];

        $row = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table WHERE id = %d", array( $id ) ) );
        
        if( !empty( $row ) ){
			if( $row->unsubscribe_hash == $hash ){
				if( $row->unsubscribe_success != '1' ){
					$where = array( 'id' => (int)$row->id );
					$current_timestamp = current_time( 'timestamp' );
					$ip = get_current_ip_address();

					$update_query = array(
						'unsubscribe_time' => $current_timestamp,
						'unsubscribe_ip' => $ip,
						'unsubscribe_success' => 1,
					);

					// Update row
					$wpdb->update(
						$table,
						$update_query,
						$where
					);

					add_action( 'wp_footer', 'bl_open_newsletter_unsubscription_popup_success_verify_hash', 1000 );
				}
			} else {
				add_action( 'wp_footer', 'bl_open_newsletter_unsubscription_popup_failed_verify_hash', 1000 );
			}
		} else {
			add_action( 'wp_footer', 'bl_open_newsletter_unsubscription_popup_failed_verify_hash', 1000 );
		}
	}
}


add_action( 'admin_init', 'bl_newsletter_export_to_csv' );
function bl_newsletter_export_to_csv(){
	if( isset( $_GET['export-newsletter-subscriptions'] ) && $_GET['export-newsletter-subscriptions'] == '1' ){
		if( is_admin() && $_GET['page'] == 'bl_options_panel_settings' ){
			global $wpdb;
		
			$table = $wpdb->prefix . BL_NEWSLETTER_SUBSCRIPTION_TABLE;
			$date_time = current_time( 'Y.m.d. H.i' );
			$filename = 'Best Lashes newsletter subsctiptions - ' . $date_time;
			$rows = array();
		
			header('Pragma: public');
			header('Expires: 0');
			header("Cache-Control: cache, must-revalidate" ) ;
			header('Content-Description: File Transfer');
			header('Content-Type: text/csv; charset=UTF-8');
			header('Content-Disposition: attachement; filename="'. $filename .'.csv"');
			header('Content-Transfer-Encoding: binary');

			$subscriptions = $wpdb->get_results( 'SELECT id, name, email, unsubscribe_hash from '. $table .' WHERE accept_success = 1 AND unsubscribe_success = 0', ARRAY_A );

			if( !empty( $subscriptions ) ){
				$i = 0;

				foreach ( $subscriptions as $subscriber ) {
					$rows[$i][0] = $subscriber['name'];
					$rows[$i][1] = $subscriber['email'];
					$rows[$i][2] = add_query_arg(
						array(
							'newsletter-unsubscription' => '1',
							'id' => $subscriber['id'],
							'hash' => $subscriber['unsubscribe_hash']
						),
						home_url()
					);

					$i++;
				}
			}
			
			$output = fopen("php://output", "w");
			if ($output) {
				fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
				foreach ($rows as $line) {
					fputcsv($output, $line, ';');
				}
			}
			fclose($output);
			exit();
		}
	}
}


/* Lash inc form validation */
add_filter( 'wpcf7_validate_email*', 'bl_validation_for_newsletter_subscription', 20, 2 );
function bl_validation_for_newsletter_subscription( $result, $tag ){
	if ( 'lash-inc-contact-email' == $tag->name ) {
		$subscription_type = $_POST['lash-inc-contact-information'];

		if( $subscription_type == 'Olvasóként' ){
			global $wpdb;
			$table = $wpdb->prefix . BL_NEWSLETTER_SUBSCRIPTION_TABLE;
			
			// Check already exists
			$check = $wpdb->get_results( 'SELECT * FROM '. $table .' WHERE email = "'. $_POST['lash-inc-contact-email'] .'"' );
			
			if( !empty( $check ) ){
				$result->invalidate( $tag, __( 'Already subscribed e-mail address', 'bl' ) );
			}
		}
	}

	return $result;
}

/* Lash inc form submission */
add_action( 'wpcf7_mail_sent', 'bl_lash_inc_form_submission' ); 
function bl_lash_inc_form_submission( $contact_form ) {
    $title = $contact_form->title;
    $submission = WPCF7_Submission::get_instance();

    if ( $submission ) {
        $posted_data = $submission->get_posted_data();
		
		if( $posted_data['_wpcf7'] == BL_CF7_ID_LASH_INC ){

			if( $posted_data['lash-inc-contact-information'] == 'Olvasóként' ){
				$name = $posted_data['lash-inc-contact-name'];
				$email = $posted_data['lash-inc-contact-email'];
				$ip = get_current_ip_address();
				$current_timestamp = current_time( 'timestamp' );

				// Hashes
				$hash = md5( $email . $name . BL_NEWSLETTER_SUBSCRIPTION_FIX_HASH );
				$unsubscribe_hash = md5( $email . $name . BL_NEWSLETTER_SUBSCRIPTION_FIX_UNSUBSCRIBE_HASH );

				$subsription_row = array(
					'email' => $email,
					'name' => $name,
					'hash' => $hash,
					'subscribe_time' => $current_timestamp,
					'subscribe_ip' => $ip,
					'unsubscribe_hash' => $unsubscribe_hash
			 	);

			 	$insert_answer = insert_newsletter_subscription( $subsription_row );

			 	if( $insert_answer['error'] != true ){
			 		$link = home_url() . '?newsletter-subscription=1&id=' . $insert_answer['id'] . '&hash=' . $hash;

			 		// Email to user
					if( class_exists( 'Auretto_Email_Editor' ) ){
						$AWEmail = new Auretto_Email_Editor();
						$extra_datas = array(
							'receiver_email' => $email,
							'replaceable_content_words' => array(
								'%name%' => $name,
								'%link%' => $link
							)
						);
						$AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_NEWSLETTER_SUBSCRIPTION_EMAIL_VERIFICATION, $extra_datas );
					}
			 	}
			}
		}
    }
}

/* Lash inc form disable email */
add_action( 'wpcf7_skip_mail', 'bl_lash_inc_form_disable_email' );
function bl_lash_inc_form_disable_email( $skip_mail = false, $contact_form = array() ){
	$subscription_type = $_POST['lash-inc-contact-information'];

	if( $subscription_type == 'Olvasóként' ){
		$skip_mail = true;
	}

	return $skip_mail;
}