<?php

// Check unpaid orders with training courses
add_action( 'wp', 'bl_check_unpaid_orders_with_training_courses' );
function bl_check_unpaid_orders_with_training_courses(){
	if( !empty( $_GET['check-unpaid-orders'] ) && $_GET['check-unpaid-orders'] == BL_CRON_TRAINING_COURSES_CHECK_HASH ){
		$current_time = current_time('Y-m-d H:i');
		$current_date = new DateTime( $current_time );
		$current_timestamp = current_time( 'timestamp' );

		$args['posts_per_page'] = -1;
		$args['post_type'] = wc_get_order_types();
		$args['post_status'] = array_keys( wc_get_order_statuses() );
		$args['meta_query'] = array();
		$args['meta_query']['relation'] = 'OR';
		$args['meta_query'][] = array(
			'key' => '_training_course_cron_checked',
			'value' => '1',
			'compare' => '!='
		);
		$args['meta_query'][] = array(
			'key' => '_training_course_cron_checked',
			'compare' => 'NOT EXISTS'
		);
		$args['fields'] = 'ids';

		$orders_loop = new WP_Query( $args );

		if( $orders_loop->have_posts() ){
			while ( $orders_loop->have_posts() ) {
				$order_id = $orders_loop->next_post();
				$order = wc_get_order( $order_id );
				$only_training_course = true;
				$training_course_names = array();
				
				foreach ($order->get_items() as $item_id => $item_data) {
					$product_id = $item_data['product_id'];
					$is_training_course = get_post_meta( $product_id, 'training_course', true );

					$training_course_names[] = get_the_title( $product_id );

					if( $is_training_course != '1' ){
						$only_training_course = false;
					}
				}

				if( $only_training_course ){
					if( $order->get_status() == 'on-hold' ){
						//$marked_for_delete = get_post_meta( $order_id, '_training_course_cron_delete', true );

						//if( $marked_for_delete != '1' ){
							
						//}

						$date_created = $order->get_date_created();
						$date_create_hour = $date_created->date('H');
						$date_create_min = $date_created->date('i');
						$date_create_sec = $date_created->date('s');
						$pay_deadline_date = strtotime( $date_created->date('Y-m-d') . ' +'. BL_TRAINING_COURSES_MAX_DAY_WITHOUT_PAY .' weekdays');
						$pay_deadline = $pay_deadline_date + ( $date_create_hour * 60 * 60 ) + ( $date_create_min * 60 ) + $date_create_sec;

						if( $pay_deadline < $current_timestamp ){
							
							// Email to user
							if( class_exists( 'Auretto_Email_Editor' ) ){
								$AWEmail = new Auretto_Email_Editor();
								$extra_datas = array(
									'receiver_email' => $order->get_billing_email(),
									'replaceable_content_words' => array(
										'%name%' => $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
										'%course-name%' => implode( ', ', $training_course_names )
									)
								);

								$response = $AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_DELETE_ORDER_ONLY_TRAINING_COURSE_EMAIL_VERIFICATION, $extra_datas );
							}

							// Set product stock
							if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
								return;
							}

							foreach ( $order->get_items() as $item ) {
								if ( $item['product_id'] > 0 ) {
									$_product = $order->get_product_from_item( $item );

									if ( $_product && $_product->exists() && $_product->managing_stock() ) {
										$old_stock = $_product->stock;
										$qty = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $order, $item );
										$new_quantity = $_product->increase_stock( $qty );
										
										do_action( 'woocommerce_auto_stock_restored', $_product, $item );

										$order->add_order_note( sprintf( __( 'Item #%s stock incremented from %s to %s.', 'bl' ), $item['product_id'], $old_stock, $new_quantity ) );

										$order->send_stock_notifications( $_product, $new_quantity, $item['qty'] );

										// Log stock
										bl_product_stock_change( $_product, (int)$old_stock, (int)$new_quantity, 'training-course-order-cron-delete' );
									}
								}
							}

							// Move to trash
							wp_trash_post( $order_id );
						}
					}
					
				} else {
					update_post_meta( $order_id, '_training_course_cron_checked', '1' );
				}
			}
		}

		die();
	}
}


// Cron for delete
//add_action( 'wp', 'bl_delete_unpaid_orders_with_training_courses' );
function bl_delete_unpaid_orders_with_training_courses(){
	if( !empty( $_GET['delete-unpaid-orders'] ) && $_GET['delete-unpaid-orders'] == BL_CRON_TRAINING_COURSES_DELETE_HASH ){
		$current_timestamp = current_time( 'timestamp' );

		$args['posts_per_page'] = -1;
		$args['post_type'] = wc_get_order_types();
		$args['post_status'] = array_keys( wc_get_order_statuses() );
		$args['meta_query'] = array();
		$args['meta_query']['relation'] = 'AND';
		$args['meta_query'][] = array(
			'key' => '_training_course_cron_delete',
			'value' => '1',
			'compare' => '='
		);
		$args['meta_query'][] = array(
			'key' => '_training_course_cron_delete_time',
			'value' => $current_timestamp,
			'compare' => '<'
		);
		$args['fields'] = 'ids';

		$orders_loop = new WP_Query( $args );

		if( $orders_loop->have_posts() ){
			while ( $orders_loop->have_posts() ) {
				$order_id = $orders_loop->next_post();
				$order = new WC_Order( $order_id );
				
				// Set product stock
				if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
					return;
				}

				foreach ( $order->get_items() as $item ) {

					if ( $item['product_id'] > 0 ) {
						$_product = $order->get_product_from_item( $item );

						if ( $_product && $_product->exists() && $_product->managing_stock() ) {

							$old_stock = $_product->stock;

							$qty = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $order, $item );

							$new_quantity = $_product->increase_stock( $qty );

							do_action( 'woocommerce_auto_stock_restored', $_product, $item );

							$order->add_order_note( sprintf( __( 'Item #%s stock incremented from %s to %s.', 'bl' ), $item['product_id'], $old_stock, $new_quantity ) );

							$order->send_stock_notifications( $_product, $new_quantity, $item['qty'] );

							// Log stock
							bl_product_stock_change( $_product, (int)$old_stock, (int)$new_quantity, 'order-refunded-cancelled' );
						}
					}
				}

				wp_trash_post( $order_id );
			}
		}
		
		die();
	}
}


// Birthday email withe coupon
add_action( 'wp', 'bl_birthday_email_with_coupon' );
function bl_birthday_email_with_coupon(){
	if( !empty( $_GET['birthday-email-with-coupon'] ) && $_GET['birthday-email-with-coupon'] == BL_CRON_BIRTHDAY_EMAIL_WITH_COUPON_HASH ){

		// Check admin settings
		if( get_option( 'bl-enable-birthday-coupon' ) == '1' ){
			$current_timestamp = current_time( 'timestamp' );
			$current_month_day = date( 'm-d', $current_timestamp );

			// Basic query
			$args['role__in'] = array( 'Subscriber' );
			$args['meta_query']['relation'] = 'OR';
			$args['meta_query'][] = array(
				'key' => 'birthday_month_day',
				'value' => $current_month_day
			);

			$birthday_user_query = new WP_User_Query( $args );

			if( !empty( $birthday_user_query ) ){
				foreach ( $birthday_user_query->get_results() as $user ) {
					$user_email = $user->user_email;
					$user_name = get_user_meta( $user->ID, 'first_name', true ) . ' ' . get_user_meta( $user->ID, 'last_name', true );
					$fix_user_discount = get_user_meta( $user->ID, 'fix-user-discount', true );
					
					// Don't send coupon who has fix user discount 
					if( empty( $fix_user_discount ) ){
						$expiry_date = date( 'Y-m-d', strtotime( '+'. BL_COUPON_BITHDAY_EXPIRY_DAYS .' days', $current_timestamp ) );

						// Generate coupon
						$args = array(
							'prefix' => BL_COUPON_PREFIX_HAPPY_BIRTHDAY,
							'code' => wp_generate_password( 10, false, false ),
							'content' => 'Birthday coupon - ' . $user_email,
							'discount_type' => 'percent',
							'coupon_amount' => BL_COUPON_BITHDAY_AMOUNT,
							'individual_use' => 'yes',
							'usage_limit' => 1,
							'usage_limit_per_user' => 1,
							'free_shipping' => 'no',
							'customer_email' => array( $user_email ),
							'exclude_sale_items' => 'yes',
							'exclude_product_categories' => array( BL_PRODUCT_CAT_ID_TRAINING_COURSES ),
							'expiry_date' => $expiry_date
						);

						$coupon = bl_generate_coupon( $args );

						if( $coupon ){
							// Email to user
							if( class_exists( 'Auretto_Email_Editor' ) ){
								$AWEmail = new Auretto_Email_Editor();
								$extra_datas = array(
									'receiver_email' => $user_email,
									'replaceable_content_words' => array(
										'%name%' => $user_name,
										'%coupon-code%' => $coupon['coupon-code']
									)
								);

								$response = $AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_HAPPY_BIRTHDAY_WITH_COUPON, $extra_datas );
							}
						}
					}
				}
			}
		}

		die();
	}
}


// Scheduled sales
add_action( 'wp', 'bl_scheduled_sales_cron' );
function bl_scheduled_sales_cron(){
	if( !empty( $_GET['scheduled-sale-cron'] ) && $_GET['scheduled-sale-cron'] == BL_CRON_SCHEDULED_SALES_HASH ){
		wc_scheduled_sales();

		die();
	}
}

// Check _price in db
add_action( 'wp', 'bl_alert_system_administrators_about_prices_cron' );
function bl_alert_system_administrators_about_prices_cron(){
	if( !empty( $_GET['alert-system-administrator-about-prices'] ) && $_GET['alert-system-administrator-about-prices'] == BL_CRON_ALERT_SYSTEM_ADMINISTRATOR_ABOUT_PRICES_HASH ){
		global $wpdb;
		$fixed_price = array();

		$results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key LIKE '_price' AND meta_value < ". BL_PRODUCT_PRICE_LIMIT_FOR_CHECK ." AND meta_value != ''", ARRAY_A );

		if( !empty( $results ) ){
			$posts = array();
			$post_ids = array();

			foreach ( $results as $result ) {
				$post_ids[] = $result['post_id'];
				$posts[] = array(
					'post_id' => $result['post_id'],
					'meta_id' => $result['meta_id'],
					'price' => $result['meta_value']
				);
			}

			$post_ids_string = implode( ', ', $post_ids );

			// Fix price
			if( !empty( $posts ) ){
				foreach ( $posts as $post ) {
					$product_post = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}posts WHERE ID=" . $post['post_id'], ARRAY_A );

					if( !empty( $product_post ) ){
						// Variation
						if( $product_post['post_type'] === 'product_variation' ){
							$variation_regular_price = get_post_meta( $post['post_id'], '_regular_price', true );
							
							if( $variation_regular_price != $post['price'] ){
								update_post_meta( $post['post_id'], '_price', $variation_regular_price );

								$fixed_price[] = array(
									'post_id' => $post['post_id'],
									'old_price' => $post['price'],
									'new_price' => $variation_regular_price,
									'meta_id' => $post['meta_id'],
									'post_type' => 'variation'
								);
							}
						}

						// Product
						if( $product_post['post_type'] === 'product' ){
							$product = wc_get_product( $post['post_id'] );

							if( $product->is_type('variable') ){
								// Variable
								$product_prices = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key LIKE '_price' AND post_id = ". $post['post_id'], ARRAY_A );
								
								if( !empty( $product_prices ) ){
									if( sizeof( $product_prices ) > 1 ){
										$rows_to_delete = array();

										foreach ( $product_prices as $meta_row ) {
											if( $meta_row['meta_value'] < BL_PRODUCT_PRICE_LIMIT_FOR_CHECK ){
												$rows_to_delete[] = $meta_row['meta_id'];
											}
										}

										if( !empty( $rows_to_delete ) ){
											foreach ( $rows_to_delete as $row_id ) {
												$wpdb->delete(
													$wpdb->prefix. 'postmeta', 
													array( 'meta_id' => $row_id )
												);

												$fixed_price[] = array(
													'post_id' => $post['post_id'],
													'old_price' => $post['price'],
													'new_price' => 'deleted-price',
													'meta_id' => $post['meta_id'],
													'post_type' => 'variable'
												);
											}
										}
									}
								}
							} else if( $product->is_type('simple') ){
								// Simple
								$product_regular_price = get_post_meta( $post['post_id'], '_regular_price', true );
							
								if( $product_regular_price != $post['price'] ){
									update_post_meta( $post['post_id'], '_price', $product_regular_price );

									$fixed_price[] = array(
										'post_id' => $post['post_id'],
										'old_price' => $post['price'],
										'new_price' => $product_regular_price,
										'meta_id' => $post['meta_id'],
										'post_type' => 'simple'
									);
								}
							}
						}
					}
				}
			}

			// Mail to system administrators
            $to = array( 'gabor@dxlabz.hu', 'gefusz@gmail.com' );
	        $subject = 'BestLashes - '. BL_PRODUCT_PRICE_LIMIT_FOR_CHECK .' Ft-nál alacsonyabb ár van az adatbázisban';
	        $headers = array('Content-Type: text/html; charset=UTF-8');

	        $body .= 'Termék azonosítók: ' . $post_ids_string . '<br><br>';

	     	if( !empty( $fixed_price ) ){
	     		$body .= 'Javított árak (log): <br>';
	     		$body .= '<hr>';

	     		foreach ( $fixed_price as $fix ) {
	     			$body .= '<ul>
						<li><strong>Post id: </strong>'. $fix['post_id'] .'</li>
						<li><strong>Old price: </strong>'. $fix['old_price'] .'</li>
						<li><strong>New price: </strong>'. $fix['new_price'] .'</li>
						<li><strong>Meta id: </strong>'. $fix['meta_id'] .'</li>
						<li><strong>Post type: </strong>'. $fix['post_type'] .'</li>
					</ul>';
					$body .= '<hr>';
	     		}
	     	}
	        
	        wp_mail( $to, $subject, $body, $headers );
		}

		die();
	}
}