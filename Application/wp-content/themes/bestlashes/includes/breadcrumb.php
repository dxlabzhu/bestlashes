<?php


function bl_breadcrumb( $eclass = array() ) {
           
    // Settings
    $separator          = '<div class="breadcrumb-separator">→</div>';
    $home_title         = __( 'Home', 'bl' );
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post, $wp_query;

    $eclasses = '';
    if( !empty( $eclass ) ){
        $eclasses = implode( ' ', $eclass );
    }
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<div class="breadcrumb ' . $eclasses . '"><div class="breadcrumb-container">';
           
        // Home page
        echo '<a class="bread-link bread-home breadcrumb-link" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a>';
        echo $separator;
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo post_type_archive_title( '', false);
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
            
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                
            	if( $post_type == 'product' ){
                    
	                echo '<a class="bread-cat bread-custom-post-type-' . $post_type . ' breadcrumb-link" href="' . get_permalink( BL_PAGE_WEBSHOP ) . '" title="' . get_the_title( BL_PAGE_WEBSHOP ) . '">' . get_the_title( BL_PAGE_WEBSHOP ) . '</a>';
	                echo $separator;

            	} else {

	                $post_type_object = get_post_type_object( $post_type );
	                $post_type_archive = get_post_type_archive_link( $post_type );
	                echo '<a class="bread-cat bread-custom-post-type-' . $post_type . ' breadcrumb-link" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a>';
	                echo $separator;

	            }
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<div class="breadcrumb-link">' . $custom_tax_name . '</div>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if( $post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                if( $post_type == 'product' ) {
                    echo '<a class="bread-cat bread-custom-post-type-' . $post_type . ' breadcrumb-link" href="' . get_permalink( BL_PAGE_WEBSHOP ) . '" title="' . get_the_title( BL_PAGE_WEBSHOP ) . '">' . get_the_title( BL_PAGE_WEBSHOP ) . '</a>';
                    echo $separator;
                } else if( $post_type == 'video' ){
                    echo '<a class="bread-cat bread-custom-post-type-' . $post_type . ' breadcrumb-link" href="' . get_permalink( BL_PAGE_VIDEO_CATEGORIES ) . '" title="' . get_the_title( BL_PAGE_VIDEO_CATEGORIES ) . '">' . get_the_title( BL_PAGE_VIDEO_CATEGORIES ) . '</a>';
                    echo $separator;
                } else if( $post_type == 'trainer' ){
                     echo '<a class="bread-cat bread-custom-post-type-' . $post_type . ' breadcrumb-link" href="' . get_permalink( BL_PAGE_ABOUT_US ) . '" title="' . get_the_title( BL_PAGE_ABOUT_US ) . '">' . get_the_title( BL_PAGE_ABOUT_US ) . '</a>';
                    echo $separator;
                } else {
                    echo '<a class="bread-cat bread-custom-post-type-' . $post_type . ' breadcrumb-link" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a>';
                    echo $separator;
                }
            }
              
            // Get post category info
            $category = get_the_category();

            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim( get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= $parents;
                    $cat_display .= $separator;
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists( $custom_taxonomy );
            if(empty($last_category) && !empty( $custom_taxonomy ) && $taxonomy_exists ) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                if( $taxonomy_terms ){
                    $last_term      = end( array_values( $taxonomy_terms ) );
                    $cat_id         = $last_term->term_id;
                    $cat_nicename   = $last_term->slug;
                    $cat_link       = get_term_link($last_term->term_id, $custom_taxonomy);
                    $cat_name       = $last_term->name;
                }
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                if( $post_type != 'post' ){
                	echo '<div class="breadcrumb-link">' . $cat_display . '</div>';
                } else {
                    echo '<a class="bread-blog breadcrumb-link" href="' . get_permalink( get_option( 'page_for_posts' ) ) . '" title="' . get_the_title( get_option( 'page_for_posts', true ) ) . '">' . get_the_title( get_option( 'page_for_posts', true ) ) . '</a>';
                    echo $separator;
                }

                echo '<div class="breadcrumb-link">' . get_the_title() . '</div>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . ' breadcrumb-link" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a>';
                echo $separator;
                echo '<div class="breadcrumb-link">' . get_the_title() . '</div>';
              
            } else {
                
                echo '<div class="breadcrumb-link">' . get_the_title() . '</div>';
                  
            }
              
        } else if ( is_category() ) {

            // Category page
            echo '<div class="breadcrumb-link">' . single_cat_title('', false) . '</div>';

        } else if ( is_home() ) {

            if ( get_query_var('paged') ) {
               
                // Blog paginated
                echo '<a class="bread-blog breadcrumb-link" href="' . get_permalink( get_option( 'page_for_posts' ) ) . '" title="' . get_the_title( get_option( 'page_for_posts', true ) ) . '">' . get_the_title( get_option( 'page_for_posts', true ) ) . '</a>';
                echo $separator;
                echo '<div class="breadcrumb-link">'.  __( 'Page', 'bl') . ' ' . get_query_var('paged') . '</div>';
                   
            } else {
                // Blog
                echo '<div class="breadcrumb-link">'. get_the_title( get_option( 'page_for_posts', true ) ) .'</div>';
            }

        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<a class="bread-parent bread-parent-' . $ancestor . ' breadcrumb-link" href="' . get_permalink($ancestor) . '" title="' . get_the_title( $ancestor ) . '">' . get_the_title( $ancestor ) . '</a>';
                }
                   
                // Display parent pages
                echo '<div class="breadcrumb-link">' . $parents . '</div>' . $separator;
                   
                // Current page
                echo '<div class="breadcrumb-link">' . get_the_title() . '</div>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<div class="breadcrumb-link">' . get_the_title() . '</div>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<div class="breadcrumb-link">' . $get_term_name . '</div>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<a class="bread-year bread-year-' . get_the_time('Y') . ' breadcrumb-link" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a>';
            echo $separator;
               
            // Month link
            echo '<a class="bread-month bread-month-' . get_the_time('m') . ' breadcrumb-link" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a>';
            echo $separator;
               
            // Day display
            echo '<div class="breadcrumb-link">' . get_the_time('M') . ' ' . __( 'Archives', 'bl' ) . '</div>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<a class="bread-year bread-year-' . get_the_time('Y') . ' breadcrumb-link" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a>';
            echo $separator;
               
            // Month display
            echo '<div class="breadcrumb-link">' . get_the_time('M') . ' ' . __( 'Archives', 'bl' ) . '</div>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<div class="breadcrumb-link">' . get_the_time('Y') . ' ' . __( 'Archives', 'bl' ) . '</div>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<div class="breadcrumb-link">' . __( 'Author', 'bl' ) . ': ' . $userdata->display_name . '</div>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<div class="breadcrumb-link">'.  __( 'Page', 'bl') . ' ' . get_query_var('paged') . '</div>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<div class="breadcrumb-link">' . __( 'Search results for', 'bl') . ': ' . get_search_query() . '</div>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<div class="breadcrumb-link">' . __( 'Error 404', 'bl') . '</div>';
        }
       	
       	echo '<div class="breadcrumb-separator">↩</div>';
        echo '</div></div>';
           
    }
       
}



add_filter( 'the_content', 'bl_breadcrumb_to_content_first_container', 100 );
function bl_breadcrumb_to_content_first_container( $content = '' ){
    global $post;
    if ($post->post_type == 'page') {
        $new_contaniner_html = '';

        ob_start();
        bl_breadcrumb();

        $new_contaniner_html .= ob_get_contents();
        ob_end_clean();
        $content = preg_replace('/<div class="bl-breadcrumb-placeholder"><\/div>/', $new_contaniner_html, $content, 1);
    }

    return $content;
}


add_action( 'bl_before_page_content', 'bl_add_breadcrumb_to_woocommerce_my_account_page' );
function bl_add_breadcrumb_to_woocommerce_my_account_page(){
    if( is_page( get_option( 'woocommerce_myaccount_page_id' ) ) || get_post_meta( get_the_ID(), 'need-breadcrumb', true ) == '1' ){
        bl_breadcrumb( array( 'no-padding' ) );
    }
}