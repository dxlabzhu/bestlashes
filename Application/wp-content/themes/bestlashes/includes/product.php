<?php

function bl_get_product_prices( $_product = null ){
	if( !empty( $_product ) ){
		global $wpdb;
		$prices = array();

		if( is_int( $_product ) ){
			$_product = wc_get_product( $_product );
		}
		$product_type = $_product->get_type();

		// Delete price transient
		if( is_vip_user() ){
			add_filter( 'transient_wc_var_prices_' . $_product->get_id(), 'bl_price_transient_for_clear_data');
		}
		
		if( $product_type == 'simple' || $product_type == 'variation' ){
			$price = wc_get_price_including_tax( $_product );
			$price_formatted = strip_tags( wc_price( $price ) );
			
			$regular_price = $_product->get_regular_price();
			$gross_regular_price = wc_get_price_including_tax( $_product, array( 'price' => $regular_price ) );
			$gross_regular_price_formatted = strip_tags( wc_price( $gross_regular_price ) );
			
			$net_price = wc_get_price_excluding_tax( $_product );
			$net_price_formatted = sprintf( __( '%1$s + VAT', 'bl' ), strip_tags( wc_price( $net_price ) ) );

		} else if( $product_type == 'variable' ){
			$price = wc_get_price_including_tax( $_product );
			$min_gross_price = $_product->get_variation_price('min', true);
			$max_gross_price = $_product->get_variation_price('max', true);

			if( $min_gross_price == $max_gross_price ){
				$price_formatted = strip_tags( wc_price( $min_gross_price ) );
			} else {
				$price_formatted = strip_tags( wc_price( $min_gross_price ) ) . ' - ' . strip_tags( wc_price( $max_gross_price ) );
			}

			if( $_product->is_on_sale('edit') ){
				// Get variations min and max regular gross price
				$min_max_result = $wpdb->get_results(
					"
					SELECT MIN( CAST( $wpdb->postmeta.meta_value AS UNSIGNED ) ) as min_price, MAX( CAST( $wpdb->postmeta.meta_value AS UNSIGNED ) ) as max_price FROM $wpdb->postmeta
					LEFT JOIN $wpdb->posts ON $wpdb->postmeta.post_id = $wpdb->posts.ID
					WHERE $wpdb->posts.post_parent = " . $_product->get_id() ."
					AND $wpdb->postmeta.meta_key = '_regular_price'
					AND $wpdb->postmeta.meta_value != '';
					", ARRAY_A
				);

				if( !empty( $min_max_result ) ){
					if( !empty( $min_max_result[0]['min_price'] ) && !empty( $min_max_result[0]['max_price'] ) ){
						if( ICL_LANGUAGE_CODE == 'en' ){
							$currency = "EUR";
							$min_eur_price = apply_filters('wcml_raw_price_amount', $min_max_result[0]['min_price'], $currency);
							$max_eur_price = apply_filters('wcml_raw_price_amount', $min_max_result[0]['max_price'], $currency);
							
							$gross_regular_price_formatted = strip_tags( wc_price( $min_eur_price ) ) . ' - ' . strip_tags( wc_price( $max_eur_price ) );
						} else {
							$gross_regular_price_formatted = strip_tags( wc_price( $min_max_result[0]['min_price'] ) ) . ' - ' . strip_tags( wc_price( $min_max_result[0]['max_price'] ) );
						}
					}
				}
			} else {
				$regular_price = $_product->get_regular_price();
				$gross_regular_price = wc_get_price_including_tax( $_product, array( 'price' => $regular_price ) );
				$gross_regular_price_formatted = strip_tags( wc_price( $gross_regular_price ) );
			}

			$min_net_price = wc_get_price_excluding_tax( $_product, array( 'price' => $_product->get_variation_price('min') ) );
			$max_net_price = wc_get_price_excluding_tax( $_product, array( 'price' => $_product->get_variation_price('max') ) );
			
			if( $min_net_price == $max_net_price ){
				$net_price_formatted = sprintf( __( '%1$s + VAT', 'bl' ), strip_tags( wc_price( $min_net_price ) ) );
			} else {
				$net_price_raw = strip_tags( wc_price( $min_net_price ) ) . ' - ' . strip_tags( wc_price( $max_net_price ) );
				$net_price_formatted = sprintf( __( '%1$s + VAT', 'bl' ), $net_price_raw );
			}
		}

		$prices = array(
			'gross_price' => $price,
			'gross_price_formatted' => $price_formatted,
			'gross_regular_price' => $gross_regular_price,
			'gross_regular_price_formatted' => $gross_regular_price_formatted,
			'net_price' => $net_price,
			'net_price_formatted' => $net_price_formatted
		);

		return $prices;
	}

	return false;
}

function bl_price_transient_for_clear_data( $value = null, $transient = null ){
	return false;
}



	/**************************/
	/*      Custom Fields     */
	/**************************/

add_action('add_meta_boxes', 'bl_add_custom_meta_boxes_to_product');
function bl_add_custom_meta_boxes_to_product() {
	
	// Beállítások
	add_meta_box("product_settings", __('Product settings', 'bl'), "product_settings_meta", "product", "normal", "default" );

}


function product_settings_meta() {
	global $post, $woocommerce_wpml;
	$custom = get_post_custom( $post->ID );
	
	$_product = wc_get_product( $post->ID );
	$currencies = $woocommerce_wpml->multi_currency->get_currencies('include_default = true');
	$all_langs = array();
	$current_lang = ICL_LANGUAGE_CODE;

	if( function_exists( 'icl_get_languages' ) ){
		$all_langs = icl_get_languages('skip_missing=0');
	} ?>
	
	<div class="aw-settings-section">
		<div class="menu-wrapper">
			<ul class="menu">
				<li data-menu-item="main-settings" class="active">
					<span><?php _e( 'Main settings', 'bl' ); ?></span>
				</li>
				<li data-menu-item="list-view" class="">
					<span><?php _e( 'List view', 'bl' ); ?></span>
				</li>
				<li data-menu-item="single-view" class="">
					<span><?php _e( 'Single view', 'bl' ); ?></span>
				</li>
				<li data-menu-item="training-course" class="">
					<span><?php _e( 'Training course', 'bl' ); ?></span>
				</li>
				<?php if( $_product->is_virtual() ){ ?>
					<li data-menu-item="video" class="">
						<span><?php _e( 'Video', 'bl' ); ?></span>
					</li>
				<?php } ?>
			</ul>
		</div>

		<div class="content-wrapper">
			<div class="content-box main-settings active">
				<h3><?php _e( 'Main settings', 'bl' ); ?></h3>
				<h4><?php _e('After add to cart notice video id', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="product-after-add-to-cart-notice-video-id" value="<?php echo ( isset( $custom['product-after-add-to-cart-notice-video-id'][0] ) ? $custom['product-after-add-to-cart-notice-video-id'][0] : '' ); ?>" />
				</div>
                <hr>
                <h4><?php _e('Course listing ID', 'bl'); ?></h4>
                <div class="chckbox-box">
                    <label><input type="checkbox" name="course-listing-id-needed" value="1" <?php echo (isset($custom['course-listing-id-needed'][0]) ? checked( $custom['course-listing-id-needed'][0], '1' , false) : ''); ?>><?php _e('Course listing ID needed', 'bl') ?></label>
                </div>
                <div class="text-box short">
                    <input type="text" name="course-listing-id" value="<?php echo ( isset( $custom['course-listing-id'][0] ) ? $custom['course-listing-id'][0] : 'B/2020/000485' ); ?>" />
                </div>
				<hr>
				<h4><?php _e('New product', 'bl'); ?></h4>
				<div class="chckbox-box">
					<label><input type="checkbox" name="product_new_product" value="1" <?php checked( $custom['product_new_product'][0], '1' ); ?>><?php _e('This product is new product', 'bl') ?></label>
				</div>
			</div>
			<div class="content-box list-view">
				<h3><?php _e( 'List view', 'bl' ); ?></h3>
				<h4><?php _e('Label text', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="product-label-text" value="<?php echo ( isset( $custom['product-label-text'][0] ) ? $custom['product-label-text'][0] : '' ); ?>" />
				</div>
			</div>

			<div class="content-box single-view">
				<h3><?php _e( 'Single view', 'bl' ); ?></h3>
				<?php if( $_product->is_type( 'simple' ) ){ ?>
					<h4><?php _e('Properties', 'bl'); ?></h4>
					<div class="text-box short">
						<textarea name="product-properties-text"><?php echo ( isset( $custom['product-properties-text'][0] ) ? $custom['product-properties-text'][0] : '' ); ?></textarea>
					</div>
					<hr>
				<?php } ?>
				<h4><?php _e('How to use video ID', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="product-how-to-use-video-id" value="<?php echo ( isset( $custom['product-how-to-use-video-id'][0] ) ? $custom['product-how-to-use-video-id'][0] : '' ); ?>" /> <em><?php _e('You can use this field, if you want the same video for every variation in a variable product', 'bl') ?></em>
				</div>
			</div>

			<div class="content-box training-course">
				<h3><?php _e( 'Training course', 'bl' ); ?></h3>
				<div class="chckbox-box">
					<label><input type="checkbox" name="training_course" value="1" <?php checked( $custom['training_course'][0], '1' ); ?>><?php _e('This product is Training course', 'bl') ?></label>
				</div>
				<h4><?php _e('Certificate of qualification', 'bl' ); ?></h4>
				<div class="chckbox-box">
					<label><input type="checkbox" name="training_course_certificate_must_upload" value="1" <?php checked( $custom['training_course_certificate_must_upload'][0], '1' ); ?>><?php _e('User needs to upload certificate for this training course', 'bl') ?></label>
				</div>
				<hr>
				<h4><?php _e('Date/Time', 'bl'); ?></h4>
				<div class="text-box short">
					<?php 
					$training_course_date = '';

					if( !empty( $custom['training_course_date_time'][0] ) ){
						$training_course_date = date_i18n( 'Y-m-d H:i', $custom['training_course_date_time'][0] );
					} ?>
					<input type="text" name="training_course_date_time" value="<?php echo $training_course_date; ?>" class="datetimepicker" autocomplete="off" />
				</div>
				<h4><?php _e('City', 'bl'); ?></h4>
				<div class="text-box short">
					<input type="text" name="training_course_city" value="<?php echo ( isset( $custom['training_course_city'][0] ) ? $custom['training_course_city'][0] : '' ); ?>" />
				</div>
				<h4><?php _e('Address', 'bl'); ?></h4>
				<div class="text-box">
					<input type="text" name="training_course_address" value="<?php echo ( isset( $custom['training_course_address'][0] ) ? $custom['training_course_address'][0] : '' ); ?>" />
				</div>
				<hr>
				<h3><?php _e('Full prices', 'bl'); ?></h3>
				<?php if( !empty( $all_langs ) ){
					foreach( $all_langs as $lang ){
						$currency = '';
						if( !empty( $currencies ) ){
							foreach ( $currencies as $curr_key => $curr ) {
								if( $curr['languages'][$lang['code']] == '1' ){
									$currency = $curr_key;
								}
							}
						} ?>
							
						<h4><?php echo sprintf( __( 'Full price (%1$s - %2$s)', 'bl' ), strtoupper( $lang['code'] ), $currency ); ?></h4>
						<div class="text-box short">
							<input type="text" name="training_course_full_price_<?php echo $lang['code']; ?>" value="<?php echo ( isset( $custom['training_course_full_price_' . $lang['code']][0] ) ? $custom['training_course_full_price_' . $lang['code']][0] : '' ); ?>" />
						</div>
					<?php }
				} ?>
			</div>

			<?php if( $_product->is_virtual() ){ ?>	
				<div class="content-box video">
					<h3><?php _e( 'Video settings', 'bl' ); ?></h3>
					<h4><?php _e('Video embed code', 'bl' ); ?></h4>
					<div class="text-box short">
						<textarea name="video-embed-code"><?php echo ( isset( $custom['video-embed-code'][0] ) ? $custom['video-embed-code'][0] : '' ); ?></textarea>
					</div>
				</div>
			<?php } ?>
			
		</div>
	</div>

<?php }

///////////////////////////// Save CFs

add_action('save_post', 'bl_product_save_custom_postdata');
function bl_product_save_custom_postdata($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	global $post;

	if( isset( $_POST['post_type'] ) ){
		if ($_POST['post_type'] == "product") {
			$all_langs = array();

			if( function_exists( 'icl_get_languages' ) ){
				$all_langs = icl_get_languages('skip_missing=0');
			}

			// Main settings
			update_post_meta( $post->ID, 'product-after-add-to-cart-notice-video-id', $_POST['product-after-add-to-cart-notice-video-id'] );

			if ($_POST['course-listing-id-needed']) {
                update_post_meta( $post->ID, 'course-listing-id-needed', $_POST['course-listing-id-needed'] );
                update_post_meta( $post->ID, 'course-listing-id', $_POST['course-listing-id'] );
            } else {
                update_post_meta( $post->ID, 'course-listing-id-needed', 0 );
            }
			update_post_meta( $post->ID, 'product_new_product', ( isset( $_POST['product_new_product'] ) ? $_POST['product_new_product'] : '0' ) );
			
			// List view
			update_post_meta( $post->ID, 'product-label-text', $_POST['product-label-text'] );

			// Single view
			update_post_meta( $post->ID, 'product-properties-text', $_POST['product-properties-text'] );
			update_post_meta( $post->ID, 'product-how-to-use-video-id', $_POST['product-how-to-use-video-id'] );

			// Training course
			update_post_meta( $post->ID, 'training_course', ( isset( $_POST['training_course'] ) ? $_POST['training_course'] : '0' ) );
			update_post_meta( $post->ID, 'training_course_certificate_must_upload', ( isset( $_POST['training_course_certificate_must_upload'] ) ? $_POST['training_course_certificate_must_upload'] : '0' ) );

			if( !empty( $_POST['training_course_date_time'] ) ){
				$training_course_date = DateTime::createFromFormat( 'Y-m-d H:i', $_POST['training_course_date_time'] );
				$training_course_timestamp = $training_course_date->format('U');

				update_post_meta( $post->ID, 'training_course_date_time', $training_course_timestamp );
			} else {
				update_post_meta( $post->ID, 'training_course_date_time', '' );
			}
			update_post_meta( $post->ID, 'training_course_city', ucfirst( strtolower( $_POST['training_course_city'] ) ) );
			update_post_meta( $post->ID, 'training_course_address', $_POST['training_course_address'] );
			
			if( !empty( $all_langs ) ){
				foreach( $all_langs as $lang ){
					update_post_meta( $post->ID, 'training_course_full_price_'. $lang['code'], $_POST['training_course_full_price_'. $lang['code']] );
				}
			}

			// Video
			update_post_meta( $post->ID, 'video-embed-code', $_POST['video-embed-code'] );

			// Generate cache data
			$lang = ICL_LANGUAGE_CODE;
			if( !empty( $lang ) ){
				bl_generate_variations_data( $_product, $lang );
			}
		}
	}
}




// Variation fields
add_action( 'woocommerce_product_after_variable_attributes', 'bl_variation_settings_fields', 10, 3 );
add_action( 'woocommerce_save_product_variation', 'bl_save_variation_settings_fields', 10, 2 );

// Add Variation Settings
function bl_variation_settings_fields( $loop, $variation_data, $variation ) {
	global $sitepress, $woocommerce_wpml;

	$currency = $woocommerce_wpml->multi_currency->get_client_currency();
	$vip_roles = bl_get_vip_roles();

	// Product properties
	woocommerce_wp_textarea_input( 
		array( 
			'id'			=> '_product_properies_text['. $variation->ID .']', 
			'label'			=> __( 'Product properies content', 'bl' ),
			'value'			=> get_post_meta( $variation->ID, '_product_properies_text', true ),
			'wrapper_class'	=> 'form-row form-row-full'
		)
	);

	// How to use video id
	woocommerce_wp_text_input( 
		array( 
			'id'			=> '_product_how_to_use_video_id[' . $variation->ID . ']', 
			'class'			=> 'short',
			'label'			=> __( 'How to use video ID', 'bl' ),
			'value'			=> get_post_meta( $variation->ID, '_product_how_to_use_video_id', true ),
			'wrapper_class'	=> 'form-row-first form-row' 
		)
	);

	echo '<div class="clear"></div><hr>';

	if( !empty( $vip_roles ) ){
		foreach ( $vip_roles as $role_key => $role ) {

			// VIP price
			woocommerce_wp_text_input( 
				array( 
					'id'			=> '_'. $role_key .'_price_' . ICL_LANGUAGE_CODE .'[' . $variation->ID . ']', 
					'class'			=> 'short',
					'label'			=> sprintf( __( '%3$s price (%1$s - %2$s)', 'bl' ), strtoupper( ICL_LANGUAGE_CODE ), $currency, ucfirst( str_replace( '_', ' ', $role->name ) ) ),
					'value'			=> get_post_meta( $variation->ID, '_'. $role_key .'_price_' . ICL_LANGUAGE_CODE, true ),
					'wrapper_class'	=> 'form-row-first form-row'
				)
			);

			echo '<div class="clear"></div><hr>';
		}
	}

	echo '<div class="clear"></div>';
}

function bl_save_variation_settings_fields( $post_id ) {
	global $sitepress;

	$vip_roles = bl_get_vip_roles();

	// Product properties
	$content_before_price = $_POST['_product_properies_text'][ $post_id ];
	if( ! empty( $content_before_price ) ) {
		update_post_meta( $post_id, '_product_properies_text', esc_attr( $content_before_price ) );
	}

	// How to use video id
	$video_id = $_POST['_product_how_to_use_video_id'][ $post_id ];
	if( ! empty( $video_id ) ) {
		update_post_meta( $post_id, '_product_how_to_use_video_id', esc_attr( $video_id ) );
	}

	if( !empty( $vip_roles ) ){
		foreach ( $vip_roles as $role_key => $role ) {
			if( ! empty( $_POST['_'. $role_key .'_price_' . ICL_LANGUAGE_CODE][ $post_id ] ) ) {
				update_post_meta( $post_id, '_'. $role_key .'_price_' . ICL_LANGUAGE_CODE, esc_attr( $_POST['_'. $role_key .'_price_' . ICL_LANGUAGE_CODE][ $post_id ] ) );
			}
		}
	}
}




// Simple fields
// Add simple settings
add_action( 'woocommerce_product_options_general_product_data', 'bl_simple_settings_fields' );
add_action( 'woocommerce_process_product_meta', 'bl_save_simple_settings_fields' );

function bl_simple_settings_fields(){
	global $sitepress, $woocommerce_wpml;

	$currency = $woocommerce_wpml->multi_currency->get_client_currency();
	$vip_roles = bl_get_vip_roles();

	echo '<div class="options_group show_if_simple show_if_external">';

	$i = 0;
	if( !empty( $vip_roles ) ){
		foreach ( $vip_roles as $role_key => $role ) {
			// VIP price
			woocommerce_wp_text_input( 
				array( 
					'id'		=> '_'. $role_key .'_price_' . ICL_LANGUAGE_CODE, 
					'class'		=> 'short',
					'label'		=> sprintf( __( '%3$s price (%1$s - %2$s)', 'bl' ), strtoupper( ICL_LANGUAGE_CODE ), $currency, ucfirst( str_replace( '_', ' ', $role->name ) ) ), 
				)
			);

			if( ($i + 1) != sizeof( $vip_roles ) ){
				echo '<div class="clear"></div><hr>';
			}

			$i++;
		}
	}

	echo '</div>';
}

function bl_save_simple_settings_fields( $post_id ){
	global $sitepress;

	$vip_roles = bl_get_vip_roles();

	if( !empty( $vip_roles ) ){
		foreach ( $vip_roles as $role_key => $role ) {
			if( !empty( $_POST['_'. $role_key .'_price_' . ICL_LANGUAGE_CODE] ) ){
				// VIP price
				if( !empty( $_POST['_'. $role_key .'_price_' . ICL_LANGUAGE_CODE] ) && !is_array( $_POST['_'. $role_key .'_price_' . ICL_LANGUAGE_CODE] ) ){
					update_post_meta( $post_id, '_'. $role_key .'_price_' . ICL_LANGUAGE_CODE, esc_attr( $_POST['_'. $role_key .'_price_' . ICL_LANGUAGE_CODE] ) );
				}
			}
		}
	}
}


function bl_generate_variations_data( $_product = null, $lang = null ){
	$cache_data = array();

	if( !empty( $_product ) ){
		if( $_product->is_type( 'variable' ) ){
			$product_how_to_use_video_id = get_post_meta( $_product->get_id(), 'product-how-to-use-video-id', true );

			// Lang
			if( empty( $lang ) ){
				$lang = ICL_LANGUAGE_CODE;
			}

			// Variations
			//$available_variations = $_product->get_available_variations();
			$available_variations = array();
			$all_variations = $_product->get_children();
			if( !empty( $all_variations ) ){
				foreach ( $all_variations as $child_id ) {
					$variation = wc_get_product( $child_id );
					
					if( $variation->get_status() === 'publish' ){
						$available_variations[] = $_product->get_available_variation( $variation );
					}
				}
			}

			/*// Check is in stock
			$checked_avaiable_variations = array();
			if( !empty( $available_variations ) ){
				foreach ( $available_variations as $variation ) {
					if( $variation['is_in_stock'] ){
						$checked_avaiable_variations[] = $variation;
					}
				}

				$available_variations = $checked_avaiable_variations;
			}*/

			// Attributes
			$attributes = array();
			if( $_product->has_attributes() ){
				$attributes = $_product->get_attributes();
			}

			if( !empty( $attributes ) ){
				foreach ( $attributes as $attribute ) {
					if( !empty( $attribute['options'] ) ){
						$terms = get_terms(
							array(
								'taxonomy' => $attribute['name'],
								'include' => $attribute['options']
							)
						);

						if( !empty( $terms ) ){
							foreach ( $terms as $term ) {
								$available_attributes[ $attribute['name'] ]['variation_slugs'][] = $term->slug;
								$available_attributes[ $attribute['name'] ]['variations'][] = $term;
								$available_attributes[ $attribute['name'] ]['attribute'] = $attribute;
							}
						}
					}
				}
			}

			/*$available_attributes = array();
			$ordered_avaiable_attributes = array();
			foreach ( $available_variations as $variation ) {
				if( !empty( $variation['attributes'] ) ){
					foreach ( $variation['attributes'] as $attribute_name => $variation_attribute ) {
						if( !empty( $available_attributes[ str_replace( 'attribute_', '', $attribute_name ) ]['variation_slugs'] ) ){
							if( !in_array( $variation_attribute, $available_attributes[ str_replace( 'attribute_', '', $attribute_name ) ]['variation_slugs'] ) ) {
								$term = get_term_by( 'slug', $variation_attribute, str_replace( 'attribute_', '', $attribute_name ) );

								$available_attributes[ str_replace( 'attribute_', '', $attribute_name ) ]['variation_slugs'][] = $variation_attribute;
								//$available_attributes[ str_replace( 'attribute_', '', $attribute_name ) ]['variations'][] = $term;
								$ordered_avaiable_attributes[ str_replace( 'attribute_', '', $attribute_name ) ][] = $term->term_id;
							}
						} else {
							$term = get_term_by( 'slug', $variation_attribute, str_replace( 'attribute_', '', $attribute_name ) );

							$available_attributes[ str_replace( 'attribute_', '', $attribute_name ) ]['variation_slugs'][] = $variation_attribute;
							//$available_attributes[ str_replace( 'attribute_', '', $attribute_name ) ]['variations'][] = $term;
							$ordered_avaiable_attributes[ str_replace( 'attribute_', '', $attribute_name ) ][] = $term->term_id;
						}
					}
				}
			}

			// Order attributes
			if( !empty( $ordered_avaiable_attributes ) ){
				foreach ( $ordered_avaiable_attributes as $taxonomy => $include_terms ) {
					$terms = get_terms(
						array(
							'taxonomy' => $taxonomy,
							'include' => $include_terms
						)
					);

					if( !empty( $terms ) ){
						foreach ( $terms as $term ) {
							$available_attributes[ $taxonomy ]['variations'][] = $term;
						}
					}
				}
			}

			// Attributes to available_attributes
			if( !empty( $available_attributes ) ){
				foreach ( $available_attributes as $attr_name => $attr ) {
					foreach ( $attributes as $attribute_name => $attribute ) {
						if( $attr_name == $attribute_name ){
							$available_attributes[ $attr_name ]['attribute'] = $attribute;
						}
					}
				}
			}*/

			// Variable datas
			$variable_datas = array();
			if( !empty( $available_variations ) ){
				foreach ( $available_variations as $variation ) {
					$product_variation = wc_get_product( $variation['variation_id'] );
					$variation_how_to_use_video_id = get_post_meta( $variation['variation_id'], '_product_how_to_use_video_id', true );
					$how_to_use_video_id = '';

					if( !empty( $variation_how_to_use_video_id ) ){
						$how_to_use_video_id = $variation_how_to_use_video_id;
					}

					$variable_datas[] = array(
						'id' => $variation['variation_id'],
						'attributes' => $variation['attributes'],
						'prices' => bl_get_product_prices( $variation['variation_id'] ),
						'image' => get_the_post_thumbnail( $variation['variation_id'], 'product-list-boxed', array( 'class' => 'product-image', 'data-zoomed' => get_the_post_thumbnail_url( $variation['variation_id'] ) ) ),
						'properties_text' => get_post_meta( $variation['variation_id'], '_product_properies_text', true ),
						'product_description' => wpautop( $variation['variation_description'] ),
						'how_to_use_video_id' => $how_to_use_video_id,
						'is_purchasable' => ( $product_variation->is_purchasable() && $product_variation->is_in_stock() )
					);
				}
			}

			// Collect cache data
			$cache_data = array(
				'available_variations' => $available_variations,
				'available_attributes' => $available_attributes,
				'attributes' => $attributes,
				'variable_datas' => $variable_datas
			);

			wp_cache_set( 'bl_product_variation_available_variations_data_cache_' . $_product->get_id() . '_' . strtolower( $lang ), $available_variations, 'bl_product' );
			wp_cache_set( 'bl_product_variation_available_attributes_data_cache_' . $_product->get_id() . '_' . strtolower( $lang ), $available_attributes, 'bl_product' );
			wp_cache_set( 'bl_product_variation_attributes_data_cache_' . $_product->get_id() . '_' . strtolower( $lang ), $attributes, 'bl_product' );
			wp_cache_set( 'bl_product_variation_variable_datas_data_cache_' . $_product->get_id() . '_' . strtolower( $lang ), $variable_datas, 'bl_product' );

			if( is_vip_user() ){
				$role_slug = bl_get_user_role_slug();

				if( !empty( $role_slug ) ){
					wp_cache_set( 'bl_product_variation_variable_datas_data_cache_' . $_product->get_id() . '_' . $role_slug . '_' . strtolower( $lang ), $variable_datas, 'bl_product' );
				}
			}
		}
	}

	return $cache_data;
}


function bl_get_product_variations_on_sale( $products_on_sale = array() ){
	if( !empty( $products_on_sale ) ){
		// Basic query
		$args = array();
		$args['post_type'] = array( 'product_variation' );
		$args['posts_per_page'] = '-1';
		$args['post_status'] = 'publish';
		$args['post_parent__in'] = $products_on_sale;
		$args['meta_query']['relation'] = 'OR';
		$args['meta_query'][] = array(
			'key' => '_sale_price',
			'value' => '0',
			'compare' => '>',
			'type' => 'numeric'
		);
		$args['meta_query'][] = array(
			'key' => '_min_variation_sale_price',
			'value' => '0',
			'compare' => '>',
			'type' => 'numeric'
		);
		$args['fields'] = 'ids';
		
		$product_variations_loop = new WP_Query( $args );

		return $product_variations_loop->posts;
	}
}