 <!-- Social -->
<div class="content-box social-settings <?php echo implode( ' ', $eclass ); ?>">
    <h3><?php _e( 'Social settings', 'bl' ); ?></h3>
    <h4><?php _e('Right side menu - Facebook link', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-right-side-menu-facebook-link" value="<?php echo get_option('bl-right-side-menu-facebook-link'); ?>" />
    </div>
    <hr>
    <h4><?php _e('Right side menu - Instagram link', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-right-side-menu-instagram-link" value="<?php echo get_option('bl-right-side-menu-instagram-link'); ?>" />
    </div>
    <hr>
    <h4><?php _e('Right side menu - Email', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-right-side-menu-email" value="<?php echo get_option('bl-right-side-menu-email'); ?>" />
    </div>
</div>
<!-- /Social -->