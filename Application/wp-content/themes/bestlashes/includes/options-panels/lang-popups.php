 <!-- Popups -->
<div class="content-box popups-settings-<?php echo $lang['code']; ?>">
    <!-- Registration verify success -->
    <h3><?php _e( 'Registration verify success', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-registration-verify-success-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-registration-verify-success-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-registration-verify-success-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-registration-verify-success-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    
    <!-- Registration verify fail -->
    <h3><?php _e( 'Registration verify fail', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-registration-verify-fail-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-registration-verify-fail-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-registration-verify-fail-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-registration-verify-fail-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    
    <!-- Forgotten password - password change verify fail -->
    <h3><?php _e( 'Forgotten password - password change verify fail', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-forgotten-password-password-change-fail-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-forgotten-password-password-change-fail-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-forgotten-password-password-change-fail-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-forgotten-password-password-change-fail-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    
    <!-- Newsletter subscription verify success -->
    <h3><?php _e( 'Newsletter subcription verify success', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-newsletter-subscription-verify-success-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-newsletter-subscription-verify-success-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-newsletter-subscription-verify-success-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-newsletter-subscription-verify-success-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    
    <!-- Newsletter subscription verify fail -->
    <h3><?php _e( 'Newsletter subcription verify fail', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-newsletter-subscription-verify-fail-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-newsletter-subscription-verify-fail-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-newsletter-subscription-verify-fail-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-newsletter-subscription-verify-fail-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    
    <!-- Newsletter unsubscription verify success -->
    <h3><?php _e( 'Newsletter unsubscription verify success', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-newsletter-unsubscription-verify-success-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-newsletter-unsubscription-verify-success-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-newsletter-unsubscription-verify-success-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-newsletter-unsubscription-verify-success-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    
    <!-- Newsletter unsubscription verify fail -->
    <h3><?php _e( 'Newsletter unsubscription verify fail', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-newsletter-unsubscription-verify-fail-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-newsletter-unsubscription-verify-fail-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-newsletter-unsubscription-verify-fail-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-newsletter-unsubscription-verify-fail-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    
    <!-- Delete user - delete confirmation -->
    <h3><?php _e( 'Delete user - delete confirmation', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-delete-user-confirmation-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-delete-user-confirmation-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-delete-user-confirmation-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-delete-user-confirmation-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    
    <!-- Delete user - delete success -->
    <h3><?php _e( 'Delete user - delete success', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-delete-user-success-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-delete-user-success-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-delete-user-success-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-delete-user-success-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    
    <!-- Search --> 
    <h3><?php _e( 'Search', 'bl' ); ?></h3>
    <h4><?php _e('Popup title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-popup-search-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-popup-search-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Popup content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-popup-search-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-popup-search-content', $lang['code'] ); ?></textarea>
    </div>
</div>
<!-- /Popups -->