 <!-- Webshop -->
<div class="content-box shop-settings-<?php echo $lang['code']; ?>">
    <h3><?php _e( 'Shop settings', 'bl' ); ?></h3>
    <h4><?php _e('Before category list content', 'bl'); ?></h4>
    <div class="text-box">
        <?php 
        wp_editor( 
            bl_get_option_lang('bl-before-category-list-banner-content', $lang['code'] ), 
            'bl-before-category-list-banner-content-' . $lang['code']
        ); ?>
    </div>
    <h4><?php _e('Before category list background', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-before-category-list-banner-background-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-before-category-list-banner-background', $lang['code'] ); ?>" />
    </div>
    <hr>
    <h4><?php _e('After category list content', 'bl'); ?></h4>
    <div class="text-box">
        <?php 
        wp_editor( 
            bl_get_option_lang('bl-after-category-list-banner-content', $lang['code'] ),
            'bl-after-category-list-banner-content-' . $lang['code']
        ); ?>
    </div>
    <h4><?php _e('After category list background', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-after-category-list-banner-background-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-after-category-list-banner-background', $lang['code'] ); ?>" />
    </div>
    <hr>
    <h3><?php _e('Product variation labels', 'bl'); ?></h3>
    <?php
    $attributes = wc_get_attribute_taxonomies();
    if ( !empty( $attributes ) ){
        foreach ( $attributes as $attribute ) { ?>
            <h4><?php echo $attribute->attribute_label; ?></h4>
            <div class="text-box">
                <input type="text" name="bl-product-variation-label-<?php echo $attribute->attribute_id; ?>-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-product-variation-label-'.  $attribute->attribute_id, $lang['code'] ); ?>" />
            </div>
        <?php }
    } ?>
</div>
<!-- /Webshop -->