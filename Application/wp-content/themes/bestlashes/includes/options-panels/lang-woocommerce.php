 <!-- Woocommerce -->
<div class="content-box woocommerce-settings-<?php echo $lang['code']; ?>">
    <h3><?php _e( 'Woocommerce settings', 'bl' ); ?></h3>
    <h4><?php _e('Email text - only products', 'bl'); ?></h4>
    <div class="text-box">
        <?php
        wp_editor(
            bl_get_option_lang('bl-woocommerce-email-text-only-product', $lang['code'] ),
            'bl-woocommerce-email-text-only-product-'. $lang['code']
        ); ?>
    </div>
    <h4><?php _e('Email text - only online training courses', 'bl'); ?></h4>
    <div class="text-box">
        <?php
        wp_editor(
            bl_get_option_lang('bl-woocommerce-email-text-only-online-courses', $lang['code'] ),
            'bl-woocommerce-email-text-only-online-courses-'. $lang['code']
        ); ?>
    </div>
    <h4><?php _e('Email text - only training courses', 'bl'); ?></h4>
    <div class="text-box">
        <?php
        wp_editor(
            bl_get_option_lang('bl-woocommerce-email-text-only-training-courses', $lang['code'] ),
            'bl-woocommerce-email-text-only-training-courses-'. $lang['code']
        ); ?>
    </div>
    <h4><?php _e('Email text - products and training courses', 'bl'); ?></h4>
    <div class="text-box">
        <?php
        wp_editor(
            bl_get_option_lang('bl-woocommerce-email-text-product-and-training-courses', $lang['code'] ),
            'bl-woocommerce-email-text-product-and-training-courses-'. $lang['code']
        ); ?>
    </div>
</div>
<!-- /Woocommerce -->