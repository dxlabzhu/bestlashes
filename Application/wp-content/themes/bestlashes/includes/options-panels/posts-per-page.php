 <!-- Posts per page -->
<div class="content-box posts-per-page-settings">
    <h3><?php _e( 'Posts per page settings', 'bl' ); ?></h3>
    <h4><?php _e('Show X featured products on frontpage', 'bl'); ?></h4>
    <div class="text-box">
        <input type="number" name="bl-posts-per-page-featured-products-on-frontpage" value="<?php echo get_option('bl-posts-per-page-featured-products-on-frontpage'); ?>" />
    </div>
    <hr>
    <h4><?php _e('Webshop products view limit', 'bl'); ?> - <em><?php _e('Below X: row view, Over X: boxed view', 'bl'); ?></em></h4>
    <div class="text-box">
        <input type="number" name="bl-posts-per-page-webshop-products-view-limit" value="<?php echo get_option('bl-posts-per-page-webshop-products-view-limit'); ?>" />
    </div>
    <hr>
    <h4><?php _e('Show X related products on single products', 'bl'); ?></em></h4>
    <div class="text-box">
        <input type="number" name="bl-posts-per-page-related-products-on-single-products" value="<?php echo get_option('bl-posts-per-page-related-products-on-single-products'); ?>" />
    </div>
    <hr>
    <h4><?php _e('Show X related posts on single post sidebar', 'bl'); ?></em></h4>
    <div class="text-box">
        <input type="number" name="bl-posts-per-page-related-posts-on-single-post-sidebar" value="<?php echo get_option('bl-posts-per-page-related-posts-on-single-post-sidebar'); ?>" />
    </div>
    <hr>
    <h4><?php _e('Show X related products on single post sidebar', 'bl'); ?></em></h4>
    <div class="text-box">
        <input type="number" name="bl-posts-per-page-related-products-on-single-post-sidebar" value="<?php echo get_option('bl-posts-per-page-related-products-on-single-post-sidebar'); ?>" />
    </div>
</div>
<!-- /Posts per page -->