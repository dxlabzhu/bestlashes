 <!-- Registration -->
<div class="content-box registration-settings-<?php echo $lang['code']; ?>">
    <!-- Registration verify success -->
    <h3><?php _e( 'Registration settings', 'bl' ); ?></h3>
    <h4><?php _e('Content before registration form', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-content-before-registration-form-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-content-before-registration-form', $lang['code'] ); ?></textarea>
    </div>

</div>
 <!-- /Registration -->