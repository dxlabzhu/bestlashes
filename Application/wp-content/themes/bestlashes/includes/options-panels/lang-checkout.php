 <!-- Checkout -->
<div class="content-box checkout-settings-<?php echo $lang['code']; ?>">
    <h3><?php _e( 'Checkout settings', 'bl' ); ?></h3>
    <h4><?php _e('Progress bar text - step 1', 'bl'); ?> - <em>(<?php _e('Cart page', 'bl'); ?></em>)</h4>
    <div class="text-box">
        <input type="text" name="bl-progress-bar-text-step-1-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-progress-bar-text-step-1', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Progress bar text - step 2', 'bl'); ?> - <em>(<?php _e('Checkout page', 'bl'); ?></em>)</h4>
    <div class="text-box">
        <input type="text" name="bl-progress-bar-text-step-2-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-progress-bar-text-step-2', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Progress bar text - step 3', 'bl'); ?> - <em>(<?php _e('Thank you page', 'bl'); ?></em>)</h4>
    <div class="text-box">
        <input type="text" name="bl-progress-bar-text-step-3-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-progress-bar-text-step-3', $lang['code'] ); ?>" />
    </div>
</div>
<!-- /Checkout -->