<!-- Other -->
<div class="content-box other-settings <?php echo implode( ' ', $eclass ); ?>">
    <h3><?php _e( 'Other settings', 'bl' ); ?></h3>
    <h4><?php _e('Birthday coupon enabled', 'bl'); ?></h4>
    <div class="chckbox-box">
        <label><input type="checkbox" name="bl-enable-birthday-coupon" value="1" <?php checked( get_option('bl-enable-birthday-coupon'), '1' ); ?>><?php _e('Enable birthday coupon', 'bl') ?></label>
    </div>
</div>
<!-- /Other -->