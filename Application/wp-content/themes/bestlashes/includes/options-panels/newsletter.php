 <!-- Newsletter -->
<div class="content-box newsletter-settings <?php echo implode( ' ', $eclass ); ?>">
    <h3><?php _e( 'Newsletter settings', 'bl' ); ?></h3>
    <h4><?php _e('Newsletter coupon', 'bl'); ?></h4>
    <div class="chckbox-box">
        <label><input type="checkbox" name="bl-enable-newsletter-coupon" value="1" <?php checked( get_option('bl-enable-newsletter-coupon'), '1' ); ?>><?php _e('Enable newsletter coupon', 'bl') ?></label>
    </div>
    <hr>
    <h4><?php _e('Export newsletter subscriptions', 'bl'); ?></h4>
    <div class="button-box">
        <a href="<?php echo admin_url('admin.php?page=bl_options_panel_settings&export-newsletter-subscriptions=1'); ?>" class="export-newsletter-subscriptions button button-primary button-large"><?php _e('Export to *.csv', 'bl') ?></a>
    </div>
</div>
<!-- /Newsletter -->