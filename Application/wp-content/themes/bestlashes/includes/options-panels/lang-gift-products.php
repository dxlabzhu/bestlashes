 <!-- Gift-products -->
<div class="content-box gift-products-settings-<?php echo $lang['code']; ?> <?php echo implode( ' ', $eclass ); ?>">
    <?php
    $from_date = '';
    $to_date = '';
    $from_date_raw = bl_get_option_lang('bl-gift-product-from', $lang['code'] );
    $to_date_raw = bl_get_option_lang('bl-gift-product-to', $lang['code'] );

    if( !empty( $from_date_raw ) ){
        $from_date = date_i18n( 'Y-m-d H:i', $from_date_raw );
    }
    if( !empty( $to_date_raw ) ){
        $to_date = date_i18n( 'Y-m-d H:i', $to_date_raw );
    }
    ?>

    <h3><?php _e( 'Gift products settings', 'bl' ); ?></h3>
    <h4><?php _e('Gift products enabled', 'bl'); ?></h4>
    <div class="chckbox-box">
        <label><input type="checkbox" name="bl-enable-gift-products-<?php echo $lang['code']; ?>" value="1" <?php checked( bl_get_option_lang('bl-enable-gift-products', $lang['code'] ), '1' ); ?>><?php _e('Enable gift products', 'bl') ?></label>
    </div>
    <h4><?php _e('From', 'bl'); ?></h4>
    <div class="text-box short">
        <input type="text" name="bl-gift-product-from-<?php echo $lang['code']; ?>" class="datetimepicker" value="<?php echo $from_date; ?>" />
    </div>
    <h4><?php _e('To', 'bl'); ?></h4>
    <div class="text-box short">
        <input type="text" name="bl-gift-product-to-<?php echo $lang['code']; ?>" class="datetimepicker" value="<?php echo $to_date; ?>" />
    </div>
    <hr>
    <h4><?php _e('Gift product', 'bl'); ?></h4>
    <div class="duplicatable table-style">
        <?php $gift_products_discount = maybe_unserialize( bl_get_option_lang('bl-gift-products-discount', $lang['code'] ) );
        
        if( !empty( $gift_products_discount ) ){
            foreach ( $gift_products_discount as $discount ) {
                if( !empty( $discount ) ){ ?>

                    <div class="duplicatable-element">
                        <div class="flex-row">
                            <div class="cart-limit flex flex-1">
                                <h5><?php _e('Cart total limit', 'bl'); ?></h5>
                                <input type="text" class="" name="bl-gift-products-cart-limit-<?php echo $lang['code']; ?>[]" value="<?php echo $discount['limit']; ?>" placeholder="<?php _e('Cart total limit', 'bl'); ?>" />
                            </div>
                            <div class="product flex flex-1">
                                <h5><?php _e('Gift product', 'bl'); ?></h5>
                                <select class="bl-gift-products select-2" name="bl-gift-products-<?php echo $lang['code']; ?>[]">
                                    <option value=""><?php _e('Please choose one', 'bl'); ?></option>
                                    <?php if( !empty( $gift_products ) ){
                                        foreach ( $gift_products as $product ) { ?>
                                            <option value="<?php echo $product['id'] ?>" <?php selected( $discount['product'], $product['id'] ) ?>><?php echo $product['name']; ?></option>
                                        <?php }
                                    } ?>
                                </select>
                            </div>
                            <div class="product flex flex-1">
                                <h5><?php _e('Gift product variation', 'bl'); ?></h5>
                                <select class="bl-gift-products-variation select-2" name="bl-gift-products-variation-<?php echo $lang['code']; ?>[]">
                                    <?php if( empty( $discount['variation'] ) ){ ?>
                                        <option value=""><?php _e('Please choose one', 'bl'); ?></option>
                                    <?php } else {
                                        $_product = wc_get_product( $discount['product'] );

                                        if( $_product ){
                                            if( $_product->is_type( 'variable' ) ){
                                                $variations = $_product->get_available_variations();

                                                if( !empty( $variations ) ){
                                                    // Fix variation names
                                                    foreach ( $variations as $variation_key => $variation ) {
                                                        
                                                        if( !empty( $variation['attributes'] ) ){
                                                            foreach ( $variation['attributes'] as $attribute_name => $variation_attribute ) {
                                                                $term = get_term_by( 'slug', $variation_attribute, str_replace( 'attribute_', '', $attribute_name ) );

                                                                if( !empty( $term ) ){
                                                                    $variations[ $variation_key ]['attributes'][ $attribute_name ] = $term->name;
                                                                }
                                                            }
                                                        }
                                                    }

                                                    foreach ( $variations as $variation_key => $variation ) {
                                                        $variation_name = array();

                                                        if( !empty( $variation['attributes'] ) ){
                                                            foreach ( $variation['attributes'] as $attribute_name => $variation_attribute ) {
                                                                $variation_name[] = $variation_attribute;
                                                            }
                                                        } ?>

                                                        <option value="<?php echo $variation['variation_id']; ?>" <?php selected( $discount['variation'], $variation['variation_id'] ) ?>><?php echo implode( ', ', $variation_name ); ?></option>
                                                    <?php }
                                                }
                                            }
                                        }
                                    } ?>
                                </select>
                                <div class="ajax-loader"></div>
                            </div>
                        </div>
                        <div class="remove-row">
                            <div class="remove">X</div>
                        </div>
                    </div>

                <?php }
            }
        } ?>

        <div class="duplicatable-element prototype">
            <div class="flex-row">
                <div class="cart-limit flex flex-1">
                    <h5><?php _e('Cart total limit', 'bl'); ?></h5>
                    <input type="text" class="" name="bl-gift-products-cart-limit-<?php echo $lang['code']; ?>[]" value="" placeholder="<?php _e('Cart total limit', 'bl'); ?>" />
                </div>
                <div class="product flex flex-1">
                    <h5><?php _e('Gift product', 'bl'); ?></h5>
                    <select class="bl-gift-products select-2-able" name="bl-gift-products-<?php echo $lang['code']; ?>[]">
                        <option value=""><?php _e('Please choose one', 'bl'); ?></option>
                        <?php if( !empty( $gift_products ) ){
                            foreach ( $gift_products as $product ) { ?>
                                <option value="<?php echo $product['id'] ?>"><?php echo $product['name']; ?></option>
                            <?php }
                        } ?>
                    </select>
                </div>
                <div class="product flex flex-1">
                    <h5><?php _e('Gift product variation', 'bl'); ?></h5>
                    <select class="bl-gift-products-variation select-2-able" name="bl-gift-products-variation-<?php echo $lang['code']; ?>[]">
                        <option value=""><?php _e('Please choose one', 'bl'); ?></option>
                    </select>
                    <div class="ajax-loader"></div>
                </div>
            </div>
            <!--<div class="flex-row">
                <div class="flex flex-1">
                    <h5><?php _e('From', 'bl'); ?></h5>
                    <input type="text" name="bl-gift-product-from-<?php echo $lang['code']; ?>[]" class="datetimepicker" value="" placeholder="">
                </div>
                <div class="flex flex-1">
                    <h5><?php _e('To', 'bl'); ?></h5>
                    <input type="text" name="bl-gift-product-to-<?php echo $lang['code']; ?>[]" class="datetimepicker" value="" placeholder="">
                </div>
            </div>-->
            <div class="remove-row">
                <div class="remove">X</div>
            </div>
        </div>

        <div class="add-new-row button"><?php _e( 'Add new row', 'bl' ); ?></div>
    </div>
</div>
<!-- /Gift-products -->