 <!-- Footer -->
<div class="content-box footer-settings-<?php echo $lang['code']; ?>">
    <h3><?php _e( 'Footer settings', 'bl' ); ?></h3>
    <h4><?php _e('Newsletter subscription title', 'bl'); ?></h4>
    <div class="text-box">
        <input type="text" name="bl-newsletter-subscription-title-<?php echo $lang['code']; ?>" value="<?php echo bl_get_option_lang('bl-newsletter-subscription-title', $lang['code'] ); ?>" />
    </div>
    <h4><?php _e('Before newsletter subscription content', 'bl'); ?></h4>
    <div class="text-box">
        <textarea name="bl-before-newsletter-subscription-content-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-before-newsletter-subscription-content', $lang['code'] ); ?></textarea>
    </div>
    <hr>
    <h4><?php _e('Copyright text', 'bl'); ?></h4>
    <div class="text-box">
    	<textarea name="bl-copyright-text-<?php echo $lang['code'] ?>"><?php echo bl_get_option_lang('bl-copyright-text', $lang['code'] ); ?></textarea>
    </div>
</div>
<!-- /Footer -->