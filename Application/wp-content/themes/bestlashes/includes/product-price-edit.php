<?php


/* Price filter */
add_action( 'init', 'bl_edit_price_filters' );
function bl_edit_price_filters(){
	add_filter( 'woocommerce_product_get_price', 'bl_edit_product_price', 100, 2 );
	add_filter( 'woocommerce_product_get_regular_price', 'bl_edit_product_price', 100, 2 );
	add_filter( 'woocommerce_product_get_sale_price', 'bl_edit_product_price', 100, 2 );
	add_filter( 'woocommerce_product_variation_get_price', 'bl_edit_product_price', 100, 2 );
	add_filter( 'woocommerce_product_variation_get_regular_price', 'bl_edit_product_price', 100, 2 );
	add_filter( 'woocommerce_product_variation_get_sale_price', 'bl_edit_product_price', 100, 2 );
	add_filter( 'woocommerce_get_variation_price', 'bl_edit_product_price', 100, 2 );
	
	if( is_user_logged_in() ){
		if( is_vip_user() ){
			add_filter( 'woocommerce_variation_prices_price', 'bl_edit_product_price', 1, 2 );
			add_filter( 'woocommerce_variation_prices_regular_price', 'bl_edit_product_price', 1, 2 );
			add_filter( 'woocommerce_variation_prices_sale_price', 'bl_edit_product_price', 1, 2 );
		}
	}
}

function bl_edit_product_price( $price, $product ){

	if( is_user_logged_in() ){	
		$training_courses = get_post_meta( $product->get_id(), 'training_course', 'true' );

		if( empty( $training_courses ) ){
			if( is_vip_user() ){
				$vip_role = bl_get_user_vip_role();

				if( !empty( $vip_role ) ){
					// VIP price
					$current_lang = ICL_LANGUAGE_CODE;
					$vip_price = get_post_meta( $product->get_id(), '_'. $vip_role .'_price_' . $current_lang, true );
					
					if( !empty( $vip_price ) && $vip_price != 'Array' ){ // Around same meta name for variation and product.
						return $vip_price;
					}
				}
			} else {
				$user_discount = false;
				$main_product = $product;

				// If its a variable, use the main product for category and product discount
				if( $product->get_type() == 'variation' ){
					$main_product = wc_get_product( $product->get_parent_id() );
				}

				// Get product categories
				if( $main_product ){
					$product_category_ids = $main_product->get_category_ids();
				}
				
				// Fix User discount
				$fix_user_discount = get_user_meta( get_current_user_id(), 'fix-user-discount', true );

				// Category-product discount
				if( empty( $GLOBALS['category-product-user-discount'] ) ){
					if( get_user_meta( get_current_user_id(), 'enable-category-product-user-discount', true ) == '1' ){
						$category_discount = maybe_unserialize( get_user_meta( get_current_user_id(), 'category-user-discount', true ) );
						$product_discount = maybe_unserialize( get_user_meta( get_current_user_id(), 'product-user-discount', true ) );

						$GLOBALS['category-product-user-discount'] = array(
							'category' => $category_discount,
							'product' => $product_discount
						);
					}
				} else {
					$category_discount = $GLOBALS['category-product-user-discount']['category'];
					$product_discount = $GLOBALS['category-product-user-discount']['product'];
				}

				// Calculate discount
				// Product discount
				if( !empty( $product_discount ) ){
					foreach ( $product_discount as $prod_discount ) {
						if( apply_filters( 'wpml_object_id', $prod_discount['id'], 'product', TRUE ) == $main_product->get_id() ){
							$user_discount = $prod_discount['value'];
						}
					}
				}

				// Category discount
				if( $user_discount === false ){
					if( !empty( $category_discount ) ){
						foreach ( $category_discount as $cat_discount ) {
							if( !empty( $product_category_ids ) ){
								if( in_array( apply_filters( 'wpml_object_id', $cat_discount['id'], 'product_cat', TRUE ), $product_category_ids ) ){
									$user_discount = $cat_discount['value'];
								}
							}
						}
					}
				}

				// Fix user discount
				if( $user_discount === false ){
					if( $fix_user_discount != '' ){
						$user_discount = $fix_user_discount;
					}
				}

				if( $user_discount !== false ){
					remove_filter( 'woocommerce_product_get_price', 'bl_edit_product_price', 100, 2 );
					remove_filter( 'woocommerce_product_get_regular_price', 'bl_edit_product_price', 100, 2 );
					remove_filter( 'woocommerce_product_get_sale_price', 'bl_edit_product_price', 100, 2 );
					remove_filter( 'woocommerce_product_variation_get_price', 'bl_edit_product_price', 100, 2 );
					remove_filter( 'woocommerce_product_variation_get_regular_price', 'bl_edit_product_price', 100, 2 );
					remove_filter( 'woocommerce_product_variation_get_sale_price', 'bl_edit_product_price', 100, 2 );
					remove_filter( 'woocommerce_get_variation_price', 'bl_edit_product_price', 100, 2 );
					
					if( $product->is_on_sale() ){
						$price_before_calculation = $product->get_price();
						$regular_price_before_calculation = $product->get_regular_price();
						if( !empty( $regular_price_before_calculation ) ){
							$calculated_regular_price = $regular_price_before_calculation * ( ( 100 - (int)$user_discount ) / 100 );
						} else {
							$calculated_regular_price = 0;
						}
						
						if( $calculated_regular_price > $price_before_calculation ){
							if( !empty( $price_before_calculation ) ){
								$price = $price_before_calculation;
							}
						} else {
							if( !empty( $price ) ){
								$price = $price * ( ( 100 - (int)$user_discount ) / 100 );
							}
						}
					} else {
						if( !empty( $price ) ){
							$price = $price * ( ( 100 - (int)$user_discount ) / 100 );
						}
					}
					
					add_filter( 'woocommerce_product_get_price', 'bl_edit_product_price', 100, 2 );
					add_filter( 'woocommerce_product_get_regular_price', 'bl_edit_product_price', 100, 2 );
					add_filter( 'woocommerce_product_get_sale_price', 'bl_edit_product_price', 100, 2 );
					add_filter( 'woocommerce_product_variation_get_price', 'bl_edit_product_price', 100, 2 );
					add_filter( 'woocommerce_product_variation_get_regular_price', 'bl_edit_product_price', 100, 2 );
					add_filter( 'woocommerce_product_variation_get_sale_price', 'bl_edit_product_price', 100, 2 );
					add_filter( 'woocommerce_get_variation_price', 'bl_edit_product_price', 100, 2 );
				}
			}
		}
	}

    return $price;
}