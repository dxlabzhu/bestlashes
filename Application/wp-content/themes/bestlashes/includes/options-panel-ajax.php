<?php

add_action( 'wp_ajax_bl_admin_get_product_variations_for_gift_products', 'bl_admin_get_product_variations_for_gift_products' );
function bl_admin_get_product_variations_for_gift_products() {
	$return = array();
	$return['list'] = array();

	if( !empty( $_POST['product_id'] ) ){
		$_product = wc_get_product( $_POST['product_id'] );
		
		if( $_product->is_type( 'variable' ) ){
			$variations = $_product->get_available_variations();

			if( !empty( $variations ) ){
				// Fix variation names
				foreach ( $variations as $variation_key => $variation ) {
					
					if( !empty( $variation['attributes'] ) ){
						foreach ( $variation['attributes'] as $attribute_name => $variation_attribute ) {
							$term = get_term_by( 'slug', $variation_attribute, str_replace( 'attribute_', '', $attribute_name ) );

							if( !empty( $term ) ){
								$variations[ $variation_key ]['attributes'][ $attribute_name ] = $term->name;
							}
						}
					}
				}

				foreach ( $variations as $variation_key => $variation ) {
					$variation_name = array();

					if( !empty( $variation['attributes'] ) ){
						foreach ( $variation['attributes'] as $attribute_name => $variation_attribute ) {
							$variation_name[] = $variation_attribute;
						}
					}

					$return['list'][] = array(
						'id' => $variation['variation_id'],
						'name' => implode( ', ', $variation_name )
					);
				}
			}
		} else {
			$return['list'][] = array(
				'id' => '',
				'name' => __('No variation found', 'bl')
			);
		}
	} 

	wp_send_json_success( $return );
}

