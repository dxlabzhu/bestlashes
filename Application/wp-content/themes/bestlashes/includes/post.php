<?php


	/**************************/
	/*      Custom Fields     */
	/**************************/

add_action('add_meta_boxes', 'bl_add_custom_meta_boxes_to_post');
function bl_add_custom_meta_boxes_to_post() {
	
	// Beállítások
	add_meta_box("post_settings", __('Post settings', 'bl'), "post_settings_meta", "post", "normal", "default" );

}


function post_settings_meta() {
	global $post;
	$custom = get_post_custom( $post->ID ); 

	// Related posts
	$args = array();
	$args['post_type'] = 'post';
	$args['posts_per_page'] = '-1';
	$args['post_status'] = 'publish';
	$args['post__not_in'] = array( $post->ID );
	$args['fields'] = 'ids';

	$posts_loop = new WP_Query( $args );

	// Related product
	$args = array();
	$args['post_type'] = 'product';
	$args['posts_per_page'] = '-1';
	$args['post_status'] = 'publish';
	$args['fields'] = 'ids';

	$products_loop = new WP_Query( $args ); ?>
	
	<div class="aw-settings-section">
		<div class="menu-wrapper">
			<ul class="menu">
				<li data-menu-item="sidebar-settings" class="active">
					<span><?php _e( 'Sidebar settings', 'bl' ); ?></span>
				</li>
			</ul>
		</div>

		<div class="content-wrapper">
			<div class="content-box sidebar-settings active">
				<h3><?php _e( 'Sidebar settings', 'bl' ); ?></h3>
				<h4><?php _e( 'Related posts', 'bl' ); ?></h4>
				<div class="duplicatable table-style">
					<?php $related_posts = maybe_unserialize( $custom['related_posts'][0] );
					if( !empty( $related_posts ) ){
						foreach ( $related_posts as $post ) {
							if( $post != '' ){ ?>

							 	<div class="duplicatable-element">
									<div class="title flex-3">
										<select name="related_posts[]">
											<option value=""><?php _e('Please choose one', 'bl'); ?></option>
											<?php if( $posts_loop->have_posts() ){
												while ( $posts_loop->have_posts() ) {
													$post_id = $posts_loop->next_post(); ?>
													<option value="<?php echo $post_id ?>" <?php selected( $post_id, $post ); ?>><?php echo get_the_title( $post_id ); ?></option>
												<?php }
											} ?>
										</select>
									</div>
									<div class="remove-row">
										<div class="remove">X</div>
									</div>
								</div>

							<?php }
						}
					} ?>
					
					<div class="duplicatable-element prototype">
						<div class="title flex-3">
							<select name="related_posts[]">
								<option value=""><?php _e('Please choose one', 'bl'); ?></option>
								<?php if( $posts_loop->have_posts() ){
									while ( $posts_loop->have_posts() ) {
										$post_id = $posts_loop->next_post(); ?>
										<option value="<?php echo $post_id ?>"><?php echo get_the_title( $post_id ); ?></option>
									<?php }
								} ?>
							</select>
						</div>
						<div class="remove-row">
							<div class="remove">X</div>
						</div>
					</div>
					<div class="add-new-row button"><?php _e('Add new post', 'bl'); ?></div>
				</div>

				<br>
				<hr>

				<h4><?php _e( 'Related products', 'bl' ); ?></h4>
				<div class="duplicatable table-style">
					<?php $related_products = maybe_unserialize( $custom['related_products'][0] );
					
					if( !empty( $related_products ) ){
						foreach ( $related_products as $product ) {
							if( $product != '' ){ ?>

							 	<div class="duplicatable-element">
									<div class="title flex-3">
										<select name="related_products[]">
											<option value=""><?php _e('Please choose one', 'bl'); ?></option>
											<?php if( $products_loop->have_posts() ){
												while ( $products_loop->have_posts() ) {
													$prod_id = $products_loop->next_post(); ?>
													<option value="<?php echo $prod_id ?>" <?php selected( $prod_id, $product ); ?>><?php echo get_the_title( $prod_id ); ?></option>
												<?php }
											} ?>
										</select>
									</div>
									<div class="remove-row">
										<div class="remove">X</div>
									</div>
								</div>

							<?php }
						}
					} ?>
					
					<div class="duplicatable-element prototype">
						<div class="title flex-3">
							<select name="related_products[]">
								<option value=""><?php _e('Please choose one', 'bl'); ?></option>
								<?php if( $products_loop->have_posts() ){
									while ( $products_loop->have_posts() ) {
										$prod_id = $products_loop->next_post(); ?>
										<option value="<?php echo $prod_id ?>"><?php echo get_the_title( $prod_id ); ?></option>
									<?php }
								} ?>
							</select>
						</div>
						<div class="remove-row">
							<div class="remove">X</div>
						</div>
					</div>
					<div class="add-new-row button"><?php _e('Add new product', 'bl'); ?></div>
				</div>

			</div>
			
		</div>
	</div>

<?php }

///////////////////////////// Save CFs

add_action('save_post', 'bl_post_save_custom_postdata');
function bl_post_save_custom_postdata( $post_id ) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

	if( isset( $_POST['post_type'] ) ){
		if ($_POST['post_type'] == "post") {
			
			// Sidebar settings
			// Fix related_posts
			$related_posts = array();
			if( !empty( $_POST['related_posts'] ) ){
				foreach ( $_POST['related_posts'] as $rel_post_id ) {
					if( !empty( $rel_post_id ) ){
						$related_posts[] = $rel_post_id;
					}
				}
			}
			update_post_meta( $post_id, 'related_posts', $related_posts);

			// Fix related_products
			$related_products = array();

			if( !empty( $_POST['related_products'] ) ){
				foreach ( $_POST['related_products'] as $prod_id ) {
					if( !empty( $prod_id ) ){
						$related_products[] = $prod_id;
					}
				}
			}
			update_post_meta( $post_id, 'related_products', $related_products);
		}
	}
}