<?php

/* Generate invoice when order created and Payment is SimplePay */
//if( BL_GENERATE_INVOICE_IS_ENABLED ){
	//add_action('woocommerce_order_status_completed', 'bl_generate_invoice_for_simplepay', 100, 1);
//}
function bl_generate_invoice_for_simplepay( $order_id ) {
	if ( ! $order_id )
		return;

	$order = wc_get_order( $order_id );

	if( ($order->get_payment_method() == 'otpsimple' || $order->get_payment_method() == 'paypal') && $order->get_status() == 'completed' && $order->is_paid() ){
		// Generate invoice
		try {
			$invoice_id = bl_generate_invoice( $order_id );
		} catch (Exception $e) {
			$order->add_order_note( sprintf( __( "Can't generate invoice now. Error: %s", 'bl'), $e->getMessage() ), 0, false );
		}

		$email = $order->billing_email;
		$name = $order->billing_first_name .' '. $order->billing_last_name;
		// $invoice_id = 3570; // Only test

		if( !empty( $invoice_id ) ){
			if( !empty( $email ) && !empty( $name ) ){
				if( class_exists( 'Auretto_Email_Editor' ) ){
					$AWEmail = new Auretto_Email_Editor();
					$extra_datas = array(
						'receiver_email' => $email,
						'replaceable_content_words' => array(
							'%name%' => $name
						),
						'added_attachments' => array( $invoice_id )
					);
					$email_sent = $AWEmail->sendMailFunction->send_email( BL_EMAIL_ID_SEND_INVOICE_TO_CUSTOMER, $extra_datas );

					if( $email_sent ){
						$order->add_order_note( __('The invoice has been sent sent to the customer', 'bl'), 0, false );
					}
				}
			}
		}
	}
}

/* Pos buttons */
add_action( 'wp_ajax_bl_pos_get_receipt_buttons', 'bl_pos_get_receipt_buttons' );
function bl_pos_get_receipt_buttons(){
	$return = array();
	
	if ( isset( $_POST['id'] ) && !empty( $_POST['id'] ) ) {
		$invoice_id = get_post_meta( $_POST['id'], '_invoice-id', true );

		if( !empty( $invoice_id ) ){
			$return['button'] = '<a href="'. wp_get_attachment_url( $invoice_id ) .'" class="btn btn-primary pull-left invoice-link" target="_blank">'. __('Open invoice', 'bl') .'</a>';
		} else {
			$return['button'] = '<button class="btn btn-primary pull-left generate-invoice">'. __('Generate invoice', 'bl') .'</button>';
		}
	} else {
		$return['button'] = '<button class="btn btn-primary pull-left generate-invoice">'. __('Generate invoice', 'bl') .'</button>';
	}

	wp_send_json( $return );
}


/* POS Ajax */
add_action( 'wp_ajax_bl_generate_invoice_for_order', 'bl_generate_invoice_for_order' );
function bl_generate_invoice_for_order() {
	$return = array();
	
	if ( isset( $_POST['id'] ) && !empty( $_POST['id'] ) ) {

		$order_id = $_POST['id'];
		$final = filter_var( $_POST['final'], FILTER_VALIDATE_BOOLEAN );
		$payment_deadline = $_POST['payment_deadline'];
		$date_of_completion = $_POST['date_of_completion'];
		$payment_method = $_POST['payment_method'];
		$vat_type = $_POST['vat_type'];
		$paid = filter_var( $_POST['paid'], FILTER_VALIDATE_BOOLEAN );
		$from_pos = false;
		$need_preview = isset( $_POST['need_preview'] ) ? filter_var( $_POST['need_preview'],  FILTER_VALIDATE_BOOLEAN ) : true;

		if( isset( $_POST['from_pos'] ) ){
			$from_pos = true;
			$need_preview = false;
		}
		if( ! preg_match('#^\d\d\d\d-\d\d-\d\d$#', $payment_deadline) ) {
			$payment_deadline = date('Y-m-d');
		}
		if( ! preg_match('#^\d\d\d\d-\d\d-\d\d$#', $date_of_completion) ) {
			$date_of_completion = date('Y-m-d');
		}

		/*if( BL_GENERATE_INVOICE_IS_ENABLED ){
			
		} else {
			$return = array(
				'success' => true,
				'message' => __( 'Invoice was generated successfully. You can download the invoice above.', 'bl' ),
				'invoice_id' => 3570,
				'invoice_name' => get_the_title( 3570 ),
				'invoice_url' => wp_get_attachment_url( 3570 )
			);

			// Ha pos oldalról hívjuk meg, akkor kell a gomb DOM
			if( $from_pos ){
				$return['button'] = '<a href="'. wp_get_attachment_url( 3570 ) .'" class="btn btn-primary pull-left invoice-link" target="_blank">'. __('Open invoice', 'bl') .'</a>';
			}
		}*/
		try {
			$invoice_return = bl_generate_invoice($order_id, $final, $payment_deadline, $date_of_completion, $payment_method, $vat_type, $paid, $need_preview);

			if( $need_preview ){
				$return = array(
					'success' => true,
					'invoice_preview_html' => $invoice_return
				);
			} else {
				$return = array(
					'success' => true,
					'message' => __( 'Invoice was generated successfully. You can download the invoice above.', 'bl' ),
					'invoice_id' => $invoice_return,
					'invoice_name' => get_the_title( $invoice_return ),
					'invoice_url' => wp_get_attachment_url( $invoice_return )
				);
			}

			if( $from_pos ){
				$return['button'] = '<a href="'. wp_get_attachment_url( $invoice_return ) .'" class="btn btn-primary pull-left invoice-link" target="_blank">'. __('Open invoice', 'bl') .'</a>';
			}
		} catch (Exception $e) {
			$return = array(
				'success' => false,
				'message' => __( "Can't generate invoice now. Error: " . $e->getMessage(), 'bl')
			);
		}

	} else {
		$return = array(
			'success' => false,
			'message' => __( "Application error. Please contact the developers.", 'bl')
		);
	}

	wp_send_json( $return );
}





function bl_generate_invoice($order_id = 0, $final = false, $payment_deadline = NULL, $date_of_completion = NULL, $payment_method = NULL, $vat_type = BL_HUN_VAT, $paid = true, $need_preview = false){
	if ($payment_deadline == NULL) {
		$payment_deadline = date('Y-m-d');
	}
	if ($date_of_completion == NULL) {
		$date_of_completion = date('Y-m-d');
	}
	$order_custom = get_post_custom($order_id);
	$custom_field_name = $final ? '_final-invoice-id' : '_invoice-id';
	$order = new WC_Order($order_id);
		
	$upload_dir = wp_upload_dir();
	$temp_dir = $upload_dir['basedir'] . '/szamlazzhu-xml/';
	$temp_file = $temp_dir . sha1(microtime() . rand()) . '.xml';

	$xml_date = date('Y-m-d');
	$xml_fizetesi_mod = '';

	if ($payment_method == NULL) {
		$payment_method = $order->get_payment_method();
	}

	if ($payment_method == 'pos_cash') {
		$xml_fizetesi_mod = 'Készpénz';
	} elseif ($payment_method == 'paypal') {
		$xml_fizetesi_mod = 'PayPal';
	} elseif ($payment_method == 'otpsimple' || $payment_method == 'pos_card') {
		$xml_fizetesi_mod = 'Bankkártya';
	} elseif ($payment_method == 'bacs') {
		$xml_fizetesi_mod = 'Átutalás';
	} elseif ($payment_method == 'cod') {
		$xml_fizetesi_mod = 'Utánvét';
	}
	$xml_penznem = $order->get_currency();
	$xml_lang = 'hu';
	$xml_arfolyam = '0.0';
	if ($order->get_billing_company()) {
		$xml_vevo_nev = $order->get_billing_company();
	} else {
		$xml_vevo_nev = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();
	}
	$xml_vevo_irsz = $order->get_billing_postcode();
	$xml_vevo_city = $order->get_billing_city();
	$xml_vevo_cim = $order->get_billing_address_1() . ($order->get_billing_address_2() ? ( ' '.$order->get_billing_address_2() ) : '');
	$xml_vevo_email = $order->get_billing_email();
	$xml_vevo_adoszam = get_post_meta( $order->id, '_billing_tax_number', true );
	if( empty( $xml_vevo_adoszam ) ){
		$user_id = get_post_meta( $order->get_id(), '_customer_user', true );

		if( !empty( $user_id ) ){
			$xml_vevo_adoszam = get_user_meta( $user_id, 'tax-number', true );
		}
	}
	$xml_vevo_adoszam_tag = $xml_vevo_adoszam ? ('<adoszam>'. $xml_vevo_adoszam .'</adoszam>') : '';

	$xml_vevo_nev = str_replace( '&', '&amp;', $xml_vevo_nev);
	$xml_vevo_city = str_replace( '&', '&amp;', $xml_vevo_city);
	$xml_vevo_cim = str_replace( '&', '&amp;', $xml_vevo_cim);

	$tetelek_xml = ''; 
	foreach ($order->get_items() as $item_id => $item_data) {
		$product_item_id = 0;

		if( $item_data->get_variation_id() != 0 ){
			$product_item_id = $item_data->get_variation_id();
		} else {
			$product_item_id = $item_data->get_product_id();
		}

		//$xml_tetel_nev = $item_data->get_name();
		$xml_tetel_nev = strip_tags( get_the_title( $product_item_id ) );
		$xml_tetel_netto_osszeg = $item_data->get_total();
		$xml_tetel_menny = $item_data->get_quantity();
		$xml_tetel_netto = $xml_tetel_netto_osszeg / $xml_tetel_menny;
		$xml_tetel_afakulcs = $vat_type;
		$xml_tetel_total_osszeg = $xml_tetel_netto_osszeg * (1 + $xml_tetel_afakulcs / 100);
		$xml_tetel_afa = $xml_tetel_total_osszeg - $xml_tetel_netto_osszeg;
		$xml_megjegyzes = '';

        // If course listing ID needed
        $course_listing_id_needed = get_post_meta($product_item_id,'course-listing-id-needed', true);

        if ($course_listing_id_needed == '1') {
            $course_listing_id = get_post_meta($product_item_id,'course-listing-id', true);
            if ($course_listing_id) {
                $xml_tetel_nev .= ' [Képzési azonosító: ' . $course_listing_id . ']';
            }
        }

		// Get product attributes
		$product = $item_data->get_product();
		if( 'variation' === $product->get_type() ){
			$attribute_list = array();
			$attributes = $product->get_attributes();

			if( !empty( $attributes ) ){
				foreach ( $attributes as $tax => $term_slug ) {
					$attribute_name = $product->get_attribute( $tax );

					if( !empty( $attribute_name ) ){
						$attribute_list[] = $attribute_name;
					}
				}
			}

			if( !empty( $attribute_list ) ){
				$xml_megjegyzes = implode( ', ', $attribute_list );
			}
		}

		// If training course add city and date
		$training_course = get_post_meta( $product->get_id(), 'training_course', true );

		if( $training_course == '1' ){
			$training_course_infos = array();
			$training_course_timestamp = get_post_meta( $product->get_id(), 'training_course_date_time', true );
			$training_course_formatted_date_time = date_i18n( 'Y.m.d. H:i', (int)$training_course_timestamp );
			$city = get_post_meta( $product->get_id(), 'training_course_city', true );

			if( !empty( $training_course_formatted_date_time ) ){
				$training_course_infos[] = $training_course_formatted_date_time;
			}

			if( !empty( $city ) ){
				$training_course_infos[] = $city;
			}

			if( !empty( $training_course_infos ) ){
				$xml_megjegyzes = implode( ', ', $training_course_infos );
			}
		}


		$tetelek_xml .= "
			<tetel>
			  <megnevezes>$xml_tetel_nev</megnevezes>
			  <mennyiseg>$xml_tetel_menny</mennyiseg>
			  <mennyisegiEgyseg>db</mennyisegiEgyseg>
			  <nettoEgysegar>". strip_tags( wc_price( $xml_tetel_netto, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</nettoEgysegar>
			  <afakulcs>$xml_tetel_afakulcs</afakulcs>
			  <nettoErtek>". strip_tags( wc_price( $xml_tetel_netto_osszeg, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</nettoErtek>
			  <afaErtek>". strip_tags( wc_price( $xml_tetel_afa, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</afaErtek>
			  <bruttoErtek>". strip_tags( wc_price( $xml_tetel_total_osszeg, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</bruttoErtek>";

		if( !empty( $xml_megjegyzes ) ){
			$tetelek_xml .= "
			  <megjegyzes>$xml_megjegyzes</megjegyzes>";
		}

		$tetelek_xml .= "
			</tetel>";
	}
	foreach( $order->get_items('shipping') as $item_id => $shipping_item ){
		$xml_tetel_nev = strip_tags( $shipping_item->get_name() );
		$xml_tetel_total_osszeg = $shipping_item->get_total();
		$xml_tetel_menny = 1;
		$xml_tetel_afakulcs = $vat_type;
		$xml_tetel_netto_osszeg = $xml_tetel_total_osszeg / (1 + $xml_tetel_afakulcs / 100);
		$xml_tetel_netto = $xml_tetel_netto_osszeg;
		$xml_tetel_afa = $xml_tetel_total_osszeg - $xml_tetel_netto_osszeg;
		$xml_megjegyzes = '';

		$tetelek_xml .= "
			<tetel>
			  <megnevezes>$xml_tetel_nev</megnevezes>
			  <mennyiseg>$xml_tetel_menny</mennyiseg>
			  <mennyisegiEgyseg>db</mennyisegiEgyseg>
			  <nettoEgysegar>". strip_tags( wc_price( $xml_tetel_netto, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</nettoEgysegar>
			  <afakulcs>$xml_tetel_afakulcs</afakulcs>
			  <nettoErtek>". strip_tags( wc_price( $xml_tetel_netto_osszeg, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</nettoErtek>
			  <afaErtek>". strip_tags( wc_price( $xml_tetel_afa, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</afaErtek>
			  <bruttoErtek>". strip_tags( wc_price( $xml_tetel_total_osszeg, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</bruttoErtek>
			</tetel>
		";
	}
	foreach( $order->get_fees() as $item_id => $fee_item ){
		$xml_tetel_nev = strip_tags( $fee_item->get_name() );
		$xml_tetel_total_fee_osszeg = $fee_item->get_total();
		$xml_tetel_total_fee_tax_osszeg = $fee_item->get_total_tax();
		$xml_tetel_total_osszeg = $xml_tetel_total_fee_osszeg + $xml_tetel_total_fee_tax_osszeg;
		$xml_tetel_menny = 1;
		$xml_tetel_afakulcs = $vat_type;
		$xml_tetel_netto_osszeg = $xml_tetel_total_osszeg / (1 + $xml_tetel_afakulcs / 100);
		$xml_tetel_netto = $xml_tetel_netto_osszeg;
		$xml_tetel_afa = $xml_tetel_total_osszeg - $xml_tetel_netto_osszeg;
		$xml_megjegyzes = '';

		$tetelek_xml .= "
			<tetel>
			  <megnevezes>$xml_tetel_nev</megnevezes>
			  <mennyiseg>$xml_tetel_menny</mennyiseg>
			  <mennyisegiEgyseg>db</mennyisegiEgyseg>
			  <nettoEgysegar>". strip_tags( wc_price( $xml_tetel_netto, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</nettoEgysegar>
			  <afakulcs>$xml_tetel_afakulcs</afakulcs>
			  <nettoErtek>". strip_tags( wc_price( $xml_tetel_netto_osszeg, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</nettoErtek>
			  <afaErtek>". strip_tags( wc_price( $xml_tetel_afa, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</afaErtek>
			  <bruttoErtek>". strip_tags( wc_price( $xml_tetel_total_osszeg, array( 'price_format' => '%2$s', 'decimal_separator' => '.', 'thousand_separator' => '', 'decimals' => 2 ) ) ) ."</bruttoErtek>
			</tetel>
		";
	}

	$szamlazzhu_user = SZAMLAZZHU_USER;
	$szamlazzhu_pass = SZAMLAZZHU_PASS;
	$xml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<xmlszamla xmlns="http://www.szamlazz.hu/xmlszamla" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.szamlazz.hu/xmlszamla http://www.szamlazz.hu/docs/xsds/agent/xmlszamla.xsd">
  <beallitasok>
	<felhasznalo>$szamlazzhu_user</felhasznalo>
	<jelszo>$szamlazzhu_pass</jelszo>
	<eszamla>false</eszamla>
	<kulcstartojelszo></kulcstartojelszo>
	<szamlaLetoltes>true</szamlaLetoltes>
	<szamlaLetoltesPld>1</szamlaLetoltesPld>
	<valaszVerzio>1</valaszVerzio>
	<aggregator></aggregator>
  </beallitasok>
  <fejlec>
	<keltDatum>$xml_date</keltDatum>
	<teljesitesDatum>$date_of_completion</teljesitesDatum>
	<fizetesiHataridoDatum>$payment_deadline</fizetesiHataridoDatum>
	<fizmod>$xml_fizetesi_mod</fizmod>
	<penznem>$xml_penznem</penznem>
	<szamlaNyelve>$xml_lang</szamlaNyelve>
	<megjegyzes></megjegyzes>
	<arfolyamBank>MNB</arfolyamBank>
	<arfolyam>$xml_arfolyam</arfolyam>
	<rendelesSzam></rendelesSzam>
	<elolegszamla>false</elolegszamla>
	<vegszamla>false</vegszamla>
	<helyesbitoszamla>false</helyesbitoszamla>
	<dijbekero>false</dijbekero>
	<szallitolevel>false</szallitolevel>
	<szamlaszamElotag>BL</szamlaszamElotag>
	<fizetve>$paid</fizetve>
  </fejlec>
  <elado>
	<bank>K&amp;H Bank</bank>
	<bankszamlaszam>10405066-50526777-65501004</bankszamlaszam>
	<emailReplyto></emailReplyto>
	<emailTargy>Számla értesítő</emailTargy>
	<emailSzoveg></emailSzoveg>
  </elado>
  <vevo>
	<nev>$xml_vevo_nev</nev>
	<irsz>$xml_vevo_irsz</irsz>
	<telepules>$xml_vevo_city</telepules>
	<cim>$xml_vevo_cim</cim>
	<email>$xml_vevo_email</email>
	<sendEmail>false</sendEmail>
	$xml_vevo_adoszam_tag
  </vevo>
  <tetelek>
  	$tetelek_xml
  </tetelek>
</xmlszamla>
XML;
/*
Vevőben lehet még:
	<!-- <email>vevoneve@example.org</email> -->
	<!-- <sendEmail>true</sendEmail> -->
	<!-- <postazasiNev>Kovács Bt. postázási név</postazasiNev> -->
	<!-- <postazasiIrsz>2040</postazasiIrsz> -->
	<!-- <postazasiTelepules>Budaörs</postazasiTelepules> -->
	<!-- <postazasiCim>Szivárvány utca 8. VI.em. 82.</postazasiCim> -->
	<!-- <alairoNeve>Vevő Aláírója</alairoNeve> -->
	<!-- <azonosito></azonosito> -->
	<!-- <telefonszam>+3630-555-55-55, Fax:+3623-555-555</telefonszam> -->
	<!-- <megjegyzes>A portáról felszólni a 214-es mellékre.</megjegyzes> -->
*/
	
	// If need preview, generate invoice preview html
	if( $need_preview ){
		$xml_obj = simplexml_load_string( $xml );
		$invoice_json = json_encode( $xml_obj );
		$invoice_data = json_decode( $invoice_json, TRUE );

		ob_start();
		
		include( locate_template( 'template-parts/invoice/invoice-preview.php', false, false ) ); 

		return ob_get_clean();
	}

	file_put_contents($temp_file, $xml);

	$agent_url = 'https://www.szamlazz.hu/szamla/';
	$ch = curl_init($agent_url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);

	curl_setopt($ch, CURLOPT_POSTFIELDS, array('action-xmlagentxmlfile'=>new CURLFile($temp_file, 'application/xml', 'filenev'))); 
	curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);

	$agent_response = curl_exec($ch);
	$http_error = curl_error($ch);

	$agent_header = '';
	$agent_body = '';
	$agent_http_code = '';

	$agent_http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
	$header_size = curl_getinfo($ch,CURLINFO_HEADER_SIZE);
	$agent_header = substr($agent_response, 0, $header_size);
	$header_array = explode("\n", $agent_header);
	$agent_body = substr( $agent_response, $header_size ); // a body tárolása, ez lesz a pdf, vagy szöveges üzenet
	curl_close($ch);

	$volt_hiba = false;
	$agent_error = '';
	$agent_error_code = '';
	$agent_szamlaszam = '';
	foreach ($header_array as $val) {
		if (substr($val, 0, strlen('szlahu')) === 'szlahu') {
			if (substr($val, 0, strlen('szlahu_error:')) === 'szlahu_error:') {
				$volt_hiba = true;
				$agent_error = substr($val, strlen('szlahu_error:'));
			}
			if (substr($val, 0, strlen('szlahu_error_code:')) === 'szlahu_error_code:') {
				$volt_hiba = true;
				$agent_error_code = substr($val, strlen('szlahu_error_code:'));
			}
			if (substr($val, 0, strlen('szlahu_szamlaszam:')) === 'szlahu_szamlaszam:') {
				$agent_szamlaszam = urldecode( substr($val, strlen('szlahu_szamlaszam:')) );
			}
		}
	}

	unlink($temp_file);
	if ( $http_error != "" ) {
		throw new Exception( __( "HTTP error. Please try again later.", 'bl') . " " . $http_error );
	} else if ($volt_hiba) {
		$error_message = $agent_error_code . ' / ' . urldecode($agent_error) . ' / ' . urldecode($agent_body);
		throw new Exception( __( sprintf("Error generating invoice: %s", $error_message), 'bl') );
	} else {
		$order_invoice = sha1(microtime() . rand(1, 10000000)) . '.pdf';
		$filetype = wp_check_filetype( basename( $order_invoice ), null );
		$wp_upload_dir = wp_upload_dir();
		wp_mkdir_p( $wp_upload_dir['basedir'] .'/invoice' );
		$invoice_path = $wp_upload_dir['basedir'] .'/invoice/'. $order_invoice;
		file_put_contents($invoice_path, $agent_body);

		$attachment = array(
				'post_mime_type' => $filetype['type'],
				'post_title'	 => 'Számla ' . $agent_szamlaszam,
				'post_content'   => '',
				'post_status'	=> 'inherit'
		);
		$attach_id = wp_insert_attachment( $attachment, $invoice_path, $order_id );
		update_post_meta($attach_id, '_is_invoice', 1);

		update_post_meta($order_id, $custom_field_name, $attach_id);
		return $attach_id;
	}
	
	return 0;
}
