<?php 

/*

	Template Name: Forgotten password - change password

*/

get_header(); ?>

<div class="subpage-wrapper">
	<section class="section top">
		<div class="container padding-fixed">

			<?php bl_breadcrumb(); ?>

			<?php if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					the_content();
				}
			} ?>

			<div class="separator-heading">
				<h3 class="heading-03"><?php echo get_the_title( get_the_ID() ); ?></h3>
			</div>

			<div class="table _50-percent">
				<div class="table-wrapper">
					<div class="table-content">
						<form action="" method="post" class="change-password bl-form" data-error-text-all-field-required="<?php _e( 'All fields are required', 'bl' ); ?>" data-error-text-wrong-link="<?php _e( "We can't change your password, please contact us.", 'bl' ); ?>" data-error-text-hash-not-match="<?php _e( 'The provided link is not valid. Please try again your process.', 'bl' ); ?>">
							<div class="error-text" style="display: none;">
								<div class="table-column _100percent">
									<div class="text"></div>
								</div>
							</div>
							<div class="table-row _2col">
								<div class="table-column _30percent">
									<div><?php _e( 'Password', 'bl' ); ?>*</div>
								</div>
								<div class="table-column _70percent">
									<div class="table-form _100percent w-form">
										<input type="password" class="change-password table-form-input w-input req" value="" name="change-password" placeholder="***" data-error-text-not-matching-passwords="<?php _e( 'Passwords not match', 'bl' ) ?>" />
									</div>
								</div>
							</div>
							<div class="table-row _2col">
								<div class="table-column _30percent">
									<div><?php _e( 'Password again', 'bl' ); ?>*</div>
								</div>
								<div class="table-column _70percent">
									<div class="table-form _100percent w-form">
										<input type="password" class="change-password-again table-form-input w-input req" value="" name="change-password-again" placeholder="***" />
									</div>
								</div>
							</div>
							<div class="table-footer">
								<div class="table-column _100percent">
									<a href="<?php echo get_permalink( BL_PAGE_LOGIN ); ?>" class="rounded-button w-button"><span class="chart-button-icon">+</span> <?php _e( 'Login', 'bl' ); ?></a>
									<a href="#" class="rounded-button w-button change-password-submit loading-animation-button">
										<?php _e( 'Change password', 'bl' ) ?>
										<span class="chart-button-icon">→</span>
										<span class="loading-button"></span>
									</a>
								</div>
							</div>
							<input type="hidden" name="uid" value="<?php echo $_GET['uid']; ?>">
							<input type="hidden" name="hash" value="<?php echo $_GET['hash']; ?>">
						</form>
					</div>
				</div>
			</div>

		</div>
	</section>
</div>

<?php get_footer(); ?>