<?php 

/*

	Template Name: Login

*/

get_header(); ?>

<div class="subpage-wrapper">
	<section class="section top">
		<div class="container padding-fixed">

			<?php bl_breadcrumb(); ?>

			<?php if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					the_content();
				}
			} ?>

			<div class="table _50-percent">
				<div class="table-wrapper">
					<div class="login-welcome-sign">
						<h1 class="login-welcome-heading"><?php _e( 'Welcome to', 'bl' ); ?><br></h1>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-bestlashes.svg" class="login-logo">
                        <p><span style='color: #d2bc8f;'><?php _e('Professional', 'bl'); ?></span><?php if (get_locale() == 'hu_HU') { echo ' weboldalán';} ?></p>
					</div>
					<div class="table-content">
						<form action="" method="post" class="login bl-form" data-error-text-user-not-activated="<?php _e( 'You have not activated your acount yet.', 'bl' ); ?>">
							<div class="error-text" style="display: none;">
								<div class="table-column _100percent">
									<div class="text"></div>
								</div>
							</div>
							<div class="table-row _2col">
								<div class="table-column _30percent">
									<div><?php _e( 'E-mail address', 'bl' ); ?></div>
								</div>
								<div class="table-column _70percent">
									<div class="table-form _100percent w-form">
										<input type="text" class="login-email table-form-input w-input" value="" name="login-email" placeholder="<?php _e( 'E-mail address', 'bl' ); ?>" data-error-text-no-user-found="<?php _e('Wrong e-mail address', 'bl'); ?>" />
									</div>
								</div>
							</div>
							<div class="table-row _2col">
								<div class="table-column _30percent">
									<div><?php _e( 'Password', 'bl' ); ?></div>
								</div>
								<div class="table-column _70percent">
									<div class="table-form _100percent w-form">
										<input type="password" class="login-password table-form-input w-input" value="" name="login-password" placeholder="***" autocomplete="new-password" data-error-text-wrong-password="<?php _e( 'Wrong password', 'bl' ); ?>" />
									</div>
								</div>
							</div>
							<div class="table-footer">
								<div class="table-column _100percent">
									<a href="<?php echo get_permalink( BL_PAGE_REGISTRATION ); ?>" class="rounded-button w-button"><span class="chart-button-icon">+</span> <?php _e( 'Registration', 'bl' ); ?></a>
									<a href="#" class="rounded-button w-button login-submit loading-animation-button">
										<?php _e( 'Login', 'bl' ) ?>
										<span class="chart-button-icon">→</span>
										<span class="loading-button"></span>
									</a>
								</div>
							</div>
						</form>
					</div>
					<div class="table-smalltext w-clearfix">
						<a href="<?php echo get_permalink( BL_PAGE_FORGOTTEN_PASSWORD_EMAIL_VERIFICATION ) ?>" class="underline-button w-inline-block">
							<div class="underline-button-text">
								<?php _e( 'Forgotten password', 'bl' ); ?><span class="underline-button-icon"></span>
							</div>
							<div class="link-underline"></div>
						</a>
					</div>
				</div>
			</div>

		</div>
	</section>
</div>

<?php get_footer(); ?>