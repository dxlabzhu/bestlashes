<?php


/* Pugin modify
// POS - to use the default view, set BL_POS_IS_BL_VIEW = false
- PLUGINS_DIR/woocommerce-pos/includes/class-wc-pos-template.php:369-373
- PLUGINS_DIR/woocommerce-pos/includes/class-wc-pos-template.php:115-119
- PLUGINS_DIR/woocommerce-pos/assets/js/app.min.js - Add placeholder to input and textarea.'placeholder="Meta cím", 'placeholder="Meta érték"'


// POS pro - to use the default view, set BL_POS_IS_BL_VIEW = false
- PLUGINS_DIR/woocommerce-pos-pro/includes/class-wc-pos-pro-template.php:94-98

// POS pro - translatable strings
- PLUGINS_DIR/woocommerce-pos-pro/includes/class-wc-pos-pro-template.php:49-87
- PLUGINS_DIR/woocommerce-pos-pro/includes/class-wc-pos-pro-template.php:52-84 // apply_filter for shipping and billing infos
- PLUGINS_DIR/woocommerce-pos-pro/includes/apiv2/class-wc-pos-pro-customers.php:81 // do_action for save tax number
- PLUGINS_DIR/woocommerce-pos-pro/assets/js/app.min.js:2 // add tax number

// Woocommerce - filter shipping zones
- PLUGINS_DIR/woocommerce/includes/data-stores/class-wc-shipping-zone-data-store.php:271-278,287-292

// WOOCOMMERCE WMPL
- PLUGINS_DIR/woocommerce-multilangual/inc/currencies/class-wcml-multi-currency-orders.php:39 // comment

*/

define('TEXTDOMAIN', 'bl'); // For info

define('BL_GOOGLE_MAPS_API_KEY', 'AIzaSyDO6g0k_xgWQ7kLY64INzu9ycXqUpeVtPA');

define( 'BL_PAGE_WEBSHOP', apply_filters( 'wpml_object_id', 106, 'page', TRUE ) );
define( 'BL_PAGE_LOGIN', apply_filters( 'wpml_object_id', 319, 'page', TRUE ) );
define( 'BL_PAGE_REGISTRATION', apply_filters( 'wpml_object_id', 321, 'page', TRUE ) );
define( 'BL_PAGE_REGISTRATION_SUCCESSFULL', apply_filters( 'wpml_object_id', 342, 'page', TRUE ) );
define( 'BL_PAGE_FORGOTTEN_PASSWORD_EMAIL_VERIFICATION', apply_filters( 'wpml_object_id', 366, 'page', TRUE ) );
define( 'BL_PAGE_FORGOTTEN_PASSWORD_EMAIL_VERIFICATION_SUCCESS', apply_filters( 'wpml_object_id', 370, 'page', TRUE ) );
define( 'BL_PAGE_FORGOTTEN_PASSWORD_CHANGE_PASSWORD', apply_filters( 'wpml_object_id', 373, 'page', TRUE ) );
define( 'BL_PAGE_FORGOTTEN_PASSWORD_CHANGE_PASSWORD_SUCCESS', apply_filters( 'wpml_object_id', 378, 'page', TRUE ) );
define( 'BL_PAGE_TRAINING_COURSES', apply_filters( 'wpml_object_id', 382, 'page', TRUE ) );
define( 'BL_PAGE_VIDEO_CATEGORIES', apply_filters( 'wpml_object_id', 271, 'page', TRUE ) );
define( 'BL_PAGE_LASH_INC_MAGAZINE', apply_filters( 'wpml_object_id', 3158, 'page', TRUE ) );
define( 'BL_PAGE_ABOUT_US', apply_filters( 'wpml_object_id', 132, 'page', TRUE ) );
define( 'BL_PAGE_CONTACT', apply_filters( 'wpml_object_id', 142, 'page', TRUE ) );
define( 'BL_PAGE_BESTBROWS', apply_filters( 'wpml_object_id', 17371, 'page', TRUE ) );

define( 'BL_PRODUCT_CAT_ID_OTHERS', apply_filters( 'wpml_object_id', 15, 'product_cat', TRUE ) );
define( 'BL_PRODUCT_CAT_ID_TRAINING_COURSES', apply_filters( 'wpml_object_id', 98, 'product_cat', TRUE ) );
define( 'BL_PRODUCT_CAT_ID_DORCAY', apply_filters( 'wpml_object_id', 85, 'product_cat', TRUE ) );
define( 'BL_PRODUCT_CAT_ID_LASH_INC', apply_filters( 'wpml_object_id', 138, 'product_cat', TRUE ) );
define( 'BL_PRODUCT_CAT_ID_GLAMCOR_LAPMS', apply_filters( 'wpml_object_id', 97, 'product_cat', TRUE ) );
define( 'BL_PRODUCT_CAT_ID_REFLECTOCIL', apply_filters( 'wpml_object_id', 430, 'product_cat', TRUE ) );
define( 'BL_PRODUCT_CAT_ID_TOOLS', apply_filters( 'wpml_object_id', 87, 'product_cat', TRUE ) );
define( 'BL_PRODUCT_CAT_ID_EYELASH_COLLECTION', apply_filters( 'wpml_object_id', 77, 'product_cat', TRUE ) );

define( 'BL_PRODUCT_ID_MAGAZIN', apply_filters( 'wpml_object_id', 1987, 'product', TRUE ) );

define( 'BL_EMAIL_ID_REGISTRATION_VERIFICATION', apply_filters( 'wpml_object_id', 326, 'auretto_email_editor', TRUE ) );
define( 'BL_EMAIL_ID_FORGOTTEN_PASSWORD_EMAIL_VERIFICATION', apply_filters( 'wpml_object_id', 368, 'auretto_email_editor', TRUE ) );
define( 'BL_EMAIL_ID_NEWSLETTER_SUBSCRIPTION_EMAIL_VERIFICATION', apply_filters( 'wpml_object_id', 1415, 'auretto_email_editor', TRUE ) );
define( 'BL_EMAIL_ID_DELETE_ORDER_ONLY_TRAINING_COURSE_EMAIL_VERIFICATION', apply_filters( 'wpml_object_id', 3995, 'auretto_email_editor', TRUE ) );
define( 'BL_EMAIL_ID_SEND_INVOICE_TO_CUSTOMER', apply_filters( 'wpml_object_id', 4970, 'auretto_email_editor', TRUE ) );
define( 'BL_EMAIL_ID_BLP_STYLIST_VERIFY_TO_USER', apply_filters( 'wpml_object_id', 5608, 'auretto_email_editor', TRUE ) );
define( 'BL_EMAIL_ID_HAPPY_BIRTHDAY_WITH_COUPON', apply_filters( 'wpml_object_id', 8062, 'auretto_email_editor', TRUE ) );
define( 'BL_EMAIL_ID_NEWSLETTER_SUBSCRIPTION_WITH_COUPON', apply_filters( 'wpml_object_id', 8278, 'auretto_email_editor', TRUE ) );

define( 'BL_VIDEO_BACKGROUND_IMAGE_ID', 261 );

define( 'BL_FRONTPAGE_FEATURED_PRODUCTS_POSTS_PER_PAGE', 6 );
define( 'BL_WEBSHOP_PRODUCTS_VIEW_LIMIT', 6 );
define( 'BL_SINGLE_PRODUCT_GALLERY_ROW_LIMIT', 4 );
define( 'BL_VIDEO_CATEGORY_POSTS_PER_PAGE', -1 );
define( 'BL_SINGLE_PRODUCT_RELATED_PRODUCT_LIMIT', 4 );
define( 'BL_SINGLE_POST_RELATED_POST_LIMIT', 4 );
define( 'BL_SINGLE_POST_RELATED_PRODUCT_LIMIT', 1 );
define( 'BL_PRODUCT_LIST_DEFAULT_VIEW', 'row' );
define( 'BL_PRODUCT_LIST_DEFAULT_ORDER', 'menu_order' );
define( 'BL_TRAINING_COURSES_MAX_DAY_WITHOUT_PAY', 3 );
define( 'BL_CRON_TRAINING_COURSES_CHECK_HASH', 'J6CvUMSB6GZ1tEsZYHBF' );
define( 'BL_CRON_TRAINING_COURSES_DELETE_HASH', 'qXcQiKRqPwbR7E4Y4kMN' );
define( 'BL_CRON_BIRTHDAY_EMAIL_WITH_COUPON_HASH', 'jbpzKjHD4OLCVDgqbeYk' );
define( 'BL_CRON_SCHEDULED_SALES_HASH', 'k0JMab1mi2BkOjqQ2z5o' );
define( 'BL_CRON_ALERT_SYSTEM_ADMINISTRATOR_ABOUT_PRICES_HASH', 'cWFtb44qAXvyo3YJ3Nbk' );

define( 'BL_NEWSLETTER_SUBSCRIPTION_FIX_HASH', 'Bj7cuwos4CTJiVBV1Riv' );
define( 'BL_NEWSLETTER_SUBSCRIPTION_FIX_UNSUBSCRIBE_HASH', 'r1zzuY31o21wIq6JJgul' );
define( 'BL_NEWSLETTER_SUBSCRIPTION_TABLE', 'newsletter_subscriptions' );

define( 'BL_CF7_ID_LASH_INC', 3165 );
define( 'BL_CF7_ID_BLP_HU', 5481 );
define( 'BL_CF7_ID_BLP_EN', 5434 );

define( 'BL_STOCK_LOG_TABLE', 'stock_log' );

define( 'BL_POS_IS_BL_VIEW', true );
define( 'BL_GENERATE_INVOICE_IS_ENABLED', true );

define( 'BL_DEFAULT_PRODUCT_IMAGE_URL', '/wp-content/uploads/2018/12/default.jpg' );

define( 'BL_HUN_VAT', 27 );

define( 'BL_POS_DEFAULT_USER_ID', 21 );

define( 'BL_SALE_VARIATIONS_LIMIT', 4 );

define( 'BL_PRODUCT_PRICE_LIMIT_FOR_CHECK', 200 );

define( 'BL_COUPON_PREFIX_HAPPY_BIRTHDAY', 'HB' );
define( 'BL_COUPON_PREFIX_NEWSLETTER_SUBSCRIPTION', 'NS' );
define( 'BL_COUPON_BITHDAY_EXPIRY_DAYS', 30 );
define( 'BL_COUPON_BITHDAY_AMOUNT', 10 );
define( 'BL_COUPON_NEWSLETTER_AMOUNT', 10 );

define( 'BL_IMAGE_NEW_PRODUCT_HU', 15850 );
define( 'BL_IMAGE_NEW_PRODUCT_SK', 10516 );
define( 'BL_IMAGE_NEW_PRODUCT_AT', 8011 );
define( 'BL_IMAGE_SALE_PRODUCT_HU', 17187 );
define( 'BL_IMAGE_SALE_PRODUCT_SK', 10736 );
define( 'BL_IMAGE_SALE_PRODUCT_AT', 8267 );

define ('MARKETER_USERNAME', 'bestlashes');
define ('MARKETER_API_KEY', '9b6f69a13a8bc75133ef4b6abb3d2879e9892ab7');
define ('MARKETER_LIST_BESTLASHES_ID', '1078');
define ('MARKETER_LIST_BESTBROWS_ID', '1088');

/*// Options panel settings to define
if( !empty( get_option('bl-posts-per-page-featured-products-on-frontpage') ) ){
	define( 'BL_FRONTPAGE_FEATURED_PRODUCTS_POSTS_PER_PAGE', (int)get_option('bl-posts-per-page-featured-products-on-frontpage') );
} else {
	define( 'BL_FRONTPAGE_FEATURED_PRODUCTS_POSTS_PER_PAGE', 6 );
}
if( !empty( get_option('bl-posts-per-page-webshop-products-view-limit') ) ){
	define( 'BL_WEBSHOP_PRODUCTS_VIEW_LIMIT', (int)get_option('bl-posts-per-page-webshop-products-view-limit') );
} else {
	define( 'BL_WEBSHOP_PRODUCTS_VIEW_LIMIT', 6 );
}
if( !empty( get_option('bl-posts-per-page-related-products-on-single-products') ) ){
	define( 'BL_SINGLE_PRODUCT_RELATED_PRODUCT_LIMIT', (int)get_option('bl-posts-per-page-related-products-on-single-products') );
} else {
	define( 'BL_SINGLE_PRODUCT_RELATED_PRODUCT_LIMIT', 4 );
}
if( !empty( get_option('bl-posts-per-page-related-posts-on-single-post-sidebar') ) ){
	define( 'BL_SINGLE_POST_RELATED_POST_LIMIT', (int)get_option('bl-posts-per-page-related-posts-on-single-post-sidebar') );
} else {
	define( 'BL_SINGLE_POST_RELATED_POST_LIMIT', 4 );
}
if( !empty( get_option('bl-posts-per-page-related-products-on-single-post-sidebar') ) ){
	define( 'BL_SINGLE_POST_RELATED_PRODUCT_LIMIT', (int)get_option('bl-posts-per-page-related-products-on-single-post-sidebar') );
} else {
	define( 'BL_SINGLE_POST_RELATED_PRODUCT_LIMIT', 1 );
}*/


/**************************/
/*	      Includes        */
/**************************/

//////////////////////// Header

$header_inc = get_template_directory() . '/includes/header.php';
if(file_exists ( $header_inc )){
    include $header_inc;
}

//////////////////////// Footer

$footer_inc = get_template_directory() . '/includes/footer.php';
if(file_exists ( $footer_inc )){
    include $footer_inc;
}

//////////////////////// Menu walkers

$menu_walkers_inc = get_template_directory() . '/includes/menu-walkers.php';
if(file_exists ( $menu_walkers_inc )){
    include $menu_walkers_inc;
}

//////////////////////// Visual Composer extra elements

$vc_extra_shortcodes_inc = get_template_directory() . '/includes/vc-extra-shortcodes.php';
if(file_exists ( $vc_extra_shortcodes_inc )){
    include $vc_extra_shortcodes_inc;
}

//////////////////////// Shortcodes

$shortcodes_inc = get_template_directory() . '/includes/shortcodes.php';
if(file_exists ( $shortcodes_inc )){
    include $shortcodes_inc;
}

//////////////////////// Helpers

$helpers_inc = get_template_directory() . '/includes/helpers.php';
if(file_exists ( $helpers_inc )){
    include $helpers_inc;
}

//////////////////////// Product

$product_inc = get_template_directory() . '/includes/product.php';
if(file_exists ( $product_inc )){
    include $product_inc;
}

//////////////////////// Training course

$training_course_inc = get_template_directory() . '/includes/training-course.php';
if(file_exists ( $training_course_inc )){
    include $training_course_inc;
}

//////////////////////// Woocommerce

$woocommerce_inc = get_template_directory() . '/includes/woocommerce.php';
if(file_exists ( $woocommerce_inc )){
    include $woocommerce_inc;
}

//////////////////////// Videos

$videos_inc = get_template_directory() . '/includes/videos.php';
if(file_exists ( $videos_inc )){
    include $videos_inc;
}

//////////////////////// Breadcrumb

$breadcrumb_inc = get_template_directory() . '/includes/breadcrumb.php';
if(file_exists ( $breadcrumb_inc )){
    include $breadcrumb_inc;
}

//////////////////////// Post

$post_inc = get_template_directory() . '/includes/post.php';
if(file_exists ( $post_inc )){
    include $post_inc;
}

//////////////////////// Page

$page_inc = get_template_directory() . '/includes/page.php';
if(file_exists ( $page_inc )){
    include $page_inc;
}

//////////////////////// Widgets

$widgets_inc = get_template_directory() . '/includes/widgets.php';
if(file_exists ( $widgets_inc )){
    include $widgets_inc;
}

//////////////////////// Users

$users_inc = get_template_directory() . '/includes/users.php';
if(file_exists ( $users_inc )){
    include $users_inc;
}

//////////////////////// Users meta

$users_meta_inc = get_template_directory() . '/includes/users-meta.php';
if(file_exists ( $users_meta_inc )){
    include $users_meta_inc;
}

//////////////////////// Popups

$popups_inc = get_template_directory() . '/template-parts/popups.php';
if(file_exists ( $popups_inc )){
    include $popups_inc;
}

//////////////////////// Options panel

$options_panel_inc = get_template_directory() . '/includes/options-panel.php';
if(file_exists ( $options_panel_inc )){
    include $options_panel_inc;
}

//////////////////////// Options panel ajax

$options_panel_ajax_inc = get_template_directory() . '/includes/options-panel-ajax.php';
if(file_exists ( $options_panel_ajax_inc )){
    include $options_panel_ajax_inc;
}

//////////////////////// Newsletter

$newsletter_inc = get_template_directory() . '/includes/newsletter.php';
if(file_exists ( $newsletter_inc )){
    include $newsletter_inc;
}

//////////////////////// Product price edit

$product_price_edit_inc = get_template_directory() . '/includes/product-price-edit.php';
if(file_exists ( $product_price_edit_inc )){
    include $product_price_edit_inc;
}

//////////////////////// Woocommerce emails

$woocommerce_emails_inc = get_template_directory() . '/includes/woocommerce-emails.php';
if(file_exists ( $woocommerce_emails_inc )){
    include $woocommerce_emails_inc;
}

//////////////////////// Order

$order_inc = get_template_directory() . '/includes/order.php';
if(file_exists ( $order_inc )){
    include $order_inc;
}

//////////////////////// Invoice

$invoice_inc = get_template_directory() . '/includes/invoice.php';
if(file_exists ( $invoice_inc )){
    include $invoice_inc;
}

//////////////////////// Cron

$cron_inc = get_template_directory() . '/includes/cron.php';
if(file_exists ( $cron_inc )){
    include $cron_inc;
}

//////////////////////// POS

$pos_inc = get_template_directory() . '/includes/pos.php';
if(file_exists ( $pos_inc )){
    include $pos_inc;
}

//////////////////////// Media library

$media_library_inc = get_template_directory() . '/includes/media-library.php';
if(file_exists ( $media_library_inc )){
    include $media_library_inc;
}

if (is_admin()) {
    add_action('admin_enqueue_scripts', 'wp_enqueue_media');
}

//////////////////////// Trainer

$trainer_inc = get_template_directory() . '/includes/trainer.php';
if(file_exists ( $trainer_inc )){
    include $trainer_inc;
}

//////////////////////// Gift products

$gift_products_inc = get_template_directory() . '/includes/gift-products.php';
if(file_exists ( $gift_products_inc )){
    include $gift_products_inc;
}

//////////////////////// Product category

$product_category_inc = get_template_directory() . '/includes/product-category.php';
if(file_exists ( $product_category_inc )){
    include $product_category_inc;
}

//////////////////////// Popups

$popups_inc = get_template_directory() . '/includes/popups.php';
if(file_exists ( $popups_inc )){
    include $popups_inc;
}

//////////////////////// BLP - Márkanagykövet

$blp_inc = get_template_directory() . '/includes/blp-stylist.php';
if(file_exists ( $blp_inc )){
    include $blp_inc;
}

//////////////////////// Coupons

$coupons_inc = get_template_directory() . '/includes/coupons.php';
if(file_exists ( $coupons_inc )){
    include $coupons_inc;
}

//////////////////////// Inventory update

$inventory_update_inc = get_template_directory() . '/includes/inventory-update.php';
if(file_exists ( $inventory_update_inc )){
    include $inventory_update_inc;
}


/**************************/
/*	  Settings for WP	  */
/**************************/

add_action( 'after_setup_theme', 'bl_theme_setup' );
function bl_theme_setup() {
    // Load textdomain
    load_theme_textdomain( 'bl', get_template_directory() . '/languages' );

    // Thumbnails support
    add_theme_support( 'post-thumbnails' );

    // Woocommerce
    add_theme_support( 'woocommerce' );

    // Disable admin bar
    // add_filter('show_admin_bar', '__return_false');

    // Image sizes
    add_image_size( 'product-featured', 600, 800, true );
    add_image_size( 'product-featured-small', 300, 400, true );
    add_image_size( 'product-category-boxed', 560, 372, true );
    add_image_size( 'product-list-boxed', 466, 620, true );
    add_image_size( 'product-list-row', 816, 1088, true );
    add_image_size( 'product-gallery', 170, 220, true );
    add_image_size( 'textbox-image', 1200, 800, true );
    add_image_size( 'video-thumbnail', 440, 300, true );
    add_image_size( 'blog-thumbnail', 1060, 660, true );
    add_image_size( 'trainer-big', 800, 1200, true );
    add_image_size( 'slider-hero', 600, 600, true );
}

add_action( 'init', 'bl_startsession' );
function bl_startsession(){
    if ( !session_id() ) {
        session_start();
    }
}


add_filter( 'body_class', 'bl_body_classes' );
function bl_body_classes( $classes ) {
    global $post;

    $classes[] = $post->post_name;

    if ($post->ID == BL_PAGE_BESTBROWS) {
        $classes[] = 'bestbrows';
        $classes[] = 'best-brows';
    }

    return $classes;
}


// Add SVG support
add_filter('upload_mimes', 'bl_mime_types');
function bl_mime_types( $mimes ) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}


//add_action('init', 'bl_product_category_params_rewrite', 10, 0);
function bl_product_category_params_rewrite() {
    global $wp_rewrite;

    /*echo '<pre>';
    var_dump($wp_rewrite);
    echo '</pre>';*/
    //add_rewrite_rule( '^product-category/([^/]*)/([^/]*)/?', 'index.php?product_cat='. $matches[1] .'&view='. $matches[2], 'top' );
    //flush_rewrite_rules();

    //global $wp_rewrite;
    //$wp_rewrite->flush_rules( false );
}


/**
 * Disable the emoji's
 */
function bl_disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'bl_disable_emojis_tinymce' );
}
add_action( 'init', 'bl_disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function bl_disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

add_action( 'wp_default_scripts', 'bl_remove_jquery_migrate' );
function bl_remove_jquery_migrate( $scripts ) {
    if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
        $script = $scripts->registered['jquery'];

        if ( $script->deps ) { // Check whether the script has any dependencies
            $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
        }
    }
}




//////////////////////// Enqueue scripts and Styles on Front-end

add_action( 'wp_enqueue_scripts', 'bl_enqueue_theme_styles');
function bl_enqueue_theme_styles() {
    // CSS
    wp_enqueue_style( 'normalize', get_template_directory_uri() .'/css/normalize.css' );
    wp_enqueue_style( 'components', get_template_directory_uri() .'/css/components.css' );
    wp_enqueue_style( 'best-lashes', get_template_directory_uri() .'/css/best-lashes.css' );
    wp_enqueue_style( 'owl-carousel', get_template_directory_uri() .'/css/owl.carousel.min.css' );
    wp_enqueue_style( 'owl-carousel-theme-default', get_template_directory_uri() .'/css/owl.theme.default.min.css' );
    wp_enqueue_style( 'select2', get_template_directory_uri() .'/css/select2.min.css' );
    wp_enqueue_style( 'style', get_stylesheet_uri() );

    // JS
    if( is_page(BL_PAGE_CONTACT) ){
        wp_enqueue_script( 'best-lashes', 'https://maps.googleapis.com/maps/api/js?key=' . BL_GOOGLE_MAPS_API_KEY, array('jquery'), '1.0', true );
    }

    //wp_enqueue_script( 'best-lashes', get_stylesheet_directory_uri() . '/js/best-lashes.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'owl-carousel', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'scrollTo', get_stylesheet_directory_uri() . '/js/jquery.scrollTo.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'zoomit', get_stylesheet_directory_uri() . '/js/jquery.zoomit.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'touchswipe', get_stylesheet_directory_uri() . '/js/jquery.touchswipe.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'select2', get_stylesheet_directory_uri() . '/js/select2.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'site-js', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery', 'owl-carousel', 'scrollTo', 'select2', 'zoomit' ), '1.0', true );

    wp_localize_script( 'site-js', 'ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
}


//////////////////////// Enqueue scripts and Styles on Back-end

add_action('admin_enqueue_scripts', 'bl_enqueue_admin_styles');
function bl_enqueue_admin_styles() {
    // CSS
    wp_enqueue_style( 'select2', get_template_directory_uri() .'/css/select2.min.css' );
    wp_enqueue_style( 'datetimepicker', get_template_directory_uri() .'/css/admin/jquery.datetimepicker.min.css' );
    wp_enqueue_style( 'admin_aw_css', get_stylesheet_directory_uri() . '/css/admin/admin-aw-settings.css');
    wp_enqueue_style( 'bl_admin_css', get_stylesheet_directory_uri() . '/css/admin/admin.css');

    // JS
    wp_enqueue_script( 'select2', get_stylesheet_directory_uri() . '/js/select2.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'datetimepicker', get_stylesheet_directory_uri() . '/js/jquery.datetimepicker.full.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'bl_admin_aw_js', get_stylesheet_directory_uri() . '/js/admin-aw-settings.js', array('jquery', 'datetimepicker'), '1.0', true );
    wp_enqueue_script( 'bl_admin_js', get_stylesheet_directory_uri() . '/js/admin.js', array('jquery', 'datetimepicker', 'bl_admin_aw_js', 'select2'), '1.0', true );
}



/**************************/
/*			Menu 		  */
/**************************/

add_action ('init', 'bl_create_menus');
function bl_create_menus() {
    register_nav_menus ( array(
        "main_menu" => __( 'Main menu', 'bl' )
    ) );
}


/**************************/
/*			Sidebars 	  */
/**************************/

add_action ('widgets_init', 'bl_register_sidebars');
function bl_register_sidebars() {

    register_sidebar(array(
        'name' => __( 'Single post sidebar', 'bl' ),
        'id' => 'single-post-sidebar',
        'class' => '',
        'before_widget' => '<div class="single-post-widget">',
        'after_widget' => '</div>',
        'before_title' => '<div class="related-blogpost-heading"></div>',
        'after_title' => '</div></div>',
    ));

    register_sidebar(array(
        'name' => __( 'Footer sidebar - left', 'bl' ),
        'id' => 'footer-sidebar-left',
        'class' => '',
        'before_widget' => '<div class="col mobilehide w-col w-col-2 w-col-small-small-stack w-col-tiny-tiny-stack w-col-stack footer-widget"><div class="margin w-clearfix">',
        'after_widget' => '</div></div>',
        'before_title' => '<h4 class="heading-04">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'name' => __( 'Footer sidebar - right', 'bl' ),
        'id' => 'footer-sidebar-right',
        'class' => '',
        'before_widget' => '<div class="col w-col w-col-6 w-col-small-small-stack w-col-tiny-tiny-stack w-col-stack"><div class="margin">',
        'after_widget' => '</div></div>',
        'before_title' => '<h4 class="heading-04">',
        'after_title' => '</h4>',
    ));

    register_sidebar(array(
        'name' => __( 'Top navigation currency switcher - right', 'bl' ),
        'id' => 'header-currency-switcher'
    ));

    register_sidebar(array(
        'name' => __( 'WooCommerce My Account Navigation - bottom', 'bl' ),
        'id' => 'woo-myaccount-nav-bottom'
    ));
}

/* TEMP: Get empty sku products */
//add_action('wp', 'bl_get_empty_sku_products');
function bl_get_empty_sku_products(){
    if( isset( $_GET['get-products-without-sku'] ) && $_GET['get-products-without-sku'] == 'DOxcindmtMqOfPXV7Wy2' ){
        $args = array();
        $args['post_type'] = array( 'product', 'product_variation' );
        $args['posts_per_page'] = '-1';
        $args['post_status'] = 'publish';
        $args['fields'] = 'ids';
        $args['meta_query'] = array();
        $args['meta_query']['relation'] = 'AND';
        $args['meta_query'][] = array(
            'key' => '_sku',
            'value' => '',
        );
        /*$args['meta_query'][] = array(
            'key' => 'training_course',
            'value' => '1',
            'compare' => '!='
        );*/
        /*$args['meta_query'][] = array(
            'key' => 'training_course',
            'compare' => 'NOT EXISTS'
        );*/
        $args['order'] = 'ASC';
        $products_loop = new WP_Query( $args );
        echo '<table>';

        if( $products_loop->have_posts() ){
            while ( $products_loop->have_posts() ) {
                $prod_id = $products_loop->next_post();

                $_product = wc_get_product( $prod_id );

                if( $_product->is_type( 'variation' ) || $_product->is_type( 'simple' ) ){

                    echo '<tr>';
                    echo '<td>'. $prod_id .'</td><td></td><td>'. get_the_title( $prod_id ) .'</td>';

                    if( $_product->is_type( 'variation' ) ){
                        $attributes = $_product->get_attributes();

                        if( !empty( $attributes ) ){
                            foreach ( $attributes as $tax => $term_slug ) {
                                $taxonomy = get_taxonomy( $tax );
                                $term = get_terms(
                                    array(
                                        'taxonomy' => $tax,
                                        'slug' => $term_slug
                                    )
                                );


                                echo '<td>'. $taxonomy->labels->name_admin_bar .'</td><td>'. $term[0]->name .'</td>';
                            }
                        }
                    }

                    echo '</tr>';
                }
            }
        }
        echo '</table>';

        die();
    }
}


/* Fix featured products transient */
add_filter( 'pre_transient_wc_featured_products', 'bl_fix_featured_products_transient' );
function bl_fix_featured_products_transient( $transient ){
    if( empty( $transient ) ){
        if( class_exists('WC_Data_Store') ){
            $data_store           = WC_Data_Store::load( 'product' );
            $featured             = $data_store->get_featured_product_ids();
            $product_ids          = array_keys( $featured );
            $parent_ids           = array_values( array_filter( $featured ) );
            $featured_product_ids = array_unique( array_merge( $product_ids, $parent_ids ) );

            return $featured_product_ids;
        }
    }

    return $transient;
}

// Extend the auto logout period
add_filter( 'auth_cookie_expiration', 'bl_keep_me_logged_in_for_30_days' );
function bl_keep_me_logged_in_for_30_days( $expire ) {
    return 2592000; // 30 days in seconds
}

/* Automatically activate every newly registered user account */
add_action( 'user_register', 'bl_activate_new_user_account' );
function bl_activate_new_user_account( $user_id ) {
    update_user_meta( $user_id, 'activated_account', '1' );
}

/* Modify the new user notification email */
add_action( 'wp_new_user_notification_email', 'bl_custom_new_user_notification_email', 10, 3);
function bl_custom_new_user_notification_email($wp_new_user_notification_email, $user, $blogname) {
    return $wp_new_user_notification_email;
}

function list_hooked_functions($tag=false){
    global $wp_filter;

    $hook = $wp_filter;
    if ($tag) {
        if (!isset($hook[$tag])) {
            trigger_error("Nothing found for '$tag' hook", E_USER_WARNING);
            return;
        }

        echo '<pre>';
        echo "<br /><strong>$tag</strong><br />";

        ksort($hook[$tag]->callbacks);

        foreach($hook[$tag]->callbacks as $priority => $function){
            echo $priority . "<br />";
            foreach($function as $name => $properties) echo "$name<br />";
        }

        echo '</pre>';
    }
    else {
        if (is_array($hook)) {
            ksort($hook);
        }

        echo '<pre>';
        foreach($hook as $tag => $priority){
            echo "<br /><strong>$tag</strong><br />";
            if (is_array($priority)) {
                ksort($priority);
            }
            foreach($priority as $priority => $function){
                echo $priority . "<br />";
                foreach($function as $name => $properties) echo "$name<br />";
            }
        }
        echo '</pre>';
    }

    return;
}

function remove_hashed_filter($tag, $hashed_filter) {
    global $wp_filter;

    if ($wp_filter[$tag]) {
        foreach ($wp_filter[$tag]->callbacks as $priority => $function) {
            foreach($function as $name => $properties) {
                if (strpos($name, $hashed_filter) !== false) {
                    remove_filter($tag, $name);
                }
            }
        }
    }

    return;
}

remove_hashed_filter('wc_add_to_cart_message', 'custom_add_to_cart_message');

//list_hooked_functions();

//add_action( 'shutdown', function(){
//    foreach( $GLOBALS['wp_actions'] as $action => $count )
//        printf( '%s (%d) <br/>' . PHP_EOL, $action, $count );
//
//});


/**
 * Add a new country to countries list
 */
add_filter( 'woocommerce_countries',  'bl_add_country_to_shipping_list' );
function bl_add_country_to_shipping_list( $countries ) {
    $new_countries = array(
        'CH'  => __( 'Switzerland', 'woocommerce' ),
        'FI'  => __( 'Finland', 'woocommerce' ),
        'LT'  => __( 'Lithuania', 'woocommerce' ),
        'ES'  => __( 'Spain', 'woocommerce' ),
        'HU'  => __( 'Hungary', 'woocommerce' ),
    );

    return array_merge( $countries, $new_countries );
}

add_filter( 'woocommerce_continents', 'bl_add_country_to_continents' );
function bl_add_country_to_continents( $continents ) {
    $continents['EU']['countries'][] = 'CH';
    $continents['EU']['countries'][] = 'FI';
    $continents['EU']['countries'][] = 'LT';
    $continents['EU']['countries'][] = 'ES';
    $continents['EU']['countries'][] = 'HU';
    return $continents;
}