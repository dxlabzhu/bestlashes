jQuery(function($) {

	var $b = $(document.body);
	var $w = $(window);

	/* Main menu */
	var $navWhiteBackground = $('header > .nav-white-background')
	$('header .nav-dropdown').hover(function(){
		var $this = $(this);
		var $dropdownToggle = $this.children('.nav-dropdown-toggle');
		requestAnimationFrame(function() {
			var idealHeight = 0;
			$dropdownToggle.children().each(function(){ idealHeight += $this.height() });
			$dropdownToggle.css('height', idealHeight + 'px');
			$navWhiteBackground.css('height', 90 + idealHeight + 'px');
		});
	}, function(){
		var $dropdownToggle = $(this).children('.nav-dropdown-toggle');
		requestAnimationFrame(function() {
			$dropdownToggle.css('height', '');
			$navWhiteBackground.css('height', '');
		})
	});

	/* Home Slider */
	if ($b.hasClass('home')) {
		var startSlide = 0;
		var $homeHero = $('.home-hero').data('slide', startSlide);
		var homeHeroSlideCount = $homeHero.find('.home-slider-buttons .home-slider-button').length;

		var $homeSliderTextContainer = $homeHero.find('.home-slider-text-container').attr('data-slide', startSlide);
		var $homeSliderButtons = $homeHero.find('.home-slider-buttons').attr('data-slide', startSlide);
		var $homeHeroParallaxSliderContainer = $homeHero.find('.home-hero-parallax-slider').attr('data-slide', startSlide);
		var $homeHeroBackgroundSliderContainer = $homeHero.find('.home-hero-background-slider').attr('data-slide', startSlide);

		$homeHero.find('.arrow-container-left').on('click', function(){
			$homeHero.trigger('bl.slider.prev');
		});
		$homeHero.find('.arrow-container-right').on('click', function(){
			$homeHero.trigger('bl.slider.next');
		});
		$homeSliderButtons.find('.home-slider-button').on('click', function(e){
			e.preventDefault();
			$homeHero.trigger('bl.slider.to', [ $(this).data('slide') ]);
		});

		$homeHero.on('bl.slider.next', function(){
			var currentSlide = $('.home-hero').data('slide');
			var newSlide = (currentSlide + 1) % homeHeroSlideCount;
			$homeHero.trigger('bl.slider.to', [newSlide]);
		});
		$homeHero.on('bl.slider.prev', function(){
			var currentSlide = $('.home-hero').data('slide');
			var newSlide = (currentSlide - 1 + homeHeroSlideCount) % homeHeroSlideCount;
			$homeHero.trigger('bl.slider.to', [newSlide]);
		});
		$homeHero.on('bl.slider.to', function(e, newSlide){
			$homeHero.data('slide', newSlide);
			$homeSliderTextContainer.attr('data-slide', newSlide);
			$homeSliderButtons.attr('data-slide', newSlide);
			$homeHeroParallaxSliderContainer.attr('data-slide', newSlide);
			$homeHeroBackgroundSliderContainer.attr('data-slide', newSlide);
			setNextHomeSliderAutoSlide();
		});

		$homeSliderButtons.find('.round-button').on('click', function(e){
			e.preventDefault();
			jQuery.scrollTo($(this).attr('href'), 600);
		});

		// Auto slide
		var homeSliderAutoSlideTime = 6000;
		var homeSliderAutoSlideTimeout;
		function homeSliderAutoSlide() {
			$homeHero.trigger('bl.slider.next');
		}
		function setNextHomeSliderAutoSlide() {
			if (homeSliderAutoSlideTimeout) {
				clearTimeout(homeSliderAutoSlideTimeout);
			}
			homeSliderAutoSlideTimeout = setTimeout(homeSliderAutoSlide, homeSliderAutoSlideTime);
		}
		setNextHomeSliderAutoSlide();
	}

	/* Home page scroll */
	if ($b.hasClass('home')) {
		var $homeHeroParallaxSlider = $('.home-hero-parallax-slider').css('will-change', 'transform');
		var $homeHeroBackgroundSlider = $('.home-hero .home-hero-background-slider').css('will-change', 'opacity');
		var $homeSliderButtonContainer = $homeHero.find('.home-slider-button-container').css('will-change', 'opacity');
		var $homeIntroParallaxImageContainer = $('.home-intro-parallax-image-container').css('will-change', 'transform');

		$w.scroll(function(){
			var currentScrollTop = $w.scrollTop();
			var scrollRatio = Math.max(0, Math.min(1, currentScrollTop / ($b.outerHeight() - window.innerHeight) ));
			requestAnimationFrame(function(){
				if (window.innerWidth < 992) {
					$homeHeroParallaxSlider.css('transform', 'none');
					$homeHeroBackgroundSlider.css('display', 'none');
					$homeSliderButtonContainer.css('opacity', 1);
					$homeSliderButtonContainer.css('z-index', 1);
					$homeIntroParallaxImageContainer.css('transform', 'none');
				} else {
					$homeHeroParallaxSlider.css('transform', 'translateY('+ (scrollRatio*800) +'px)');
					$homeHeroBackgroundSlider.css('opacity', Math.max(0, 0.15 - scrollRatio * 0.15 / 0.2) );
					$homeHeroBackgroundSlider.css('visibility', scrollRatio <= 0.2 ? 'visible' : 'hidden');
					$homeSliderButtonContainer.css('opacity', Math.max(0, 1 - scrollRatio / 0.1) );
					$homeSliderButtonContainer.css('z-index', scrollRatio <= 0.1 ? 99 : -1);
					$homeIntroParallaxImageContainer.css('transform', 'translateY('+ (-scrollRatio*600) +'px)');
				}
			});
		}).trigger('scroll');
	}

	/* Mobile menu */
	$('a.hamburger-container').click(function(e){
		e.preventDefault();
		$('.nav-container .menu-container.main-menu').slideToggle();
	});

	/* Cookie bar */
	var localStorageCookieKey = 'cookie_warning';
	if (localStorage.getItem(localStorageCookieKey) != '1') {
		var $elementsToAnimate = $('body > .cookies, .home-slider-button-container, .right-side-menu');
		$elementsToAnimate.addClass('cookie-in');
		$('body > .cookies a:not(.privacy-policy)').click(function(e){
			e.preventDefault();
			$elementsToAnimate.removeClass('cookie-in');
			localStorage.setItem(localStorageCookieKey, '1');
		});
	}

	/* Home page - product slider */
	var productSliderSlideTime = 4000;
	$('.product-slider-container').each(function() {
		$container = $(this);
		$container.children().children().css('will-change', 'transition');
		var $productsRow = $container.children().children('.row').attr('data-offset', 0);
		$container.closest('.wpb_wrapper').find('.arrow-container-left a, .arrow-container-right a').click(function() {
			var $this = $(this);
			var itemWidth = 100 / 6;
			$productsRow.addClass('animating');
			var newOffset = parseFloat($productsRow.attr('data-offset')) + ($this.parent().hasClass('arrow-container-left') ? 1 : -1) * itemWidth;
			if (newOffset < -2 * itemWidth) {
				newOffset = 0;
			} else if (newOffset > 0) {
				newOffset = -2 * itemWidth;
			}
			if (window.innerWidth < 992) {
				newOffset = 0;
			}
			$productsRow.attr('data-offset', newOffset).css('transform', 'translateX('+ newOffset +'%)');
			resetProductSliderTimeout($this.closest('.wpb_wrapper').find('.product-slider-container'));
		});
		resetProductSliderTimeout($container);
	});
	function resetProductSliderTimeout($container) {
		var storedTimeout = $container.data('slider-timeout');
		if (storedTimeout) {
			clearTimeout(storedTimeout);
		}
		var newTimeout = setTimeout(function(){
			$container.closest('.wpb_wrapper').find('.arrow-container-right a').click();
		}, productSliderSlideTime);
		$container.data('slider-timeout', newTimeout);
	}

	/* Owl carousel - video single */
	if( $('.video-carousel-wrapper').find('.video-thumb-container').length > 6 ){
		$('.video-carousel-wrapper').owlCarousel({
			stagePadding: 0,
		    smartSpeed: 450,
		    loop: true,
		    nav: true,
		    navText: ["<div class='arrow-container-left phonehide'><a class='round-button w-button'>←</a></div>","<div class='arrow-container-right phonehide'><a class='round-button w-button'>→</a></div>"],
		    responsive : {
		    	0: {
		    		items: 1
		    	},
		    	399: {
		    		items: 2
		    	},
		    	768: {
		    		items: 4
		    	},
		    	992: {
		    		items: 5
		    	},
		    	1300: {
		    		items: 6
		    	}
		    },
		});
	}
	
	/* Select2 */
	$('select.select-2.table-header-select').select2({
		minimumResultsForSearch: Infinity
	});
	$('select.select-2.bl-form-select').select2({
		minimumResultsForSearch: Infinity
	});
	$('body.page-template-template-registration select').select2({
		minimumResultsForSearch: Infinity
	});
	/*$('select.select-2.table-header-select').on('select2:open', function (e) {
		$('.select2-dropdown').hide();
		setTimeout(function(){ $('.select2-dropdown').slideDown("slow"); }, 200);
	});
	$('select.select-2.table-header-select').on('select2:closing', function (e) {
		$('.select2-dropdown').slideUp("slow");
		setTimeout(function(){ $('.select2-dropdown').hide(); }, 200);
	});*/

	// Header search
	$('body').on('click', 'header .nav-container .open-seach-popup', function(e){
		e.preventDefault();

		showPopup('search-popup');
	});

	$('body').on('click', '.popup-wrapper.search-popup.active .search-container a.search-submit', function(e){
		$(this).parents('form.search-form').submit();
	});

	/* Single video page */
	$('body.single-video').on('click', '.video-slide-container .video-thumb-container', function(e){
		e.preventDefault();
		
		var $this = $(this),
			$new_video_id = $this.data('video-id'),
			$new_video_image = $this.data('video-image'),
			$main_video_container = $('.video-block .video-container div.video'),
			$main_video_id = $main_video_container.data('video-id'),
			$main_video_image = $main_video_container.data('video-image');

		$new_video = '<iframe src="https://www.youtube.com/embed/'+ $new_video_id +'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
		
		// Change old container
		/*$this.data('video-id', $main_video_id);
		$this.data('video-image', $main_video_image);
		$this.find('img').attr('src', $main_video_image);*/
		if( $this.parents('.video-carousel-wrapper').hasClass('owl-carousel') ){
			$this.parents('.owl-item').addClass('selected').siblings().removeClass('selected');
		} else {
			$this.addClass('selected').siblings().removeClass('selected');
		}

		// Change new container
		/*$main_video_container.data('video-id', $new_video_id);
		$main_video_container.data('video-image', $new_video_image);*/

		$main_video_container.html( $new_video );
	});


	$('body.single-video').on('click', '.video-block .arrow-container-right, .video-block .arrow-container-left', function(e){
		e.preventDefault();

		var $this = $(this),
			$video_slider_container = $('.video-slide-container');


		if( $this.hasClass('arrow-container-right') ){
			if( $video_slider_container.find('.owl-item.selected').length ){
				$video_slider_container.find('.owl-item.selected').next().find('.video-thumb-container').click();
			} else {
				$video_slider_container.find('.owl-item.active:first').next().find('.video-thumb-container').click();
			}
		} else {
			if( $video_slider_container.find('.owl-item.selected').length ){
				$video_slider_container.find('.owl-item.selected').prev().find('.video-thumb-container').click();
			} else {
				$video_slider_container.find('.owl-item.active:first').prev().find('.video-thumb-container').click();
			}
		}
	});

	/* Single trainer */
	$('body.single-trainer').on('click', '.video-slide-container .video-thumb-container', function(e){
		e.preventDefault();
		
		var $this = $(this),
			$new_image = $this.data('image'),
			$main_container = $('.video-block .video-container'),
			$main_image = $main_container.data('image');

		// Change old container
		/*$this.data('image', $main_image);
		$this.html( $main_image );*/
		if( $this.parents('.video-carousel-wrapper').hasClass('owl-carousel') ){
			$this.parents('.owl-item').addClass('selected').siblings().removeClass('selected');
		} else {
			$this.addClass('selected').siblings().removeClass('selected');
		}

		// Change new container
		/*$main_container.data('image', $new_image);*/
		$main_container.html( $new_image );
	});


	$('body.single-trainer').on('click', '.video-block .arrow-container-right, .video-block .arrow-container-left', function(e){
		e.preventDefault();

		var $this = $(this),
			$image_slider_container = $('.video-slide-container');

		if( $image_slider_container.find('.video-carousel-wrapper').hasClass('owl-carousel') ){
			if( $this.hasClass('arrow-container-right') ){
				if( $image_slider_container.find('.owl-item.selected').length ){
					$image_slider_container.find('.owl-item.selected').next().find('.video-thumb-container').click();
				} else {
					$image_slider_container.find('.owl-item.active:first').next().find('.video-thumb-container').click();
				}
			} else {
				if( $image_slider_container.find('.owl-item.selected').length ){
					$image_slider_container.find('.owl-item.selected').prev().find('.video-thumb-container').click();
				} else {
					$image_slider_container.find('.owl-item.active:first').prev().find('.video-thumb-container').click();
				}
			}
		} else {
			if( $this.hasClass('arrow-container-right') ){
				if( $image_slider_container.find('.video-thumb-container.selected').next().length ){
					$image_slider_container.find('.video-thumb-container.selected').next().click();
				} else {
					$image_slider_container.find('.video-thumb-container:first-of-type').click();
				}
			} else {
				if( $image_slider_container.find('.video-thumb-container.selected').prev().length ){
					$image_slider_container.find('.video-thumb-container.selected').prev().click();
				} else {
					$image_slider_container.find('.video-thumb-container:last-of-type').click();
				}
			}
		}
	});


	/* First block slide-to */
	$('body').on('click', '.floating-boxes-content .whtie-floating-textbox a, .subpage-hero.bestbrows a.bestbrows-hero-readmore', function(e){
		if( $(this).attr('href').substring( 0, 1 ) == '#' ){
			e.preventDefault();

			if( $(window).outerWidth() > 1350 ){
				$('body').scrollTo( $(this).attr('href'), 800, { offset: -110 } );
			} else {
				$('body').scrollTo( $(this).attr('href'), 800 );
			}
		}
	});

	/* Product list - sidebar category slidedown */
	$('body').on( 'click', '.sidebar-wrapper .sidebar-dropdown-trigger', function(e){
		e.preventDefault();
		var $dropdownToggle = $(this).siblings('.dropdown-toggle'),
			idealHeight = 0;

		if( !$dropdownToggle.hasClass('open') ){
			$dropdownToggle.children().each(function(){ idealHeight += $(this).outerHeight() });
			$dropdownToggle.parents('.sidebar-wrapper').css('height', '+=' + idealHeight + 'px');
			$dropdownToggle.addClass('open');
		} else {
			$dropdownToggle.parents('.sidebar-wrapper').css('height', '-=' + $dropdownToggle.outerHeight() + 'px');
			$dropdownToggle.removeClass('open');
		}

		$dropdownToggle.css('height', idealHeight + 'px');
	});

	/* Product list mobile - sidebar category slidedown */
	$('body').on( 'click', '.sidebar-menu-container a.sidebar-button', function(e){
		e.preventDefault();
		var $dropdownToggle = $(this).parents('.sidebar-menu-container').siblings('.sidebar-wrapper'),
			idealHeight = 0;

		if( !$dropdownToggle.hasClass('open') ){
			$dropdownToggle.children().each(function(){ idealHeight += $(this).outerHeight() });
			$dropdownToggle.addClass('open');
		} else {
			$dropdownToggle.removeClass('open');
		}

		$dropdownToggle.css('height', idealHeight + 'px');
	});
	

	/* Product list - product variation options */
	$('body').on('click', 'a.productlist-option-button', function(e){
		e.preventDefault();
		var $dropdownToggle = $(this).siblings('.productlist-options'),
			idealHeight = 0;

		if( !$dropdownToggle.hasClass('open') ){
			$dropdownToggle.find('.option-container').each(function(){ idealHeight += $(this).outerHeight(true) });
			$dropdownToggle.css('height', idealHeight + 'px');

			setTimeout( function(){ $dropdownToggle.css('height', 'auto'); }, 500 );

			$dropdownToggle.addClass('open');
		} else {
			currentHeight = $dropdownToggle.height();
			$dropdownToggle.css('height', currentHeight);

			setTimeout( function(){ $dropdownToggle.css('height', idealHeight + 'px'); }, 100 );
			$dropdownToggle.removeClass('open');
		}
	});

	/* Product list - filters */
	$('body').on('click', 'form.product-filter a.shop-layout', function(e){
		e.preventDefault();

		if( !$(this).hasClass('active') ){
			$(this).parents('form.product-filter').find('input[type="hidden"].product-view').val( $(this).data('view') );
			$(this).addClass('active').siblings().removeClass('active');

			$(this).parents('form.product-filter').submit();
		}
	});

	$('body').on('change', 'form.product-filter select', function(e){
		$(this).parents('form.product-filter').submit();
	});

	/* Single product - image zoom */
	if ($b.hasClass('single-product')) {
		zoomit_init();
	}

	/* Single product - variable product */
	$('body.single-product, body.tax-product_cat, body.page-template-template-webshop').on( 'click', '.option-container .option-trigger', function(e){
		e.preventDefault();
		var $dropdownToggle = $(this).siblings('.dropdown-toggle'),
			idealHeight = 0;

		if( !$dropdownToggle.hasClass('open') ){
			$dropdownToggle.children(':not(.not-avaiable)').each(function(){ idealHeight += $(this).outerHeight() });	
			$dropdownToggle.addClass('open');
		} else {
			$dropdownToggle.removeClass('open');
		}

		$dropdownToggle.css('height', idealHeight + 'px');
	});

	$('body.single-product, body.tax-product_cat, body.page-template-template-webshop').on( 'click', '.option-container .option-button-conainer .option-button:not(.not-avaiable):not(.delete-selection)', function(e){
		e.preventDefault();

		var $this = $(this),
			label = $this.find('div').text(),
			selected_attribute_slug = $this.data('variation-slug'),
			$container = $this.parents('.option-container'),
			attribute = $container.data('attribute-slug');

		if( $('body').hasClass('single-product') ){
			var $column_container = $this.parents('.productpage-content-container');
		} else {
			var $column_container = $this.parents('.product-row');
		}

		// Buttons
		if( ! $this.hasClass('selected') ){
			$this.addClass( 'selected' );
			$this.siblings().removeClass( 'selected' );
			$this.parents('.option-button-conainer').addClass('has-selection');

			$container.find('.option-trigger .option-name strong').text( label );
			$column_container.find('input[type="hidden"][name="attribute_'+ attribute +'"]').val( selected_attribute_slug ).trigger('change');
		} else {
			$this.removeClass('selected');
			$this.siblings().removeClass('selected');
			$this.parents('.option-button-conainer').removeClass('has-selection');

			$container.find('.option-trigger .option-name strong').text( '' );
			$column_container.find('input[type="hidden"][name="attribute_'+ attribute +'"]').val('').trigger('change');

			$column_container.find( '.price-block .price' ).text( $column_container.find( '.price-block' ).data("default-price") );
			$column_container.find( '.price-block .net-price' ).text( $column_container.find( '.price-block' ).data("default-net-price") );
		}

		// Recalculate the container height
		var $dropdownToggle = $this.parents('.dropdown-toggle'),
			idealHeight = 0;

		$dropdownToggle.children(':not(.not-avaiable)').each(function(){ idealHeight += $(this).outerHeight() });
		$dropdownToggle.css('height', idealHeight + 'px');
	});

	$('body.single-product, body.tax-product_cat, body.page-template-template-webshop').on( 'click', '.option-container .option-button-conainer .option-button.not-avaiable', function(e){
		e.preventDefault();
	});

	$('body.single-product, body.tax-product_cat, body.page-template-template-webshop').on( 'click', '.option-container .option-button-conainer .option-button.delete-selection', function(e){
		e.preventDefault();

		$(this).siblings('.option-button.selected').click();
	});

	$('body.single-product, body.tax-product_cat, body.page-template-template-webshop').on('change', '.add-to-cart-container input.variation-slugs', function(e){
		var $this = $(this),
			$container = $this.parents('.add-to-cart-container'),
			$properties_container = $('.productpage-details-container'),
			$inputs = $container.find('input[type="hidden"][name*="attribute_"]'),
			search_variation = true,
			variations = $container.data( 'variation-data' );

		if( $('body').hasClass('single-product') ){
			var $product_container = $('.productpage-content-container');

			// Show Blocks
			$('.productpage-content-container .hide-if-not-purchasable').show();
			$('.productpage-content-container .show-if-not-purchasable').hide();
		} else {
			var $product_container = $this.parents('.product-row');

			// Show Blocks
			$('.add-to-cart-container .hide-if-not-purchasable').show();
			$('.add-to-cart-container .show-if-not-purchasable').hide();
		}
		

		if( $inputs.length ){
			var selected_variation_check = {},
				avaiable_variations = [];

			$.each( $inputs, function( index, $input ){
				if( $($input).val() != '' ){
					selected_variation_check[ $($input).attr('name') ] = $($input).val();
				}
			});

			if( variations.length ){
				$.each( variations, function( index, variation ){
					selected_variation_count = 0;

					$.each( selected_variation_check, function( i, e ){
						if( variation.attributes[i] == e ){
							selected_variation_count++;
						}
					});

					if( selected_variation_count == Object.keys(selected_variation_check).length ){
						avaiable_variations.push( variation.attributes );
					}
				});
			}

			$.each( $product_container.find('.variation-options a.option-button:not(.delete-selection)'), function( index, button ){
				var attribute = 'attribute_' + $(button).parents('.option-container').data('attribute-slug'),
					avaiable = false;

				if( avaiable_variations.length ){
					$.each( avaiable_variations, function( i, variation ){
						if( variation[ attribute ] == $(button).data('variation-slug') ){
							avaiable = true;
						}
					});

					if( avaiable ){
						$(button).removeClass('not-avaiable');
					} else {
						$(button).addClass('not-avaiable');
					}
				} else {
					$(button).removeClass('not-avaiable');
				}
			});
		}

		
		if( $inputs.length ){
			$.each( $inputs, function( index, $input ){
				if( $($input).val() == '' ) {
					search_variation = false;
				}
			});
		}

		// Set main image height
		$('.productpage-image-container .productpage-image-bg').height( $('.productpage-image-container .productpage-image-bg').height() )

		if( search_variation ){
			var selected_variations = [],
				selected_variation = [];

			$.each( $inputs, function( index, $input ){
				selected_variations[ $($input).attr('name') ] = $($input).val();
			});
			

			if( variations.length ){
				$.each( variations, function( index, variation ){
					var equal = true;

					$.each( variation.attributes, function( i, e ) {
					    if( selected_variations[i].length ){
					    	if( selected_variations[i] != e ){
					    		equal = false;
					    	}
					    }
					});

					if( equal ){
						selected_variation = variation;
					}

				});
			}

			if( $(selected_variation).length ){

				if( selected_variation.is_purchasable == true ){
					if( $('body').hasClass('single-product') ){
						// Show Blocks
						$('.productpage-content-container .hide-if-not-purchasable').show();
						$('.productpage-content-container .show-if-not-purchasable').hide();
					} else {
						// Show Blocks
						$('.add-to-cart-container .hide-if-not-purchasable').show();
						$('.add-to-cart-container .show-if-not-purchasable').hide();
					}

					// Prices
					$container.find( '.price-block .price' ).text( selected_variation.prices.gross_price_formatted );
					$container.find( '.price-block .net-price' ).text( selected_variation.prices.net_price_formatted );

					// Meta infos - properties
					if( selected_variation.properties_text != '' ){
						$properties_container.find('.properties-container .productpage-details-right .paragraph').text( selected_variation.properties_text ).parents('.info-container').removeClass('hide');
					} else {
						$properties_container.find('.properties-container.info-container').addClass('hide');
					}

					// Meta infos - product description
					if( selected_variation.product_description != '' ){
						$properties_container.find('.description-container .productpage-details-right .paragraph').html( selected_variation.product_description ).parents('.info-container').removeClass('hide');
					} else {
						$properties_container.find('.description-container.info-container').addClass('hide');
					}

					// Meta infos - how to use video
					/*if( selected_variation.how_to_use_video_id != '' ){
						$new_video = '<iframe src="https://www.youtube.com/embed/'+ selected_variation.how_to_use_video_id +'" class="embedly-embed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
						$properties_container.find('.how-to-use-video-container .productpage-details-right .product-detail-video').html( $new_video ).parents('.info-container').removeClass('hide');
					} else {
						$properties_container.find('.how-to-use-video-container.info-container').addClass('hide');
					}*/

					/* Image */
					if( selected_variation.image != '' ){
						$('.productpage-image-container .productpage-image-bg').html( selected_variation.image );
					} else {
						$('.productpage-image-container .productpage-image-bg').html( $('.productpage-image-container .productpage-image-bg').data('main-image') );
					}

					zoomit_init();
				} else {
					// Meta infos - properties
					if( selected_variation.properties_text != '' ){
						$properties_container.find('.properties-container .productpage-details-right .paragraph').text( selected_variation.properties_text ).parents('.info-container').removeClass('hide');
					} else {
						$properties_container.find('.properties-container.info-container').addClass('hide');
					}

					// Meta infos - product description
					if( selected_variation.product_description != '' ){
						$properties_container.find('.description-container .productpage-details-right .paragraph').html( selected_variation.product_description ).parents('.info-container').removeClass('hide');
					} else {
						$properties_container.find('.description-container.info-container').addClass('hide');
					}

					// Meta infos - how to use video
					/*if( selected_variation.how_to_use_video_id != '' ){
						$new_video = '<iframe src="https://www.youtube.com/embed/'+ selected_variation.how_to_use_video_id +'" class="embedly-embed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
						$properties_container.find('.how-to-use-video-container .productpage-details-right .product-detail-video').html( $new_video ).parents('.info-container').removeClass('hide');
					} else {
						$properties_container.find('.how-to-use-video-container.info-container').addClass('hide');
					}*/

					/* Image */
					if( selected_variation.image != '' ){
						$('.productpage-image-container .productpage-image-bg').html( selected_variation.image );
					} else {
						$('.productpage-image-container .productpage-image-bg').html( $('.productpage-image-container .productpage-image-bg').data('main-image') );
					}

					if( $('body').hasClass('single-product') ){
						// Hide Blocks
						$('.productpage-content-container .hide-if-not-purchasable').hide();
						$('.productpage-content-container .show-if-not-purchasable').show();
					} else {
						// Hide Blocks
						$('.add-to-cart-container .hide-if-not-purchasable').hide();
						$('.add-to-cart-container .show-if-not-purchasable').show();
					}
				}
			}
		} else {

			// Hide all meta infoss
			$properties_container.find('.properties-container').addClass('hide');
			$properties_container.find('.description-container.info-container').addClass('hide');
			/*$properties_container.find('.how-to-use-video-container.info-container').addClass('hide');*/

			// Set product image
			$('.productpage-image-container .productpage-image-bg').html( $('.productpage-image-container .productpage-image-bg').data('main-image') );
			zoomit_init();
		}
	});


	/* Single product - Gallery image */
	$('body.single-product').on('click', '.thumbnail-container a.thumbnail-bg', function(e){
		e.preventDefault();

		var $this = $(this),
			$thumbnail_container = $this.parents('.thumbnail-container'),
			$col = $this.parents('.col'),
			$main_image_container = $col.find('.productpage-image-container .productpage-image-bg');

		$col.find('a.thumbnail-bg').not(this).removeClass('selected');

		// Set main image height
		$('.productpage-image-container .productpage-image-bg').height( $('.productpage-image-container .productpage-image-bg').height() )

		if( !$this.hasClass('selected') ){
			$this.addClass('selected');

			$main_image_container.html( $this.data('normal-image') );
		} else {
			$this.removeClass('selected');

			$main_image_container.html( $main_image_container.data('main-image') );
		}

		zoomit_init();
	});


	/* Single product - add to cart */
	$('body.single-product, body.tax-product_cat, body.page-template-template-webshop').on('click', 'form.add-to-cart a.add-to-cart-button', function(e){
		e.preventDefault();

		var $this = $(this),
			$form = $this.parents('form'),
			submit_enabled = true;

		if( $('body').hasClass('single-product') ){
			var $container = $('.productpage-content-container');
		} else {
			var $container = $this.parents('.product-row');
		}

		if( $form.find('input[type="hidden"][name*="attribute_"]').length ){
			$form.find( 'input[type="hidden"][name*="attribute_"]' ).each( function(){
				attr_slug = $(this).attr('name').replace('attribute_', '');

				if( $(this).val() == '' ){
					$container.find( '.option-container[data-attribute-slug="'+ attr_slug +'"]' ).addClass('error');
					submit_enabled = false;

					if ( !$container.find( '.productlist-options').hasClass('open') ) {
						$container.find( '.productlist-option-button').click();

						$('html, body').animate({
							scrollTop: $( $container.find( '.productlist-item') ).offset().top
						}, 500);
					}
				} else {
					$container.find( '.option-container[data-attribute-slug="'+ attr_slug +'"]' ).removeClass('error');
				}
			});
		}

		if( submit_enabled ){
			$this.addClass('loading');
			$this.parents('form').submit();
		}
	});

	/* Woocommerce */


	/* Cart */
	$('body').on('change', 'form.woocommerce-cart-form .quantity input.qty', function(e){
		$('button[name="update_cart"]').prop('disabled', false).trigger('click');
	});

	$(document).on('updated_cart_totals', function(e){
		$.post(
			ajax.ajaxurl, {
				action: "bl_get_minicart",
			},
			function( data ) {
				if( data.success == true ){
					$('header .nav-container .minicart').replaceWith( data.data );
					$('header .nav-container .main-menu .minicart').addClass('only-thin-mobile');
				}
			},
			"json"
		);
	});

	$('body').on('click', 'form.woocommerce-cart-form .product-quantity a.w-button, form.add-to-cart .quantity-container a.w-button', function(e){
		e.preventDefault();

		var $this = $(this),
			$input = $this.parent().find('input.qty'),
			step = parseInt( $input.prop('step') ),
			min = parseInt( $input.prop('min') ),
			max = $input.prop('max'),
			current_val = parseInt( $input.val() );


		if( $this.hasClass('input-minus') ){
			if( current_val - step >= min ){
				$input.val( current_val - step );
				$input.trigger('change');
			}
		} else {
			if( max != '' ){
				if( current_val + step <= max ){
					$input.val( current_val + step );
					$input.trigger('change');
				}
			} else {
				$input.val( current_val + step );
				$input.trigger('change');
			}
		}
	});

	/* Checkout - change payment trigger update_checkout */
	$('body.woocommerce-checkout').on('change', '#payment input[name="payment_method"]', function(e){
		$('body').trigger('update_checkout');
	});

	/* Checkout - Privacy policy and Term and Condition */
	$('body.woocommerce-checkout').on('change', '#payment input[type="checkbox"].accepting-documents', function(e){
		if( !$(this).is(':checked') ){
			$(this).parents('.table-row').addClass('error');
		} else {
			$(this).parents('.table-row').removeClass('error');
		}
	});

	/* Checkout document */
	$('body.woocommerce-checkout .file-attachment-container').on('click', 'a.file-attachment', function(e){
		e.preventDefault();

		$(this).parents('.input-container').find('input[name="document"]').click();
	});

	$('body.woocommerce-checkout').on('change', 'input[name="document"]', function(e){
		e.preventDefault();
		var fileName = $(this).val().split(/(\\|\/)/g).pop(),
			$this = $(this),
			$row = $this.parents('.form-row'),
			$input_container = $this.parents('.input-container'),
			$button = $input_container.find('a.file-attachment');

		$this.parents('.file-attachment-container').find('div.document-name strong').text( fileName );
		
		$row.removeClass('error');
		$this.parents('form.checkout').find( 'button[type="submit"]' ).attr('disabled', 'disabled');
		$button.addClass('loading');

		if( !$this.hasClass('loading') ){
			$this.addClass('loading');
			
			var data = new FormData();

			// File
			if( $(this).prop('files').length != 0 ){
				data.append('document', $this.prop('files')[0] );
			}

			data.append( 'action', 'bl_upload_certificate_from_checkout' );
			data.append( 'old-document-id', $input_container.data('old-document-id') );
    		
    		jQuery.ajax({
				url: ajax.ajaxurl,
				type: 'post',
				data: data,
				cache: false,
		        contentType: false,
		        processData: false,
				success: function(data){
					if(data.success == true){
						$input_container.data('old-document-id', data.attachemnt_id );
						$this.siblings('input[name="document-id"]').val( data.attachemnt_id );
					}

					$this.removeClass('loading');
					$this.parents('form.checkout').find( 'button[type="submit"]' ).removeAttr("disabled");
					$button.removeClass('loading');
				}
			});
		}
	});

	$('body.woocommerce-checkout').on('checkout_error', function(){
		// Accepting documents
		$('#payment input[type="checkbox"].accepting-documents').each( function(){
			if( ! $(this).is(':checked') ){
				$(this).parents('.table-row').addClass('error');
			} else {
				$(this).parents('.table-row').removeClass('error');
			}
		});

		// Certificate document upload
		if( $('.file-attachment-container input[name="document-id"]').val() == '' ){
			$('.file-attachment-container input[name="document-id"]').parents('.table-row').addClass('error');
		} else {
			$('.file-attachment-container input[name="document-id"]').parents('.table-row').removeClass('error');
		}
	});

	/* Checkout - copy alert in to container */
	$(document).ready( function(){
		if( $('body').hasClass('woocommerce-checkout') ){
			$('.woocommerce > .woocommerce-info').insertBefore('.subpage-wrapper form.checkout.woocommerce-checkout');
		}
	});

	/* Thank you - copy alert in to container */
	$(document).ready( function(){
		if( $('body').hasClass('woocommerce-order-received') ){
			$('.woocommerce-message').insertAfter('.subpage-wrapper .progress-bar');
		}
	});



	/* Courses - add to cart */
	$('body').on('click', 'form.add-to-cart-course a.add-to-cart-button', function(e){
		e.preventDefault();

		$(this).parents('.table-row.big').next().addClass('open');
		$('#training-courses-info-overlay').addClass('open');
	});

	$('body').on('click', '.traning-information-close.add-training-to-cart', function(e){
		$(this).parents('.training-information.open').prev().find('form.add-to-cart-course a.add-to-cart-button').addClass('loading');
		$(this).parents('.training-information.open').prev().find('form').submit();
	});

	/* Courses - filter */
	$('body').on('change', 'form.training-courses-filter-form select', function(e){
		e.preventDefault();

		$(this).parents('form.training-courses-filter-form').submit();
	});

	$('body').on('click', '.training-courses .information-button', function(e){
		e.preventDefault();
		var $dropdownToggle = $(this).parents('.table-row').next('.training-information'),
			$coursesListWrapper = $('#training-courses-info-overlay');

		$coursesListWrapper.addClass('open');
		$dropdownToggle.addClass('open');
	});

	$('body').on('click', '.training-information.open .traning-information-close, #training-courses-info-overlay', function(e){
		e.preventDefault();
		var $dropdownToggle = $('.training-information.open'),
			$coursesListWrapper = $('#training-courses-info-overlay');

		$dropdownToggle.removeClass('open');
		$coursesListWrapper.removeClass('open');
	});

	$w.load(function(){
		if( $('.table-wrapper.training-courses').length ){
			is_filtered = /[?&]filter=/.test(location.search);

			if( is_filtered ){
				if( $(window).outerWidth() > 1350 ){
					$('body').scrollTo( $('#courses'), 800, { offset: -110 } );
				} else {
					$('body').scrollTo( $('#courses'), 800 );
				}
			}
		}
	});


	/* Login */
	$('body').on('click', 'form.login a.login-submit', function(e){
		e.preventDefault();
		var $this = $(this),
			$form = $this.parents('form.login'),
			$email = $form.find('input.login-email'),
			$pass = $form.find('input.login-password');

		$('.error').removeClass('error');
		$form.find('.error-text').slideUp();

		if( !$this.hasClass('loading') ){
			$this.addClass('loading');

			jQuery.post(
				ajax.ajaxurl, {
					action: "bl_login",
					form: $form.serialize(),
				}, function(data) {
					if(data.success == true){
						location.reload();
					} else {
						error_text = '';
						if( data.error == 'no-user-found' ){
							error_text = $email.data('error-text-no-user-found');
							$email.parents('.table-row').addClass('error');
						} else if( data.error == 'wrong-password' ){
							error_text = $pass.data('error-text-wrong-password');
							$pass.parents('.table-row').addClass('error');
						} else if( data.error == 'user-not-activated' ){
							error_text = $form.data('error-text-user-not-activated');
						}

						$form.find('.error-text .text').html( error_text ).parents('.error-text').slideDown();

						$this.removeClass('loading');
					}
				},
				"json"
			);
		}
	});

	// Login form - enter key
	$('body').on('keydown', 'form.login', function(e){
		var key = e.which,
			$form = $('form.login');

		if ( key == 13 ) { // Enter
			$form.find('a.login-submit').click();
		}
	})


	/* Registration */
	$('body').on('click', 'form.registration a.registration-submit', function(e){
	e.preventDefault();

	var $this = $(this),
		$form = $this.parents('form.registration'),
		$email = $form.find('input.reg-email'),
		$pass = $form.find('input.reg-password'),
		$pass_again = $form.find('input.reg-password-again'),
		$phone = $form.find('input.reg-phone'),
		$birthday = $form.find('input.reg-birthday'),
		$terms = $form.find('input.terms-and-condition'),
		error_text = '',
		error = 0;

		$('.table-row').removeClass('error');
		$form.find('.error-text').slideUp();

		if( !$this.hasClass('loading') ){
			$this.addClass('loading');

			// Check required fields
			$form.find('input.req, textarea.req, select.req').each( function(){
				if( $(this).val() == '' ){
					$(this).parents('.table-row').addClass('error');
					error++;
				}
			});

			// Validation
			if( error == 0 ){
				if( !valid_email( $email.val() ) ){
					error_text = $email.data('error-text-not-valid');

					$email.parents('.table-row').addClass('error');
					error++;
				}

				if( $pass.val() != $pass_again.val() ){
					error_text = $pass.data('error-text-not-matching-passwords');

					$pass.parents('.table-row').addClass('error');
					$pass_again.parents('.table-row').addClass('error');
					error++;
				}

				if( $birthday.val() != '' ){
					if( !valid_date( $birthday.val() ) ){
						error_text = $birthday.data('error-text-not-valid');

						$birthday.parents('.table-row').addClass('error');
						error++;
					}
				}

				/*if( !valid_mobile( $phone.val() ) ){
					error_text = $phone.data('error-text-not-valid');

					$phone.parents('.table-row').addClass('error');
					error++;
				}*/

				if( !$terms.is(':checked') ){
					error_text = $terms.data('error-text-terms-and-condition');

					$terms.parents('.table-row').addClass('error');
					error++;
				}

			} else {
				error_text = $form.data('error-text-all-field-required');
			}

			/*if( $document.prop('files').length != 0 ){
				if ( !has_extension( $document.val(), ['.jpg', '.jpeg', '.png', '.pdf'] ) ) {
					error_text = $document.data('error-wrong-extension');

					$document.parents('.table-row').addClass('error');
					error++;
				}
			}*/

			if( error == 0 ){
        		var data = new FormData();

        		// File
        		/*if( $document.prop('files').length != 0 ){
        			data.append('document', $document.prop('files')[0] );
        		}*/

        		data.append( 'action', 'bl_registration' );
        		data.append( 'form', $form.serialize() );
        		
        		jQuery.ajax({
					url: ajax.ajaxurl,
					type: 'post',
					data: data,
					cache: false,
			        contentType: false,
			        processData: false,
					success: function(data){
						if(data.success == true){
							window.location.href = data.redirect;
						} else {
							error_text = '';
							if( data.error == 'user-already-registred' ){
								error_text = $form.data('error-text-user-already-registred');
								$email.parents('.table-row').addClass('error');
							}

							$form.find('.error-text .text').html( error_text ).parents('.error-text').slideDown();
							$('body').scrollTo( $('.error-text'), 800, { offset: -110 } );

							$this.removeClass('loading');
						}
					}
				});

			} else {
				$form.find('.error-text .text').html(error_text).parents('.error-text').slideDown();
				$('body').scrollTo( $('.error-text'), 800, { offset: -110 } );

				$this.removeClass('loading');
			}
		}
	});

	/* Registration document */
	/*$('body.page-template-template-registration').on('click', 'a.file-attachment', function(e){
		e.preventDefault();

		$(this).parents('.table-column').find('input[name="document"]').click();
	});

	$('body.page-template-template-registration').on('change', 'input[name="document"]', function(e){
		e.preventDefault();
		var fileName = $(this).val().split(/(\\|\/)/g).pop();

		$(this).parents('.table-row').find('span.document-name strong').text( fileName );
	});*/

	/* Copy from shipping to billing */
	$('body.page-template-template-registration').on('click', 'a.copy-shipping-to-billing', function(e){
		e.preventDefault();

		var $this = $(this),
			$shipping_container = $('.table-wrapper.shipping'),
			$billing_container = $('.table-wrapper.billing');

		$shipping_container.find( 'input' ).each( function(){
			$billing_container.find( 'input[name="'+ $(this).attr('name').replace( "shipping", "billing" ) +'"]' ).val( $(this).val() );
		});
	});


	/* Forgotten password */
	$('body').on('click', 'form.forgotten-password-email-verification a.forgotten-password-email-verification-submit', function(e){
		e.preventDefault();
		var $this = $(this),
			$form = $this.parents('form.forgotten-password-email-verification'),
			$email = $form.find('input.forgotten-password-email'),
			error = 0;

		$('.table-row').removeClass('error');
		$form.find('.error-text').slideUp();

		if( !$this.hasClass('loading') ){
			$this.addClass('loading');

			// Check required fields
			$form.find('input.req').each( function(){
				if( $(this).val() == '' ){
					$(this).parents('.table-row').addClass('error');
					error++;
				}
			});

			// Validation
			if( error == 0 ){
				if( !valid_email( $email.val() ) ){
					error_text = $email.data('error-text-not-valid');

					$email.parents('.table-row').addClass('error');
					error++;
				}
			} else {
				error_text = $form.data('error-text-all-field-required');
			}

			if( error == 0 ){
				jQuery.post(
					ajax.ajaxurl, {
						action: "bl_forgotten_password_email_verification",
						form: $form.serialize(),
					}, function(data) {
						if(data.success == true){
							window.location.href = data.redirect;
						} else {
							error_text = '';
							if( data.error == 'no-user-found' ){
								error_text = $email.data('error-text-no-user-found');
								$email.parents('.table-row').addClass('error');
							}

							$form.find('.error-text .text').html( error_text ).parents('.error-text').slideDown();
							$('body').scrollTo( $('.error-text'), 800, { offset: -110 } );
						}

						$this.removeClass('loading');
					},
					"json"
				);
			} else {
				$form.find('.error-text .text').html( error_text ).parents('.error-text').slideDown();
				$('body').scrollTo( $('.error-text'), 800, { offset: -110 } );
			
				$this.removeClass('loading');
			}
		}
	});

	/* Change password */
	$('body').on('click', 'form.change-password a.change-password-submit', function(e){
		e.preventDefault();
		var $this = $(this),
			$form = $this.parents('form.change-password'),
			$pass = $form.find('input.change-password'),
			$pass_again = $form.find('input.change-password-again'),
			error = 0;

		$('.table-row').removeClass('error');
		$form.find('.error-text').slideUp();

		if( !$this.hasClass('loading') ){
			$this.addClass('loading');

			// Check required fields
			$form.find('input.req').each( function(){
				if( $(this).val() == '' ){
					$(this).parents('.table-row').addClass('error');
					error++;
				}
			});

			// Validation
			if( error == 0 ){
				if( $pass.val() != $pass_again.val() ){
					error_text = $pass.data('error-text-not-matching-passwords');

					$pass.parents('.table-row').addClass('error');
					$pass_again.parents('.table-row').addClass('error');
					error++;
				}
			} else {
				error_text = $form.data('error-text-all-field-required');
			}

			if( error == 0 ){
				jQuery.post(
					ajax.ajaxurl, {
						action: "bl_change_password",
						form: $form.serialize(),
					}, function(data) {
						if(data.success == true){
							window.location.href = data.redirect;
						} else {
							error_text = '';
							if( data.error == 'wrong-link' ){
								error_text = $form.data('error-text-wrong-link');
							} else if( data.error == 'hash-not-match' ){
								error_text = $form.data('error-text-hash-not-match');
							}

							$form.find('.error-text .text').html( error_text ).parents('.error-text').slideDown();
							$('body').scrollTo( $('.error-text'), 800, { offset: -110 } );
						}


						$this.removeClass('loading');
					},
					"json"
				);
			} else {
				$form.find('.error-text .text').html( error_text ).parents('.error-text').slideDown();
				$('body').scrollTo( $('.error-text'), 800, { offset: -110 } );

				$this.removeClass('loading');
			}
		}
	});

	/* Delete account */
	$('body.woocommerce-account').on('click', 'form.delete-account button[name="delete-account-submit"]', function(e){
		e.preventDefault();

		showPopup('delete-account.delete-user-delete-confirmation');

		/*var $confirm = confirm( $(this).parents('form.delete-account').data('verify-text') );
		if ( $confirm ) {
			$(this).parents('form.delete-account').submit();
		}*/
	});

	$('body.woocommerce-account').on('click', '.popup-wrapper.delete-account.delete-user-delete-confirmation.active a', function(e){
		e.preventDefault();

		if( $(this).hasClass('delete-user-confirm-no') ){
			closePopup('delete-account.delete-user-delete-confirmation');
		} else {
			$('form.delete-account').submit();
		}
	});


	/* Newsletter - footer */
	$('body').on('click', 'form.newsletter-form a.submit-newsletter-subscription', function(e){
		e.preventDefault();

		var $this = $(this),
			$form = $this.parents('form.newsletter-form'),
			$email = $form.find('input.newsletter-email'),
			$terms = $form.find('input.terms-and-condition'),
			error = 0,
			error_text = '';

		if( $form.hasClass('with-topic') ){
			var $topic = $form.find('select.newsletter-topic');
		} else {
			var $topic = $form.find('input.newsletter-topic');
		}

		$('.error').removeClass('error success');
		$form.find('.error-text').slideUp();

		if( !$this.hasClass('loading') ){
			$this.addClass('loading');

			// Check required fields
			$form.find('input.req, textarea.req, select.req').each( function(){
				if( $(this).val() == '' || $(this).val() == null ){
					$(this).addClass('error');
					error++;
				}
			});

			// Validation
			if( error == 0 ){
				if( !valid_email( $email.val() ) ){
					error_text = $email.data('error-text-not-valid');

					$email.addClass('error');
					error++;
				}

				if( !$terms.is(':checked') ){
					error_text = $terms.data('error-text-terms-and-condition');

					$terms.addClass('error');
					error++;
				}
			} else {
				error_text = $form.data('error-text-all-field-required');
			}

			if( error == 0 ){
        		jQuery.post(
					ajax.ajaxurl, {
						action: "bl_newsletter_subscription",
						form: $form.serialize(),
					}, function(data) {
						if(data.success == true){
							error_text = data.text;
							$form.find('.error-text .text').html( error_text ).parents('.error-text').addClass('success').slideDown();
						} else {
							error_text = data.error;
							$form.find('.error-text .text').html( error_text ).parents('.error-text').slideDown();
						}

						$this.removeClass('loading');
					},
					"json"
				);

			} else {
				$form.find('.error-text .text').html( error_text ).parents('.error-text').slideDown();

				$this.removeClass('loading');
			}
		}
	});

	/* Contact form */
	document.addEventListener( 'wpcf7invalid', function( event ) {
		var $form = $('.contact-form-wrapper form');

		$form.find('input, textarea, select').each( function(){
			if( $(this).hasClass( 'wpcf7-not-valid' ) ){
				$(this).parents('.table-row._2col').addClass('error');
			}
		});

		//$('body').scrollTo( $('.wpcf7-response-output'), 800, { offset: -400 } );
	}, false );

	$('body').on('click', '.contact-form-wrapper form input[type="submit"]', function(){
		$(this).parents('form').find('.error').removeClass('error');
	})

	/* Add to cart video popup */
	$('body').on('click', '.woocommerce-message a.cart-show-popup', function(e){
		e.preventDefault();

		$video = '<iframe src="https://www.youtube.com/embed/'+ $(this).attr('href') +'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';

		$('.popup-wrapper.add-to-cart-video-popup').find('.popup-video-container').html( $video );

		showPopup('add-to-cart-video-popup');
	});

	/* Popup */
	$('body').on('click', '.popup-wrapper .close_btn, .marketing-popup-wrapper .close_btn, .close-popup-btn', function(e){
		e.preventDefault();
		closePopup();
	});


	/* Zoom-it */
	function zoomit_init(){
		if( $w.outerWidth() > 991 ){
			var $productpageImageContainer = $('.productpage-image-container');
			var $productHider;
			$productpageImageContainer.find('.zoomit-ghost').remove();
			if ($productpageImageContainer.find('img').parent().is('.zoomit-container')) {
				$productpageImageContainer.find('img').unwrap();
			}
			$productpageImageContainer.find('img')
				.removeData('zoom')
				.removeData('zoomed')
				.zoomIt({
					onZoomIn : function(){
						$productHider.addClass('on').siblings('zoomit-zoomed').addClass('on');
					},
					onZoomOut : function(){
						$productHider.removeClass('on');
					},
					imgTag: null
				});
			$productpageImageContainer.find('.product-image').after('<div class="product-image-hide productpage-image-bg"></div>');
			$productHider = $productpageImageContainer.find('.product-image-hide');
		}
	}

	// Marketing popup
	$w.load(function(){
		if($('.marketing-popup-overlay.delayed').length && $('.marketing-popup-wrapper.delayed').length){
			var $popup_overly = $('.marketing-popup-overlay.delayed'),
				$popup_box = $('.marketing-popup-wrapper.delayed');
				delay_to_show = parseInt( $popup_overly.data('delay-mp') ) * 1000, // Sec -> milisec
			
			setTimeout(function(){
				if( ( $('.overlay-wrapper.active').length == 0 && $('.popup-wrapper.active').length == 0 ) && ( $('.marketing-popup-overlay.active').length == 0 && $('.marketing-popup-box.active').length == 0 ) ){
					$popup_overly.addClass('active');
					$popup_box.addClass('active');
				}
			}, delay_to_show);
		}
	});

	// Google map
	if( $('.bl-map').length ){
		$('.bl-map').each( function(){
			var lat = $(this).data('lat'),
				long = $(this).data('long');

			if( lat != '' && long != '' ){
				var myLatLng = {lat: lat, lng: long};

				var map = new google.maps.Map( this, {
					zoom: 17,
					center: myLatLng,
					streetViewControl: false,
					mapTypeControl: false,
					scrollwheel: false,
		        });

		        var marker = new google.maps.Marker({
					position: myLatLng,
					map: map,
		        });
			}

		});
	}

	/* Simple button scroll-to */
	$('body').on('click', '.simple-button-container a', function(e){
		if( $(this).attr('href').substring( 0, 1 ) == '#' ){
			e.preventDefault();

			if( $(window).outerWidth() > 1350 ){
				$('body').scrollTo( $(this).attr('href'), 800, { offset: -110 } );
			} else {
				$('body').scrollTo( $(this).attr('href'), 800 );
			}
		}
	});

	/* BEST BROWS */
	if ($b.hasClass('best-brows') || $b.hasClass('szemoldok-laminalas') || $b.hasClass('brow-lamination')) {
		// loading overlay
		$('.bestbrows-overlay').fadeOut(2000, function(){
			$(this).remove();
		});

		// best brows hero
		setTimeout(function(){
			$('.subpage-hero.bestbrows').addClass('changed');
		}, 2000);
		setTimeout(function(){
			$('.bestbrows-hero-packages').addClass('changed');
		}, 2100);
		setTimeout(function(){
			$('.bestbrows-hero-stuff').addClass('changed');
		}, 2200);
		setTimeout(function(){
			$('.bestbrows-hero-background').addClass('changed');
		}, 2300);

		// slider
		$('.bestbrows-slider.w-slider').each(function(){
			$slider = $(this);

			var sliderCount = $slider.find('.w-slider-mask').children('.w-slide').length;
			var $sliderNav = $slider.find('.w-slider-nav');
			var $slides = $slider.find('.w-slide');
			for (var i = 0; i < sliderCount; i++) {
				$sliderNav.append('<div class="w-slider-dot"></div>');
			}
			$sliderNav.children().first().addClass('w-active');

			$slides.css({'transition': 'opacity 1000ms', 'z-index': 1});
			$slides.not(':first-child').css({'visibility': 'hidden', opacity: 0});

			var slideTimeout;
			function jumpTo(idx) {
				var slideWidth = $slides.parent().width();
				var newSlideTransform = -idx * slideWidth;
				var maxZ = 0;

				if ($slides.first().css('z-index') > 20) {
					$slides.each(function(){
						this.style.zIndex -= 10;
					});
				}

				$slides.each(function(){
					if (this.style.zIndex > maxZ) {
						maxZ = this.style.zIndex;
					}
				});	
				$slides.eq(idx).css({
					'transform': 'translateX(' + newSlideTransform + 'px)',
					'visibility': '',
					'opacity': 0,
					'z-index': parseInt(maxZ) + 1,
				});
				requestAnimationFrame(function(){
					$slides.eq(idx).css('opacity', 1);
				});

				setTimeout(function(){
					$slides.not(':eq('+ idx +')').css({
						'transform': 'translateX(' + newSlideTransform + 'px)',
						'visibility': 'hidden',
						'opacity': 0
					});
				}, 1000);

				$sliderNav.children().eq(idx).addClass('w-active').siblings().removeClass('w-active');
				$slides.eq(idx).addClass('active').siblings().removeClass('active');

				if ($slides.length > 1) {
					if (slideTimeout) {
						clearTimeout(slideTimeout);
					}
					slideTimeout = setTimeout(function(){
						jumpTo((idx + 1) % $slides.length);
					}, 4000);
				}
			}
			if ($slides.length > 1) {
				slideTimeout = setTimeout(function(){
					jumpTo(1);
				}, 4000);
			}
			
			// dot click
			$sliderNav.children().click(function(){
				jumpTo( $(this).index() );
			});

			// left/right nav
			$slider.find('.w-slider-arrow-left').click(function(){
				var idx = $slides.filter('.active').index();
				idx --;
				if (idx < 0) idx = $slides.length - 1;
				jumpTo( idx );
			});
			$slider.find('.w-slider-arrow-right').click(function(){
				var idx = $slides.filter('.active').index();
				idx ++;
				if (idx > $slides.length - 1) idx = 0;
				jumpTo( idx );
			});

			// mobile swipe
			var swipeTimeout;
			var swipeEnabled = true;
			$slider.find('.home-slide-product, .w-slider-mask, .w-slide').swipe({
				swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
					if (swipeEnabled) {
						swipeEnabled = false;
						swipeTimeout = setTimeout(function(){
							swipeEnabled = true;
						}, 1000);
						var idx = $slides.filter('.active').index();
						if (direction == 'left') {
							idx --;
							if (idx < 0) idx = $slides.length - 1;
						} else {
							idx ++;
							if (idx > $slides.length - 1) idx = 0;
						}
						jumpTo(idx);
					}
				},
				threshold: 100
			});

			// window resize => transform állítás
			$w.resize(function(){
				requestAnimationFrame(function(){
					var activeIndex = Math.max($slides.filter('.active').index(), 0);
					var newSlideTransform = - activeIndex * $slides.parent().width();
					$slides.css({
						'transform': 'translateX(' + newSlideTransform + 'px)'
					});
				});
			});

		});
	}
});

/* Popup */
function showPopup( $id ){
	if( $id.length ){
		jQuery('.overlay-wrapper').addClass('active');
		jQuery('.popup-wrapper:not(.' + $id + ')').removeClass('active');
		jQuery('.popup-wrapper.' + $id ).addClass('active');
	}
}
function closePopup( $id ){
	jQuery('.overlay-wrapper, .marketing-popup-overlay').removeClass('active');
	
	if( $id != '' && $id != undefined ){
		jQuery('.popup-wrapper.' + $id ).removeClass('active');
		jQuery('.marketing-popup-wrapper.' + $id ).removeClass('active');
	} else {
		jQuery('.popup-wrapper, .marketing-popup-wrapper').removeClass('active');
	}
}


function valid_email(emailAddress) {
	var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	return pattern.test(emailAddress);
}

function valid_date(dateString) {
	var regEx = /^\d{4}[.]\d{2}[.]\d{2}$/;
	return dateString.match(regEx) != null;
}

function valid_mobile( telszam ){
	var regEx = /^(?:20|30|31|70)[\/-]\d{7}$/;
	return telszam.match(regEx) != null;
}

function has_extension( fileName, exts ) {
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test( fileName );
}

function areDifferentCount( a, b ){
	var count = 0;
	jQuery.each( a, function( key, value ){
		if( a[key] == b[key] ){
			count++;
		}
	});

    return count;
}


function updateUrl( url,key,value ){
	if(value!=undefined){
		value = encodeURI(value);
	}
	var urls = url.split('?');
	var baseUrl = urls[0];
	var parameters = '';
	var outPara = {};
	
	if(urls.length>1){
		parameters = urls[1];
	}
	if(parameters!=''){
		parameters = parameters.split('&');
		for(k in parameters){
			var keyVal = parameters[k];
			keyVal = keyVal.split('=');
			var ekey = keyVal[0];
			var eval = '';
			if(keyVal.length>1){
				eval = keyVal[1];
			}
			outPara[ekey] = eval;
		}
	}

	if(value!=undefined){
		outPara[key] = value;
	}else{
		delete outPara[key];
	}
	parameters = [];
	for(var k in outPara){
		parameters.push(k + '=' + outPara[k]);
	}

	var finalUrl = baseUrl;

	if(parameters.length>0){
		finalUrl += '?' + parameters.join('&'); 
	}

	return finalUrl; 
}