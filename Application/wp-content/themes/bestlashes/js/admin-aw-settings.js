jQuery(function($) {

    $('body').on( 'click', '.aw-settings-section .menu-wrapper ul li', function(e){
        e.preventDefault();

        var $this = $(this),
            $container = $(this).parents('.aw-settings-section'),
            selectedItem = $this.attr('data-menu-item'),
            $contentWrapper = $container.find('.content-wrapper');

        $this.siblings('li.active').removeClass('active');
        $this.addClass('active');

        $contentWrapper.find('.content-box').each(function() {
            $(this).removeClass('active');
            if ( $(this).hasClass(selectedItem) === true ) { 
                $(this).addClass('active');
            }
        });
    });

    $('body').on( 'click', '.aw-settings-section .duplicatable .add-new-row', function(e){
        e.preventDefault();

        var $this = $(this),
            $container = $this.parents('.duplicatable');

        $new_conteiner = $container.find('.duplicatable-element.prototype').clone( true, true ).insertBefore( $container.find('.duplicatable-element.prototype').last() ).removeClass('prototype');

        $new_conteiner.find('select.select-2-able').each( function(){
            $(this).removeClass('select-2-able').addClass('select-2').select2();
        })
    });

    $('body').on( 'click', '.aw-settings-section .duplicatable .remove-row .remove', function(e){
        e.preventDefault();

        $(this).parents('.duplicatable-element').remove();
    });
});