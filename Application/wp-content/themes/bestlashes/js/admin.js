jQuery(function($) {

	$('select.select-2').select2();
	$('select.select-2-featured-products').select2({
		maximumSelectionLength: 4
	});
	$("select.select-2-featured-products").on("select2:select", function (evt) {
		var element = evt.params.data.element;
		var $element = $(element);

		$element.detach();
		$(this).append($element);
		$(this).trigger("change");
	});
	$('.datetimepicker').datetimepicker({
		format:'Y-m-d H:i',
	});

	var frame;

	// Upload new incoice for order
	$('body.wp-admin.post-type-shop_order').on('click', '.aw-settings-section .invoice a.upload-invoice-for-order', function(e){
		e.preventDefault();

		// If the media frame already exists, reopen it.
		if ( frame ) {
			frame.open();
			return;
		}
		
		// Create the media frame.
		frame = wp.media.frames.file_frame = wp.media({
			library: {
				type: 'application/pdf'
			},
			multiple: false,
			editing_sidebar: false,
			default_tab: 'upload',
			tabs: 'upload, library',
		});
		
		// When an image is selected, run a callback.
		frame.on( 'select', function() {
			var selection = frame.state().get('selection');
			
			selection.map( function( attachment ) {
				attachment = attachment.toJSON();
				
		      	pdf_url = attachment.url;
				pdf_id  = attachment.id;

				if( $(this).parents('.content-box').hasClass('final-invoice') ){
					$('.aw-settings-section .content-box input[name="_final-invoice-id"]').val( pdf_id );
				} else {
					$('.aw-settings-section .content-box input[name="_invoice-id"]').val( pdf_id );
				}
				$('.aw-settings-section .content-box a.invoice-link').attr( 'href', pdf_url ).removeClass('hidden');
				$('.aw-settings-section .content-box a.invoice-link span.invoice-title').text( attachment.title );

				$('.aw-settings-section .content-box span.text').text( $('.aw-settings-section .content-box .text-box.invice-upload').data('upload-text') );
			});
		});
	 
		// Finally, open the modal
		frame.open();
	});


	// Generate new invoice for order
	$('body.wp-admin.post-type-shop_order').on('click', '.aw-settings-section .content-box a.generate-new-invoice-for-order', function(e){
		e.preventDefault();

		var $this = $(this),
			order_id = $this.data('order-id'),
			$spinner = $this.siblings('.spinner'),
			$current_invoice_box = $this.parents('.content-box').find('.invice-upload'),
			$error_container = $this.siblings('div.error-container');
			payment_method = $this.parents('.content-box').find('select[name="payment_method"]').val(),
			regenerate_confirm_text = $current_invoice_box.data('confirm-regenerate-invoice'),
			need_preview = $this.hasClass('need-preview');

		if( $this.parents('.content-box').hasClass('final-invoice') ){
			var payment_deadline = $this.parents('.content-box').find('input[name="final_payment_deadline"]').val(),
				vat_type = $this.parents('.content-box').find('input[name="final_vat_type"]:checked').val(),
				paid = $this.parents('.content-box').find('input[name="final_paid"]').val(),
				date_of_completion = $this.parents('.content-box').find('input[name="final_date_of_completion"]').val(),
				current_invoice_id = $this.parents('.content-box').find('input[name="_final-invoice-id"]').val();
		} else {
			var payment_deadline = $this.parents('.content-box').find('input[name="payment_deadline"]').val(),
				vat_type = $this.parents('.content-box').find('input[name="vat_type"]:checked').val(),
				paid = $this.parents('.content-box').find('input[name="paid"]').val(),
				date_of_completion = $this.parents('.content-box').find('input[name="date_of_completion"]').val(),
				current_invoice_id = $this.parents('.content-box').find('input[name="_invoice-id"]').val();
		}

		$error_container.removeClass('success').slideUp();

		if( payment_deadline == '' || vat_type == '' || paid == '' ){
			if( payment_deadline == '' ){
				$error_container.slideDown().find('.error-text').text( $this.data('error-text-payment-deadline') );
			}
			if( vat_type == '' ){
				$error_container.slideDown().find('.error-text').text( $this.data('error-text-vat-type') );
			}
			if( paid == '' ){
				$error_container.slideDown().find('.error-text').text( $this.data('error-text-paid') );
			}
		} else {
			if( !$this.hasClass('loading') ){
				var enabled_invoice_generate = false;
				
				if( current_invoice_id != '' ){
					var confirm_response = confirm( regenerate_confirm_text );

					if (confirm_response == true) {
						enabled_invoice_generate = true;
					}
				} else {
					enabled_invoice_generate = true;
				}
				
				if( enabled_invoice_generate == true ){
					$this.addClass('loading');
					$spinner.addClass('is-active');

					jQuery.post(
						ajaxurl, {
							action: "bl_generate_invoice_for_order",
							id: order_id,
							final: $this.parents('.content-box').hasClass('final-invoice'),
							payment_deadline: payment_deadline,
							vat_type: vat_type,
							paid: paid,
							payment_method: payment_method,
							date_of_completion: date_of_completion,
							need_preview: need_preview
						}, function(data) {
							if(data.success == true){
								if( need_preview ){
									$('.popup-wrapper.invoice-preview .container .inside-box').replaceWith( data.invoice_preview_html );
									showPopup('invoice-preview');

									// Remove need-preview class from button
									$this.removeClass('need-preview');
								} else {
									if( $this.parents('.content-box').hasClass('final-invoice') ){
										$this.parents('.content-box').find('input[name="_final-invoice-id"]').val( data.invoice_id );
									} else {
										$this.parents('.content-box').find('input[name="_invoice-id"]').val( data.invoice_id );
									}
									$current_invoice_box.find('a.invoice-link').attr('href', data.invoice_url).removeClass('hidden');
									$current_invoice_box.find('a.invoice-link span.invoice-title').text( data.invoice_name );

									$('.aw-settings-section .content-box span.text').text( $('.aw-settings-section .content-box .text-box.invice-upload').data('upload-text') );

									$error_container.addClass('success');

									// Add need-preview class for button
									$this.addClass('need-preview');
								}
							} else {
								
							}

							$this.removeClass('loading');
							$spinner.removeClass('is-active');

							if( !need_preview ){
								$error_container.slideDown().find('.error-text').text( data.message );
							}
						},
						"json"
					);
				}
			}
		}
	});


	/* Options panel ajax */
	$('body.wp-admin.toplevel_page_bl_options_panel_settings').on( 'change', '.bl-gift-products', function(e){
		var $this = $(this),
			product_id = $this.val(),
			$container = $this.parents('.duplicatable-element'),
			$variation_select = $container.find('.bl-gift-products-variation');

		$container.find('.ajax-loader').addClass('loading');
		jQuery.post(
			ajaxurl, {
				action: "bl_admin_get_product_variations_for_gift_products",
				product_id: product_id,
			}, function(return_data) {
				if( return_data.success == true ){
					$variation_select.empty();
					
					if( return_data.data.list.length != 0 ){
						$.each( return_data.data.list, function( index, value ){
							$option = $("<option></option>").attr("value", value.id).text(value.name);
						    $variation_select.append( $option );
						});
					} else {
						/*$input = $variation_select.append('<input type="hidden" name="" value="">');
						$input.attr('name', $variation_select.attr('name')).addClass('');*/
					}

					$variation_select.select2('destroy');
					$variation_select.select2();

					$container.find('.ajax-loader').removeClass('loading');
				}
			},
			"json"
		);
	});

	/* User settings */
	$('body').on( 'click', '.category-product-user-discount.duplicatable .add-new-row', function(e){
        e.preventDefault();

        var $this = $(this),
            $container = $this.parents('.duplicatable');

        $new_conteiner = $container.find('.duplicatable-element.prototype').clone( true, true ).insertBefore( $container.find('.duplicatable-element.prototype').last() ).removeClass('prototype');

        $new_conteiner.find('select.select-2-able').each( function(){
            $(this).removeClass('select-2-able').addClass('select-2').select2();
        })
    });

    $('body').on( 'click', '.category-product-user-discount.duplicatable .remove-row .remove', function(e){
        e.preventDefault();

        $(this).parents('.duplicatable-element').remove();
    });

    /* Popup */
	$('body').on('click', '.popup-wrapper .close_btn, .close-popup-btn', function(e){
		e.preventDefault();

		if( $(this).parents('.popup-wrapper').hasClass('invoice-preview') ){
			$('.aw-settings-section .content-box a.generate-new-invoice-for-order').addClass('need-preview');
		}

		closePopup();
	});

	$('body').on('click', '.popup-wrapper.invoice-preview .button-container button.submit-invoice', function(e){
		e.preventDefault();

		$('.aw-settings-section .content-box a.generate-new-invoice-for-order').click();

		closePopup();
	});

});

/* Popup */
function showPopup( $id ){
	if( $id.length ){
		jQuery('.overlay-wrapper').addClass('active');
		jQuery('.popup-wrapper:not(.' + $id + ')').removeClass('active');
		jQuery('.popup-wrapper.' + $id ).addClass('active');
	}
}
function closePopup( $id ){
	jQuery('.overlay-wrapper').removeClass('active');
	
	if( $id != '' && $id != undefined ){
		jQuery('.popup-wrapper.' + $id ).removeClass('active');
	} else {
		jQuery('.popup-wrapper').removeClass('active');
	}
}