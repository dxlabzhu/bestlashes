jQuery(function($) {
	/*$('body').on( 'DOMSubtreeModified', '.modal-content .modal-body', function(e){
		var $modal = $('#modal .modal-content'),
			$modal_footer = $modal.find('.modal-footer'),
			$modal_body = $modal.find('.modal-body');

		if( $modal_body.find('section.receipt .table-wrapper.invoice').length != 0 ){
			if( $modal_footer.find('button.generate-invoice').length == 0 ){
				$modal_footer.append('<button class="btn btn-primary pull-left generate-invoice" data-action="">Számla kiállítása</button>');
			}
		}
	});*/

	// Order modal shown
	$('#modal .modal').on('shown.bs.modal', function(e){
		var $modal = $('#modal .modal-content'),
			$modal_footer = $modal.find('.modal-footer'),
			$modal_body = $modal.find('.modal-body'),
			order_id = $modal_body.find('.table-wrapper.invoice .panel-body').data('order-id');

		if( $modal_body.find('section.receipt .table-wrapper.invoice').length != 0 ){
			$modal_footer.find('button[data-action="print"]').hide();
			$modal_footer.find('button[data-action="email"]').hide();
			$modal_footer.find('div').append( '<span class="loading-icon pull-left loading"></span>' );
			
			jQuery.post(
				POS.options.ajaxurl, {
					action: "bl_pos_get_receipt_buttons",
					id: order_id
				}, function(data){
					$modal_footer.find('.loading-icon').removeClass('loading');
					$modal_footer.find('div').append(data.button);
				},
				'json'
			);
		}
	});

	// Modal button clicked
	$('#modal .modal').on('click', '.modal-footer button.generate-invoice', function(e){
		e.preventDefault();
		
		var $this = $(this),
			$modal = $('#modal .modal-content'),
			$modal_footer = $modal.find('.modal-footer'),
			$panel_body = $this.parents('.modal-content').find('.panel-body.receipt-list');

		$modal_footer.find('.loading-icon').addClass('loading');

		jQuery.post(
			POS.options.ajaxurl, {
				action: "bl_generate_invoice_for_order",
				id: $panel_body.data('order-id'),
				final: false,
				payment_deadline: $panel_body.data('payment-deadline'),
				vat_type: $panel_body.data('vat-type'),
				paid: $panel_body.data('paid'),
				from_pos: true
			}, function(data) {
				if(data.success == true){
					$this.replaceWith(data.button);
					$modal_footer.find('.loading-icon').removeClass('loading');
				} else {
					
				}
			},
			"json"
		);
	});

	// POS frontpage receipt view, after order placed
	$(window).on('hashchange', function(e){
		var current_url = window.location.href;

		if( current_url.indexOf("#receipt") >= 0 ){
			var $this = $(this),
				$container = $('#right .product-recieipt'),
				$actions = $container.find('.receipt-actions'),
				$panel_body = $container.find('.panel-body.receipt-list'),
				order_id = $panel_body.data('order-id');
				
			$actions.find('button[data-action="print"]').hide();
			$actions.find('button[data-action="email"]').hide();
			$actions.find('div').append( '<span class="loading-icon pull-left loading"></span>' );

			jQuery.post(
				POS.options.ajaxurl, {
					action: "bl_pos_get_receipt_buttons",
					id: order_id
				}, function(data){
					$actions.find('.loading-icon').removeClass('loading');
					$actions.find('div').append(data.button);
				},
				'json'
			);
		}
	});

	/*$(window).load(function() {
		var current_url = window.location.href;

		if( current_url.indexOf("#receipt") >= 0 ){
			var $this = $(this),
				$container = $('#right .product-recieipt'),
				$actions = $container.find('.receipt-actions'),
				$panel_body = $container.find('.panel-body.receipt-list'),
				order_id = $panel_body.data('order-id');
				
			$actions.find('button[data-action="print"]').hide();
			$actions.find('button[data-action="email"]').hide();
			$actions.find('div').append( '<span class="loading-icon pull-left loading"></span>' );

			jQuery.post(
				POS.options.ajaxurl, {
					action: "bl_pos_get_receipt_buttons",
					id: order_id
				}, function(data){
					$actions.find('.loading-icon').removeClass('loading');
					$actions.find('div').append(data.button);
				},
				'json'
			);
		}
	});*/

	// POS frontpage receipt view, button clicked
	$('body').on('click', '#right .product-recieipt button.generate-invoice', function(e){
		e.preventDefault();
		
		var $this = $(this),
			$container = $('#right .product-recieipt'),
			$actions = $container.find('.receipt-actions'),
			$panel_body = $container.find('.panel-body.receipt-list');

		$actions.find('.loading-icon').addClass('loading');

		jQuery.post(
			POS.options.ajaxurl, {
				action: "bl_generate_invoice_for_order",
				id: $panel_body.data('order-id'),
				final: false,
				payment_deadline: $panel_body.data('payment-deadline'),
				vat_type: $panel_body.data('vat-type'),
				paid: $panel_body.data('paid'),
				from_pos: true
			}, function(data) {
				if(data.success == true){
					$this.replaceWith(data.button);
					$actions.find('.loading-icon').removeClass('loading');
				} else {
					
				}
			},
			"json"
		);
	});

	// Add tax number to user
	$( document ).ajaxComplete( function( e, xhr, settings ){
		if( settings.url.match(/\/wp-json\/wc\/v3\/customers/) ){
			var user_id = xhr.responseJSON.id,
				tax_number = $('.modal-body form.customer-edit .billing-address input[name="billing_address[tax_number]"]').val();

			if( user_id && tax_number ){
				jQuery.post(
					POS.options.ajaxurl, {
						action: "bl_pos_save_tax_number_to_user",
						user_id: user_id,
						tax_number: tax_number
					}, function(data) {
					},
					"json"
				);
			}
		}
	});

	// Add user to order
	$( document ).ajaxComplete( function( e, xhr, settings ){
		if( settings.url.match(/\/wp-json\/wc\/v3\/orders$/) ){
			var order_id = xhr.responseJSON.id,
				user_id = $('#main #right .product-recieipt .panel-body.receipt-list').data('user-id');

			if( user_id && order_id ){
				jQuery.post(
					POS.options.ajaxurl, {
						action: "bl_pos_fix_user_on_new_order",
						user_id: user_id,
						order_id: order_id
					}, function(data) {
					},
					"json"
				);
			}
		}
	});

	// Customer modal shown
	$('#modal .modal').on('shown.bs.modal', function(e){
		var $modal = $('#modal .modal-content'),
			$modal_body = $modal.find('.modal-body');

		if( $modal_body.find('.customer-header.tax-number').length != 0 ){
			$tax_number_container = $modal_body.find('.customer-header.tax-number'),
			user_id = $modal_body.find('.table-wrapper.customer.bl-wrapper').data('user-id');

			$tax_number_container.find('.loading-icon').addClass('loading');
			
			jQuery.post(
				POS.options.ajaxurl, {
					action: "bl_pos_get_user_tax_number",
					id: user_id
				}, function(data){
					$tax_number_container.find('input[name="billing_address[tax_number]"]').val( data.tax_number );
					$tax_number_container.find('.loading-icon').removeClass('loading');
				},
				'json'
			);
		}
	});
});