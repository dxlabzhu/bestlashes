<div class="progress-bar">
	<div class="progress-bar-container">
		<div class="progress-bar-step">
			<div class="circle-counter <?php echo ( ( is_cart() || is_checkout() || is_wc_endpoint_url( 'order-received' ) ) ? 'active' : '' ); ?>">01</div>
			<a href="<?php echo wc_get_cart_url(); ?>" class="progress-bar-link"><?php echo bl_get_option_lang('bl-progress-bar-text-step-1' ); ?></a>
		</div>
		<div class="progress-bar-step">
		<div class="circle-counter <?php echo ( ( is_checkout() || is_wc_endpoint_url( 'order-received' ) ) ? 'active' : '' ); ?>">02</div>
			<a href="<?php echo wc_get_checkout_url(); ?>" class="progress-bar-link"><?php echo bl_get_option_lang('bl-progress-bar-text-step-2' ); ?></a>
		</div>
		<div class="progress-bar-step">
			<div class="circle-counter <?php echo ( ( is_wc_endpoint_url( 'order-received' ) ) ? 'active' : '' ); ?>">03</div>
			<a href="" class="progress-bar-link "><?php echo bl_get_option_lang('bl-progress-bar-text-step-3' ); ?></a>
		</div>
		<div class="progress-bar-line"></div>
	</div>
</div>