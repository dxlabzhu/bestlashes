<?php $cart_contents = (int)WC()->cart->get_cart_contents_count();

if( !empty( $eclass_for_minicart ) ){
	$eclass = implode( ' ', $eclass_for_minicart );
} ?>

<a href="<?php echo wc_get_cart_url(); ?>" class="nav-item-basket w-inline-block minicart <?php echo $eclass; ?>">
    <div class="basket-counter"><?php echo  $cart_contents; ?></div>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-bag.svg" class="icon-basket" alt="<?php _e('Add to cart', 'bl') ?>">
</a>