<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_before_cart' );?>

<div class="subpage-wrapper">
    <section class="section top">
		<div class="container">

			<?php bl_breadcrumb(); ?>
			
			<?php wc_get_template( 'order-progress-bar.php' ); ?>

			<?php wc_print_notices(); ?>
			
			<div class="table">
				<div class="w-row">
					<div class="table-wrapper">
						<div class="woocommerce-cart-form">
						
							<?php
							/**
							 * @hooked wc_empty_cart_message - 10
							 */
							do_action( 'woocommerce_cart_is_empty' );

							if ( wc_get_page_id( 'shop' ) > 0 ) : ?>

								<div class="table-footer">
									<div class="table-column _100percent">
										<a class="rounded-button w-button" href="<?php echo get_permalink( BL_PAGE_WEBSHOP ); ?>">
											<span class="chart-button-icon">←</span>
											<?php _e( 'Continue shopping', 'woocommerce' ) ?>
										</a>
									</div>
								</div>

							<?php endif; ?>

						</div>
					</div>
				</div>
			</div>
			<div class="cart_totals"></div>
		</div>
	</section>
</div>
<div class="clearfix"></div>

<?php do_action( 'woocommerce_after_cart' ); ?>