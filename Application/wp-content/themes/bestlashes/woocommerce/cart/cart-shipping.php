<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

$formatted_destination    = isset( $formatted_destination ) ? $formatted_destination : WC()->countries->get_formatted_address( $package['destination'], ', ' );
$has_calculated_shipping  = ! empty( $has_calculated_shipping );
$show_shipping_calculator = ! empty( $show_shipping_calculator );
$calculator_text          = '';
?>


<div class="cart-row shipping">
	<div class="table-header">
		<div class="table-column _100percent">
			<?php if( is_checkout() ){ ?>
				<div>
					<?php echo wp_kses_post( $package_name ); ?>
				</div>
			<?php } else {
				echo wp_kses_post( $package_name );
			} ?>
		</div>
	</div>

	<?php $available_methods = apply_filters( 'bl_fix_shipping_methods_filter', $available_methods ); ?>

	<?php if ( 1 < count( $available_methods ) ) : ?>

		<?php foreach ( $available_methods as $method ) : ?>
			<div class="table-row _2col"  data-title="<?php echo esc_attr( $package_name ); ?>">
				<div class="table-column _70percent">
					<div class="radio-checkbox-form-block w-form">
						<?php
							printf( '<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" %4$s />
								<label for="shipping_method_%1$d_%2$s">%5$s</label>',
								$index, esc_attr( sanitize_title( $method->id ) ), esc_attr( $method->id ), checked( $method->id, $chosen_method, false ), wc_cart_totals_shipping_method_label( $method ) );

							do_action( 'woocommerce_after_shipping_rate', $method, $index );
						?>
					</div>
				</div>
				<div class="table-column _30percent right">
					<div><?php echo strip_tags( wc_price( $method->cost ) ); ?></div>
                </div>
            </div>
		<?php endforeach; ?>

	<?php elseif ( 1 === count( $available_methods ) ) :  ?>
		<div class="table-row _2col  data-title="<?php echo esc_attr( $package_name ); ?>">
			<div class="table-column _70percent">
				<div class="radio-checkbox-form-block w-form">
					<?php
						$method = current( $available_methods );
						printf( '%3$s <input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d" value="%2$s" class="shipping_method" />', $index, esc_attr( $method->id ), wc_cart_totals_shipping_method_label( $method ) );
						do_action( 'woocommerce_after_shipping_rate', $method, $index );
					?>
				</div>
			</div>
			<div class="table-column _30percent right">
				<div><?php echo strip_tags( wc_price( $method->cost ) ); ?></div>
            </div>
		</div>
	<?php elseif ( ! $has_calculated_shipping ) : ?>
		<?php
			if ( is_cart() ) { ?>
				<div class="table-row _2col  data-title="<?php echo esc_attr( $package_name ); ?>">
					<div class="table-column _100percent">
						<?php echo wp_kses_post( apply_filters( 'woocommerce_no_shipping_available_html', __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) ); ?>
					</div>
				</div>
			<?php } else { ?>
				<div class="table-row _2col  data-title="<?php echo esc_attr( $package_name ); ?>">
					<div class="table-column _100percent">
						<?php echo wp_kses_post( apply_filters( 'woocommerce_no_shipping_available_html', __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) ); ?>
					</div>
				</div>
			<?php }
		?>
	<?php elseif ( ! is_cart() ) : ?>
		<div class="table-row _2col  data-title="<?php echo esc_attr( $package_name ); ?>">
			<div class="table-column _100percent">
				<?php echo wpautop( __( 'Enter your address to view shipping options.', 'woocommerce' ) ); ?>
			</div>
		</div>
	<?php endif; ?>

	<?php if ( $show_package_details ) : ?>
		<div class="table-row _2col  data-title="<?php echo esc_attr( $package_name ); ?>">
			<div class="table-column _100percent">
				<?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html( $package_details ) . '</small></p>'; ?>
			</div>
		</div>
	<?php endif; ?>

	<?php /*if ( ! empty( $show_shipping_calculator ) ) : ?>
		<?php woocommerce_shipping_calculator(); ?>
	<?php endif;*/ ?>
</div>