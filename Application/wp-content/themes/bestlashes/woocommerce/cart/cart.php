<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_cart' ); ?>

<div class="subpage-wrapper">
    <section class="section top">
		<div class="container">

			<?php bl_breadcrumb(); ?>

			<?php wc_get_template( 'order-progress-bar.php' ); ?>
			
			<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
				
				<?php if( isset( $_GET['cancel_order'] ) ){
					if( $_GET['cancel_order'] == 'true' && !empty( $_GET['order_id'] ) ){ ?>
						<div class="margin">
							<h3><?php _e('Payment failed', 'bl') ?></h3>
						</div>
					<?php }
				} ?>

				<?php wc_print_notices(); ?>
				
				<?php do_action( 'woocommerce_before_cart_table' ); ?>

				<div class="table">
					<div class="w-row">
						<div class="col w-col w-col-8 w-col-stack">
							<div class="shop_table shop_table_responsive cart woocommerce-cart-form__contents cart-table" cellspacing="0">
							
				            	<div class="table-wrapper">
				            		<div class="table-content">
				                		<div class="table-header mobilehide">
				                			<div class="table-column _40percent">
						                    	<div><?php esc_html_e( 'Product', 'woocommerce' ); ?></div>
						                    </div>
						                    <div class="table-column _20percent">
						                    	<div><?php esc_html_e( 'Price', 'woocommerce' ); ?></div>
						                    </div>
						                    <div class="table-column _20percent center">
						                    	<div><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></div>
						                    </div>
						                    <div class="table-column _20percent right">
						                    	<div><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></div>
						                    </div>
						                </div>
							
										<?php do_action( 'woocommerce_before_cart_contents' ); ?>
							
										<?php
										foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
											$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
											$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
											$gift_product = false;

											if( isset( $cart_item['gift-product'] ) ){
												if( $cart_item['gift-product'] == true ){
													$gift_product = true;
												}
											}
											
											if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
												$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );

												$is_double_row = false;

												// Is variable product
												if( $_product->is_type( 'variation' ) ){
													$is_double_row = true;
												}

												// Is training course
												$training_course = get_post_meta( $cart_item['product_id'], 'training_course', true );

												if( $training_course == '1' ){
													$is_double_row = true;
												} ?>

												<div class="woocommerce-cart-form__cart-item table-row <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?> <?php if( $is_double_row ) echo 'double-row'; ?>">
								
													<div class="table-column _10percent">
														<?php
														$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
								
														if ( ! $product_permalink ) {
															echo wp_kses_post( $thumbnail );
														} else {
															printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), wp_kses_post( $thumbnail ) );
														}
														?>
													</div>
							
													<div class="table-column _30percent product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
														<?php
														
														if( $gift_product ){
															echo '<span class="gift-product">'. __('Gift product', 'bl') .'</span>';
														}

														$product_name = apply_filters( 'bl_cart_item_name', $_product, $_product->get_name(), $cart_item, $cart_item_key );

														if ( ! $product_permalink ) {
															echo wp_kses_post( apply_filters( 'bl_woocommerce_cart_item_name', $product_name, $cart_item, $cart_item_key ) . '&nbsp;' );
															//echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
														} else {
															echo wp_kses_post( apply_filters( 'bl_woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $product_name ), $cart_item, $cart_item_key ) );
															//echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $product_name ), $cart_item, $cart_item_key ) );
														}

														do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );
								
														// Meta data.
														echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
								
														// Backorder notification.
														if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
															echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>' ) );
														}
														?>							
														<?php
															if( !$gift_product ){
																// @codingStandardsIgnoreLine
																echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
																	'<a href="%s" class="remove table-action" aria-label="%s" data-product_id="%s" data-product_sku="%s">%s</a>',
																	esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
																	esc_html__( 'Remove this item', 'woocommerce' ),
																	esc_attr( $product_id ),
																	esc_attr( $_product->get_sku() ),
																	esc_html__('Remove', 'bl')
																), $cart_item_key );
															}
														?>
													</div>
							
													<div class="table-column _20percent product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
														<div class="table-price">
															<?php
																if( $_product->is_on_sale() ){ ?>
																	<div class="old-price"><?php echo wc_price( $_product->get_regular_price() ); ?></div>
																<?php }

																echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
															?>
														</div>
														<div class="mobilehide">x</div>
													</div>
							
													<div class="product-quantity table-column _20percent center" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>">
														<?php

														if ( $_product->is_sold_individually() ) {
															$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
														} else {
															$product_quantity = woocommerce_quantity_input( array(
																'input_name'   => "cart[{$cart_item_key}][qty]",
																'input_value'  => $cart_item['quantity'],
																'max_value'    => $_product->get_max_purchase_quantity(),
																'min_value'    => '0',
																'product_name' => $_product->get_name(),
															), $_product, false );
														}

														if( $gift_product ){ ?>
															<div class="gift-product-quantity">
																<?php echo $cart_item['quantity']; ?>
															</div>
														<?php } else { ?>
															<a href="#" class="input-minus w-button">-</a>
															<?php echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok. ?>
															<a href="#" class="input-plus w-button">+</a>
														<?php } ?>
													</div>
							
													<div class="table-column _20percent product-subtotal" data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>">
														<div class="mobilehide">=</div>
														<?php
															echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
														?>
													</div>
												</div>
												<?php
											}
										} ?>

										<?php do_action( 'woocommerce_cart_contents' ); ?>
					
										<div class="table-footer">
											<div class="table-column _100percent mobilecenter">
												<a href="#" class="rounded-button mobilehide w-button">
													<span class="chart-button-icon">←</span> <?php _e( 'Back', 'bl' ); ?>
												</a>
												<?php if ( wc_coupons_enabled() ) {
													if( !is_vip_user() ){ ?>
														<div class="cartcoupon">
															<div class="mobilehide"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></div>
															<div class="table-form w-form cartcoupon coupon">
																<input type="text" name="coupon_code" class="form-input table-form-input w-input input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" />
																<button type="submit" class="round-button w-button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>">→</button>
																<?php do_action( 'woocommerce_cart_coupon' ); ?>
															</div>
														</div>
													<?php }
												} ?>

												<button type="submit" class="button hidden-content" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

												<?php do_action( 'woocommerce_cart_actions' ); ?>

												<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
											</div>
										</div>

						            </div>
						        </div>
					
								<?php do_action( 'woocommerce_after_cart_contents' ); ?>
							</div>
						</div>

						<?php do_action( 'woocommerce_after_cart_table' ); ?>

                        <?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

						<div class="cart-collaterals">
							<?php
								/**
								 * Cart collaterals hook.
								 *
								 * @hooked woocommerce_cross_sell_display
								 * @hooked woocommerce_cart_totals - 10
								 */
								do_action( 'woocommerce_cart_collaterals' );
							?>
						</div>
					</div>
				</div>

			</form>

		</div>
	</div>
</div>
<div class="clearfix"></div>

<?php do_action( 'woocommerce_after_cart' ); ?>
