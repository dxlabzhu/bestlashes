<?php
/**
 * Checkout terms and conditions area.
 *
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit; 

if ( apply_filters( 'woocommerce_checkout_show_terms', true ) && function_exists( 'wc_terms_and_conditions_checkbox_enabled' ) ) {
			do_action( 'woocommerce_checkout_before_terms_and_conditions' ); ?>

	<div class="table-row">
		<div class="table-column _100percent">
			<div class="radio-checkbox-form-block w-form">

			<div class="woocommerce-privacy-policy-and-conditions-wrapper">

				<div class="form-row">
					<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<input type="checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox accepting-documents" name="privacy-policy" <?php checked( isset( $_POST['privacy-policy'] ), true ); // WPCS: input var ok, csrf ok. ?> id="privacy-policy" value="1" />
						<span class="woocommerce-privacy-policy-and-conditions-checkbox-text">
                            <?php $lang = ICL_LANGUAGE_CODE; ?>
                            <?php if ($lang == 'hu') : ?>
                                <?php echo sprintf( __( 'I have read and agree to the %1$sPrivacy policy%2$s', 'bl' ), '<a href="'. get_permalink( get_option( 'wp_page_for_privacy_policy' ) ) .'">', '</a>'  ); ?>
                            <?php elseif ($lang == 'en') : ?>
                                <?php echo sprintf( __( 'I have read and agree to the %1$sPrivacy policy%2$s', 'bl' ), '<a href="'. get_permalink( 5384 ) .'">', '</a>'  ); ?>
                            <?php endif; ?>
						</span>&nbsp;<span class="required">*</span>
					</label>
					<input type="hidden" name="privacy-policy-field" value="1" />
				</div>

				<?php
				/**
				 * Terms and conditions hook used to inject content.
				 *
				 * @since 3.4.0.
				 * @hooked wc_privacy_policy_text() Shows custom privacy policy text. Priority 20.
				 * @hooked wc_terms_and_conditions_page_content() Shows t&c page content. Priority 30.
				 */
				do_action( 'woocommerce_checkout_terms_and_conditions' );
				?>

				</div>
			</div>
		</div>
	</div>
	<div class="table-row">
		<div class="table-column _100percent">
			<div class="radio-checkbox-form-block w-form">
				<div class="woocommerce-terms-and-conditions-wrapper">
					<div class="form-row">
						<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
						<input type="checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox accepting-documents" name="terms" <?php checked( apply_filters( 'woocommerce_terms_is_checked_default', isset( $_POST['terms'] ) ), true ); // WPCS: input var ok, csrf ok. ?> id="terms" />
							<span class="woocommerce-terms-and-conditions-checkbox-text"><?php wc_terms_and_conditions_checkbox_text(); ?></span>&nbsp;<span class="required">*</span>
						</label>
						<input type="hidden" name="terms-field" value="1" />
					</div>
				</div>
				<?php

				do_action( 'woocommerce_checkout_after_terms_and_conditions' ); ?>
			</div>
		</div>
	</div>

	<?php /* Newsletter subscription */ ?>
	<div class="table-row">
		<div class="table-column _100percent">
			<div class="radio-checkbox-form-block w-form">
				<div class="woocommerce-terms-and-conditions-wrapper">
					<div class="form-row">
						<?php parse_str( $_POST['post_data'], $postData ); ?>
						<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
						<input type="checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" name="newsletter" <?php checked( isset( $postData['newsletter'] ), true ); // WPCS: input var ok, csrf ok. ?> id="newsletter" />
							<span class="woocommerce-terms-and-conditions-checkbox-text"><?php _e('Subscribe to our newsletter', 'bl'); ?></span>
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php /* /Newsletter subscription */ ?>


<?php }
