<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.8.0
 */

defined( 'ABSPATH' ) || exit;

?>
<div class="shop_table woocommerce-checkout-review-order-table cart-table">
	<div class="table-wrapper">
        <div class="table-content">

			<div class="table-header">
				<div class="table-column _50percent"><?php esc_html_e( 'Product', 'woocommerce' ); ?></div>
				<div class="table-column _50percent right"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></div>
			</div>

			<?php
            do_action( 'woocommerce_review_order_before_cart_contents' );

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

					$gift_product = false;
					if( isset( $cart_item['gift-product'] ) ){
						if( $cart_item['gift-product'] == true ){
							$gift_product = true;
						}
					}

					$is_double_row = false;

					// Is variable product
					if( $_product->is_type( 'variation' ) ){
						$is_double_row = true;
					}

					// Is training course
					$training_course = get_post_meta( $cart_item['product_id'], 'training_course', true );

					if( $training_course == '1' ){
						$is_double_row = true;
					} ?>

					<div class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?> table-row _2col <?php if( $is_double_row ) echo 'double-row'; ?>">
						<div class="table-column _50percent product-name">
							<div>
								<?php if( $gift_product ){
									echo '<span class="gift-product">'. __('Gift product', 'bl') .'</span>';
								}

								$product_name = apply_filters( 'bl_cart_item_name', $_product, $_product->get_name(), $cart_item, $cart_item_key ); ?>

								<?php /*echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );*/ ?>
								<?php echo apply_filters( 'bl_woocommerce_cart_item_name', $product_name, $cart_item, $cart_item_key ); ?><?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', sprintf( '&times;&nbsp;%s', $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
								<?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
							</div>
						</div>
						<div class="table-column _50percent right product-total">
							<div class="price">
								<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
							</div>
						</div>
					</div>
					<?php
				}
			}

			do_action( 'woocommerce_review_order_after_cart_contents' );
			?>

			<div class="table-row _2col">
				<div class="table-column _50percent">
					<div class="bold-text">
						<?php esc_html_e( 'Subtotal', 'woocommerce' ); ?>
					</div>
				</div>
				<div class="table-column _50percent right">
					<div class="bold-text">
						<?php wc_cart_totals_subtotal_html(); ?>
					</div>			
				</div>
			</div>

			<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
				<div class="table-row _2col cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
					<div class="table-column _50percent">
						<div class="bold-text">
							<?php wc_cart_totals_coupon_label( $coupon ); ?>
						</div>
					</div>
					<div class="table-column _50percent right">
						<div class="bold-text">
							<?php wc_cart_totals_coupon_html( $coupon ); ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

			<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

				<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

				<?php wc_cart_totals_shipping_html(); ?>

				<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

			<?php endif; ?>

			<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
				<div class="table-row _2col fee">
					<div class="table-column _50percent">
						<div class="bold-text">
							<?php echo esc_html( $fee->name ); ?>
						</div>
					</div>
					<div class="table-column _50percent right">
						<div class="bold-text">
							<?php wc_cart_totals_fee_html( $fee ); ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

			<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
				<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
					<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
						<div class="table-row _2col tax-rate tax-rate-<?php echo esc_attr(sanitize_title( $code )); ?>">
							<div class="table-column _50percent">
								<div class="bold-text">
									<?php echo esc_html( $tax->label ); ?>
								</div>
							</div>
							<div class="table-column _50percent right">
								<div class="bold-text">
									<?php echo wp_kses_post( $tax->formatted_amount ); ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="table-row _2col tax-total">
						<div class="table-column _50percent">
							<div class="bold-text">
								<?php echo esc_html( WC()->countries->tax_or_vat() ); ?>
							</div>
						</div>
						<div class="table-column _50percent right">
							<div class="bold-text">
								<?php wc_cart_totals_taxes_total_html(); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>

			<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

			<div class="table-row _2col order-total">
				<div class="table-column _50percent">
					<div class="bold-text">
						<?php esc_html_e( 'Total', 'woocommerce' ); ?>
					</div>
				</div>
				<div class="table-column _50percent right">
					<div class="bold-text">
						<?php wc_cart_totals_order_total_html(); ?>
					</div>
				</div>
			</div>

			<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>
		</div>
	</div>
</div>
