<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php wp_title(''); ?></title>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <?php wp_head(); ?>
    <link href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon-32x32.png" rel="shortcut icon" type="image/x-icon">
    <link href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon-256x256.png" rel="apple-touch-icon">
    <?php if(ICL_LANGUAGE_CODE=='hu'){?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131142495-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-131142495-1');
        </script>
    <?php } elseif(ICL_LANGUAGE_CODE=='en'){?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131846036-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-131846036-1');
        </script>
    <?php } ?>
</head>
<body <?php body_class((is_page('szemoldok-laminalas') || (is_page('brow-lamination')) ? 'bestbrows' : '')); ?>>
    <div id="training-courses-info-overlay"></div>
    <?php do_action( 'bl_after_body_open' ); ?>
    
    <header class="header main">
        <div class="navigation">
            <div class="logo-hamburger-container">
                <a href="<?php echo home_url(); ?>" class="logo-container w-inline-block">
                    <div class="logo-block"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/logo-b.svg" width="60" height="60" class="logo-b" alt="BestLashesPro Logo">
                        <div class="logo-background"></div>
                    </div>
                    <div class="logo-text-container">
                        <?php if (is_page(BL_PAGE_BESTBROWS)) { ?>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/logo-bestbrows.svg" width="170" class="logo-best-brows" alt="BestBrows Logo">
                        <?php } else { ?>
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/logo-bestlashes.svg" width="185" class="logo-best-lashes" alt="BestLashesPro Logo">
                        <?php } ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/logo-professional.svg" width="141" class="logo-professional" alt="BestLashesPro Logo">
                    </div>
                </a>
                <a href="#" class="hamburger-container w-inline-block">
                    <div class="hamburger-slices">
                        <div class="hamburger-slice"></div>
                        <div class="hamburger-slice"></div>
                        <div class="hamburger-slice"></div>
                    </div>
                </a>
                <?php
                    ob_start();
                    wc_get_template( 'minicart.php' );
                    $mobile_menu_cart_button = ob_get_clean();

                    echo $mobile_menu_cart_button;
                ?>
            </div>
            <div class="nav-container">
                <?php wp_nav_menu(array(
                        'theme_location' => 'main_menu',
                        'container' => 'div',
                        'container_class' => 'menu-container main-menu',
                        'menu_class' => '',
                        'menu_id' => 'main-menu',
                        'items_wrap' => '%3$s',
                        'walker' => new Bestlashes_Main_Menu_Walker()
                    ));
                ?>
                <?php do_action('bl_after_header_menu'); ?>
            </div>
        </div>
        <div class="nav-white-background"></div>
    </header>

    <?php do_action( 'bl_after_header_close' ); ?>