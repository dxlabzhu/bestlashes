<?php echo get_header(); ?>

<div class="subpage-wrapper">
	<?php if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); 

			$image = get_the_post_thumbnail( get_the_id(), 'trainer-big', array( 'class' => 'image' ) );
			$short_dexcription = get_post_meta( get_the_id(), 'short-description', true );
			$gallery_conteiner_bg_img = wp_get_attachment_image( BL_VIDEO_BACKGROUND_IMAGE_ID, 'full', false, array( 'class' => 'image' ) );
			$gallery_images = json_decode( get_post_meta( get_the_id(), 'galeria_azonositok', true ) ); ?>

			<section class="section top">
				<div class="container">
					
					<?php bl_breadcrumb(); ?>

					<div class="columns">
						<div class="w-row">
							<div class="col w-col w-col-4">
								<div class="margin">
									<?php echo $image; ?>
								</div>
							</div>
							<div class="col w-col w-col-8 heading-fix">
								<div class="margin">
									<h3><?php the_title(); ?></h3>
									<h4><?php echo $short_dexcription; ?></h4>
									<?php the_content(); ?>
								</div>
							</div>
						</div>
						<div class="w-row">
							<div class="col w-col w-col-6"></div>
							<div class="col w-col w-col-6"></div>
						</div>
					</div>

					<?php if( !empty( $gallery_images ) ){ ?>
						<div class="separator-heading">
							<h3><?php echo sprintf( __( '%1$s\'s works', 'bl' ), the_title() ); ?></h3>
						</div>
						<div class="video-block">
							<?php $first_image = wp_get_attachment_image( $gallery_images[0], 'textbox-image', false, array( 'class' => 'image' ) ); ?>
							<div class="video-container" data-image='<?php echo $first_image; ?>'>
								<?php echo $first_image; ?>
							</div>
							<?php echo $gallery_conteiner_bg_img; ?>
							<div class="arrow-container-right">
								<a href="#" class="round-button w-button">→</a>
							</div>
							<div class="arrow-container-left">
								<a href="#" class="round-button w-button">←</a>
							</div>
						</div>
						<div class="video-slide-container">
							<div class="video-slide-mask video-carousel-wrapper <?php if( sizeof( $gallery_images ) > 6 ) echo 'owl-carousel'; ?>">
								<?php 
								$i = 0;
								foreach ( $gallery_images as $image_id ) {
									$image = wp_get_attachment_image( $image_id, 'video-thumbnail', false, array( 'class' => 'image' ) );
									$full_image = wp_get_attachment_image( $image_id, 'textbox-image', false, array( 'class' => 'image' ) ); ?>

									<div class="video-thumb-container <?php echo ( $i == 0 ? 'selected' : '' ); ?>" data-image='<?php echo $full_image; ?>'>
										<?php echo $full_image; ?>
									</div>

									<?php $i++;
								} ?>
							</div>
						</div>

					<?php } ?>
				</div>
			</section>

		<?php }
	} ?>
</div>

<?php get_footer(); ?>