<?php 

/*

	Template Name: Forgotten password

*/

get_header(); ?>

<div class="subpage-wrapper">
	<section class="section top">
		<div class="container padding-fixed">

			<?php bl_breadcrumb(); ?>

			<?php if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					the_content();
				}
			} ?>

			<div class="separator-heading">
				<h3 class="heading-03"><?php echo get_the_title( get_the_ID() ); ?></h3>
			</div>

			<div class="table _50-percent">
				<div class="table-wrapper">
					<div class="table-content">
						<form action="" method="post" class="forgotten-password-email-verification bl-form" data-error-text-all-field-required="<?php _e( 'All fields are required', 'bl' ); ?>">
							<div class="error-text" style="display: none;">
								<div class="table-column _100percent">
									<div class="text"></div>
								</div>
							</div>
							<div class="table-row _2col">
								<div class="table-column _30percent">
									<div><?php _e( 'E-mail address', 'bl' ); ?></div>
								</div>
								<div class="table-column _70percent">
									<div class="table-form _100percent w-form">
										<input type="text" class="forgotten-password-email table-form-input w-input req" value="" name="forgotten-password-email" placeholder="<?php _e( 'E-mail address', 'bl' ); ?>" data-error-text-no-user-found="<?php _e('Wrong e-mail address', 'bl'); ?>" data-error-text-not-valid="<?php _e( 'Not valid email address', 'bl' ); ?>" />
									</div>
								</div>
							</div>
							<div class="table-footer">
								<div class="table-column _100percent">
									<a href="<?php echo get_permalink( BL_PAGE_LOGIN ); ?>" class="rounded-button w-button"><span class="chart-button-icon">+</span> <?php _e( 'Login', 'bl' ); ?></a>
									<a href="#" class="rounded-button w-button forgotten-password-email-verification-submit loading-animation-button">
										<?php _e( 'Send', 'bl' ) ?>
										<span class="chart-button-icon">→</span>
										<span class="loading-button"></span>
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</section>
</div>

<?php get_footer(); ?>