<?php get_header(); ?>

<div class="subpage-wrapper">
	<div class="container">
		<div class="columns">
			<div class="w-row">
				<div class="col w-col w-col-12">
					<div class="wrapper-404">
						<div class="number-404">	
							404
						</div>
						<div class="text-404">	
							<p><?php _e('The page you\'re looking for does not exist.', 'bl'); ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>