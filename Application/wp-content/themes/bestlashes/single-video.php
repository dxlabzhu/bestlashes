<?php get_header(); 

$video_bg_img = wp_get_attachment_image( BL_VIDEO_BACKGROUND_IMAGE_ID, 'full', false, array( 'class' => 'image' ) );
$video_id = get_post_meta( get_the_ID(), 'video_id', true );
$video_categories = wp_get_post_terms( get_the_ID(), 'video_category' );
$main_video_image_url = get_the_post_thumbnail_url( get_the_ID(), 'video-thumbnail' ) ;

$main_video_category = $video_categories[0];
?>

<div class="subpage-wrapper">
    <?php if ( have_posts() ) {
        while ( have_posts() ) {
            the_post(); ?>
            
        	<div class="container padding-fixed">

        		<?php bl_breadcrumb(); ?>
        		
        		<div class="video-block">
                	<div class="video-container">
                    	<div style="padding-top:56.17021276595745%" class="video w-video w-embed" data-video-id="<?php echo $video_id; ?>" data-video-image='<?php echo $main_video_image_url; ?>'>
                    		<iframe src="https://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    	</div>
                	</div>
                  	<?php echo $video_bg_img; ?>
                  	<div class="arrow-container-right">
                  		<a class="round-button w-button">→</a>
                  	</div>
                  	<div class="arrow-container-left">
                  		<a class="round-button w-button">←</a>
                  	</div>
                </div>
                <div class="separator-heading">
                	<h3 class="heading-03"><?php _e( 'More Best lashes videos', 'bl' ); ?></h3>
                </div>
                <?php
                // Query
        		$args = array();
        		$args['post_type'] = 'video';
        		$args['posts_per_page'] = BL_VIDEO_CATEGORY_POSTS_PER_PAGE;
        		$args['post_status'] = 'publish';
        		$args['tax_query'] = array();
        		$args['tax_query'][] = array(
        			'taxonomy' => 'video_category',
        			'field' => 'term_id',
        			'terms' => $main_video_category->term_id
        		);
        		$args['fields'] = 'ids';
        		
        		$video_loop = new WP_Query( $args );
                
                ?>
                
                <div class="video-slide-container">
                	<div class="video-slide-mask video-carousel-wrapper <?php if( $video_loop->found_posts > 6 ) echo 'owl-carousel'; ?>">

                		<?php if( $video_loop->have_posts() ){
        					while ( $video_loop->have_posts() ) {
        						$video_id = $video_loop->next_post();
        						$video_yt_id = get_post_meta( $video_id, 'video_id', true );

        						// Image
        						$image = get_the_post_thumbnail( $video_id, 'video-thumbnail', array( 'class' => 'video-image' ) ) ;
        						$image_url = get_the_post_thumbnail_url( $video_id, 'video-thumbnail' );
        						
        						?>
                        		
                        		<div class="video-thumb-container" data-video-id="<?php echo $video_yt_id; ?>" data-video-image="<?php echo $image_url; ?>">
                          			<div class="play-button-container">
                          				<a href"" class="play-button w-button"></a>
                          			</div>
                        			<?php echo $image; ?>
                      			</div>

        					<?php }
        				} ?>
                  	</div>
                </div>
        	</div>

        <?php }
    } ?>	
</div>
<?php get_footer(); ?>