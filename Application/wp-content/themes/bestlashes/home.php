<?php 
	get_header();

	global $wp_query;
	//var_dump($wp_query);
?>

<div class="subpage-wrapper">
	<section class="bloglist-section">
		
		<div class="container">
			<?php bl_breadcrumb(); ?>
			
			<?php 
			$i = 1;
			
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();	?>
					
					<div class="bloglist-item">
			    		<div class="bloglist-image">
			    			<?php if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'blog-thumbnail', array( 'class' => 'image' ) );
							} ?>
			    		</div>
		    			<div class="bloglist-textbox w-clearfix">
		    				<h4 class="blogpost-date"><?php the_time( 'Y | F | d' ); ?></h4>
		    				<a href="<?php the_permalink(); ?>">
		    					<?php the_title( '<h2 class="blogpost-title">', '</h2>' ); ?>
		    				</a>
		    				<div class="paragraph">
		    					<?php the_excerpt(); ?>	
		    				</div>
							<a href="<?php the_permalink(); ?>" class="underline-button w-inline-block">
								<div class="underline-button-text"><?php _e( 'Read more', 'bl' ); ?> →</div>
		              			<div class="link-underline"></div>
							</a>
						</div>
			        </div>
						
					<?php
				}
			} ?>

			<div class="pagination">
				<?php wp_pagenavi(); ?>
			</div>
			
		</div>
	</section>
</div>

<?php get_footer(); ?>