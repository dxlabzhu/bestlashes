<?php 

/*

	Template Name: Registration

*/

get_header();

global $woocommerce;

$countries_obj   = new WC_Countries();
$countries   = $countries_obj->__get('countries'); ?>

<div class="subpage-wrapper">
	<section class="section top">
		<div class="container padding-fixed">

			<?php bl_breadcrumb(); ?>

			<?php if ( have_posts() ) {
				while ( have_posts() ) {
					the_post();
					the_content();
				}
			} ?>

			
			<div class="separator-heading">
				<h3 class="heading-03"><?php echo get_the_title( get_the_ID() ); ?></h3>
			</div>
			<form action="" method="post" class="registration bl-form" data-error-text-all-field-required="<?php _e( 'All fields are required', 'bl' ); ?>" data-error-text-user-already-registred="<?php _e( 'Alreay registred e-mail address', 'bl' ); ?>" enctype="multipart/form-data">

				<div class="error-text registration" style="display: none;">
					<div class="table-column _100percent">
						<div class="text"></div>
					</div>
				</div>

				<div class="table">
					<div class="w-row">
						<div class="col w-col w-col-6">
							<div class="table-wrapper">
								<div class="table-content">
									<div class="table-header">
										<div class="table-column _100percent">
											<div><?php _e( 'User information', 'bl' ) ?></div>
										</div>
									</div>
									<div class="table-row">
										<div class="table-column">
											<div><?php echo bl_get_option_lang('bl-content-before-registration-form' ); ?></div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'E-mail address', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="email" class="reg-email table-form-input w-input req" value="" name="reg-email" placeholder="<?php _e( 'E-mail address', 'bl' ); ?>" data-error-text-not-valid="<?php _e( 'Not valid email address', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Phone number', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-phone table-form-input w-input req" value="" name="reg-phone" placeholder="<?php _e( '+36 ...', 'bl' ); ?>" data-error-text-not-valid="<?php _e( 'Not valid phone number', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Password', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="password" class="reg-password table-form-input w-input req" value="" name="reg-password" placeholder="***" data-error-text-not-matching-passwords="<?php _e( 'Passwords not match', 'bl' ) ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Password again', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="password" class="reg-password-again table-form-input w-input req" value="" name="reg-password-again" placeholder="***" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Birthday', 'bl' ); ?></div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-birthday table-form-input w-input" value="" name="reg-birthday" placeholder="<?php _e( 'YYYY.MM.DD', 'bl' ); ?>" data-error-text-not-valid="<?php _e( "Not valid date format. Please enter the date in the format 'YYYY.MM.DD'", 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row small">
										<div class="table-column">
											<div class="radio-checkbox-form-block w-form">
												<div class="checkbox-field w-checkbox">
													<input type="checkbox" id="newsletter" name="newsletter" class="checkbox w-checkbox-input">
													<label for="newsletter" class="checkbox-label w-form-label">
														<?php _e('Subscribe our newsletter', 'bl'); ?>
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="table-row small">
										<div class="table-column">
											<div class="radio-checkbox-form-block w-form">
												<div class="checkbox-field w-checkbox">
													<input type="checkbox" id="terms-and-condition" name="terms-and-condition" class="checkbox w-checkbox-input terms-and-condition req" data-error-text-terms-and-condition="<?php _e('Please accept the Privacy policy', 'bl'); ?>">
													<label for="terms-and-condition" class="checkbox-label w-form-label">
                                                        <?php $lang = ICL_LANGUAGE_CODE; ?>
                                                        <?php if ($lang == 'hu') : ?>
                                                            <?php echo sprintf( __( 'I accept the %1$sPrivacy policy%2$s', 'bl' ), '<a href="'. get_permalink( get_option( 'wp_page_for_privacy_policy' ) ) .'">', '</a>'  ); ?>*
                                                        <?php elseif ($lang == 'en') : ?>
                                                            <?php echo sprintf( __( 'I accept the %1$sPrivacy policy%2$s', 'bl' ), '<a href="'. get_permalink( 5384 ) .'">', '</a>'  ); ?>*
                                                        <?php endif; ?>
													</label>
												</div>
											</div>
										</div>
									</div>
									<!--<div class="table-row small">
										<div class="table-column _50percent">
											<div><?php _e( 'Upload a certificate of qualification', 'bl' ) ?>: <span class="document-name"><strong></strong></span></div>
										</div>
										<div class="table-column _50percent right moblecenter">
											<input type="file" name="document" value="" id="document" style="display: none;" accept="application/pdf, image/*" data-error-wrong-extension="<?php _e( 'Wrong file extension. Please use .JPG, .JPEG, .PNG, .PDF', 'bl' ); ?>" />
											<a href="#" class="rounded-button w-button file-attachment">
												<?php _e( 'File', 'bl' ); ?><span class="chart-button-icon">↑</span>
											</a>
										</div>
									</div>-->
								</div>
							</div>
						</div>
						<div class="col w-col w-col-6">
							<div class="table-wrapper shipping">
								<div class="table-content">
									<div class="table-header">
										<div class="table-column _100percent">
											<div><?php _e( 'Shipping information', 'bl' ); ?></div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'First name', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-shipping-first-name table-form-input w-input req" value="" name="reg-shipping-first-name" placeholder="<?php _e( 'First name', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Last name', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-shipping-last-name table-form-input w-input req" value="" name="reg-shipping-last-name" placeholder="<?php _e( 'Last name', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Country', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<select name="reg-shipping-country" class="country_to_state country_select table-form-input w-input req">
													<option value=""><?php _e( 'Please choose one', 'bl' ) ?></option>
													<?php if( !empty( $countries ) ){
														foreach ( $countries as $code => $label ) { ?>
															<option value="<?php echo $code; ?>"><?php echo $label; ?></option>
														<?php }
													} ?>
												</select>
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'ZIP / City', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form w-form">
												<input type="text" class="reg-shipping-zip table-form-input w-input req" value="" name="reg-shipping-zip" placeholder="<?php _e( 'Zip code', 'bl' ); ?>" />
											</div>
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-shipping-city table-form-input w-input req" value="" name="reg-shipping-city" placeholder="<?php _e( 'City', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Address', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-shipping-address table-form-input w-input req" value="" name="reg-shipping-address" placeholder="<?php _e( 'Street, house number, floor, door...', 'bl' ); ?>" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="table-wrapper">
								<div class="table-content">
									<div class="table-header">
										<div class="table-column _100percent w-clearfix">
											<a href="#" class="underline-button w-inline-block copy-shipping-to-billing">
												<div class="underline-button-text"><?php _e( 'Copy to billing informations', 'bl' ) ?> <span class="underline-button-icon">↓</span></div>
												<div class="link-underline"></div>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="table-wrapper billing">
								<div class="table-content">
									<div class="table-header">
										<div class="table-column _100percent">
											<div><?php _e( 'Billing information', 'bl' ); ?></div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'First name', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-billing-first-name table-form-input w-input req" value="" name="reg-billing-first-name" placeholder="<?php _e( 'First name', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Last name', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-billing-last-name table-form-input w-input req" value="" name="reg-billing-last-name" placeholder="<?php _e( 'Last name', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Company', 'bl' ); ?></div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-billing-company table-form-input w-input" value="" name="reg-billing-company" placeholder="<?php _e( 'Company name', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Country', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<select name="reg-billing-country" class="country_to_state country_select table-form-input w-input req">
													<option value=""><?php _e( 'Please choose one', 'bl' ) ?></option>
													<?php if( !empty( $countries ) ){
														foreach ( $countries as $code => $label ) { ?>
															<option value="<?php echo $code; ?>"><?php echo $label; ?></option>
														<?php }
													} ?>
												</select>
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'ZIP / City', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form w-form">
												<input type="text" class="reg-billing-zip table-form-input w-input req" value="" name="reg-billing-zip" placeholder="<?php _e( 'Zip code', 'bl' ); ?>" />
											</div>
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-billing-city table-form-input w-input req" value="" name="reg-billing-city" placeholder="<?php _e( 'City', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Address', 'bl' ); ?>*</div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-billing-address table-form-input w-input req" value="" name="reg-billing-address" placeholder="<?php _e( 'Street, house number, floor, door...', 'bl' ); ?>" />
											</div>
										</div>
									</div>
									<div class="table-row _2col">
										<div class="table-column _30percent">
											<div><?php _e( 'Tax number', 'bl' ); ?></div>
										</div>
										<div class="table-column _70percent">
											<div class="table-form _100percent w-form">
												<input type="text" class="reg-billing-tax-number table-form-input w-input" value="" name="reg-billing-tax-number" placeholder="<?php _e( 'Tax number', 'bl' ); ?>" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-wrapper">
						<div class="table-content">
							<div class="table-footer">
								<div class="table-column _100percent center">
									<a href="#" class="rounded-button w-button registration-submit loading-animation-button">
										<?php _e( 'Registration', 'bl' ); ?>
										<span class="chart-button-icon">→</span>
										<span class="loading-button"></span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>

		</div>
	</section>
</div>

<?php get_footer(); ?>