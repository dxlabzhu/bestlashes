<?php 

/*

	Template Name: About us

*/

get_header();

$lang = ICL_LANGUAGE_CODE; ?>

<div class="subpage-wrapper">
	<div class="container padding-fixed">

		<?php do_action( 'bl_before_page_content' ); ?>
		
		<?php if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				the_content();
			}
		} ?>
		
		<!-- Best Lashes Pro -->
		<div class="vc_row">
			<?php

			// Query
			$args = array();
			$args['post_type'] = 'trainer';
			$args['posts_per_page'] = '-1';
			$args['post_status'] = 'publish';
			$args['fields'] = 'ids';
			$args['tax_query'] = array();
			$args['tax_query'][] = array(
				'taxonomy' => 'trainer_category',
				'field' => 'slug',
				'terms' => 'best-lashes-pro'
			);
			
			$trainer_loop = new WP_Query( $args ); ?>
			
			<?php if( $trainer_loop->have_posts() ){
				$i = 0; ?>
				<div class="separator-heading">
					<h3>
						<?php echo sprintf( __( 'The %1$sBest Lashes Pro%2$s trainers', 'bl' ), '<span class="text-logo-h3">', '</span>' ); ?>
					</h3>
		        </div>
			
				<div class="columns">
					<div class="w-row">
						<?php while ( $trainer_loop->have_posts() ) {
							$trainer_id = $trainer_loop->next_post();
							$description =  get_post_meta( $trainer_id, 'short-description', true );
							$city =  get_post_meta( $trainer_id, 'city', true );
							$image = get_the_post_thumbnail( $trainer_id, 'full', array( 'class' => 'image' ) ); ?>

							<div class="col w-col w-col-3">
								<div class="trainer-short margin center w-clearfix">
									<?php echo $image; ?>
									<h4 class="about-list-people-heading-4"><strong><?php echo get_the_title( $trainer_id ); ?></strong></h4>
									<?php echo wpautop( $description . '<br>' . '– '. $city .' –' ); ?>
									<a href="<?php echo get_permalink( $trainer_id ); ?>" class="underline-button w-inline-block">
										<div class="underline-button-text"><?php _e('Read more', 'bl'); ?> →</div>
										<div class="link-underline"></div>
									</a>
								</div>
							</div>

							<?php $i++;

							if( $i % 4 == 0 && $i != 0 ){ ?>
								</div><div class="w-row">
							<?php }
						} ?>
					</div>
				</div>
			<?php } ?>
		</div>
		<!-- //Best Lashes Pro -->
		
		<?php if( BL_LANG == 'HU' && $lang == 'hu' ){ // Only HU ?>
			<!-- Best Brows Pro -->
			<div class="vc_row">
				<?php

				// Query
				$args = array();
				$args['post_type'] = 'trainer';
				$args['posts_per_page'] = '-1';
				$args['post_status'] = 'publish';
				$args['fields'] = 'ids';
				$args['tax_query'] = array();
				$args['tax_query'][] = array(
					'taxonomy' => 'trainer_category',
					'field' => 'slug',
					'terms' => 'best-brows-pro'
				);
				
				$trainer_loop = new WP_Query( $args ); ?>

				<?php if( $trainer_loop->have_posts() ){
					$i = 0; ?>

					<div class="separator-heading">
						<h3>
							<?php echo sprintf( __( 'The %1$sBest Brows Pro%2$s trainers', 'bl' ), '<span class="text-logo-h3">', '</span>' ); ?>
						</h3>
			        </div>
					<div class="columns">
						<div class="w-row">
							<?php while ( $trainer_loop->have_posts() ) {
								$trainer_id = $trainer_loop->next_post();
								$description =  get_post_meta( $trainer_id, 'short-description', true );
								$city =  get_post_meta( $trainer_id, 'city', true );
								$image = get_the_post_thumbnail( $trainer_id, 'full', array( 'class' => 'image' ) ); ?>

								<div class="col w-col w-col-3">
									<div class="trainer-short margin center w-clearfix">
										<?php echo $image; ?>
										<h4 class="about-list-people-heading-4"><strong><?php echo get_the_title( $trainer_id ); ?></strong></h4>
										<?php echo wpautop( $description . '<br>' . '– '. $city .' –' ); ?>
										<a href="<?php echo get_permalink( $trainer_id ); ?>" class="underline-button w-inline-block">
											<div class="underline-button-text"><?php _e('Read more', 'bl'); ?> →</div>
											<div class="link-underline"></div>
										</a>
									</div>
								</div>

								<?php $i++;

								if( $i % 4 == 0 && $i != 0 ){ ?>
									</div><div class="w-row">
								<?php }
							} ?>
						</div>
					</div>
				<?php } ?>
			</div>
			<!-- //Best Brows Pro -->
		<?php } ?>

		<?php do_action( 'bl_after_page_content' ); ?>
	</div>
</div>
<?php get_footer(); ?>