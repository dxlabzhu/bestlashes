<?php get_header(); ?>
<div class="subpage-wrapper">
	<div class="container padding-fixed">

		<?php do_action( 'bl_before_page_content' ); ?>
		
		<?php if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				the_content();
			}
		} ?>

		<?php do_action( 'bl_after_page_content' ); ?>
	</div>
</div>
<?php get_footer(); ?>