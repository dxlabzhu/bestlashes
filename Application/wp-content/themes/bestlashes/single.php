<?php echo get_header(); ?>

<div class="subpage-wrapper">
	<?php if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); ?>

			<section class="subpage-hero">
				<div class="container">

					<?php bl_breadcrumb(); ?>
					
					<div class="floating-boxes-content">
						<div class="whtie-floating-textbox">
							<h4 class="blogpost-date"><?php the_time( 'Y | F | d' ); ?></h4>
							<?php the_title( '<h1 class="blogpost-title">', '</h1>' ); ?>
							<div class="separator-line"></div>
							<div class="paragraph"><?php _e('Wrote', 'bl') ?>: <?php echo get_the_author_meta( 'display_name' ); ?></div> 
						</div>
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail( 'blog-thumbnail', array( 'class' => 'floating-image' ) );
						} ?>
					</div>
				</div>
			</section>
		    
		    <section id="post" class="section">
				<div class="container">
					<div class="columns">
						<div class="w-row">
							<div class="col w-col w-col-4">
								<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('single-post-sidebar') ) : endif; ?>
							</div>
							<div class="col w-col w-col-8">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</section>

		<?php }
	} ?>
</div>

<?php get_footer(); ?>