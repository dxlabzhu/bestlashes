<?php 

/*

	Template Name: Webshop

*/

get_header();

// View
if( !empty( $_COOKIE['product-list']['view'] ) ){
	$product_view = $_COOKIE['product-list']['view'];
} else {
	if( $products_loop->found_posts <= BL_WEBSHOP_PRODUCTS_VIEW_LIMIT ){
		$product_view = 'row';
	} else {
		$product_view = 'boxed';
	}
}

$bl_open_sidebar_wrapper = false;

if( !empty( $_GET[ __('filter', 'bl') ] ) || !empty( $_GET[ __('search', 'bl') ] ) ){
	$bl_open_sidebar_wrapper = false;
}

// Before content
include( locate_template( 'template-parts/webshop/webshop-before-content.php', false, false ) ); 

// Sidebar
include( locate_template( 'template-parts/webshop/webshop-sidebar.php', false, false ) ); 

if( !empty( $_GET[ __('filter', 'bl') ] ) || !empty( $_GET[ __('search', 'bl') ] ) ){
	// Before product list loop
	include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-before-product-loop.php', false, false ) );

	// Product list loop
	include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-loop.php', false, false ) );

	// After product list loop
	include( locate_template( 'template-parts/webshop/product-list/webshop-product-list-after-product-loop.php', false, false ) ); 
} else {
	// Before category list loop
	include( locate_template( 'template-parts/webshop/category-list/webshop-category-list-before-category-loop.php', false, false ) );

	// Category list loop
	include( locate_template( 'template-parts/webshop/category-list/webshop-category-list-loop.php', false, false ) );

	// After category list loop
	include( locate_template( 'template-parts/webshop/category-list/webshop-category-list-after-category-loop.php', false, false ) ); 
}

// After content
include( locate_template( 'template-parts/webshop/webshop-after-content.php', false, false ) ); 


get_footer(); ?>