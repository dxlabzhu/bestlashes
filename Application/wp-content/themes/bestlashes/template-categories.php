<?php 

/*

	Template Name: Video categories

*/

get_header(); ?>


<div class="subpage-wrapper">
	<div class="container padding-fixed">
		
		<?php if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				the_content();
			}
		} ?>
		
	</div>
	
	<section id="about" class="section">
		<div class="container">
			<div class="separator-heading">
				<h3><?php _e('Best Lashes video categories', 'bl'); ?></h3>
			</div>
			<div class="columns">
				<div class="w-row">
	          	
		          	<?php $video_terms = get_terms( array(
					    'taxonomy' => 'video_category',
					    'hide_empty' => false,
					) );
					
					$i = 0;
					if ( ! empty( $video_terms ) ) {
						foreach ( $video_terms as $term ) {
							$term_image = get_term_meta( $term->term_id, 'featured_image', true );
							$link = '#';
							
							// Query
							$args = array();
							$args['post_type'] = 'video';
							$args['posts_per_page'] = 1;
							$args['post_status'] = 'publish';
							$args['tax_query'] = array();
							$args['tax_query'][] = array(
								'taxonomy' => 'video_category',
								'field' => 'term_id',
								'terms' => $term->term_id
							);
							$args['fields'] = 'ids';
							
							$video_loop = new WP_Query( $args );
							
							if( $video_loop->have_posts() ){
								while ( $video_loop->have_posts() ) {
									$video_id = $video_loop->next_post();
									$link = get_permalink( $video_id );
								}
							}
							
							if( $i % 4 == 0 && $i != 0 ){ ?>
								</div><div class="w-row">
							<?php } ?>
						
							<div class="col w-col w-col-3">
				            	<div class="margin">
				            		<a href="<?php echo $link; ?>" class="categories-link-block w-inline-block">
				            			<img src="<?php echo $term_image ?>" class="image">
										<h3 class="categories-heading"><?php echo $term->name; ?></h3>
									</a>
								</div>
				            </div>
					            
							<?php
							$i++;
						}
			        } ?>
	            
	          	</div>
			</div>
		</div>
	</section>	
</div>


<?php get_footer(); ?>