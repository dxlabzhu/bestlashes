<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header();

global $post;

$_product = wc_get_product( get_the_ID() );

// Images
$large_image_src = wp_get_attachment_image_src( $_product->get_image_id(), 'full' )[0];
$image = $_product->get_image( 'product-list-boxed', array( 'class' => 'product-image', 'data-zoomed' => $large_image_src ) );
$gallery_images = $_product->get_gallery_attachment_ids();
$default_selected_attributes = array();

if( $_product->is_type( 'variable' ) ){

	// Variable cache datas
	$product_variation_available_variations_data_cache = wp_cache_get( 'bl_product_variation_available_variations_data_cache_' . get_the_ID() . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );
	$product_variation_available_attributes_data_cache = wp_cache_get( 'bl_product_variation_available_attributes_data_cache_' . get_the_ID() . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );
	$product_variation_attributes_data_cache = wp_cache_get( 'bl_product_variation_attributes_data_cache_' . get_the_ID() . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );
	//$product_variation_variable_datas_data_cache = wp_cache_get( 'bl_product_variation_variable_datas_data_cache_' . get_the_ID() . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );

	// VIP Role check
	if( is_vip_user() ){
		$role_slug = bl_get_user_role_slug();

		if( !empty( $role_slug ) ){
			$product_variation_variable_datas_data_cache = wp_cache_get( 'bl_product_variation_variable_datas_data_cache_' . get_the_ID() . '_' . $role_slug . '_' . strtolower( ICL_LANGUAGE_CODE ), 'bl_product' );
		}
	}

	// Cache check
	if( empty( $product_variation_available_variations_data_cache ) || empty( $product_variation_available_attributes_data_cache ) || empty( $product_variation_attributes_data_cache ) || empty( $product_variation_variable_datas_data_cache ) ){
		
		// Variation data
		$variation_data = bl_generate_variations_data( $_product );

		$available_variations = $variation_data['available_variations'];
		$available_attributes = $variation_data['available_attributes'];
		$attributes = $variation_data['attributes'];
		$variable_datas = $variation_data['variable_datas'];

	} else {
		$available_variations = $product_variation_available_variations_data_cache;
		$available_attributes = $product_variation_available_attributes_data_cache;
		$attributes = $product_variation_attributes_data_cache;
		$variable_datas = $product_variation_variable_datas_data_cache;
	}
} else {
	$available_variations = array();
	$attributes = array();
	$variable_datas = array();

	// Meta info
	$properties_text = get_post_meta( get_the_ID(), 'product-properties-text', true );
	$product_description = get_the_excerpt_by_id_for_products( get_the_ID() );
	$course_listing_id_needed = get_post_meta(get_the_ID(),'course-listing-id-needed', true);

	if ($course_listing_id_needed == '1') {
        $course_listing_id = get_post_meta(get_the_ID(),'course-listing-id', true);
    }
}

$how_to_use_video_id = get_post_meta( get_the_ID(), 'product-how-to-use-video-id', true );

// Prices
$prices = bl_get_product_prices( $_product ); ?>

<div class="subpage-wrapper">
	<section class="section top">
		<div class="container">
			<?php bl_breadcrumb(); ?>

			<?php wc_print_notices(); ?>
			
			<?php if( have_posts() ){
				while ( have_posts() ){
					the_post(); ?>

					<div class="w-row">
						<div class="col w-col w-col-4">
							<div class="productpage-image-container">
								<div class="productpage-image-bg" data-main-image='<?php echo $image; ?>'>
									<?php echo $image; ?>
								</div>
							</div>
							<?php if( !empty( $gallery_images ) ){
								$i = 0; ?>
								<div class="thumbnail-container">
									<?php foreach ( $gallery_images as $gallery_image_id ) {
										$gallery_normal_image_url = wp_get_attachment_image_src( $gallery_image_id, 'full' )[0];
										$gallery_image = wp_get_attachment_image( $gallery_image_id, 'product-gallery', array( 'class' => 'product-image' ) ) ;
										$gallery_normal_image = wp_get_attachment_image( $gallery_image_id, 'product-list-boxed', false, array( 'class' => 'product-image', 'data-zoomed' => $gallery_normal_image_url ) );

										if( $i % BL_SINGLE_PRODUCT_GALLERY_ROW_LIMIT == 0 && $i != 0 ){ ?>
											</div><div class="thumbnail-container">
										<?php } ?>

										<div class="thumbnail-col">
											<a href="#" class="thumbnail-bg w-inline-block" data-normal-image='<?php echo $gallery_normal_image; ?>'>
												<?php echo $gallery_image; ?>
											</a>
										</div>

										<?php $i++;
									}

									$missing_thumbs_count = 0;

									if( sizeof( $gallery_images ) > BL_SINGLE_PRODUCT_GALLERY_ROW_LIMIT ){
										$missing_thumbs_count = BL_SINGLE_PRODUCT_GALLERY_ROW_LIMIT - ( sizeof( $gallery_images ) % BL_SINGLE_PRODUCT_GALLERY_ROW_LIMIT );
									} else {
										$missing_thumbs_count = BL_SINGLE_PRODUCT_GALLERY_ROW_LIMIT - sizeof( $gallery_images );
									}


									/*if( $missing_thumbs_count != 0 ){ 
										for ( $i = 0; $i < $missing_thumbs_count; $i++) { ?>
										 	<div class="thumbnail-col">
												<div class="thumbnail-bg"></div>
											</div>
										 <?php }
									}*/ ?>
								</div>
							<?php } ?>
						</div>
						<div class="col w-col w-col-8">
							<div class="productpage-content-container w-clearfix">
								<h1 class="heading-01">
									<?php the_title(); ?>
								</h1>
								<div class="separator-line"></div>
								<div class="paragraph">
									<?php the_content(); ?>
								</div>
                                <?php if (isset($course_listing_id)) : ?>
                                    <p>A képzés nyilvántartási száma: <?php echo $course_listing_id; ?></p>
                                <?php endif; ?>
								<div class="variation-options">
									<?php if( !empty( $available_attributes ) ){
										foreach ( $available_attributes as $attribute_slug => $attribute ) {
											if( !empty( $attribute['attribute'] ) ){
												if ( $attribute['attribute']->is_taxonomy() ) { ?>

													<div class="option-container" data-attribute-slug="<?php echo $attribute_slug; ?>">
														<div class="option-trigger" data-ix="dropdown">
															<div class="option-name">
																<?php 
																$default_variation_label = '';
																if( !empty( $attribute['variations'] ) ){
																	if( sizeof( $attribute['variations'] ) == 1 ){
																		$default_selected_attributes[$attribute_slug] = $attribute['variations'][0]->slug;
																		$default_variation_label = $attribute['variations'][0]->name;
																	}

																	if( !empty( $_GET['attribute_' . $attribute_slug] ) ){
																		foreach ( $attribute['variations'] as $term ) {
																			if( $term->slug == $_GET['attribute_' . $attribute_slug] ){
																				$default_variation_label = $term->name;
																			}
																		}
																	}
																}

																$attribute_name = bl_get_option_lang('bl-product-variation-label-'. $attribute['attribute']->get_id() );
																if( empty( $attribute_name ) ){
																	$attribute_name = wc_attribute_label( $attribute['attribute']->get_name(), $_product );
																} ?>
																<?php echo $attribute_name; ?>: 
																<strong><?php echo $default_variation_label; ?></strong>
															</div>
															<div class="link-icon-right">+</div>
														</div>
														<div class="dropdown-toggle">
															<?php
															$need_selection = false;
															$selected_term = '';

															if( !empty( $_GET['attribute_' . $attribute_slug] ) ){
																$need_selection = true;
																$selected_term = $_GET['attribute_' . $attribute_slug];
															} ?>

															<div class="option-button-conainer <?php if( sizeof( $attribute['variations'] || $need_selection ) == 1 ){ echo 'has-selection'; } ?>">
																<?php if( !empty( $attribute['variations'] ) ){
																	if( $need_selection ){
																		foreach ( $attribute['variations'] as $term ) {
																			if( $selected_term == $term->slug ){ ?>
																				<a href="#" class="option-button w-inline-block selected" data-variation-slug="<?php echo $term->slug; ?>">
																					<div><?php echo $term->name; ?></div>
																				</a>
																			<?php } else { ?>
																				<a href="#" class="option-button w-inline-block not-avaiable" data-variation-slug="<?php echo $term->slug; ?>">
																					<div><?php echo $term->name; ?></div>
																				</a>
																			<?php }
																		}
																	} else{
																		foreach ( $attribute['variations'] as $term ) { ?>
																		 	<a href="#" class="option-button w-inline-block <?php if( sizeof( $attribute['variations'] ) == 1 ){ echo 'selected'; } ?>" data-variation-slug="<?php echo $term->slug; ?>">
																				<div><?php echo $term->name; ?></div>
																			</a>
																		<?php }
																	}
																} ?>

																<a href="#" class="option-button w-inline-block delete-selection"">
																	<div>&#x2715;</div>
																</a>
															</div>
														</div>
													</div>

												<?php }
											}
										}
									} ?>
								</div>
								<?php if( $_product->is_in_stock() ){ ?>
									<div class="add-to-cart-container hide-if-not-purchasable" data-variation-data='<?php if( $_product->is_type('variable') ){ echo wp_json_encode( $variable_datas ); } ?>'>
										<form action="" method="post" accept-charset="utf-8" class="add-to-cart">
											<div class="price-block" data-default-price="<?php echo $prices['gross_price_formatted']; ?>" data-default-net-price="<?php echo $prices['net_price_formatted'] ?>">
												<div class="price"><?php echo $prices['gross_price_formatted']; ?></div>
												<div class="net-price"><?php echo $prices['net_price_formatted'] ?></div>
											</div>
											<div class="quantity-container"><a href="#" class="input-minus w-button">-</a>
												<div class="w-form">
													<input type="number" class="quantity-input qty w-input" maxlength="256" name="quantity" placeholder="1" step="1" min="1" pattern="[0-9]*" inputmode="numeric" value="1">
												</div>
												<a href="#" class="input-plus w-button">+</a>
											</div>
											<?php if( bl_is_user_eligible_to_buy_product( $_product->get_id() ) ){ ?>
												<div class="add-to-cart-button-container w-inline-block">
													<a href="#" class="add-to-cart-button w-inline-block">
														<div class="add-to-text"><?php _e('Add to cart', 'bl') ?></div>
														<img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-bag.svg" class="icon-basket" alt="<?php _e('Add to cart', 'bl') ?>">
														<div class="loading-button"></div>
													</a>
												</div>
											<?php } ?>
											<?php if( $_product->is_type('variable') ){
												if( !empty( $attributes ) ){
													foreach ( $attributes as $attribute_slug => $attribute ) {
														$default_value = '';

														if( isset( $default_selected_attributes[ $attribute_slug ] ) ){
															if( !empty( $default_selected_attributes[ $attribute_slug ] ) ){
																$default_value = $default_selected_attributes[ $attribute_slug ];
															}
														}

														if( !empty( $_GET['attribute_' . $attribute_slug] ) ){
															$default_value = $_GET['attribute_' . $attribute_slug];
														} ?>
														<input type="hidden" name="attribute_<?php echo $attribute_slug; ?>" class="variation-slugs" value="<?php echo $default_value; ?>">


														<?php 
														// Trigger change
														if( $_GET['attribute_' . $attribute_slug] ){ ?>
															<script type="text/javascript" charset="utf-8">
																jQuery(window).load(function() {
																    jQuery('input[name="attribute_<?php echo $attribute_slug; ?>"]').trigger('change');
																});
															</script>
														<?php } ?>
													<?php }
												}
											} ?>
											<input type="hidden" name="add-to-cart" value="<?php echo get_the_ID(); ?>">
										</form>
									</div>
									<div class="separator-line mobilehide hide-if-not-purchasable"></div>
									<a href="<?php echo get_permalink( BL_PAGE_WEBSHOP ); ?>" class="underline-button mobilehide w-inline-block hide-if-not-purchasable">
										<div class="underline-button-text">
											<span class="underline-button-icon">← </span><?php _e( 'back to webshop', 'bl' ); ?>
										</div>
										<div class="link-underline"></div>
									</a>
									<div class="add-to-cart-container not-in-stock show-if-not-purchasable" style="display: none;">
										<div class="price-block">
											<div class="not-in-stock"><?php _e( 'Not in stock', 'bl' ); ?></div>
										</div>
										<div class="separator-line mobilehide"></div>
										<a href="<?php echo get_permalink( BL_PAGE_WEBSHOP ); ?>" class="underline-button mobilehide w-inline-block">
											<div class="underline-button-text">
												<span class="underline-button-icon">← </span><?php _e( 'back to webshop', 'bl' ); ?>
											</div>
											<div class="link-underline"></div>
										</a>
									</div>
								<?php } else { ?>
									<div class="add-to-cart-container not-in-stock">
										<div class="price-block">
											<div class="not-in-stock"><?php _e( 'Not in stock', 'bl' ); ?></div>
										</div>
										<div class="separator-line mobilehide"></div>
										<a href="<?php echo get_permalink( BL_PAGE_WEBSHOP ); ?>" class="underline-button mobilehide w-inline-block">
											<div class="underline-button-text">
												<span class="underline-button-icon">← </span><?php _e( 'back to webshop', 'bl' ); ?>
											</div>
											<div class="link-underline"></div>
										</a>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="productpage-details-container">
						<div class="separator-line"></div>
						<?php if( $_product->is_type( 'simple' ) ){
							
							if( !empty( $properties_text ) ){ ?>
								<div class="properties-container info-container">
									<div class="w-row">
										<div class="col w-col w-col-4">
											<div class="productpage-details-left">
												<h4 class="heading-04"><?php _e( 'Properties', 'bl' ); ?>:<br></h4>
											</div>
										</div>
										<div class="col w-col w-col-8">
											<div class="productpage-details-right">
												<div class="paragraph"><?php echo $properties_text; ?></div>
											</div>
										</div>
									</div>
									<div class="separator-line"></div>
								</div>
							<?php }

							if( !empty( $product_description ) ){ ?>
								<div class="description-container info-container">
									<div class="w-row">
										<div class="col w-col w-col-4">
											<div class="productpage-details-left">
												<h4 class="heading-04"><?php _e( 'Product description', 'bl' ); ?>:<br></h4>
											</div>
										</div>
										<div class="col w-col w-col-8">
											<div class="productpage-details-right">
												<div class="paragraph"><?php echo $product_description; ?></div>
											</div>
										</div>
									</div>
									<div class="separator-line"></div>
								</div>
							<?php }

							/* if( !empty( $how_to_use_video_id ) ){ ?>
								<div class="how-to-use-video-container info-container">
									<div class="w-row">
										<div class="col w-col w-col-4">
											<div class="productpage-details-left">
												<h4 class="heading-04"><?php _e( 'How to use', 'bl' ); ?>:<br></h4>
											</div>
										</div>
										<div class="col w-col w-col-8">
											<div class="productpage-details-right">
												<div style="padding-top:56.17021276595745%" id="w-node-4ff4cff94b0f" class="product-detail-video w-video w-embed">
													<iframe src="https://www.youtube.com/embed/<?php echo $how_to_use_video_id; ?>" class="embedly-embed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
												</div>
											</div>
										</div>
									</div>
									<div class="separator-line"></div>
								</div>
							<?php } */ ?>

						<?php } else if( $_product->is_type( 'variable' ) ){ ?>
							<div class="properties-container info-container hide">
								<div class="w-row">
									<div class="col w-col w-col-4">
										<div class="productpage-details-left">
											<h4 class="heading-04"><?php _e( 'Properties', 'bl' ); ?>:<br></h4>
										</div>
									</div>
									<div class="col w-col w-col-8">
										<div class="productpage-details-right">
											<div class="paragraph"></div>
										</div>
									</div>
								</div>
								<div class="separator-line"></div>
							</div>

							<div class="description-container info-container hide">
								<div class="w-row">
									<div class="col w-col w-col-4">
										<div class="productpage-details-left">
											<h4 class="heading-04"><?php _e( 'Product description', 'bl' ); ?>:<br></h4>
										</div>
									</div>
									<div class="col w-col w-col-8">
										<div class="productpage-details-right">
											<div class="paragraph"></div>
										</div>
									</div>
								</div>
								<div class="separator-line"></div>
							</div>

							<!--<div class="how-to-use-video-container info-container hide">
								<div class="w-row">
									<div class="col w-col w-col-4">
										<div class="productpage-details-left">
											<h4 class="heading-04"><?php _e( 'How to use', 'bl' ); ?>:<br></h4>
										</div>
									</div>
									<div class="col w-col w-col-8">
										<div class="productpage-details-right">
											<div style="padding-top:56.17021276595745%" id="w-node-4ff4cff94b0f" class="product-detail-video w-video w-embed"></div>
										</div>
									</div>
								</div>
								<div class="separator-line"></div>
							</div>-->
						<?php } ?>

						<?php if( !empty( $how_to_use_video_id ) ){ ?>
							<div class="how-to-use-video-container info-container">
								<div class="w-row">
									<div class="col w-col w-col-4">
										<div class="productpage-details-left">
											<h4 class="heading-04"><?php _e( 'How to use', 'bl' ); ?>:<br></h4>
										</div>
									</div>
									<div class="col w-col w-col-8">
										<div class="productpage-details-right">
											<div style="padding-top:56.17021276595745%" id="w-node-4ff4cff94b0f" class="product-detail-video w-video w-embed">
												<iframe src="https://www.youtube.com/embed/<?php echo $how_to_use_video_id; ?>?rel=0 class="embedly-embed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
											</div>
										</div>
									</div>
								</div>
								<div class="separator-line"></div>
							</div>
						<?php } ?>

					</div>
				<?php }
			} ?>

			<div class="separator-heading">
				<h3 class="heading-03"><?php _e( 'Related products', 'bl' ); ?><br></h3>
			</div>
			<div class="w-row">
				<?php
					
				$upsell_products = array();

				if( !empty( $_product->get_upsells() ) ){
					$upsell_products = $_product->get_upsells();
				} else {
					$terms = wp_get_post_terms( get_the_ID(), 'product_cat' );

					if( !empty( $terms ) ){
						foreach ( $terms as $term ) {
							if( empty( $upsell_products ) ){
								$upsell_products = maybe_unserialize( get_term_meta( $term->term_id, 'featured-products', true ) );
							}
						}
					}
				}

				$args = array();
				$args['post_type'] = 'product';
				$args['posts_per_page'] = BL_SINGLE_PRODUCT_RELATED_PRODUCT_LIMIT;
				$args['post_status'] = 'publish';
				$args['fields'] = 'ids';
				$args['post__in'] = $upsell_products;
				$args['post__not_in'] = bl_get_training_courses_ids();
				if( !empty( $upsell_products ) ){
					$args['orderby'] = 'post__in';
				} else {
					$args['orderby'] = 'rand';
				}

				$products_loop = new WP_Query( $args );

				if( $products_loop->have_posts() ){
					while ( $products_loop->have_posts() ) {
						$prod_id = $products_loop->next_post(); 

						$_product = wc_get_product( $prod_id );

						if( $_product->is_type( 'variable' ) ){
							$featured_product_available_variations = $_product->get_available_variations();
							$variation_key = array_rand( $featured_product_available_variations );
							$rand_variation = $featured_product_available_variations[ $variation_key ];

							if( !empty( $rand_variation['variation_id'] ) ){
								$_variation = wc_get_product( $rand_variation['variation_id'] );

								// Image
								$image = $_variation->get_image( 'product-featured', array( 'class' => 'product-image' ) );
								
								// Price
								$prices = bl_get_product_prices( $_variation );

								// Label
								$label_text = get_post_meta( $prod_id, 'product-label-text', true );
								
								if( empty( $label_text ) && $_product->is_on_sale() ){
									if( !empty( $prices['gross_regular_price'] ) ){
										$price_percentage = round( ( 1 - ( $prices['gross_price'] / $prices['gross_regular_price'] ) ) * 100 );

										$label_text = -1 * abs( $price_percentage ) . '%';
									}
								}
							}
						} else {
							// Image
							$image = $_product->get_image( 'product-featured', array( 'class' => 'product-image' ) );
							
							// Price
							$prices = bl_get_product_prices( $_product );

							// Label
							$label_text = get_post_meta( $prod_id, 'product-label-text', true );
							
							if( empty( $label_text ) && $_product->is_on_sale() ){
								if( !empty( $prices['gross_regular_price'] ) ){
									$price_percentage = round( ( 1 - ( $prices['gross_price'] / $prices['gross_regular_price'] ) ) * 100 );

									$label_text = -1 * abs( $price_percentage ) . '%';
								}
							}
						}

						
						$container_classes = 'col w-col w-col-3 w-col-small-6 w-col-tiny-6 w-col-medium-6';

						include( locate_template( 'template-parts/single-product-list-view.php', false, false ) );

					}
				} ?>

			</div>
		</div>
	</section>
</div>
<?php
$ImageUrl = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' )[0];
$ItemId = $product->id;
$Title = $product-> get_title();
$ProductUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$Price = $product->get_price();
$RegularPrice = $product->get_regular_price();
$DiscountAmount = $RegularPrice - $Price;
$terms = get_terms( 'product_tag' );
?>

<script>
 var Title = "<?php echo $Title; ?>";
 var ItemId = "<?php echo $ItemId; ?>";
 var ImageUrl = "<?php echo $ImageUrl; ?>";
 var ProductUrl = "<?php echo $ProductUrl; ?>";
 var Price = "<?php echo $Price; ?>";
 var DiscountAmount = "<?php echo $DiscountAmount; ?>";
 var RegularPrice = "<?php echo $RegularPrice; ?>";
 var _learnq = _learnq || [];

    _learnq.push(['track', 'Viewed Product', {
      Title: Title,
      ItemId: ItemId,
      ImageUrl: ImageUrl,
      Url: ProductUrl,
      Metadata: {
        Price: Price,
        DiscountAmount: DiscountAmount,
        RegularPrice: RegularPrice
      }
 }]);
</script>
<?php get_footer(); ?>