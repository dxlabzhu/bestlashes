up:
	docker-compose up -d

down:
	docker-compose stop

clean:
	$(MAKE) down
	docker-compose rm -fv

pull:
	docker-compose pull

download:
	docker-compose run --rm wp core download